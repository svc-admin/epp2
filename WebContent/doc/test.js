/**
 *
 */

meta = {
    "settings":{
      "oFeatures":{
        "bAutoWidth":true,"bDeferRender":false,"bFilter":false,	"bInfo":true,"bLengthChange":true,
        "bPaginate":false,"bProcessing":false,"bServerSide":false,"bSort":true,"bSortMulti":true,
        "bSortClasses":true,"bStateSave":null},
      "oScroll":{
        "bCollapse":true,"iBarWidth":13,"sX":"100%","sXInner":"","sY":"150px"},
      "oLanguage":{
        "oAria":{
          "sSortAscending":": activate to sort column ascending",
          "sSortDescending":": activate to sort column descending",
          "_hungarianMap":{
            "sortAscending":"sSortAscending",
            "sortDescending":"sSortDescending"}},
        "oPaginate":{
          "sFirst":"First","sLast":"Last","sNext":"Next","sPrevious":"Previous",
          "_hungarianMap":{
            "first":"sFirst","last":"sLast","next":"sNext","previous":"sPrevious"}},
        "sEmptyTable":"No data available in table",
        "sInfo":"Showing _START_ to _END_ of _TOTAL_ entries",
        "sInfoEmpty":"Showing 0 to 0 of 0 entries",
        "sInfoFiltered":"(filtered from _MAX_ total entries)",
        "sInfoPostFix":"","sDecimal":"","sThousands":",",
        "sLengthMenu":"Show _MENU_ entries","sLoadingRecords":"Loading...","sProcessing":"Processing...",
        "sSearch":"Search:","sSearchPlaceholder":"","sUrl":"","sZeroRecords":"No matching records found",
        "_hungarianMap":{
          "aria":"oAria","paginate":"oPaginate","emptyTable":"sEmptyTable","info":"sInfo","infoEmpty":"sInfoEmpty",
          "infoFiltered":"sInfoFiltered","infoPostFix":"sInfoPostFix","decimal":"sDecimal","thousands":"sThousands",
          "lengthMenu":"sLengthMenu","loadingRecords":"sLoadingRecords","processing":"sProcessing","search":"sSearch",
          "searchPlaceholder":"sSearchPlaceholder","url":"sUrl","zeroRecords":"sZeroRecords"}},
      "oBrowser":{"bScrollOversize":false,"bScrollbarLeft":false,"bBounding":true,"barWidth":13},
      "ajax":null,"aanFeatures":[],
      "aoData":[{
        "nTr":{"_DT_RowIndex":0},
        "anCells":[{"_DT_CellIndex":{"row":0,"column":0}},{"_DT_CellIndex":{"row":0,"column":1}}],
        "_aData":["0","157-T7701","伊藤　創太 (Ito Sota)","工学部 数理工学科 ",1.72,21,315,"","","0"],
        "_aSortData":null,"_aFilterData":null,"_sFilterRow":null,"_sRowStripe":"","src":"data","idx":0}],
      "aiDisplay":[],"aiDisplayMaster":[0],"aIds":{},
      "aoColumns":[{

        "idx":0,"aDataSort":[0],"asSorting":["asc","desc"],"bSearchable":true,"bSortable":false,"bVisible":true,
        "_sManualType":null,"_bAttrSrc":false,"fnCreatedCell":null,"mData":0,"nTh":{},"nTf":null,"sClass":"alignRight",
        "sContentPadding":"","sDefaultContent":null,"sName":"","sSortDataType":"std","sSortingClass":"sorting_disabled",
        "sSortingClassJUI":"","sTitle":"行","sType":null,"sWidth":null,"sWidthOrig":null,"iDataSort":-1,"sCellType":"td",
        "_hungarianMap":{
          "dataSort":"iDataSort","sorting":"asSorting","searchable":"bSearchable","sortable":"bSortable","visible":"bVisible",
          "createdCell":"fnCreatedCell","data":"mData","render":"mRender","cellType":"sCellType","class":"sClass",
          "contentPadding":"sContentPadding","defaultContent":"sDefaultContent","name":"sName","sortDataType":"sSortDataType",
          "title":"sTitle","type":"sType","width":"sWidth"},
        "targets":0,"title":"行","sortable":false,"class":"alignRight"},{

        "idx":1,"aDataSort":[1],"asSorting":["asc","desc"],"bSearchable":true,"bSortable":true,"bVisible":true,"_sManualType":"stdNo",
        "_bAttrSrc":false,"fnCreatedCell":null,"mData":1,"nTh":{},"nTf":null,"sClass":"","sContentPadding":"","sDefaultContent":null,
        "sName":"","sSortDataType":"std","sSortingClass":"sorting","sSortingClassJUI":"","sTitle":"学生番号","sType":null,"sWidth":null,
        "sWidthOrig":null,"iDataSort":-1,"sCellType":"td","_hungarianMap":{
          "dataSort":"iDataSort","sorting":"asSorting","searchable":"bSearchable","sortable":"bSortable","visible":"bVisible",
          "createdCell":"fnCreatedCell","data":"mData","render":"mRender","cellType":"sCellType","class":"sClass",
          "contentPadding":"sContentPadding","defaultContent":"sDefaultContent","name":"sName","sortDataType":"sSortDataType",
          "title":"sTitle","type":"sType","width":"sWidth"},
        "targets":1,"title":"学生番号","sortable":true,"type":"stdNo"},{

        "idx":2,"aDataSort":[2],"asSorting":["asc","desc"],"bSearchable":true,"bSortable":true,"bVisible":true,"_sManualType":null,
        "_bAttrSrc":false,"fnCreatedCell":null,"mData":2,"nTh":{},"nTf":null,"sClass":"","sContentPadding":"","sDefaultContent":null,
        "sName":"","sSortDataType":"std","sSortingClass":"sorting","sSortingClassJUI":"","sTitle":"氏名","sType":null,"sWidth":null,
        "sWidthOrig":null,"iDataSort":-1,"sCellType":"td","_hungarianMap":{
          "dataSort":"iDataSort","sorting":"asSorting","searchable":"bSearchable","sortable":"bSortable","visible":"bVisible",
          "createdCell":"fnCreatedCell","data":"mData","render":"mRender","cellType":"sCellType","class":"sClass",
          "contentPadding":"sContentPadding","defaultContent":"sDefaultContent","name":"sName","sortDataType":"sSortDataType",
          "title":"sTitle","type":"sType","width":"sWidth"},
        "targets":2,"title":"氏名"},{

        "idx":3,"aDataSort":[3],"asSorting":["asc","desc"],"bSearchable":true,"bSortable":true,"bVisible":true,"_sManualType":null,
        "_bAttrSrc":false,"fnCreatedCell":null,"mData":3,"mRender":null,"nTh":{},"nTf":null,"sClass":"","sContentPadding":"",
        "sDefaultContent":null,"sName":"","sSortDataType":"std","sSortingClass":"sorting","sSortingClassJUI":"","sTitle":"所属",
        "sType":null,"sWidth":null,"sWidthOrig":null,"iDataSort":-1,"sCellType":"td","_hungarianMap":{
          "dataSort":"iDataSort","sorting":"asSorting","searchable":"bSearchable","sortable":"bSortable","visible":"bVisible",
          "createdCell":"fnCreatedCell","data":"mData","render":"mRender","cellType":"sCellType","class":"sClass",
          "contentPadding":"sContentPadding","defaultContent":"sDefaultContent","name":"sName","sortDataType":"sSortDataType",
          "title":"sTitle","type":"sType","width":"sWidth"},
        "targets":3,"title":"所属"},{

        "idx":4,"aDataSort":[4],"asSorting":["asc","desc"],"bSearchable":true,"bSortable":true,"bVisible":true,"_sManualType":null,
        "_bAttrSrc":false,"fnCreatedCell":null,"mData":4,"nTh":{},"nTf":null,"sClass":"","sContentPadding":"","sDefaultContent":null,
        "sName":"","sSortDataType":"std","sSortingClass":"sorting","sSortingClassJUI":"","sTitle":"GPA","sType":null,"sWidth":null,
        "sWidthOrig":null,"iDataSort":-1,"sCellType":"td","_hungarianMap":{
          "dataSort":"iDataSort","sorting":"asSorting","searchable":"bSearchable","sortable":"bSortable","visible":"bVisible",
          "createdCell":"fnCreatedCell","data":"mData","render":"mRender","cellType":"sCellType","class":"sClass",
          "contentPadding":"sContentPadding","defaultContent":"sDefaultContent","name":"sName","sortDataType":"sSortDataType",
          "title":"sTitle","type":"sType","width":"sWidth"},
        "targets":4,"title":"GPA"},{

        "idx":5,"aDataSort":[5],"asSorting":["asc","desc"],"bSearchable":true,"bSortable":true,"bVisible":true,"_sManualType":null,
        "_bAttrSrc":false,"fnCreatedCell":null,"mData":5,"mRender":null,"nTh":{},"nTf":null,"sClass":"alignRight","sContentPadding":"",
        "sDefaultContent":null,"sName":"","sSortDataType":"std","sSortingClass":"sorting","sSortingClassJUI":"","sTitle":"取得単位",
        "sType":null,"sWidth":null,"sWidthOrig":null,"iDataSort":-1,"sCellType":"td","_hungarianMap":{
          "dataSort":"iDataSort","sorting":"asSorting","searchable":"bSearchable","sortable":"bSortable","visible":"bVisible",
          "createdCell":"fnCreatedCell","data":"mData","render":"mRender","cellType":"sCellType","class":"sClass",
          "contentPadding":"sContentPadding","defaultContent":"sDefaultContent","name":"sName","sortDataType":"sSortDataType",
          "title":"sTitle","type":"sType","width":"sWidth"},
        "targets":5,"title":"取得単位","class":"alignRight"},{

        "idx":6,"aDataSort":[6],"asSorting":["asc","desc"],"bSearchable":true,"bSortable":true,"bVisible":true,"_sManualType":null,
        "_bAttrSrc":false,"fnCreatedCell":null,"mData":6,"mRender":null,"nTh":{},"nTf":null,"sClass":"alignRight","sContentPadding":"",
        "sDefaultContent":null,"sName":"","sSortDataType":"std","sSortingClass":"sorting","sSortingClassJUI":"","sTitle":"TOEIC",
        "sType":null,"sWidth":null,"sWidthOrig":null,"iDataSort":-1,"sCellType":"td","_hungarianMap":{
          "dataSort":"iDataSort","sorting":"asSorting","searchable":"bSearchable","sortable":"bSortable","visible":"bVisible",
          "createdCell":"fnCreatedCell","data":"mData","render":"mRender","cellType":"sCellType","class":"sClass",
          "contentPadding":"sContentPadding","defaultContent":"sDefaultContent","name":"sName","sortDataType":"sSortDataType",
          "title":"sTitle","type":"sType","width":"sWidth"},
        "targets":6,"title":"TOEIC","class":"alignRight"},{

        "idx":7,"aDataSort":[7],"asSorting":["asc","desc"],"bSearchable":true,"bSortable":true,"bVisible":true,"_sManualType":null,
        "_bAttrSrc":false,"fnCreatedCell":null,"mData":7,"mRender":null,"nTh":{},"nTf":null,"sClass":"alignRight","sContentPadding":"",
        "sDefaultContent":null,"sName":"","sSortDataType":"std","sSortingClass":"sorting","sSortingClassJUI":"","sTitle":"卒業",
        "sType":null,"sWidth":null,"sWidthOrig":null,"iDataSort":-1,"sCellType":"td","_hungarianMap":{
          "dataSort":"iDataSort","sorting":"asSorting","searchable":"bSearchable","sortable":"bSortable","visible":"bVisible",
          "createdCell":"fnCreatedCell","data":"mData","render":"mRender","cellType":"sCellType","class":"sClass",
          "contentPadding":"sContentPadding","defaultContent":"sDefaultContent","name":"sName","sortDataType":"sSortDataType",
          "title":"sTitle","type":"sType","width":"sWidth"},
        "targets":7,"title":"卒業","class":"alignRight"},{

        "idx":8,"aDataSort":[8],"asSorting":["asc","desc"],"bSearchable":true,"bSortable":true,"bVisible":true,"_sManualType":null,
        "_bAttrSrc":false,"fnCreatedCell":null,"mData":8,"mRender":null,"nTh":{},"nTf":null,"sClass":"alignRight","sContentPadding":"",
        "sDefaultContent":null,"sName":"","sSortDataType":"std","sSortingClass":"sorting","sSortingClassJUI":"","sTitle":"進路",
        "sType":null,"sWidth":null,"sWidthOrig":null,"iDataSort":-1,"sCellType":"td","_hungarianMap":{
          "dataSort":"iDataSort","sorting":"asSorting","searchable":"bSearchable","sortable":"bSortable","visible":"bVisible",
          "createdCell":"fnCreatedCell","data":"mData","render":"mRender","cellType":"sCellType","class":"sClass",
          "contentPadding":"sContentPadding","defaultContent":"sDefaultContent","name":"sName","sortDataType":"sSortDataType",
          "title":"sTitle","type":"sType","width":"sWidth"},
        "targets":8,"title":"進路","class":""}]}};

full = [ "0", "157-T7701", "伊藤　創太 (Ito Sota)", "工学部 数理工学科 ", 1.72, 21, 315, "", "", "0" ];
data = "157-T7701";
type = "display";