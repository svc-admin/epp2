<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>現在メンテナンス中 / Now in Maintenance</title>
</head>
<body>
<h3>現在メンテナンス中</h3>
<p>
毎夜の定期メンテナンスが$DATETIMEから始まりました。
ご不便をお掛けしますが、全部で1時間程度要しますので、その後でアクセスしてください。
</p>

<h3>Now in Maintenance</h3>
<p>
Dayly maintenunce have started from $DATETIME.
We are sorry for your incovenience. It takes around 1 hour, please access later.
</p>

</body>
</html>