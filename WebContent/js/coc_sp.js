var url = location.href;
params = url.split("?");
if ( url.indexOf('&') != -1) {
    paramms = params[1].split("&");
    var paramArray = [];
    for ( i = 0; i < paramms.length; i++ ) {
        neet = paramms[i].split("=");
        paramArray.push(neet[0]);
        paramArray[neet[0]] = neet[1];
    }
    var uid = paramArray["uid"];
    var lang = (navigator.language /* Mozilla */
            || navigator.userLanguage /* IE */).substring(0, 2); // Webブラウザのデフォルトの言語
    if(paramArray["lang"] != ""){
        lang = paramArray["lang"];
    }
}

// PC表示切り替え用のURLを設定
var pc_url = $("#pc_url").attr("href");
$("#pc_url").attr("href", pc_url+"?&uid="+uid+"&lang="+lang)

$(function(){
    $.ajax({
        type: "GET",
        url : "pf/centerOfCommunity.json?&uid="+uid+"&lang="+lang,
        contentType : "application/json; charset=utf-8",
        dataType:"json",
        success: function(data, status) {
            for(var i in data){
                var activateChartFlg = true;
                //  COC+の各ステップ取得に必要な単位数
                $("#cocplus").each(function(){
                    var countUnit1 = 0, countUnit2 = 0, countUnit3 = 0, countUnit4 = 0, countUnit5 = 0, countUnit6 = 0, countUnit7 = 0, countUnit8 = 0, countUnit9 = 0;
                    var step1Denom = 0, step2Denom = 0, step3Denom = 0, step4Denom = 0, step5Denom = 0, step6Denom = 0, step7Denom = 0, step8Denom = 0, step9Denom = 0;
                    var step1Per = 0, step2Per = 0, step3Per = 0, step4Per = 0, step5Per = 0, step6Per = 0, step7Per = 0, step8Per = 0, step9Per = 0;
                    var modalId = new String();
                    for(var k in data[i].stepCocPlus[0]){                                               //stepCocの各ステップをループ　「-1」の時はステップを非表示
                        if(k == "step1"){
                            $("#step1").each(function(){
                                step1Denom = data[i].stepCocPlus[0][k];         //各ステップで必要な単位数を代入
                                if(step1Denom == "-1"){                         //step1Denomが「-1」の時、ステップ１を非表示にする
                                    $(this,"#step1nav").remove();
                                    step1Per = 100;
                                }
                            });
                        } else if (k == "step2"){
                            $("#step2").each(function(){
                                step2Denom = data[i].stepCocPlus[0][k];
                                if(step2Denom == "-1"){
                                    $(this).remove();
                                    $("#step2nav").remove();
                                    step2Per = 100;
                                }
                            });
                        } else if (k == "step3"){
                            $("#step3").each(function(){
                                step3Denom = data[i].stepCocPlus[0][k];
                                if(step3Denom == "-1"){
                                    $(this).remove();
                                    $("#step3nav").remove();
                                    step3Per = 100;
                                }
                            });
                        } else if (k == "step4"){
                            $("#step4").each(function(){
                                step4Denom = data[i].stepCocPlus[0][k];
                                if(step4Denom == "-1"){
                                    $(this).remove();
                                    $("#step4nav").remove();
                                    step4Per = 100;
                                }
                            });
                        } else if (k == "step5"){
                            $("#step5").each(function(){
                                step5Denom = data[i].stepCocPlus[0][k];
                                if(step5Denom == "-1"){
                                    $(this).remove();
                                    $("#step5nav").remove();
                                    step5Per = 100;
                                }
                            });
                        } else if (k == "step6"){
                            $("#step6").each(function(){
                                step6Denom = data[i].stepCocPlus[0][k];
                                if(step6Denom == "-1"){
                                    $(this).remove();
                                    $("#step6nav").remove();
                                    step6Per = 100;
                                }
                            });
                        } else if (k == "step7"){
                            $("#step7").each(function(){
                                step7Denom = data[i].stepCocPlus[0][k];
                                if(step7Denom == "-1"){
                                    $(this).remove();
                                    $("#step7nav").remove();
                                    step7Per = 100;
                                }
                            });
                        } else if (k == "step8"){
                            $("#step8").each(function(){
                                step8Denom = data[i].stepCocPlus[0][k];
                                if(step8Denom == "-1"){
                                    $(this).remove();
                                    $("#step8nav").remove();
                                    step8Per = 100;
                                }
                            });
                        } else if (k == "step9"){
                            $("#step9").each(function(){
                                step9Denom = data[i].stepCocPlus[0][k];
                                if(step9Denom == "-1"){
                                    $(this).remove();
                                    $("#step9nav").remove();
                                    step9Per = 100;
                                }
                            });
                        }
                    }
                    //  COC+の講義の内容
                    for(var unitCocPlusStep in data[i].unitCocPlus[0]){                                                     //unitCocをループ       lにステップ数
                        for(var lecNum in data[i].unitCocPlus[0][unitCocPlusStep]){                                         //lecNumは講義の通し番号？
                            activateChartFlg = true;
                            if(unitCocPlusStep == "unit1"){
                                for(var lec in data[i].unitCocPlus[0][unitCocPlusStep][lecNum]){                                    //lecにlecture
                                    for(var zero in data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec]){                          //zeroは0
                                        lectureListName = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].lecturename;       //講義名をlectureListNameに代入
                                        lectureListPeriod = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].period;          //講義名をlectureListPeriodに代入
                                        lectureListSuccess = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;        //講義名をlectureListSuccessに代入
                                        lectureListHref = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].syllabus;          //講義名をlectureListHrefに代入
                                        countUnit1 += data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;               //単位数を集計
                                        modalId = "step1_" + lecNum;
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList1 = lectureList;
                                    $("#step1").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList1);   //step1に講義一覧を表示
                                    });
                                    $("#step1").each(function(){
                                        if(countUnit1 >= step1Denom){
                                            countUnit1 = step1Denom;                                                                            //必要な単位数よりも多く単位を取得している場合に分子を分母に合わせる
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step1Denom + "</span>");  //分母（ステップに必要な単位）の表示
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit1 + "</span>")       //分子（現在取得している単位）の表示
                                        step1Per = Math.floor(countUnit1 * 100 / step1Denom);                                                   //このステップでの単位取得率
                                        if(step1Per == 100){
                                            $(this).addClass("getunit");                                                   //100%になると、.right_unitに「.getunit」クラスを追加
                                        }
                                    });
                                }
                            } else if (unitCocPlusStep == "unit2"){             //step2
                                for(var lec in data[i].unitCocPlus[0][unitCocPlusStep][lecNum]){
                                    for(var zero in data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec]){
                                        lectureListName = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].syllabus;
                                        countUnit2 += data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        modalId = "step2_" + lecNum;
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList2 = lectureList;
                                    $("#step2").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList2);
                                    });
                                    $("#step2").each(function(){
                                        if(countUnit2 >= step2Denom){
                                            countUnit2 = step2Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step2Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit2 + "</span>");
                                        step2Per = Math.floor(countUnit2 * 100 / step2Denom);
                                        if(step2Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if (unitCocPlusStep == "unit3"){             //step3
                                for(var lec in data[i].unitCocPlus[0][unitCocPlusStep][lecNum]){
                                    for(var zero in data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec]){
                                        lectureListName = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].syllabus;
                                        countUnit3 += data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        modalId = "step3_" + lecNum;
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList3 = lectureList;
                                    $("#step3").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList3);
                                    });
                                    $("#step3").each(function(){
                                        if(countUnit3 >= step3Denom){
                                            countUnit3 = step3Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step3Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit3 + "</span>");
                                        step3Per = Math.floor(countUnit3 * 100 / step3Denom);
                                        if(step3Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if (unitCocPlusStep == "unit4"){             //step4
                                for(var lec in data[i].unitCocPlus[0][unitCocPlusStep][lecNum]){
                                    for(var zero in data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec]){
                                        lectureListName = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].syllabus;
                                        countUnit4 += data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        modalId = "step4_" + lecNum;
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList4 = lectureList;
                                    $("#step4").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList4);
                                    });
                                    $("#step4").each(function(){
                                        if(countUnit4 >= step4Denom){
                                            countUnit4 = step4Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step4Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit4 + "</span>");
                                        step4Per = Math.floor(countUnit4 * 100 / step4Denom);
                                        if(step4Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if (unitCocPlusStep == "unit5"){             //step5
                                for(var lec in data[i].unitCocPlus[0][unitCocPlusStep][lecNum]){
                                    for(var zero in data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec]){
                                        lectureListName = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].syllabus;
                                        countUnit5 += data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        modalId = "step5_" + lecNum;
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList5 = lectureList;
                                    $("#step5").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList5);
                                    });
                                    $("#step5").each(function(){
                                        if(countUnit5 >= step5Denom){
                                            countUnit5 = step5Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step5Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit5 + "</span>");
                                        step5Per = Math.floor(countUnit5 * 100 / step5Denom);
                                        if(step5Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if (unitCocPlusStep == "unit6"){             //step6
                                for(var lec in data[i].unitCocPlus[0][unitCocPlusStep][lecNum]){
                                    for(var zero in data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec]){
                                        lectureListName = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].syllabus;
                                        countUnit6 += data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        modalId = "step6_" + lecNum;
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList6 = lectureList;
                                    $("#step6").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList6);
                                    });
                                    $("#step6").each(function(){
                                        if(countUnit6 >= step6Denom){
                                            countUnit6 = step6Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step6Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit6 + "</span>");
                                        step6Per = Math.floor(countUnit6 * 100 / step6Denom);
                                        if(step6Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if (unitCocPlusStep == "unit7"){             //step7
                                for(var lec in data[i].unitCocPlus[0][unitCocPlusStep][lecNum]){
                                    for(var zero in data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec]){
                                        lectureListName = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].syllabus;
                                        countUnit7 += data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        modalId = "step7_" + lecNum;
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList7 = lectureList;
                                    $("#step7").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList7);
                                    });
                                    $("#step7").each(function(){
                                        if(countUnit7 >= step7Denom){
                                            countUnit7 = step7Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step7Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit7 + "</span>");
                                        step7Per = Math.floor(countUnit7 * 100 / step7Denom);
                                        if(step7Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if (unitCocPlusStep == "unit8"){             //step8
                                for(var lec in data[i].unitCocPlus[0][unitCocPlusStep][lecNum]){
                                    for(var zero in data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec]){
                                        lectureListName = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].syllabus;
                                        countUnit8 += data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        modalId = "step8_" + lecNum;
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList8 = lectureList;
                                    $("#step8").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList8);
                                    });
                                    $("#step8").each(function(){
                                        if(countUnit8 >= step8Denom){
                                            countUnit8 = step8Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step8Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit8 + "</span>");
                                        step8Per = Math.floor(countUnit8 * 100 / step8Denom);
                                        if(step8Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if (unitCocPlusStep == "unit9"){             //step9
                                for(var lec in data[i].unitCocPlus[0][unitCocPlusStep][lecNum]){
                                    for(var zero in data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec]){
                                        lectureListName = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].syllabus;
                                        countUnit9 += data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                        modalId = "step9_" + lecNum;
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList9 = lectureList;
                                    $("#step9").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList9);
                                    });
                                    $("#step9").each(function(){
                                        if(countUnit9 >= step9Denom){
                                            countUnit9 = step9Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step9Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit9 + "</span>");
                                        step9Per = Math.floor(countUnit9 * 100 / step9Denom);
                                        if(step9Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if(unitCocPlusStep == "unit10"){
                                for(var zero in data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec]){
                                        lectureListSuccess = data[i].unitCocPlus[0][unitCocPlusStep][lecNum][lec][zero].success;
                                    //  lectureListSuccess =0;                                                                                      //確認用
                                    //  step1Per = step2Per = step3Per = step4Per = step5Per = step6Per = step7Per = step8Per = step9Per = 100;     //確認用
                                        if(step1Per==100 && step2Per==100 && step3Per==100 && step4Per==100 && step5Per==100 && step6Per==100 && step7Per==100 && step8Per==100 && step9Per==100){      //全てのステップが100%の場合
                                            if(lectureListSuccess>0){                                                                                                                                      //step10のsuccessが1の場合                                                                                           //step10のsucessが1だった場合
                                                $("#main_visual.kakutoku_img").show();                      //「くまもとプレマイスター認定‼︎」表示
                                                $("#presentation .right_units").addClass("get_pre");        //「地方創生プレゼンテーション」背景色変更
                                                $(".unit.get").show();
                                            }else if(lectureListSuccess==0){
                                                $("#main_visual.challenge_img").show();                     //「くまもとプレマイスターに挑戦‼︎」表示
                                                $("#presentation .right_units").addClass("challenge_pre");  //「地方創生プレゼンテーション」背景色変更
                                                $(".unit.challenge").show();
                                            }
                                        } else if(step1Per<100 || step2Per<100 || step3Per<100 || step4Per<100 || step5Per<100 || step6Per<100 || step7Per<100 || step8Per<100 || step9Per<100){         //いずれかのステップが100%以下の場合
                                            $("#main_visual.yet_img").show();                           //「目指せくまもとプレマイスター‼︎」表示
                                            $("#presentation .right_units").addClass("yet_pre");        //「地方創生プレゼンテーション」背景色変更
                                            $(".unit.yet").show();
                                        }
                                    }
                            }

                            $(".button_box .unit_title").each(function(){        //講義名が長すぎた場合、途中で切って「…」を付ける
                                var txt = $(this).text();
                                if(txt.length > 6){
                                    txt = txt.substr(0, 6);
                                    $(this).text(txt + "...");
                                }
                            });

                            //ドーナツチャート表示
                            function activateScene2 () {
                                var $content = $('#main_content'),
                                    $charts1 = $content.find('.chart#step1_chart');
                                    $charts2 = $content.find('.chart#step2_chart');
                                    $charts3 = $content.find('.chart#step3_chart');
                                    $charts4 = $content.find('.chart#step4_chart');
                                    $charts5 = $content.find('.chart#step5_chart');
                                    $charts6 = $content.find('.chart#step6_chart');
                                    $charts7 = $content.find('.chart#step7_chart');
                                    $charts8 = $content.find('.chart#step8_chart');
                                    $charts9 = $content.find('.chart#step9_chart');
                                    $charts10 = $content.find('.chart#step10_chart');
                                //step1
                                $charts1.each(function(){   // 円チャートごとの処理
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),          // 「マスク」を保存し、角度 0 を指定
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),                                                // パーセンテージ値を取得
                                        percentData1 = step1Per
                                        $percentNumber.text(0);                                                                         // パーセンテージの表示をいったん 0 に
                                    $("#step1 .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData1 }, {                                  // 角度のアニメーション
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step2
                                $charts2.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData2 = step2Per
                                    $percentNumber.text(0);
                                    $("#step2 .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData2 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step3
                                $charts3.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData3 = step3Per
                                    $percentNumber.text(0);
                                    $("#step3 .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData3 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step4
                                $charts4.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData4 = step4Per
                                    $percentNumber.text(0);
                                    $("#step4 .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData4 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step5
                                $charts5.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData5 = step5Per
                                    $percentNumber.text(0);
                                    $("#step5 .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData5 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step6
                                $charts6.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData6 = step6Per
                                    $percentNumber.text(0);
                                    $("#step6 .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData6 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step7
                                $charts7.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData7 = step7Per
                                    $percentNumber.text(0);
                                    $("#step7 .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData7 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step8
                                $charts8.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData8 = step8Per
                                    $percentNumber.text(0);
                                    $("#step8 .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData8 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step9
                                $charts9.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData9 = step9Per
                                    $percentNumber.text(0);
                                    $("#step9 .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData9 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                activateChartFlg = false;
                            }
                            if(activateChartFlg){
                                activateScene2();
                            }
                        }

                    }
                });

                activateChartFlg = true;

                $("#coc").each(function(){
                    var countUnit1 = 0, countUnit2 = 0, countUnit3 = 0, countUnit4 = 0, countUnit5 = 0, countUnit6 = 0, countUnit7 = 0, countUnit8 = 0, countUnit9 = 0;
                    var step1Denom = 0, step2Denom = 0, step3Denom = 0, step4Denom = 0, step5Denom = 0, step6Denom = 0, step7Denom = 0, step8Denom = 0, step9Denom = 0;
                    var step1Per = 0, step2Per = 0, step3Per = 0, step4Per = 0, step5Per = 0, step6Per = 0, step7Per = 0, step8Per = 0, step9Per = 0;
                    var modalId = new String();
                    for(var k in data[i].stepCoc[0]){                                               //stepCocの各ステップをループ　「-1」の時はステップを非表示
                        if(k == "step1"){
                            $("#step1Coc").each(function(){
                                step1Denom = data[i].stepCoc[0][k];         //各ステップで必要な単位数を代入
                                if(step1Denom == "-1"){                         //step1Denomが「-1」の時、ステップ１を非表示にする
                                    $(this).remove();
                                    $(this,"#step1navCoc").remove();
                                    step1Per = 100;
                                }
                            });
                        } else if (k == "step2"){
                            $("#step2Coc").each(function(){
                                step2Denom = data[i].stepCoc[0][k];
                                if(step2Denom == "-1"){
                                    $(this).remove();
                                    $("#step2navCoc").remove();
                                    step2Per = 100;
                                }
                            });
                        } else if (k == "step3"){
                            $("#step3Coc").each(function(){
                                step3Denom = data[i].stepCoc[0][k];
                                if(step3Denom == "-1"){
                                    $(this).remove();
                                    $("#step3navCoc").remove();
                                    step3Per = 100;
                                }
                            });
                        } else if (k == "step4"){
                            $("#step4Coc").each(function(){
                                step4Denom = data[i].stepCoc[0][k];
                                if(step4Denom == "-1"){
                                    $(this).remove();
                                    $("#step4navCoc").remove();
                                    step4Per = 100;
                                }
                            });
                        } else if (k == "step5"){
                            $("#step5Coc").each(function(){
                                step5Denom = data[i].stepCoc[0][k];
                                if(step5Denom == "-1"){
                                    $(this).remove();
                                    $("#step5navCoc").remove();
                                    step5Per = 100;
                                }
                            });
                        } else if (k == "step6"){
                            $("#step6Coc").each(function(){
                                step6Denom = data[i].stepCoc[0][k];
                                if(step6Denom == "-1"){
                                    $(this).remove();
                                    $("#step6navCoc").remove();
                                    step6Per = 100;
                                }
                            });
                        } else if (k == "step7"){
                            $("#step7Coc").each(function(){
                                step7Denom = data[i].stepCoc[0][k];
                                if(step7Denom == "-1"){
                                    $(this).remove();
                                    $("#step7navCoc").remove();
                                    step7Per = 100;
                                }
                            });
                        } else if (k == "step8"){
                            $("#step8Coc").each(function(){
                                step8Denom = data[i].stepCoc[0][k];
                                if(step8Denom == "-1"){
                                    $(this).remove();
                                    $("#step8navCoc").remove();
                                    step8Per = 100;
                                }
                            });
                        } else if (k == "step9"){
                            $("#step9Coc").each(function(){
                                step9Denom = data[i].stepCoc[0][k];
                                if(step9Denom == "-1"){
                                    $(this).remove();
                                    $("#step9navCoc").remove();
                                    step9Per = 100;
                                }
                            });
                        }
                    }

                    //  COCの講義の内容
                    for(var unitCocStep in data[i].unitCoc[0]){                                                     //unitCocをループ       lにステップ数
                        for(var lecNum in data[i].unitCoc[0][unitCocStep]){                                         //lecNumは講義の通し番号？
                            activateChartFlg = true;
                            if(unitCocStep == "unit1"){
                                for(var lec in data[i].unitCoc[0][unitCocStep][lecNum]){                                    //lecにlecture
                                    for(var zero in data[i].unitCoc[0][unitCocStep][lecNum][lec]){                          //zeroは0
                                        lectureListName = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].lecturename;       //講義名をlectureListNameに代入
                                        lectureListPeriod = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].period;          //講義名をlectureListPeriodに代入
                                        lectureListSuccess = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;        //講義名をlectureListSuccessに代入
                                        lectureListHref = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].syllabus;          //講義名をlectureListHrefに代入
                                        countUnit1 += data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;               //単位数を集計
                                        modalId = "step1_" + lecNum + "Coc";
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList1 = lectureList;
                                    $("#step1Coc").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList1);   //step1に講義一覧を表示
                                    });
                                    $("#step1Coc").each(function(){
                                        if(countUnit1 >= step1Denom){
                                            countUnit1 = step1Denom;                                                                            //必要な単位数よりも多く単位を取得している場合に分子を分母に合わせる
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step1Denom + "</span>");  //分母（ステップに必要な単位）の表示
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit1 + "</span>")       //分子（現在取得している単位）の表示
                                        step1Per = Math.floor(countUnit1 * 100 / step1Denom);                                                   //このステップでの単位取得率
                                        if(step1Per == 100){
                                            $(this).addClass("getunit");                                                   //100%になると、.right_unitに「.getunit」クラスを追加
                                        }
                                    });
                                }
                            } else if (unitCocStep == "unit2"){             //step2
                                for(var lec in data[i].unitCoc[0][unitCocStep][lecNum]){
                                    for(var zero in data[i].unitCoc[0][unitCocStep][lecNum][lec]){
                                        lectureListName = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].syllabus;
                                        countUnit2 += data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        modalId = "step2_" + lecNum + "Coc";
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList2 = lectureList;
                                    $("#step2Coc").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList2);
                                    });
                                    $("#step2Coc").each(function(){
                                        if(countUnit2 >= step2Denom){
                                            countUnit2 = step2Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step2Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit2 + "</span>");
                                        step2Per = Math.floor(countUnit2 * 100 / step2Denom);
                                        if(step2Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if (unitCocStep == "unit3"){             //step3
                                for(var lec in data[i].unitCoc[0][unitCocStep][lecNum]){
                                    for(var zero in data[i].unitCoc[0][unitCocStep][lecNum][lec]){
                                        lectureListName = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].syllabus;
                                        countUnit3 += data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        modalId = "step3_" + lecNum + "Coc";
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList3 = lectureList;
                                    $("#step3Coc").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList3);
                                    });
                                    $("#step3Coc").each(function(){
                                        if(countUnit3 >= step3Denom){
                                            countUnit3 = step3Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step3Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit3 + "</span>");
                                        step3Per = Math.floor(countUnit3 * 100 / step3Denom);
                                        if(step3Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if (unitCocStep == "unit4"){             //step4
                                for(var lec in data[i].unitCoc[0][unitCocStep][lecNum]){
                                    for(var zero in data[i].unitCoc[0][unitCocStep][lecNum][lec]){
                                        lectureListName = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].syllabus;
                                        countUnit4 += data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        modalId = "step4_" + lecNum + "Coc";
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList4 = lectureList;
                                    $("#step4Coc").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList4);
                                    });
                                    $("#step4Coc").each(function(){
                                        if(countUnit4 >= step4Denom){
                                            countUnit4 = step4Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step4Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit4 + "</span>");
                                        step4Per = Math.floor(countUnit4 * 100 / step4Denom);
                                        if(step4Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if (unitCocStep == "unit5"){             //step5
                                for(var lec in data[i].unitCoc[0][unitCocStep][lecNum]){
                                    for(var zero in data[i].unitCoc[0][unitCocStep][lecNum][lec]){
                                        lectureListName = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].syllabus;
                                        countUnit5 += data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        modalId = "step5_" + lecNum + "Coc";
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList5 = lectureList;
                                    $("#step5Coc").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList5);
                                    });
                                    $("#step5Coc").each(function(){
                                        if(countUnit5 >= step5Denom){
                                            countUnit5 = step5Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step5Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit5 + "</span>");
                                        step5Per = Math.floor(countUnit5 * 100 / step5Denom);
                                        if(step5Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if (unitCocStep == "unit6"){             //step6
                                for(var lec in data[i].unitCoc[0][unitCocStep][lecNum]){
                                    for(var zero in data[i].unitCoc[0][unitCocStep][lecNum][lec]){
                                        lectureListName = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].syllabus;
                                        countUnit6 += data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        modalId = "step6_" + lecNum + "Coc";
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList6 = lectureList;
                                    $("#step6Coc").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList6);
                                    });
                                    $("#step6Coc").each(function(){
                                        if(countUnit6 >= step6Denom){
                                            countUnit6 = step6Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step6Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit6 + "</span>");
                                        step6Per = Math.floor(countUnit6 * 100 / step6Denom);
                                        if(step6Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if (unitCocStep == "unit7"){             //step7
                                for(var lec in data[i].unitCoc[0][unitCocStep][lecNum]){
                                    for(var zero in data[i].unitCoc[0][unitCocStep][lecNum][lec]){
                                        lectureListName = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].syllabus;
                                        countUnit7 += data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        modalId = "step7_" + lecNum + "Coc";
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList7 = lectureList;
                                    $("#step7Coc").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList7);
                                    });
                                    $("#step7Coc").each(function(){
                                        if(countUnit7 >= step7Denom){
                                            countUnit7 = step7Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step7Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit7 + "</span>");
                                        step7Per = Math.floor(countUnit7 * 100 / step7Denom);
                                        if(step7Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if (unitCocStep == "unit8"){             //step8
                                for(var lec in data[i].unitCoc[0][unitCocStep][lecNum]){
                                    for(var zero in data[i].unitCoc[0][unitCocStep][lecNum][lec]){
                                        lectureListName = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].syllabus;
                                        countUnit8 += data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        modalId = "step8_" + lecNum + "Coc";
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList8 = lectureList;
                                    $("#step8Coc").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList8);
                                    });
                                    $("#step8Coc").each(function(){
                                        if(countUnit8 >= step8Denom){
                                            countUnit8 = step8Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step8Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit8 + "</span>");
                                        step8Per = Math.floor(countUnit8 * 100 / step8Denom);
                                        if(step8Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if (unitCocStep == "unit9"){             //step9
                                for(var lec in data[i].unitCoc[0][unitCocStep][lecNum]){
                                    for(var zero in data[i].unitCoc[0][unitCocStep][lecNum][lec]){
                                        lectureListName = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].lecturename;
                                        lectureListPeriod = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].period;
                                        lectureListSuccess = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        lectureListHref = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].syllabus;
                                        countUnit9 += data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                        modalId = "step9_" + lecNum + "Coc";
                                    }
                                    if(lectureListSuccess == 0){
                                        var lectureList = "<li class='button_box'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    } else {
                                        var lectureList = "<li class='button_box get'><a class='modal-open' data-target='" + modalId + "'><img class='unit_icon' src='images/sp/icon_book.png'></a><p class='unit_title'>" + lectureListName + "</p><div id='" + modalId + "' class='modal-content'><ul><li>" + lectureListName + "</li><li>" + lectureListPeriod + "</li><li class='modal_btn'><a class='syllabus_btn' href='" + lectureListHref + "' target='_blank'>シラバスを見る</a></li><li class='modal_btn'><a class='modal-close'>閉じる</a></li></ul></div></li>" ;      //単位数が0なら暗く表示
                                    }
                                    var lectureList9 = lectureList;
                                    $("#step9Coc").each(function(){
                                        $(this).find( ".unit_list" ).append(lectureList9);
                                    });
                                    $("#step9Coc").each(function(){
                                        if(countUnit9 >= step9Denom){
                                            countUnit9 = step9Denom;
                                        }
                                        $(this).find( "span.denominator" ).replaceWith("<span class='denominator'>" + step9Denom + "</span>");
                                        $(this).find( "span.numerator" ).replaceWith("<span class='numerator'>" + countUnit9 + "</span>");
                                        step9Per = Math.floor(countUnit9 * 100 / step9Denom);
                                        if(step9Per == 100){
                                            $(this).addClass("getunit");
                                        }
                                    });
                                }
                            } else if(unitCocStep == "unit10"){
                                for(var zero in data[i].unitCoc[0][unitCocStep][lecNum][lec]){
                                        lectureListSuccess = data[i].unitCoc[0][unitCocStep][lecNum][lec][zero].success;
                                    //  lectureListSuccess =0;                                                                                      //確認用
                                    //  step1Per = step2Per = step3Per = step4Per = step5Per = step6Per = step7Per = step8Per = step9Per = 90;     //確認用                                                                                                                            //step10のsuccessが0の場合
                                        if(step1Per==100 && step2Per==100 && step3Per==100 && step4Per==100 && step5Per==100 && step6Per==100 && step7Per==100 && step8Per==100 && step9Per==100){      //全てのステップが100%の場合
                                            $("#main_visualCoc.kakutoku_img").show();                     //「くまもとプレマイスターに挑戦‼︎」表示
                                        }else if(step1Per<100 || step2Per<100 || step3Per<100 || step4Per<100 || step5Per<100 || step6Per<100 || step7Per<100 || step8Per<100 || step9Per<100){         //いずれかのステップが100%以下の場合
                                            $("#main_visualCoc.yet_img").show();                           //「目指せくまもとプレマイスター‼︎」表示
                                        }
                                    }
                            }

                            $(".button_box .unit_title").each(function(){        //講義名が長すぎた場合、途中で切って「…」を付ける
                                var txt = $(this).text();
                                if(txt.length > 6){
                                    txt = txt.substr(0, 6);
                                    $(this).text(txt + "...");
                                }
                            });

                            //ドーナツチャート表示
                            function activateScene2 () {

                                var $content = $('#main_contentCoc'),
                                    $charts1 = $content.find('.chart#step1_chartCoc');
                                    $charts2 = $content.find('.chart#step2_chartCoc');
                                    $charts3 = $content.find('.chart#step3_chartCoc');
                                    $charts4 = $content.find('.chart#step4_chartCoc');
                                    $charts5 = $content.find('.chart#step5_chartCoc');
                                    $charts6 = $content.find('.chart#step6_chartCoc');
                                    $charts7 = $content.find('.chart#step7_chartCoc');
                                    $charts8 = $content.find('.chart#step8_chartCoc');
                                    $charts9 = $content.find('.chart#step9_chartCoc');
                                //step1
                                $charts1.each(function(){   // 円チャートごとの処理
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),          // 「マスク」を保存し、角度 0 を指定
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),                                                // パーセンテージ値を取得
                                        percentData1 = step1Per
                                        $percentNumber.text(0);                                                                         // パーセンテージの表示をいったん 0 に
                                    $("#step1Coc .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData1 }, {                                  // 角度のアニメーション
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step2
                                $charts2.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData2 = step2Per
                                    $percentNumber.text(0);
                                    $("#step2Coc .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData2 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step3
                                $charts3.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData3 = step3Per
                                    $percentNumber.text(0);
                                    $("#step3Coc .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData3 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step4
                                $charts4.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData4 = step4Per
                                    $percentNumber.text(0);
                                    $("#step4Coc .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData4 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step5
                                $charts5.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData5 = step5Per
                                    $percentNumber.text(0);
                                    $("#step5Coc .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData5 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step6
                                $charts6.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData6 = step6Per
                                    $percentNumber.text(0);
                                    $("#step6Coc .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData6 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step7
                                $charts7.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData7 = step7Per
                                    $percentNumber.text(0);
                                    $("#step7Coc .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData7 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step8
                                $charts8.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData8 = step8Per
                                    $percentNumber.text(0);
                                    $("#step8Coc .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData8 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                //step9
                                $charts9.each(function(){
                                    var $chart = $(this),
                                        $circleLeft = $chart.find('.left .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $circleRight = $chart.find('.right .circle-mask-inner').css({ transform: 'rotate(0)' }),
                                        $percentNumber = $chart.find('.percent-number'),
                                        percentData9 = step9Per
                                    $percentNumber.text(0);
                                    $("#step9Coc .step_btn").one('click',function(){
                                        $({ percent: 0 }).delay(1000).animate({ percent: percentData9 }, {
                                            duration: 1500,
                                            progress: function () {
                                                var now = this.percent,
                                                    deg = now * 360 / 100,
                                                    degRight = Math.min(Math.max(deg, 0), 180),
                                                    degLeft  = Math.min(Math.max(deg - 180, 0), 180);
                                                $circleRight.css({ transform: 'rotate(' + degRight + 'deg)' });
                                                $circleLeft.css({ transform: 'rotate(' + degLeft + 'deg)' });
                                                $percentNumber.text(Math.floor(now));
                                            }
                                        });
                                    });
                                });
                                activateChartFlg = false;
                            }
                            if(activateChartFlg){
                                activateScene2();
                            }
                        }
                    }
                });
                // 「.modal-open」をクリック
                $('.modal-open').click(function(){
                    // オーバーレイ用の要素を追加
                    $('body').append('<div class="modal-overlay"></div>');
                    // オーバーレイをフェードイン
                    $('.modal-overlay').fadeIn('slow');

                    // モーダルコンテンツのIDを取得
                    var modal = '#' + $(this).attr('data-target');
                    // モーダルコンテンツの表示位置を設定
                    modalResize();
                     // モーダルコンテンツフェードイン
                    $(modal).fadeIn('slow');

                    // 「.modal-overlay」あるいは「.modal-close」をクリック
                    $('.modal-overlay, .modal-close').off().click(function(){
                        // モーダルコンテンツとオーバーレイをフェードアウト
                        $(modal).fadeOut('slow');
                        $('.modal-overlay').fadeOut('slow',function(){
                            // オーバーレイを削除
                            $('.modal-overlay').remove();
                        });
                    });

                    // リサイズしたら表示位置を再取得
                    $(window).on('resize', function(){
                        modalResize();
                    });

                    // モーダルコンテンツの表示位置を設定する関数
                    function modalResize(){
                        // ウィンドウの横幅、高さを取得
                        var w = $(window).width();
                        var h = $(window).height();

                        // モーダルコンテンツの表示位置を取得
                        var x = (w - $(modal).outerWidth(true)) / 2;
                        var y = (h - $(modal).outerHeight(true)) / 2;

                        // モーダルコンテンツの表示位置を設定
                        $(modal).css({'top': y + 'px'});
                    }

                });
            }
        }
    });
});

