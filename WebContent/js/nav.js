$(window).load(function(){
  $(function(){
    var nav = $('#global_nav');
    var nav2 = $('#global_navCoc');
    var navTop = nav.offset().top;  //navの位置
    var navTop2 = nav2.offset().top;  //navの位置

    $(window).scroll(function(){
      var winTop = $(this).scrollTop();
      var winTop2 = $(this).scrollTop();
      if(winTop >= 900){
        nav.addClass('fixed');
      } else if(winTop < 900) {
        nav.removeClass('fixed');
      }
      if(winTop2 >= 900){
        nav2.addClass('fixed');
      } else if(winTop2 < 900) {
        nav2.removeClass('fixed');
      }
    });
  });

});