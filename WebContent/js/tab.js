$(function(){
    // 「id="jQueryBox"」を非表示
    $("#coc").hide();

    // 「id="jQueryPush"」がクリックされた場合
    $("#cocplus #opencoc").click(function(){
      // 「id="jQueryBox"」の表示、非表示を切り替える
      $("#coc").fadeIn("fast");
      $("#cocplus").css("display","none");
      $( 'html,body' ).animate( {scrollTop:0} , 'fast' ) ;
    });
    $("#coc #opencoc").click(function(){
      // 「id="jQueryBox"」の表示、非表示を切り替える
      $("#cocplus").fadeIn("fast");
      $("#coc").css("display","none");
      $( 'html,body' ).animate( {scrollTop:0} , 'fast' ) ;
    });
});