$(function() {
  // フォーム submit 時の処理
  $('#frmResult').submit(function() {
    // フォームデータのバリデーション
    var dataIsValid = validationForFrmResult();
    if ( ! dataIsValid ) return false;

    // 日付の作成
    var dateIsMaked = makeDateValue();
    if ( !dateIsMaked ) return false;

// 2019.01.30 s.furuta add start 2.学修成果修正
    // 重複登録チェック
    var titleIsExists = dataExists();
    if ( !titleIsExists ) return false;
// 2019.01.30 s.furuta add end

    // Ajax で POST 送信
    var formData = new FormData(this);
    AjaxUpload(formData);
    return false;
  });
});

// 2019.01.30 s.furuta add start 2.学修成果修正
/*
 * 対象分類に同名タイトルが既に存在するか検証
 */
function dataExists() {
    var locale = ($("#labelTitle").text() == "TITLE:") ? "en" : "ja";
    var isLocaleEn = locale == "en";
    var mes = "";

    var group = "";
    if($("#txtGroup").val() == null || $("#txtGroup").val() == "") {
        group = $("#selGroup").val();
    } else {
        group = $("#txtGroup").val();
    }
    var title = $("#txtTitle").val();

    // テーブルを2次元配列へ格納
    var data = [];
    var tr = $("#existsList tr");
    for(var i = 0; i < tr.length; i++) {
        var cells = tr.eq(i).children(); // 1行目から順に子要素(td)を取得
        for(var j = 0; j < cells.length; j++) {
            if(typeof data[i] == "undefined") data[i] = [];
            data[i][j] = cells.eq(j).text(); // tdの要素順に配列へ格納
        }
    }
    // 配列内に登録対象の分類、タイトルと重複するものがあれば登録中止
    for(var i = 0; i < data.length; i++) {
        if(data[i][0] == group && data[i][1] == title){
            mes = isLocaleEn ? 'For this classification, the title "'+title+'" has already been registered. Please register as a different title.' : 'この分類には、タイトル『'+title+'』が既に登録されています。別タイトルで登録してください。';
            alert(mes);
            return false;
        }
    }
    return true;
}
// 2019.01.30 s.furuta add end

/*
 * フォームデータのバリデーション
 */
function validationForFrmResult() {

  // 共通の検証
  if ( !validationForFrmResultCommon() )
    return false;

  // [英語外部試験スコア] 登録時検証
  if ( ($('#txtGroup').val().trim() == "" || $('#txtGroup').val() == null) && selGroupValueIs('EnglishTest') && !validationForFrmResultDetailEnglishTest() )
    return false;

  // [海外での学修経験] 登録時検証
  if ( ($('#txtGroup').val().trim() == "" || $('#txtGroup').val() == null) && selGroupValueIs('studyingAbroad') && !validationForFrmResultDetailStudyingAbroad() )
    return false;

  return true;
}

/*
 * フォームデータのバリデーション (共通部分)
 */
function validationForFrmResultCommon() {

  var locale = ($("#labelTitle").text() == "TITLE:") ? "en" : "ja";
  var isLocaleEn = locale == "en";
  var mes = "";
  var $field;
  var val;
  var enabled;

  // 必須項目の入力チェック・型チェック
  // タイトル
  $field = $('#txtTitle'); val = $field.val(); enabled = $field.prop('disabled') == false;
  if (enabled && val == "") {
    mes = isLocaleEn ? "TITLE is mandatory." : "タイトルは必須項目です。";
    alert(mes);
    return false;
  }
  // 日付
  $field = $('#txtDate'); val = $field.val(); enabled = $field.prop('disabled') == false;
  if (enabled && val == "") {
    mes = isLocaleEn ? "DATE is mandatory." : "日付は必須項目です。";
    alert(mes);
    $field.focus();
    return false;
  }
  // 出版年
  $field = $('#txtPublishedYear'); val = $field.val(); enabled = $field.prop('disabled') == false;
  if (enabled && val == "") {
    mes = isLocaleEn ? "Published year is mandatory." : "出版年は必須項目です。";
    alert(mes);
    $field.focus();
    return false;
  }
  if (enabled && !isYear(val)) {
    mes = isLocaleEn ? "Published year is not valid." : "出版年が不正です。";
    alert(mes);
    $field.focus();
    return false;
  }
  // 出版月
  $field = $('#txtPublishedMonth'); val = $field.val(); enabled = $field.prop('disabled') == false;
  if (enabled && val == "") {
    mes = isLocaleEn ? "Published month is mandatory." : "出版月は必須項目です。";
    alert(mes);
    $field.focus();
    return false;
  }
  if (enabled && !isMonth(val)) {
    mes = isLocaleEn ? "Published month is not valid." : "出版月が不正です。";
    alert(mes);
    $field.focus();
    return false;
  }
  // 開催年
  $field = $('#txtHeldYear'); val = $field.val(); enabled = $field.prop('disabled') == false;
  if (enabled && val == "") {
    mes = isLocaleEn ? "Holding year is mandatory." : "開催年は必須項目です。";
    alert(mes);
    $field.focus();
    return false;
  }
  if (enabled && !isYear(val)) {
    mes = isLocaleEn ? "Holding year is not valid." : "開催年が不正です。";
    alert(mes);
    $field.focus();
    return false;
  }
  // 開催月
  $field = $('#txtHeldMonth'); val = $field.val(); enabled = $field.prop('disabled') == false;
  if (enabled && val == "") {
    mes = isLocaleEn ? "Holding month is mandatory." : "開催月は必須項目です。";
    alert(mes);
    $field.focus();
    return false;
  }
  if (enabled && !isMonth(val)) {
    mes = isLocaleEn ? "Holding month is not valid." : "開催月が不正です。";
    alert(mes);
    $field.focus();
    return false;
  }
  // ページ
  $field = $('#txtPageFrom'); val = $field.val(); enabled = $field.prop('disabled') == false;
  if (enabled && val != "" && !isPositiveInt(val)) {
    mes = isLocaleEn ? "PAGE (FROM) must be a positive integer." : "開始ページは正整数で入力してください。";
    alert(mes);
    $field.focus();
    return false;
  }
  $field = $('#txtPageTo'); val = $field.val(); enabled = $field.prop('disabled') == false;
  if (enabled && val != "" && !isPositiveInt(val)) {
    mes = isLocaleEn ? "PAGE (TO) must be a positive integer." : "終了ページは正整数で入力してください。";
    alert(mes);
    $field.focus();
    return false;
  }

  // 各日付のIDをリストに保持
  var dateList = ['#txtDate', '#idTextToeicDate', '#idTextToeflpDate', '#idTextToefliDate', '#idTextIeltsDate', '#idTextEikenDate'];
  for (var i = 1; i <= 3; i++) {
    dateList.push('#idTextSonota' + i + 'Date');
  }
  for (var i = 1; i <= 5; i++) {
    dateList.push('#idTextPeriodFrom' + i);
    dateList.push('#idTextPeriodTo' + i);
  }

  // 各日付の入力を整形（2000/1/1→2000/01/01）
  for (var i = 0; i < dateList.length; i++) {
    if ($(dateList[i]).val().length == 8) {
      // 月日いずれも十の位が省略されている場合
      $(dateList[i]).val($(dateList[i]).val().slice(0, 5) + "0" + $(dateList[i]).val().slice(5, 7) + "0" + $(dateList[i]).val().slice(7));
    } else if ($(dateList[i]).val().length == 9) {
      if ($(dateList[i]).val().slice(-2, -1) == "/") {
        // 日付の十の位が省略されている場合
        $(dateList[i]).val($(dateList[i]).val().slice(0, 8) + "0" + $(dateList[i]).val().slice(8));
      } else {
        // 月の十の位が省略されている場合
        $(dateList[i]).val($(dateList[i]).val().slice(0, 5) + "0" + $(dateList[i]).val().slice(5));
      }
    }
    if ($(dateList[i]).val().length != 0 && $(dateList[i]).val().match(/^[0-9]{4}\/[0-9]{2}\/[0-9]{2}$/) == null) {
      if (i == 0 || selGroupValueIs('EnglishTest')) {
        mes = isLocaleEn
          ? "There is an invalid input in the date input field. \n Enter in the format of [○○○○ / ○○ / ○○]. \n (○ is half-width numeral)"
          : "日付の入力欄に不正な入力があります\n【○○○○/○○/○○】の形式で入力してください（○は半角数字）。";
        alert(mes);
        return false;
      }
    }
    var y = $(dateList[i]).val().slice(0, 4);
    var m = $(dateList[i]).val().slice(5, 7);
    var d = $(dateList[i]).val().slice(8);
    if ($(dateList[i]).val().length != 0 && ValidDate(y, m, d) == false) {
      if (i == 0 || selGroupValueIs('EnglishTest') || selGroupValueIs('studyingAbroad')) {
        mes = isLocaleEn ? "A date that does not exist has been entered." : "存在しない日付が入力されています。"
        alert(mes);
        return false;
      }
    }
  }

  // 過去の日付であることをチェック
  var now = new Date();
  var mon = now.getMonth() + 1;
  if (mon < 10) mon = "0" + mon;
  var nowYMD = "";
  var date = now.getDate();
  date < 10 ? nowYMD = now.getFullYear() + "/" + mon + "/0" + date : nowYMD = now.getFullYear() + "/" + mon + "/" + date;
  for (var i = 0; i < dateList.length - 10; i++) {
    if ($(dateList[i]).val() > nowYMD) {
      if (i == 0 || selGroupValueIs('EnglishTest') || selGroupValueIs('studyingAbroad')) {
        mes = isLocaleEn ? "You can not enter dates of the future." : "日付欄と取得日には未来の日付は入力できません。";
        alert(mes);
        return false;
      }
    }
  }

  return true;
}

/*
 * [英語外部試験スコア] 登録時検証
 */
function validationForFrmResultDetailEnglishTest() {

  var locale = ($("#labelTitle").text() == "TITLE:") ? "en" : "ja";
  var isLocaleEn = locale == "en";
  var mes = "";

  // 英語外部試験の必須検証
  if (($('#idTextToeicScore').val() == "" && $('#idTextToeicDate').val() != "") || ($('#idTextToeicScore').val() != "" && $('#idTextToeicDate').val() == "")
   || ($('#idTextToeflpScore').val() == "" && $('#idTextToeflpDate').val() != "") || ($('#idTextToeflpScore').val() != "" && $('#idTextToeflpDate').val() == "")
   || ($('#idTextToefliScore').val() == "" && $('#idTextToefliDate').val() != "") || ($('#idTextToefliScore').val() != "" && $('#idTextToefliDate').val() == "")
   || ($('#idTextIeltsScore').val() == "" && $('#idTextIeltsDate').val() != "") || ($('#idTextIeltsScore').val() != "" && $('#idTextIeltsDate').val() == "")
   || ($('#idSelectEiken').val() == "none" && $('#idTextEikenDate').val() != "") || ($('#idSelectEiken').val() != "none" && $('#idTextEikenDate').val() == ""))
  {
    mes = isLocaleEn
      ? "When registering TOEIC, TOEFL PBT, TOEFL IBT, IELTS, Practical English Skill Test (Eiken), please input both score and acquisition date."
      : "TOEIC , TOEFL PBT , TOEFL IBT , IELTS , 実用英語技能検定（英検）の登録時は、スコアと取得日双方に入力を行ってください。";
    alert(mes);
    return false;
  }

  // その他資格の必須検証
  for (var i=1; i<=3; i++) {
    if ($('#idTextSonota'+i).val() == "" && $('#idTextSonota'+i+'Score').val() == "" && $('#idTextSonota'+i+'Date').val() == "") continue;
    if ($('#idTextSonota'+i).val() != "" && $('#idTextSonota'+i+'Score').val() != "" && $('#idTextSonota'+i+'Date').val() != "") continue;
    mes = isLocaleEn
      ? "When you register other qualifications, you need three pieces of information: qualification name, score, acquisition date. \n-------- Other qualification column line "+i+" --------"
      : "その他資格の登録時は、資格名,スコア,取得日の3つの情報が必要です。\n--------その他資格 "+i+" 行目--------";
    alert(mes);
    return false;
  }

  // 点数が数値で入力されていることを検証
  if (isNaN($('#idTextToeicScore').val()) || isNaN($('#idTextToeflpScore').val()) || isNaN($('#idTextToefliScore').val()) || isNaN($('#idTextIeltsScore').val())) {
    mes = isLocaleEn
      ? "Please enter the score of TOEIC, TOEFL PBT, TOEFL IBT, IELTS as a numerical value."
      : "TOEIC , TOEFL PBT , TOEFL IBT , IELTS のスコアは数値で入力してください。" ;
    alert(mes);
    return false;
  }

  return true;
}

/*
 * [海外での学修経験] 登録時検証
 */
function validationForFrmResultDetailStudyingAbroad() {

  var locale = ($("#labelTitle").text() == "TITLE:") ? "en" : "ja";
  var isLocaleEn = locale == "en";
  var mes = "";

  // 海外学修の必須検証
  for (var i = 1; i <= 5; i++) {
    if ($('#idSelectPurpose' + i).val() == "none" && $('#idTextCountry' + i).val() == "" && $('#idTextPeriodFrom' + i).val() == "" && $('#idTextPeriodTo' + i).val() == "" && $('#idTextDestination' + i).val() == "") continue;
    if ($('#idSelectPurpose' + i).val() != "none" && $('#idTextCountry' + i).val() != "" && $('#idTextPeriodFrom' + i).val() != "" && $('#idTextPeriodTo' + i).val() != "" && $('#idTextDestination' + i).val() != "") continue;
    mes = isLocaleEn
      ? "When registering overseas study experience, five pieces of information are required, purpose, country name, period (start date), period (end date), visited place name. \n-------- Overseas study experience column line " + i + " --------"
      : "海外での学修経験登録時は、目的, 国名, 期間(開始日), 期間(終了日), 訪問先名の5つの情報が必要です。\n--------海外での学修経験 " + i + " 行目--------";
    alert(mes);
    return false;
  }

  // 海外学修期間の整合性検証
  for (var i = 1; i <= 5; i++) {
    if ($('#idTextPeriodFrom' + i).val() > $('#idTextPeriodTo' + i).val()) {
      mes = isLocaleEn
        ? "Please enter [travel start date - travel end date] for the period. \n The date after the end date is entered on the start date. \n-------- Period column line " + i + " --------"
        : "渡航期間は 【 渡航開始日 ～ 渡航終了日 】 を入力してください。\n渡航終了日が渡航開始日より前の日付で入力されている可能性があります。\n--------渡航期間 " + i + " 行目--------";
      alert(mes);
      return false;
    }
  }

  return true;
}

/*
 * 日付のバリデーション
 */
function ValidDate(y,m,d) {
  dt=new Date(y,m-1,d);
  return(dt.getFullYear()==y && dt.getMonth()==m-1 && dt.getDate()==d);
}

/*
 * 月として有効な文字列か判定
 */
function isMonth(string) {
  return (string == '1' || string == '2' || string == '3' || string == '4' || string == '5' || string == '6' || string == '7' || string == '8' || string == '9' || string == '10' || string == '11' || string == '12');
}

/*
 * 年として有効な文字列か判定
 */
function isYear(string) {
  if (isNaN(string)) return false;
  var float = parseFloat(string);
  if (!Number.isInteger(float)) return false;
  if (float < 1) return false;
  if (float > 9999) return false;
  return true;
}

/*
 * 正整数かどうか判定
 */
function isPositiveInt(string) {
  if (isNaN(string)) return false;
  var float = parseFloat(string);
  if (!Number.isInteger(float)) return false;
  if (float < 1) return false;
  return true;
}

/*
 * txtDate以外が入力されていた場合は日付データを作成する。
 */
function makeDateValue() {
  var dateValue = "";
  var monthValue = "";
  $('#txtDate').removeAttr('disabled');
  if ($('#txtDate').val() != "") return true;

  if ($('#txtPublishedYear').val() != "") {
    if ($('#txtPublishedMonth').val() < 10) {
      monthValue += "0";
    }
    dateValue = $('#txtPublishedYear').val() + '/' + monthValue + $('#txtPublishedMonth').val() + '/' + '01';
    $('#txtDate').val(dateValue);
    return true;
  }

  if ($('#txtHeldYear').val() != "") {
    if ($('#txtHeldMonth').val() < 10) {
        monthValue += "0";
      }
    dateValue = $('#txtHeldYear').val() + '/' + monthValue + $('#txtHeldMonth').val() + '/' + '01';
    $('#txtDate').val(dateValue);
    return true;
  }
  return false;
}

/*
 * Ajax で POST 送信
 */
function AjaxUpload(formData) {
  $.ajaxQueue({ // $.ajax → $.ajaxQueue に書き換えてキューで処理できるようにする
    url : 'pf/inputResult.json3?uid=' + $('#uid').val(),
    type: 'POST',
    contentType: false,
    processData: false,
    data: formData,
    xhr : function() { // 進捗処理
      NProgress.start(); // NProgress開始 (メモ: NProgress起動中はhtmlにnprogress-busyクラスが付与される)
      var XHR = $.ajaxSettings.xhr();
      if (XHR.upload) {
        XHR.upload.addEventListener('progress',function(uploadevent){
          var progre = uploadevent.loaded/uploadevent.total;
          NProgress.set(progre); // NProgress 0～1の間を取る
        }, false);
      }
      NProgress.done(); // NProgress終了
      return XHR;
    },
    success: function(response) {
      location.reload();
      console.log('登録に成功しました');
    },
    error: function(xhr, error) {
      alert('登録に失敗しました。');
      console.log('登録に失敗しました');
    }
  });
}

/*! jQuery Ajax Queue - v0.1.2pre - 2013-03-19
 * https://github.com/gnarf37/jquery-ajaxQueue
 * Copyright (c) 2013 Corey Frang; Licensed MIT */
(function($) {

  // jQuery on an empty object, we are going to use this as our Queue
  var ajaxQueue = $({});

  $.ajaxQueue = function( ajaxOpts ) {
    var jqXHR = "",
    dfd = $.Deferred(),
    promise = dfd.promise();

    // run the actual query
    function doRequest( next ) {
      jqXHR = $.ajax( ajaxOpts );
      jqXHR.done( dfd.resolve )
        .fail( dfd.reject )
        .then( next, next );
    }

    // queue our ajax request
    ajaxQueue.queue( doRequest );

    // add the abort method
    promise.abort = function( statusText ) {

    // proxy abort to the jqXHR if it is active
    if ( jqXHR ) {
      return jqXHR.abort( statusText );
    }

    // if there wasn't already a jqXHR we need to remove from queue
    var queue = ajaxQueue.queue(),
    index = $.inArray( doRequest, queue );

    if ( index > -1 ) {
      queue.splice( index, 1 );
    }

    // and then reject the deferred
    dfd.rejectWith( ajaxOpts.context || ajaxOpts, [ promise, statusText, "" ] );
      return promise;
    };

    return promise;
  };

})(jQuery);
