$(function(){
  $(".stepMenu dt").on("click", function() {
    $(this).next().slideToggle();
    if($(this).hasClass("active")){
      $(this).removeClass("active");
    } else {
      $(this).addClass("active");
    }
  });
});