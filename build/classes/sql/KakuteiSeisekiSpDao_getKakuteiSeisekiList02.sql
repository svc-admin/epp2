select 
    NINTEI_NENDO 
from 
    (select distinct NINTEI_NENDO from kakutei_seiseki_sp where G_SYSNO = ? 
     union 
     select distinct year as NINTEI_NENDO from toeic where uid = ?
    ) as UNI_TABLE 
order by NINTEI_NENDO desc
