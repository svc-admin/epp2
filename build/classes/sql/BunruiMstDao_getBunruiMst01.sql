SELECT
    BUNRUICD
   ,BUNRUINM
   ,BUNRUINMENG
FROM
    bunrui_mst
WHERE
    SHOZOKUCD IN (?,?)
ORDER BY
    SHOZOKUCD
   ,BUNRUICD
