SELECT
    MAX(tables.KAMOKUCD) AS KAMOKUCD
FROM
(
    SELECT MAX(KAMOKUCD) AS KAMOKUCD
      FROM seiseki_for_evidence
     WHERE G_SYSNO = ?
  UNION ALL
    SELECT MAX(KAMOKUCD) AS KAMOKUCD
      FROM results
     WHERE G_SYSNO = ?
  UNION ALL
    SELECT MAX(KAMOKUCD) AS KAMOKUCD
      FROM attachments
     WHERE G_SYSNO = ?
  UNION ALL
    SELECT MAX(KAMOKUCD) AS KAMOKUCD
      FROM external_english_tests
     WHERE G_SYSNO = ?
  UNION ALL
    SELECT MAX(KAMOKUCD) AS KAMOKUCD
      FROM studying_abroad_experiences
     WHERE G_SYSNO = ?
) tables
