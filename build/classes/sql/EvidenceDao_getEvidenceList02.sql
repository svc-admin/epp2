select
    a.*
from
    (
     select
         uid
        ,NENDO
        ,JIKANWARI_SHOZOKUCD
        ,JIKANWARICD
        ,dir
        ,evidence
        ,size
        ,lastUpdate
        ,INSERT_DATE
        ,UPDATE_DATE
        ,USERID
     from
         evidence
     where
         uid=?
     and NENDO=?
     and JIKANWARI_SHOZOKUCD=?
     and JIKANWARICD=?
     and dir=?
     and evidence=?
     union
     select
         G_SYSNO          AS uid
        ,JIKANWARI_NENDO  AS NENDO
        ,JIKANWARI_SHOZOKUCD
        ,JIKANWARICD
        ,dir
        ,evidence
        ,size
        ,lastUpdate
        ,INSERT_DATE
        ,UPDATE_DATE
        ,USERID
     from
         seiseki_for_evidence
     where
         G_SYSNO=?
     and JIKANWARI_NENDO=?
     and JIKANWARI_SHOZOKUCD=?
     and JIKANWARICD=?
     and dir=?
     and evidence=?
    ) a
