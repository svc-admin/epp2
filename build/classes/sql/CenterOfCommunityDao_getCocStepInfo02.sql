SELECT
    JW.NENDO
   ,JW.JIKANWARI_SHOZOKUCD
   ,JW.JIKANWARICD
   ,JW.GAKKIKBNCD
   ,JW.KAMOKUCD
   ,CONCAT(JW.KAIKO_KAMOKUNM,'(',SL.JUGYO_THEME,')') AS KAMOKUNM
   ,CONCAT(JW.KAIKO_KAMOKUNMENG,'(',SL.JUGYO_THEME,')') AS KAMOKUNMENG
   ,JW.TANISU AS KAMOKU_TANISU
   ,KS.TANISU
   ,KS.HYOGOCD
   ,KS.HYOGONM
   ,SL.STEP AS STEP
FROM
    kakutei_seiseki KS
    INNER JOIN jikanwari JW
            ON (    KS.JIKANWARI_NENDO     = JW.NENDO
                AND KS.JIKANWARI_SHOZOKUCD = JW.JIKANWARI_SHOZOKUCD
                AND KS.JIKANWARICD         = JW.JIKANWARICD
               )
    INNER JOIN (SELECT
                    NENDO
                   ,JIKANWARI_SHOZOKUCD
                   ,JIKANWARICD
                   ,KEYWORD AS STEP
                   ,JUGYO_THEME
                FROM
                    syl_coc
                WHERE
                    KEYWORD LIKE '%COC%'
                AND LOCALE = ?
                AND KANRYO_FLG = 1
                ) SL
            ON (    JW.NENDO               = SL.NENDO
                AND JW.JIKANWARI_SHOZOKUCD = SL.JIKANWARI_SHOZOKUCD
                AND JW.JIKANWARICD         = SL.JIKANWARICD
               )
WHERE
    KS.G_SYSNO = ?
AND KS.HYOGOCD IN ('AA','A','B','C','G','L','M','P','R')
ORDER BY
    KS.KAMOKUCD
