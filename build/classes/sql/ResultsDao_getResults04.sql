select results.G_SYSNO
     , results.KAMOKUCD
     , results.date
     , results.classification
     , results.classification
     , results.title
     , results.comment
     , results.author_name
     , results.author_type
     , results.presentation_format
     , results.medium_title
     , results.medium_class
     , results.has_impact_factor
     , results.impact_factor
     , results.location
     , results.publisher
     , results.isbn
     , results.journal_volume
     , results.journal_number
     , results.page_from
     , results.page_to
     , results.published_year
     , results.published_month
     , results.has_reviewed
     , concat(evi.dir)                   as original_medium_name
     , concat(evi.evidence)              as original_author_name
     , results.original_publisher
     , results.original_page_from
     , results.original_page_to
     , results.original_published_year
     , results.original_published_month
     , results.insert_date
     , results.update_date
     , concat(if(evi.code1=0,0,1),if(evi.code2=0,0,1),if(evi.code3=0,0,1),if(evi.code4=0,0,1),if(evi.code5=0,0,1),if(evi.code6=0,0,1),if(evi.code7=0,0,1)) as userid
  from results
 inner join seiseki_for_evidence evi
        on results.G_SYSNO = evi.G_SYSNO
       and results.KAMOKUCD = evi.KAMOKUCD
 where replace(lower(results.G_SYSNO),"-","")= ?
 order by results.G_SYSNO,results.KAMOKUCD