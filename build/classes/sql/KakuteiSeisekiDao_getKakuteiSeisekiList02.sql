select 
    NINTEI_NENDO 
from 
    (select distinct NINTEI_NENDO from kakutei_seiseki where G_SYSNO = ? 
     union 
     select distinct year as NINTEI_NENDO from toeic where uid = ? 
     union 
     select distinct NINTEI_NENDO from seiseki_for_evidence where G_SYSNO = ?
    ) as UNI_TABLE 
order by NINTEI_NENDO desc
