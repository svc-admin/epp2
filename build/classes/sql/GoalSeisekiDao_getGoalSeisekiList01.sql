select distinct
   a.KAMOKUCD,
   c.KAIKO_KAMOKUNM KAMOKUNM,
   c.KAIKO_KAMOKUNMENG KAMOKUNMENG,
   a.HYOGONM,
   a.HYOGOCD,
   a.TANISU,
   a.NINTEI_NENDO,
   a.NINTEI_GAKKIKBNCD,
   a.NINTEI_KAIKOKBNCD,
   b.HITSUSENKBNCD,
   a.JIKANWARI_SHOZOKUCD,
   a.JIKANWARI_NENDO,
   a.JIKANWARICD,
   gg.CODE1,
   gg.CODE2,
   gg.CODE3,
   gg.CODE4,
   gg.CODE5,
   gg.CODE6,
   gg.CODE7,
   gg.ENTERD_FLG
from
   kakutei_seiseki as a
   left join kucr_yoken_kamoku as b
          on     a.KAMOKUCD = b.KAMOKUCD
             and a.CUR_NENDO = b.CUR_NENDO
             and a.CUR_SHOZOKUCD = b.CUR_SHOZOKUCD
   left join jikanwari as c
          on     a.JIKANWARI_NENDO = c.NENDO
             and a.JIKANWARI_SHOZOKUCD = c.JIKANWARI_SHOZOKUCD
             and a.JIKANWARICD = c.JIKANWARICD
   left join goals as gg
          on     a.JIKANWARI_NENDO = gg.NENDO
             and a.JIKANWARI_SHOZOKUCD = gg.JIKANWARI_SHOZOKUCD
             and a.JIKANWARICD = gg.JIKANWARICD
where
   a.G_SYSNO = ?
union
select
   KAMOKUCD
  ,KAMOKUNM
  ,KAMOKUNMENG
  ,HYOGONM
  ,HYOGOCD
  ,TANISU
  ,NINTEI_NENDO
  ,NINTEI_GAKKIKBNCD
  ,NINTEI_GAKKIKBNCD
  ,HITSUSENKBNCD
  ,JIKANWARI_SHOZOKUCD
  ,JIKANWARI_NENDO
  ,JIKANWARICD
  ,CODE1
  ,CODE2
  ,CODE3
  ,CODE4
  ,CODE5
  ,CODE6
  ,CODE7
  ,'0'
from
   seiseki_for_evidence
where
   G_SYSNO = ?
order by     JIKANWARI_NENDO    ,KAMOKUCD
