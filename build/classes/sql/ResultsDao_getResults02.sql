select results.classification,results.title from results
inner join seiseki_for_evidence
        on results.G_SYSNO = seiseki_for_evidence.G_SYSNO
       and results.KAMOKUCD = seiseki_for_evidence.KAMOKUCD
where replace(lower(results.G_SYSNO),"-","")= ?
  and results.kamokucd != ?
