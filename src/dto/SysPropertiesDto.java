package dto;

import java.util.Properties;

/**
 * 2014-11-07: add evdir by nakano@cc.kumamoto-u.ac.jp
 *
 */
public class SysPropertiesDto {

    /** dbDriver */
    private String dbDriver;

    /** dbUri */
    private String dbUri;

    /** dbProps */
    private Properties dbProps;

    /** syllabusuri */
    private String syllabusuri;

    /** evuri */
    private String evuri;

    /** evdir */
    private String evdir;

    /** sqldir */
    private String sqldir;

    // 2015.02.26 SVC 追加 >>
    /** uidPatterns */
    private String[] uidPatterns;

    /** uidRegex */
    private String uidRegex;
    // 2015.02.26 SVC 追加 <<

    // 2017-01-22 nakano 追加 >>
    private String[] openSems;
    // 2017-01-22 nakano 追加 <<


    public String getDbDriver() {
        return dbDriver;
    }

    public void setDbDriver(String dbDriver) {
        this.dbDriver = dbDriver;
    }

    public String getDbUri() {
        return dbUri;
    }

    public void setDbUri(String dbUri) {
        this.dbUri = dbUri;
    }

    public Properties getDbProps() {
        return dbProps;
    }

    public void setDbProps(Properties dbProps) {
        this.dbProps = dbProps;
    }

    public String getSyllabusuri() {
        return syllabusuri;
    }

    public void setSyllabusuri(String syllabusuri) {
        this.syllabusuri = syllabusuri;
    }

    public String getSqldir() {
        return sqldir;
    }

    public void setSqldir(String sqldir) {
        this.sqldir = sqldir;
    }

    public String getEvuri() {
        return evuri;
    }

    public void setEvuri(String evuri) {
        this.evuri = evuri;
    }

    public String getEvdir() {
        return evdir;
    }

    public void setEvdir(String evdir) {
        this.evdir = evdir;
    }

    // 2015.02.26 SVC 追加 >>
    public String[] getUidPatterns() {
        return uidPatterns;
    }
    public void setUidPatterns(String[] uidPatterns) {
        this.uidPatterns = uidPatterns;
    }

    public String getUidRegex() {
        return uidRegex;
    }

    public void setUidRegex(String uidRegex) {
        this.uidRegex = uidRegex;
    }
    // 2015.02.26 SVC 追加 <<

    // 2017-01-22 nakano 追加 >>
    /**
     * @return the openSems
     */
    public String[] getOpenSems() {
        return openSems;
    }

    /**
     * @param openSems the openSems to set
     */
    public void setOpenSems(String[] openSems) {
        this.openSems = openSems;
    }
    // 2017-01-22 nakano 追加 <<

}
