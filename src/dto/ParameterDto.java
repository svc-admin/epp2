package dto;

import dao.entity.Attachments;
import dao.entity.Evidence;
import dao.entity.ExternalEnglishTests;
import dao.entity.Gakuseki;
import dao.entity.GpaShugakuninteiBukyokuMst;
import dao.entity.GpaShugakuninteiGpMst;
import dao.entity.GpaShugakuninteiJogaikamokuMst;
import dao.entity.HanteiShubetsuData;
import dao.entity.HyojunTanisu;
import dao.entity.Results;
import dao.entity.SeisekiForEvidence;
import dao.entity.ShoriJokenMst;
import dao.entity.ShugakusienHanteiData;
import dao.entity.ShugakusienHanteiShusei;
import dao.entity.ShutokutaniTaishoMst;
import dao.entity.StudyingAbroadExperieces;

public class ParameterDto {

    /** uid */
    private String uid;

    /** shozokucd */
    private String shozokucd;

    /** nyugakunendo */
    private String nyugakunendo;

    /** lang */
    private String lang;

    /** num */
    private Integer num;

    /** exp */
    private boolean exp;

    /** ruser */
    private String ruser;

    /** gakuseki */
    private Gakuseki gakuseki;

    /** evidence */
    private Evidence evidence;

// 2014.11.21 SVC パラメータを追加 >>
    /** yokennendo */
    private String yokennendo;

    /** daigakuinkbncd */
    private String daigakuinkbncd;

    /** sysno */
    private String sysno;
// 2014.11.21 SVC パラメータを追加 <<
// 2015.02.26 SVC ADD START >> パラメータ追加
    /** seiseki_for_evidence */
    private SeisekiForEvidence seisekiForEvidence;
// 2015.02.26 SVC ADD END   <<
// 2017.01.19 SVC ADD START >> パラメータ追加
    /** cocorplus */
    private String cocorplus;
    /** searchcon */
    private String searchcon;
// 2017.01.19 SVC ADD END   <<
// 2018.07.26 SVC ADD START >> パラメータ追加
    /** results */
    private Results results;
    /** attachments */
    private Attachments attachments;
    /** external_english_tests */
    private ExternalEnglishTests externalEnglishTests;
    /** studying_abroad_experiences */
    private StudyingAbroadExperieces studyingAbroadExperiences;
// 2018.07.26 SVC ADD END   <<
// 2019.01.09 s.furuta add start 1.学生番号検索機能追加
    /** searchtype */
    private String searchtype;
    /** searchid */
    private String searchid;
// 2019.01.09 s.furuta add end
// 2019.01.16 s.furuta add start 2.学修成果修正
    /** kamokucd */
    private String kamokucd;
// 2019.01.16 s.furuta add end
// 2019.05.16 s.furuta add start バグ対応（restrictionなし教員での学生抽出時エラーが起きる）
    /** targetuids */
    private String targetuids;
// 2019.05.16 s.furuta add end
// 2019.11.05 SVC add start 修学支援制度認定対応
    /** GpaShugakuninteiBukyokuMst */
    private GpaShugakuninteiBukyokuMst gpaShugakuninteiBukyokuMst;
    /** GpaShugakuninteiGpMst */
    private GpaShugakuninteiGpMst gpaShugakuninteiGpMst;
    /** GpaShugakuninteiJogaikamokuMst */
    private GpaShugakuninteiJogaikamokuMst gpaShugakuninteiJogaikamokuMst;
    /** HyojunTanisu */
    private HyojunTanisu hyojunTanisu;
// 2019.11.05 SVC add end
// 2019.11.11 SVC add start
    /** SyutokutaniJogaiMst*/
    private ShutokutaniTaishoMst shutokutaniTaishoMst;
// 2019.11.11 SVC add end
// 2019.11.21 SVC add start
    private ShoriJokenMst ShoriJokenMst;
// 2019.11.21 SVC add end
// 2019.12.10 SVC ADD START >> 修学支援判定機能追加
    private String gBukyokucd;
    private Integer gakunen;
    private java.util.Date nendoStart;
    private java.util.Date nendoEnd;
    private String etcParam;
    private HanteiShubetsuData hanteiShubetsuData;
    private ShugakusienHanteiShusei hanteiShuseiData;
    private ShugakusienHanteiData hanteiData;
// 2019.12.10 SVC ADD END   <<

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getShozokucd() {
        return shozokucd;
    }

    public void setShozokucd(String shozokucd) {
        this.shozokucd = shozokucd;
    }

    public String getNyugakunendo() {
        return nyugakunendo;
    }

    public void setNyugakunendo(String nyugakunendo) {
        this.nyugakunendo = nyugakunendo;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public boolean getExp() {
        return exp;
    }

    public void setExp(boolean exp) {
        this.exp = exp;
    }

    public String getRuser() {
        return ruser;
    }

    public void setRuser(String ruser) {
        this.ruser = ruser;
    }

    public Gakuseki getGakuseki() {
        return gakuseki;
    }

    public void setGakuseki(Gakuseki gakuseki) {
        this.gakuseki = gakuseki;
    }

    public Evidence getEvidence() {
        return evidence;
    }

    public void setEvidence(Evidence evidence) {
        this.evidence = evidence;
    }

// 2014.11.21 SVC パラメータを追加 >>
    public String getYokenNendo() {
        return yokennendo;
    }

    public void setYokenNendo(String yokennendo) {
        this.yokennendo = yokennendo;
    }

    public String getDaigakuinKbncd() {
        return daigakuinkbncd;
    }

    public void setDaigakuinKbncd(String daigakuinkbncd) {
        this.daigakuinkbncd = daigakuinkbncd;
    }

    public String getSysNo() {
        return sysno;
    }

    public void setSysNo(String sysno) {
        this.sysno = sysno;
    }
// 2014.11.21 SVC パラメータを追加 <<
// 2015.02.26 SVC ADD START >> パラメータ追加
    public SeisekiForEvidence getSeisekiForEvidence() {
        return seisekiForEvidence;
    }

    public void setSeisekiForEvidence(SeisekiForEvidence seisekiForEvidence) {
        this.seisekiForEvidence = seisekiForEvidence;
    }
// 2015.02.26 SVC ADD END   <<

// 2017.01.19 SVC ADD START >> パラメータ追加
    public String getCocOrPlus() {
        return cocorplus;
    }

    public void setCocOrPlus(String cocorplus) {
        this.cocorplus = cocorplus;
    }

    public String getSearchCon() {
        return searchcon;
    }

    public void setSearchCon(String searchcon) {
        this.searchcon = searchcon;
    }
// 2017.01.19 SVC ADD END   <<

// 2018.07.26 SVC ADD START >> パラメータ追加
    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }

    public Attachments getAttachments() {
        return attachments;
    }

    public void setAttachments(Attachments attachments) {
        this.attachments = attachments;
    }

    public ExternalEnglishTests getExternalEnglishTests() {
        return externalEnglishTests;
    }

    public void setExternalEnglishTests(
            ExternalEnglishTests externalEnglishTests) {
        this.externalEnglishTests = externalEnglishTests;
    }

    public StudyingAbroadExperieces getStudyingAbroadExperiences() {
        return studyingAbroadExperiences;
    }

    public void setStudyingAbroadExperiences(
            StudyingAbroadExperieces studyingAbroadExperiences) {
        this.studyingAbroadExperiences = studyingAbroadExperiences;
    }
    // 2018.07.26 SVC ADD END   <<
    // 2019.01.09 s.furuta add start 1.学生番号検索機能追加
    public String getSearchType() {
        return searchtype;
    }

    public void setSearchType(String searchtype) {
        this.searchtype = searchtype;
    }

    public String getSearchId() {
        return searchid;
    }

    public void setSearchId(String searchid) {
        this.searchid = searchid;
    }
    // 2019.01.09 s.furuta add end
    // 2019.01.16 s.furuta add start 2.学修成果修正
    public String getKamokuCd() {
        return kamokucd;
    }

    public void setKamokuCd(String kamokucd) {
        this.kamokucd = kamokucd;
    }
    // 2019.01.16 s.furuta add end
    // 2019.05.16 s.furuta add start バグ対応（restrictionなし教員での学生抽出時エラーが起きる）
    // ここに、学修成果取得対象の学生一覧をカンマ区切りで保持する
    public String getTargetUids() {
        return targetuids;
    }

    public void setTargetUids(String targetuids) {
        this.targetuids = targetuids;
    }
    // 2019.05.16 s.furuta add end
    // 2019.11.05 SVC add start 修学支援制度認定対応
    public GpaShugakuninteiBukyokuMst getGpaShugakuninteiBukyokuMst() {
        return gpaShugakuninteiBukyokuMst;
    }

    public void setGpaShugakuninteiBukyokuMst(GpaShugakuninteiBukyokuMst gpaShugakuninteiBukyokuMst) {
        this.gpaShugakuninteiBukyokuMst = gpaShugakuninteiBukyokuMst;
    }

    public GpaShugakuninteiGpMst getGpaShugakuninteiGpMst() {
        return gpaShugakuninteiGpMst;
    }

    public void setGpaShugakuninteiGpMst(GpaShugakuninteiGpMst gpaShugakuninteiGpMst) {
        this.gpaShugakuninteiGpMst = gpaShugakuninteiGpMst;
    }

    public GpaShugakuninteiJogaikamokuMst getGpaShugakuninteiJogaikamokuMst() {
        return gpaShugakuninteiJogaikamokuMst;
    }

    public void setGpaShugakuninteiJogaikamokuMst(GpaShugakuninteiJogaikamokuMst gpaShugakuninteiJogaikamokuMst) {
        this.gpaShugakuninteiJogaikamokuMst = gpaShugakuninteiJogaikamokuMst;
    }

    public HyojunTanisu getHyojunTanisu() {
        return hyojunTanisu;
    }

    public void setHyojunTanisu(HyojunTanisu hyojunTanisu) {
        this.hyojunTanisu = hyojunTanisu;
    }

    public ShutokutaniTaishoMst getShutokutaniTaishoMst() {
		return shutokutaniTaishoMst;
	}

	public void setShutokutaniTaishoMst(ShutokutaniTaishoMst shutokutaniTaishoMst) {
		this.shutokutaniTaishoMst = shutokutaniTaishoMst;
	}

	public ShoriJokenMst getShoriJokenMst(){
    	return ShoriJokenMst;
    }

    public void setShoriJokenMst(ShoriJokenMst shorijokenmst) {
        this.ShoriJokenMst = shorijokenmst;
    }

    public ParameterDto() {
        this.gpaShugakuninteiBukyokuMst = new GpaShugakuninteiBukyokuMst();
        this.gpaShugakuninteiGpMst = new GpaShugakuninteiGpMst();
        this.gpaShugakuninteiJogaikamokuMst = new GpaShugakuninteiJogaikamokuMst();
        this.hyojunTanisu = new HyojunTanisu();
        this.shutokutaniTaishoMst = new ShutokutaniTaishoMst();
        this.ShoriJokenMst = new ShoriJokenMst();
        this.hanteiShubetsuData = new HanteiShubetsuData();
        this.hanteiShuseiData = new ShugakusienHanteiShusei();
        this.hanteiData = new ShugakusienHanteiData();
    }
// 2019.11.05 SVC add end

	public String getgBukyokucd() {
		return gBukyokucd;
	}

	public void setgBukyokucd(String gBukyokucd) {
		this.gBukyokucd = gBukyokucd;
	}

	public Integer getGakunen() {
		return gakunen;
	}

	public void setGakunen(Integer gakunen) {
		this.gakunen = gakunen;
	}

	public java.util.Date getNendoStart() {
		return nendoStart;
	}

	public void setNendoStart(java.util.Date nendoStart) {
		this.nendoStart = nendoStart;
	}

	public java.util.Date getNendoEnd() {
		return nendoEnd;
	}

	public void setNendoEnd(java.util.Date nendoEnd) {
		this.nendoEnd = nendoEnd;
	}

	public String getEtcParam() {
		return etcParam;
	}

	public void setEtcParam(String etcParam) {
		this.etcParam = etcParam;
	}

	public HanteiShubetsuData getHanteiShubetsuData() {
		return hanteiShubetsuData;
	}

	public void setHanteiShubetsuData(HanteiShubetsuData hanteiShubetsuData) {
		this.hanteiShubetsuData = hanteiShubetsuData;
	}

	public ShugakusienHanteiShusei getHanteiShuseiData() {
		return hanteiShuseiData;
	}

	public void setHanteiShuseiData(ShugakusienHanteiShusei hanteiShuseiData) {
		this.hanteiShuseiData = hanteiShuseiData;
	}

	public ShugakusienHanteiData getHanteiData() {
		return hanteiData;
	}

	public void setHanteiData(ShugakusienHanteiData hanteiData) {
		this.hanteiData = hanteiData;
	}
}
