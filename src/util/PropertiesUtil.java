package util;

import java.util.Properties;
import java.util.ResourceBundle;

import dto.SysPropertiesDto;

/**
 * 2014-11-07: add dto.setEvdir by nakano@cc.kumamoto-u.ac.jp
 *
 */
public class PropertiesUtil {

    public SysPropertiesDto getSysProperties() {

        SysPropertiesDto dto = new SysPropertiesDto();

        ResourceBundle bundle = ResourceBundle.getBundle("sys");
        dto.setDbDriver(bundle.getString("driver"));
        dto.setDbUri(bundle.getString("uri"));
        Properties prop = new Properties();
        prop.put("user", bundle.getString("user"));
        prop.put("password", bundle.getString("password"));
        prop.put("zeroDateTimeBehavior", bundle.getString("zeroDateTimeBehavior"));
        dto.setDbProps(prop);

        dto.setSyllabusuri(bundle.getString("syllabusUri"));
        dto.setEvuri(bundle.getString("evUri"));
        dto.setEvdir(bundle.getString("evDir"));
        dto.setSqldir(bundle.getString("sqldir"));

        // 2015.02.26 SVC 追加 >>
        String uidPatterns = bundle.getString("uidPatterns");
        dto.setUidPatterns(uidPatterns.split(","));
        dto.setUidRegex(bundle.getString("uidRegex"));
        // 2015.02.26 SVC 追加 <<

        // 2017-01-22 nakano 追加 >>
        String openSemsStr = bundle.getString("openSems");
        dto.setOpenSems(openSemsStr.split(","));
        // 2017-01-22 nakano 追加 <<

        return dto;
    }
}
