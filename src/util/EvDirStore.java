package util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import dao.EvidenceDao;
import dao.GakusekiDao;
import dao.entity.Gakuseki;
import dto.ParameterDto;
import dto.SysPropertiesDto;

/**
 * @author nakano@cc.kumamoto-u.ac.jp
 *
 */
public class EvDirStore {
    private static Logger logger = Tools.getCallerLogger();
    private String evDir = null;
    private ParameterDto pramDto = new ParameterDto();
    private ArrayList<Gakuseki> resGakuseki = null;

    public EvDirStore() {
        PropertiesUtil propUtil = new PropertiesUtil();
        SysPropertiesDto sysPropDto = propUtil.getSysProperties();
        evDir = sysPropDto.getEvdir();
        if(evDir.endsWith("/"))		evDir = evDir.substring(0, evDir.length()-1);

        GakusekiDao gakusekiDao = new GakusekiDao();
        resGakuseki = gakusekiDao.getGakusekiList07(pramDto);
        // for(Gakuseki g : resGakuseki) System.out.println(g.getgSysno()); // for
        // checking
    }

    private void doStore(String ruser) throws IOException {
        logger.info("evDir=" + evDir);
        EvidenceDao eDao = new EvidenceDao();
        List<List<Object>> paramList = new ArrayList<List<Object>>();
        List<Object> valueList = new ArrayList<Object>();
        int recN = 0;
        for (Gakuseki g : resGakuseki) {
            String uid = g.getgSysno();
            String udirStr = uid.replace("-", "").toLowerCase();
            String udirName = evDir + "/" + udirStr;
            File udir = new File(udirName);
//			logger.info(udirStr + ":" + (udir.exists() ? udir.isDirectory() + "," + udir.listFiles().length : "not exist"));
            if (udir.exists()) {
                paramList.clear();
                File[] cNames = udir.listFiles();
                for (File cName : cNames) {
                    if (cName.isDirectory()) {
                    	if (cName.toString().matches(".*\\([0-9]{4}-[0-9]{2}-[0-9,A-Z]{5}\\)")){
                            String cNameStr = cName.toString().replace(udirName + "/", "");
                            String nendo = null, jShozoku = null, jCode = null;
//							logger.info("<" + cNameStr + ">");
                            String[] cNameSub = cName.toString().split("-");
                            int cNameSubLen = cNameSub.length;
                            nendo = cNameSub[cNameSubLen-3].substring(cNameSub[cNameSubLen-3].lastIndexOf('(')+1);
                            jShozoku = cNameSub[cNameSubLen - 2];
                            jCode = cNameSub[cNameSubLen - 1].substring(0,
                                cNameSub[cNameSubLen - 1].length() - 1);
//							logger.info("[" + nendo + "][" + jShozoku + "][" + jCode + "]");
                            File[] eNames = cName.listFiles();
                            for (File eName : eNames) {
                                if (eName.isDirectory()) {
                                    String eNameStr = eName.toString().replace(
                                        udirName + "/" + cNameStr + "/", "");
                                    Date eNameDate = new Date(eName.lastModified());
                                    long eNameSize = getDirSize(eName);
//									logger.info((recN++) + ": (" + eNameStr + "," + eNameSize + "," + eNameDate.toString());
//									eDao.insertEvidence01(ruser, uid, Integer.parseInt(nendo), jShozoku, jCode,
//											cNameStr, eNameStr, eNameSize, eNameDate);
                                    if(recN++ % 10000 == 0) {
                                        logger.info(recN + " entries added");
                                    }
                                    valueList = new ArrayList<Object>();
                                    valueList.clear();
                                    valueList.add(uid);
                                    valueList.add(new Integer(Integer.parseInt(nendo)));
                                    valueList.add(jShozoku);
                                    valueList.add(jCode);
                                    valueList.add(cNameStr);
                                    valueList.add(eNameStr);
                                    valueList.add(new Long(eNameSize));
                                    valueList.add(new java.sql.Timestamp(eNameDate.getTime()));

//									logger.info("============== valueList.size() = "+valueList.size());
                                    paramList.add(valueList);
                                }
                            }
                        }
                    }
                }
                if(paramList.size() > 0) eDao.insertEvidence01all(ruser, paramList);
            }

            // File[] childDirs = dirs.listFiles();
            // for(File f: childDirs) {
            // logger.debug(f.getName() +",, " + 0 + ", \"" + f.getName()+"\", "+new
            // Date(f.lastModified())+", "+f.isDirectory() +", \"" + homeDir + "\"");
            // if(f.isDirectory()) {
            // subDirectories(f.getName(), null, 1, f);
            // }
            // }
        }

    }

    static long getDirSize(File file) throws IOException {
        long size = 0L;
        if(file == null) {
            logger.error("File is null");
            return size;
        }
        if(file.isDirectory()) {
            File files[] = file.listFiles();
            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    size += getDirSize(files[i]);
                }
            }
        } else {
            size = file.length();
        }
        return size;
    }

    public static void main(String[] args) {
        EvDirStore me = new EvDirStore();
        try {
        me.doStore("gguportal");
    } catch (IOException e) {
        e.printStackTrace();
    }
    }

}
