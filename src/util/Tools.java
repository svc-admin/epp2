package util;

import org.apache.log4j.Logger;

public class Tools {
    public static Logger getCallerLogger() {
        return Logger.getLogger(new Throwable().getStackTrace()[1].getClassName());
    }
}
