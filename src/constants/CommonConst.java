package constants;


/**
 * 定数クラス.
 *
 * @author
 *
 */
public class CommonConst {

    /** ログ出力文字列：開始 */
    public static final String START_LOG = "START_METHOD：";

    /** ログ出力文字列：終了 */
    public static final String END_LOG = "END_METHOD：";

    /** 言語：日本語 */
    public static final String JAPANESE = "ja";

    /** 言語：英語 */
    public static final String ENGLISH = "en";

    /** 学期区分：前期 */
    public static final String GAKKIKBN_ZENKI = "1";

    /** 学期区分：後期 */
    public static final String GAKKIKBN_KOUKI = "2";

    /** SEMESTER：前期 */
    public static final String SEMESTER_ZENKI = "0";

    /** SEMESTER：後期 */
    public static final String SEMESTER_KOUKI = "1";

    /** 時間割所属コード：教養 */
    public static final String JIKANWARISHOZOKUCD_KYOYO = "58";

    /** 必修選択区分：必修 */
    public static final String HITSUSENKBN_HITSU = "1";

    /** 必修選択区分：必修選択 */
    public static final String HITSUSENKBN_HITSUSEN = "2";

    /** 必修選択区分：選択 */
    public static final String HITSUSENKBN_SEN = "3";

    /** 必修選択区分：その他 */
    public static final String HITSUSENKBN_SONOTA = "4";

    /** 区分：専門・必修 */
    public static final String SENMON_HITSU = "1";

    /** 区分：専門・選択 */
    public static final String SENMON_SEN = "2";

    /** 区分：教養・必修 */
    public static final String KYOYO_HITSU = "3";

    /** 区分：教養・選択 */
    public static final String KYOYO_SEN = "4";

    /** 学習成果入力：分類 */
    public static enum ResultGroup {
        paper,          // 論文
        presentation,   // 学会発表
        EnglishTest,    // 英語外部試験
        studyingAbroad, // 海外での学修経験
        common,         // 共通
    };
}
