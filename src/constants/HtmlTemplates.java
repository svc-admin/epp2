package constants;

/**
 * HTML テンプレート
 */
public class HtmlTemplates {

    /** 共通 CSS */
    private static final String COMMON_STYLE
        = " body {font-family: 'Lucida Grande','Segoe UI','Yu Gothic',YuGothic,'Hiragino Sans',Meiryo,sans-serif; line-height: 150%;}"
        + " h1, h2, h3 {font-weight: bold; padding: .5rem;}"
        + " h1 {font-size: 1.5rem; color: white; background-color: black;}"
        + " h2 {font-size: 1.25rem;}"
        + " h3 {font-size: 1.2rem;}"
        + " table {margin: 0 1em; width: calc(100% - 2em); border-collapse: collapse;}"
        + " th, td {font-weight: normal; padding: .25em .5em; border: 1px solid gray;}"
        + " th {font-weight: bold; text-align: center;}"
        + " .abstract {margin: 1em; padding: 1em; border: 1px solid gray;}"
        + " .attachments {margin: 1em;}"
        + " .timestamp {margin: 1em; font-weight: bold;}"
        + " .detail {margin: 0 1em;}"
        + " .detail .journalNumber::before {content: '(';}"
        + " .detail .journalNumber::after {content: ')';}"
        + " .detail .PageFrom::after {content: '-';}"
        + " .detail .publishedYear::after {content: '年';}"
        + " .detail .publishedMonth::after {content: '月';}"
        + "\n";

    /** 共通ヘッダ */
    private static final String COMMON_HEADER
        = "<!DOCTYPE html>\n<html>\n\n"
        + "<head>\n"
        + "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\n"
        + "<title>TEMP_HTMLTITLE</title>\n"
        + "<style><!--\n"
        + COMMON_STYLE
        + "--></style>\n"
        + "</head>\n\n"
        + "<body>\n"
        + "<h1>TEMP_KAMOKUNM</h1>\n"
        + "<h2>TEMP_TITLE</h2>\n";

    /** 共通フッタ */
    private static final String COMMON_FOOTER
        = "<div class=\"timestamp\">TEMP_TIMESTAMP</div>\n"
        + "<ul class=\"attachments\">\nTEMP_FILELIST</ul>\n"
        + "<hr />\n"
        + "</body>\n\n"
        + "</html>\n";

    /** HTML テンプレート (共通用 - 英語版/日本語版共通) */
    public static final String COMMON
        = COMMON_HEADER
        + "<div class=\"abstract\">TEMP_COMMENT</div>\n"
        + COMMON_FOOTER;

    /** HTML テンプレート (英語外部試験スコア用 - 英語版) */
    public static final String ENGLISH_TEST_EN
        = COMMON_HEADER
        + "<div class=\"abstract\">TEMP_COMMENT</div>\n"
        + "<h3>Scores in English external exams</h3>\n"
        + "<table>\n"
        + "  <thead><th width='40%'>Examinations</th><th width='20%'>Qualification</th><th width='40%'>Date of acquisition</th></thead>\n"
        + "  <tbody>TEMP_SCORE</tbody>\n"
        + "</table>\n"
        + COMMON_FOOTER;

    /** HTML テンプレート (英語外部試験スコア用 - 日本語版) */
    public static final String ENGLISH_TEST_JA
        = COMMON_HEADER
        + "<div class=\"abstract\">TEMP_COMMENT</div>\n"
        + "<h3>英語外部試験スコア</h3>\n"
        + "<table>\n"
        + "  <thead><th width='40%'>試験名・資格名</th><th width='20%'>得点・級</th><th width='40%'>取得日</th></thead>\n"
        + "  <tbody>TEMP_SCORE</tbody>\n"
        + "</table>\n"
        + COMMON_FOOTER;

    /** HTML テンプレート (海外での学修経験用 - 英語版) */
    public static final String STUDYING_ABROAD_EN
        = COMMON_HEADER
        + "<div class=\"abstract\">TEMP_COMMENT</div>\n"
        + "<h3>Experiences in overseas studies</h3>\n"
        + "<table>\n"
        + "  <thead><th width='45%'>Purpose</th><th width='15%'>Country</th><th width='20%'>Period</th><th width='20%'>Destination</th></thead>\n"
        + "  <tbody>TEMP_ABROAD</tbody>\n"
        + "</table>\n"
        + COMMON_FOOTER;

    /** HTML テンプレート ( 海外での学修経験用 - 日本語版) */
    public static final String STUDYING_ABROAD_JA
        = COMMON_HEADER
        + "<div class=\"abstract\">TEMP_COMMENT</div>\n"
        + "<h3>海外学修経験</h3>\n"
        + "<table>\n"
        + "  <thead><th width='45%'>渡航目的</th><th width='15%'>国名</th><th width='20%'>期間</th><th width='20%'>訪問先名</th></thead>\n"
        + "  <tbody>TEMP_ABROAD</tbody>\n"
        + "</table>\n"
        + COMMON_FOOTER;

    /** HTML テンプレート （ 論文用 - 日本語版 ） */
    public static final String JOURNAL_PAPER_JA
        = COMMON_HEADER
        + "<section class=\"detail\">\n"
        + "<div class=\"authorName\">TEMP_AUTHOR_NAME</div>\n"
        + "<div class=\"journal\">\n"
        + "<span class=\"mediumTitle\">TEMP_JOURNAL_TITLE</span>\n"
        + "<span class=\"journalVolume\">TEMP_JOURNAL_VOLUME</span>\n"
        + "<span class=\"journalNumber\">TEMP_JOURNAL_NUMBER</span>\n"
        + "<span class=\"PageFrom\">TEMP_PAGE_FROM</span><span class=\"PageTo\">TEMP_PAGE_TO</span>\n"
        + "<span class=\"publishedYear\">TEMP_PUBLISHED_YEAR</span><span class=\"publishedMonth\">TEMP_PUBLISHED_MONTH</span>\n"
        + "</div>\n"
        + "<div class=\"journal\">\n"
        + "<span class=\"authorType\">TEMP_AUTHOR_TYPE</span>・<span class=\"journalType\">TEMP_JOURNAL_TYPE</span>・査読<span class=\"reviewed\">TEMP_REVIEWED</span>・IF <span class=\"impactFactor\">TEMP_IMPACT_FACTOR</span>\n"
        + "</div>\n"
        + "</section>\n"
        + "<div class=\"abstract\">TEMP_COMMENT</div>\n"
        + COMMON_FOOTER;

    /** HTML テンプレート （ 論文用 - 英語版 ） */
    public static final String JOURNAL_PAPER_EN
        = COMMON_HEADER
        + "<section class=\"detail\">\n"
        + "<div class=\"authorName\">TEMP_AUTHOR_NAME</div>\n"
        + "<div class=\"journal\">\n"
        + "<span class=\"mediumTitle\">TEMP_JOURNAL_TITLE</span>\n"
        + "<span class=\"journalVolume\">TEMP_JOURNAL_VOLUME</span>\n"
        + "<span class=\"journalNumber\">TEMP_JOURNAL_NUMBER</span>\n"
        + "<span class=\"PageFrom\">TEMP_PAGE_FROM</span><span class=\"PageTo\">TEMP_PAGE_TO</span>\n"
        + "<span>TEMP_PUBLISHED_YEAR</span>,<span>TEMP_PUBLISHED_MONTH</span>\n"
        + "</div>\n"
        + "<div class=\"journal\">\n"
        + "<span class=\"authorType\">TEMP_AUTHOR_TYPE</span>・<span class=\"journalType\">TEMP_JOURNAL_TYPE</span>・<span class=\"reviewed\">TEMP_REVIEWED</span>・IF <span class=\"impactFactor\">TEMP_IMPACT_FACTOR</span>\n"
        + "</div>\n"
        + "</section>\n"
        + "<div class=\"abstract\">TEMP_COMMENT</div>\n"
        + COMMON_FOOTER;

    /** HTML テンプレート （ 学会発表用 - 日本語版 ） */
    public static final String PRESENTATION_JA
        = COMMON_HEADER
        + "<section class=\"detail\">\n"
        + "<div class=\"authorName\">TEMP_AUTHOR_NAME</div>\n"
        + "<div class=\"presentation\">\n"
        + "<span class=\"mediumTitle\">TEMP_PRESENTATION_TITLE</span>\n"
        + "<span class=\"place\">TEMP_PLACE</span>\n"
        + "<span class=\"publishedYear\">TEMP_HELD_YEAR</span><span class=\"publishedMonth\">TEMP_HELD_MONTH</span>\n"
        + "</div>\n"
        + "<div class=\"presentation\">\n"
        + "<span class=\"presenterType\">TEMP_PRESENTER_TYPE</span>・<span class=\"presentationType\">TEMP_PRESENTATION_TYPE</span>・<span class=\"conferenceType\">TEMP_CONFERENCE_TYPE</span>・査読<span class=\"reviewed\">TEMP_REVIEWED</span>\n"
        + "</div>\n"
        + "</section>\n"
        + "<div class=\"abstract\">TEMP_COMMENT</div>\n"
        + COMMON_FOOTER;

    /** HTML テンプレート （ 学会発表用 - 英語版 ） */
    public static final String PRESENTATION_EN
        = COMMON_HEADER
        + "<section class=\"detail\">\n"
        + "<div class=\"authorName\">TEMP_AUTHOR_NAME</div>\n"
        + "<div class=\"presentation\">\n"
        + "<span class=\"mediumTitle\">TEMP_PRESENTATION_TITLE</span>\n"
        + "<span class=\"place\">TEMP_PLACE</span>\n"
        + "<span>TEMP_HELD_YEAR</span>,<span>TEMP_HELD_MONTH</span>\n"
        + "</div>\n"
        + "<div class=\"presentation\">\n"
        + "<span class=\"presenterType\">TEMP_PRESENTER_TYPE</span>・<span class=\"presentationType\">TEMP_PRESENTATION_TYPE</span>・<span class=\"conferenceType\">TEMP_CONFERENCE_TYPE</span>・<span class=\"reviewed\">TEMP_REVIEWED</span>\n"
        + "</div>\n"
        + "</section>\n"
        + "<div class=\"abstract\">TEMP_COMMENT</div>\n"
        + COMMON_FOOTER;
}
