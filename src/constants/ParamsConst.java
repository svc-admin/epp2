package constants;


/**
 * 定数クラス.
 *
 * @author
 *
 */
public class ParamsConst {

    /** パラメータ */
    /** 学生番号 */
    public static final String UID = "uid";
    /** 所属課コード */
    public static final String SHOZOKUCD = "shozokucd";
    /** 入学年度 */
    public static final String NYUGAKU_NENDO = "nyugaku_nendo";
    /** 言語 */
    public static final String LANG = "lang";
    /** num */
    public static final String NUM = "num";
    /** exp */
    public static final String EXP = "exp";
    /** syllabusUri */
    public static final String SYLLABUSURI = "syllabusUri";

//2014.11.21 SVC パラメータの追加 >>
    /** 要件年度 */
    public static final String YOKEN_NENDO = "yoken_nendo";
    /** 大学院区分コード */
    public static final String DAIGAKUINKBNCD = "daigakuinkbncd";
    /** システム番号 */
    public static final String SYSNO = "sysno";
//2014.11.21 SVC パラメータの追加 <<
// 2017.01.19 SVC ADD START >> パラメータ追加
    /** COC/COC+区分 */
    public static final String COCORPLUS = "cocorplus";
    /** 絞り込み条件文字列 */
    public static final String SEARCHCON = "searchcon";
// 2017.01.19 SVC ADD END   <<
// 2019.01.09 s.furuta add start 1.学生番号検索機能追加
    /** 検索区分 */
    public static final String SEARCHTYPE = "searchtype";
    /** 検索学生番号 */
    public static final String SEARCHID = "searchid";
// 2019.01.09 s.furuta add end
// 2019.01.16 s.furuta add start 2.学修成果修正
    /** 科目コード */
    public static final String KAMOKUCD = "kamokucd";
// 2019.01.16 s.furuta add end
// 2019.05.16 s.furuta add start バグ対応（restrictionなし教員での学生抽出時エラーが起きる）
    /** 学修成果取得対象学生番号 */
    public static final String TARGETUIDS = "targetuids";
// 2019.05.16 s.furuta add end
// 2019.11.06 SVC add start 修学支援認定対応
    /** 部局コード */
    public static final String GBUKYOKUCD = "g_bukyokucd";
    public static final String NINTEINENDO = "ninteinendo";
    public static final String HYOGONM = "hyogonm";
    public static final String GP = "gp";
    public static final String GPATAISHO = "gpataisho";
    public static final String HITSUYOTANISU = "hitsuyotanisu";
    public static final String SYUGYONENGEN = "syugyonengen";

// 2019.11.06 SVC add end 修学支援認定対応
// 2019.11.08 SVC add start 修学支援認定対応
    public static final String SHORI_START = "shori_start";
    public static final String SHORI_END = "shori_end";
    public static final String SHORI_NENDO = "shori_nendo";
// 2019.11.08 SVC add end 修学支援認定対応
// 2019.11.24 SVC add start 修学支援判定
    public static final String GAKKIKBNCD = "gakkikbncd";
    public static final String KIJUN_DATE = "kijun_date";
    public static final String GAKUNEN = "gakunen";
    public static final String ETCPARAM = "etcparam";
// 2019.11.24 SVC add end 修学支援判定

// 2020.01.22 SVC add start 修学支援認定対応
    public static final String KAMOKUDKBNCD = "kamokudkbncd";
    public static final String KAMOKUM_SHOZOKUCD = "kamokum_shozokucd";
    public static final String KAMOKUMKBNCD = "kamokumkbncd";
    public static final String KAMOKUS_SHOZOKUCD = "kamokus_shozokucd";
    public static final String KAMOKUSKBNCD = "kamokuskbncd";
    public static final String KAMOKUSS_SHOZOKUCD = "kamokuss_shozokucd";
    public static final String KAMOKUSSKBNCD = "kamokusskbncd";
// 2020.01.22 SVC add end 修学支援認定対応

}
