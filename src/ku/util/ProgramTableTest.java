package ku.util;

public class ProgramTableTest {

    /**
     * Test Program for GpaCalc.java. version 2014-02-09 since 2013-02-09 Copyright 2014
     * nendobetu_shozoku　と nyugaku_nendos　を更新する ('gakuseki'テーブル等が更新されたら更新が必要)
     * Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
     */
    public static void main(String[] args) {

        // 2015-11-17 (2015-06-23は順番が逆！)
        ProgramTable.shozokuSync("hm5317735"); // nyugaku_nendos と nendobetu_shozoku 更新
        ProgramTable.makeTable("hm5317735"); // stored_programetable 更新
        // 2015-06-23
//		ProgramTable.makeTable("hm5317735"); // stored_programetable 更新
//		ProgramTable.shozokuSync("hm5317735"); // nyugaku_nendos と nendobetu_shozoku 更新

//		ProgramTable.getJSON("admin", "ja");
//		ProgramTable.makeTable("admin"); // stored_programetable 更新
//		ProgramTable.shozokuSync("admin"); // nyugaku_nendos と nendobetu_shozoku 更新
    }

}
