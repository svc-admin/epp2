package ku.util;

import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.Logger;

import util.Tools;
import dao.GakusekiDao;
import dao.GoalsDao;
import dao.GpaAvrDao;
import dao.GpaDao;
import dao.KakuteiSeisekiDao;
import dao.NendobetuShozokuDao;
import dao.entity.Gakuseki;
import dao.entity.Goals;
import dao.entity.Gpa;
import dao.entity.KakuteiSeiseki;
import dao.entity.NendobetuShozoku;

/**
 * GPA、GPT、取得単位数の計算を行うクラス。
 * 学生番号uidを指定した場合は、 その学生分と影響がある統計情報を再計算する。
 * 学生番号を指定しない場合は、全ての再計算を行う。 (staticで実装、log記録、gpa, gpa_avr他テーブル使用)
 * , version 2014-02-10 since 2013-01-31 Copyright 2014
 * Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class GpaCalc {
//    static Dao dao = new Dao();
    private static Logger logger = Tools.getCallerLogger();
    private static Restriction rst = new Restriction();

    /**
     * 指定の入学年度の範囲で、所属毎のgpa計算
     * あまりに頻繁にDBとやり取りをすると、mySQLとの通信portが足りなくなるため、subCalcAllを改善すると同時に、
     * 1処理毎に2秒空ける。
     * @param : ruser : remoteUser (熊大ID), startYear/endYear : 入学年度の開始/終了, cdLength : 部局コード長
     * @return : boolean true/false
     */
    static final int SLEEP_MS = 2000;
    public static void calc(String ruser, int startYear, int endYear, int cdLength) {
        if(rst.allow(ruser, ruser, 0, 1)) { // rid = 0, read/write : 一元アクセス管理

            NendobetuShozokuDao nendobetuShozokuDao = new NendobetuShozokuDao();
            ArrayList<NendobetuShozoku> res = nendobetuShozokuDao.getNendobetuShozokuList01(ruser,startYear,endYear,cdLength);

            for(NendobetuShozoku nendobetuShozoku : res) {
                int year = nendobetuShozoku.getNyugakuNendo().intValue();
                String shozokuCd = nendobetuShozoku.getShozokucd();
                sub_calcAll(ruser, shozokuCd, year);
                sub_calcAvr(ruser, shozokuCd, year);
                try {
                    Thread.sleep(SLEEP_MS);
                } catch (InterruptedException e) {
                    logger.error(e);
                }
            }
            logger.info("Calculated gpa and gpa_avr by ruser="+ruser+" at startYear="
                    +startYear+", endYear="+endYear+", cdLength="+cdLength);
        } else {
            logger.info(ruser+" denied for calc gpa,gpa_avr");
        }
    }

    /**
     * 学生分の計算(計算による他の統計情報は更新しない。)
     *
     * 以下で成績は網羅(戻り値が0になる)
     * SELECT *  FROM `kakutei_seiseki` WHERE `HYOGONM` != '合格' and `HYOGONM` != '秀'
     * 	and `HYOGONM` != '優' and `HYOGONM` != '良' and `HYOGONM` != '可' and `HYOGONM` != '不可'
     * 	and `HYOGONM` != '認定' and `HYOGONM` != '不合格' and `HYOGONM` != '☓' and `HYOGONM` != 'Ｘ'
     * 	and `HYOGONM` != 'X' and `HYOGONM` != '良上' and `HYOGONM` != '優上' and `HYOGONM` != 'Ｈ'
     * 	and `HYOGONM` != 'Ｓ' and `HYOGONM` != ''
     *
     * @param : ruser : remoteUser (熊大ID), uid : 学生番号(G_SYSNO, 131-A1234のような形式)
     * @return : boolean true/false
     */
    public static boolean sub_calcStudent(String ruser, String uid) {
        boolean ret = false;
        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());

        if(rst.allow(ruser, ruser, 0, 1)) { // rid = 0, read/write : 一元アクセス管理

            KakuteiSeisekiDao kakuteiSeisekiDao = new KakuteiSeisekiDao();
            ArrayList<KakuteiSeiseki> res = kakuteiSeisekiDao.getKakuteiSeisekiList01(ruser,uid);

            double [] aCalc = {0, 0, 0}; // 途中計算用配列 : 取得単位数, GPA計算用単位数, GPT
            // 全年度合計
            for(KakuteiSeiseki kakuteiSeiseki :res) {
                subGpa(aCalc, kakuteiSeiseki.getTanisu().doubleValue(), kakuteiSeiseki.getHyogonm());
            }

            GpaDao gpaDao = new GpaDao();
            int recCnt = gpaDao.getGpaCount01(ruser,uid);

            if(recCnt == 0) {

                gpaDao.insertGpa01(ruser,uid,aCalc,today);

            } else if(recCnt == 1) {

                gpaDao.updateGpa01(ruser,uid,aCalc,today);

            } else if(recCnt > 1) {
                logger.error(uid+"のgpaで同じデータが2つあります。");
            }
            logger.info("全年度: uid="+uid+", credit="+aCalc[0]+", gpt="+aCalc[2]/aCalc[1]+", gpa="+aCalc[2]+", size="+res.size());

            // 年度・学期毎
            int year_min = 100000, year_max = 0;
            for(KakuteiSeiseki kakuteiSeiseki :res) {
                int y = kakuteiSeiseki.getNinteiNendo();
                if(year_max < y) year_max = y;
                if(year_min > y) year_min = y;
            }
            for(int year = year_min; year <= year_max; year++) {
                for(int sem = 1; sem <= 2; sem++) {
                    aCalc[0]=aCalc[1]=aCalc[2]=0; // 途中計算用配列 : 取得単位数, GPA計算用単位数, GPT 初期化
                    for(KakuteiSeiseki kakuteiSeiseki :res) {
                        if((kakuteiSeiseki.getNinteiNendo().intValue() == year)
                                && (Integer.parseInt(kakuteiSeiseki.getNinteiGakkikbncd()) == sem)) {
                            subGpa(aCalc, kakuteiSeiseki.getTanisu().doubleValue(), kakuteiSeiseki.getHyogonm());
                        }
                    }
                    if(aCalc[2] != 0) {

                        int recCnt2 = gpaDao.getGpaCount02(ruser,uid,year,sem);
                        if(recCnt2 == 0) {

                            gpaDao.insertGpa02(ruser,uid,year,sem,aCalc,today);

                        } else if(recCnt2 == 1) {

                            gpaDao.updateGpa02(ruser,uid,aCalc,today,year,sem);

                        } else if(recCnt2 > 1) {
                            logger.error(uid+"のgpaで同じデータが2つあります(year="+year+",semester="+sem+")。");
                        }
                        logger.info("年度("+year+")・学期("+sem+")毎: uid="+uid+", credit="+aCalc[0]+", gpt="+aCalc[2]/aCalc[1]+", gpa="+aCalc[2]+", size="+res.size());
                    }
                }
            }

            // 学習成果毎
            int[][] goalC = new int[res.size()][8];
            GoalsDao goalsDao = new GoalsDao();
            int cnt = 0;
            for(KakuteiSeiseki kakuteiSeiseki :res) {

                ArrayList<Goals> goalList = goalsDao.getGoalsList01(ruser,kakuteiSeiseki.getJikanwariNendo(),
                        kakuteiSeiseki.getJikanwariShozokucd(),kakuteiSeiseki.getJikanwaricd());

                if(goalList.size() > 0) {
                    Goals goals = goalList.get(0);

                    goalC[cnt][0] = 0;
                    goalC[cnt][1] = goals.getCode1().intValue();
                    goalC[cnt][2] = goals.getCode2().intValue();
                    goalC[cnt][3] = goals.getCode3().intValue();
                    goalC[cnt][4] = goals.getCode4().intValue();
                    goalC[cnt][5] = goals.getCode5().intValue();
                    goalC[cnt][6] = goals.getCode6().intValue();
                    goalC[cnt][7] = goals.getCode7().intValue();

                    if(goalList.size() > 1) {
                        logger.error("goalsの"+kakuteiSeiseki.getJikanwariNendo()+"-"
                                +kakuteiSeiseki.getJikanwariShozokucd()+"-"+kakuteiSeiseki.getJikanwaricd()
                                +"が二重に定義されています。");
                    }

                } else { // goalsにデータがないもの
                    goalC[cnt][0] = 100;
                    for(int j = 0; j < 7; j++) {
                        goalC[cnt][j+1] = 0;
                    }
                }
                cnt++;
            }
            cnt = 0;
            for(int goal = 0; goal <= 7; goal++) {
                aCalc[0]=aCalc[1]=aCalc[2]=0; // 途中計算用配列 : 取得単位数, GPA計算用単位数, GPT 初期化
                for(KakuteiSeiseki kakuteiSeiseki :res) {
                    if(goalC[cnt][goal] != 0) {
                        subGpa(aCalc, ((double)goalC[cnt][goal] / 100.0)	* kakuteiSeiseki.getTanisu().doubleValue(),
                                kakuteiSeiseki.getHyogonm());
                    }
                }
                if(aCalc[2] != 0) {

                    int recCnt3 = gpaDao.getGpaCount03(ruser,uid,goal);

                    if(recCnt3 == 0) {

                        gpaDao.insertGpa03(ruser,uid,goal,aCalc,today);

                    } else if(recCnt3 == 1) {

                        gpaDao.updateGpa03(ruser,uid,aCalc,today,goal);

                    } else if(recCnt3 > 1) {
                        logger.error(uid+"のgpaで同じデータが2つあります(goal="+goal+")。");
                    }
                    logger.info("学習成果("+goal+")毎: uid="+uid+", credit="+aCalc[0]+", gpt="+aCalc[2]/aCalc[1]
                            +", gpa="+aCalc[2]+", size="+res.size());
                }
                cnt++;
            }

            ret = true;
            logger.info("ret="+ret+", ruser="+ruser+", uid="+uid+", size="+res.size());
        } else {
            logger.info("ret="+ret+", ruser="+ruser+", uid="+uid);
        }
        return ret;
    }

    /**
     * グループの学生について全てsub_calcStudent(String ruser, String uid)を計算
     * あまりに頻繁にDBとやり取りをすると、mySQLとの通信portが足りなくなるため、sGに一度聞いた科目の学習成果を保存すると同時に、
     * dao.insertAll(ruser, sqls)でcommitを1000個単位で行う。
     *
     * @param : ruser : remoteUser (熊大ID),
     * 					 	shozoku : 所属部局コード(0513か051しか対応できないであろう。051310とは書かれていない。)
     * 						nyugakuNendo : 入学年度(2007とか)
     * @return : boolean true/false

     */
    public static void sub_calcAll(String ruser, String shozoku, int nyugakuNendo) {

        if(rst.allow(ruser, ruser, 0, 1)) { // rid = 0, read/write : 一元アクセス管理
            LinkedHashMap<String, int []> sG = new LinkedHashMap<String, int []>();

            GakusekiDao gakusekiDao = new GakusekiDao();
            ArrayList<Gakuseki> uidS = gakusekiDao.getGakusekiList05(ruser,shozoku,nyugakuNendo);

            GpaDao gpaDao = new GpaDao();


            if(uidS.size() > 0) {
                int writeN = 0;
                List<List<Object>> paramList = new ArrayList<List<Object>>();

                for(Gakuseki gakuseki : uidS) {
                    List<Object> valueList = new ArrayList<Object>();
                    valueList.add(gakuseki.getgSysno());
                    paramList.add(valueList);
                }
                writeN = gpaDao.deleteGpa01(ruser, paramList);
                logger.info(writeN + "名分消去 at 入学年度="+nyugakuNendo);

                java.sql.Date today = new java.sql.Date(System.currentTimeMillis());

                KakuteiSeisekiDao kakuteiSeisekiDao = new KakuteiSeisekiDao();

                for(Gakuseki gakuseki : uidS) {

                    ArrayList<KakuteiSeiseki> res = kakuteiSeisekiDao.getKakuteiSeisekiList01(ruser,gakuseki.getgSysno());

                    double [] aCalc = {0, 0, 0}; // 途中計算用配列 : 取得単位数, GPA計算用単位数, GPT

                    // 全年度合計
                    for(KakuteiSeiseki kakuteiSeiseki :res) {
                        subGpa(aCalc, kakuteiSeiseki.getTanisu().doubleValue(), kakuteiSeiseki.getHyogonm());
                    }
                    gpaDao.insertGpa01(ruser,gakuseki.getgSysno(),aCalc,today);

                    // 年度・学期毎
                    int year_min = 100000, year_max = 0;
                    for(KakuteiSeiseki kakuteiSeiseki :res) {
                        int y = kakuteiSeiseki.getNinteiNendo().intValue();
                        if(year_max < y) year_max = y;
                        if(year_min > y) year_min = y;
                    }
                    for(int year = year_min; year <= year_max; year++) {
                        for(int sem = 1; sem <= 2; sem++) {
                            aCalc[0]=aCalc[1]=aCalc[2]=0; // 途中計算用配列 : 取得単位数, GPA計算用単位数, GPT 初期化
                            for(KakuteiSeiseki kakuteiSeiseki :res) {
                                if(kakuteiSeiseki.getNinteiNendo() != null) {
//							    logger.info(">>>>> DEBUG:kakuteiSeiseki.getNinteiGakkikbncd()="+kakuteiSeiseki.getNinteiGakkikbncd());
//									int sD = kakuteiSeiseki.getNinteiGakkikbncd() == "" ? 0 : Integer.parseInt(kakuteiSeiseki.getNinteiGakkikbncd());
                                    int sD = 0;
                                    if(! kakuteiSeiseki.getNinteiGakkikbncd().equals("")) sD = Integer.parseInt(kakuteiSeiseki.getNinteiGakkikbncd());
                                    if((kakuteiSeiseki.getNinteiNendo().intValue() == year) && (sD == sem)) {
                                        subGpa(aCalc, kakuteiSeiseki.getTanisu().doubleValue(), kakuteiSeiseki.getHyogonm());
                                    }
                                }
                            }
                            if(aCalc[2] != 0) {
                                gpaDao.insertGpa02(ruser,gakuseki.getgSysno(),year,sem,aCalc,today);
                            }
                        }
                    }

                    // 学習成果毎
                    int[][] goalC = new int[res.size()][8];
                    int cnt = 0;
                    GoalsDao goalsDao = new GoalsDao();
                    for(KakuteiSeiseki kakuteiSeiseki :res) {
                        String key=kakuteiSeiseki.getJikanwariNendo()
                                +"-"+kakuteiSeiseki.getJikanwariShozokucd()+"-"+kakuteiSeiseki.getJikanwaricd();
                        if(sG.containsKey(key)) {
                            goalC[cnt] = sG.get(key);
                        } else {
                            ArrayList<Goals> goalList = goalsDao.getGoalsList01(ruser,kakuteiSeiseki.getJikanwariNendo(),
                                    kakuteiSeiseki.getJikanwariShozokucd(),kakuteiSeiseki.getJikanwaricd());
                            if(goalList.size() > 0) {
                                Goals goals = goalList.get(0);

                                goalC[cnt][0] = 0;
                                goalC[cnt][1] = goals.getCode1().intValue();
                                goalC[cnt][2] = goals.getCode2().intValue();
                                goalC[cnt][3] = goals.getCode3().intValue();
                                goalC[cnt][4] = goals.getCode4().intValue();
                                goalC[cnt][5] = goals.getCode5().intValue();
                                goalC[cnt][6] = goals.getCode6().intValue();
                                goalC[cnt][7] = goals.getCode7().intValue();
                                if(goalList.size() > 1) {
                                    logger.error("goalsの"+kakuteiSeiseki.getJikanwariNendo()
                                            +"-"+kakuteiSeiseki.getJikanwariShozokucd()
                                            +"-"+kakuteiSeiseki.getJikanwaricd()+"が二重に定義されています。");
                                }
                            } else { // goalsにデータがないもの
                                goalC[cnt][0] = 100;
                                for(int j = 0; j < 7; j++) {
                                    goalC[cnt][j+1] = 0;
                                }
                            }
                            sG.put(key, goalC[cnt]);
                        }
                        cnt++;
                    }
                    for(int goal = 0; goal <= 7; goal++) {
                        cnt = 0;
                        aCalc[0]=aCalc[1]=aCalc[2]=0; // 途中計算用配列 : 取得単位数, GPA計算用単位数, GPT 初期化
                        for(KakuteiSeiseki kakuteiSeiseki :res) {
                            if(cnt != 0) {
                                if(goalC[cnt][goal] != 0) {
                                    subGpa(aCalc, ((double)goalC[cnt][goal] / 100.0)	* kakuteiSeiseki.getTanisu().doubleValue(),
                                        kakuteiSeiseki.getHyogonm());
                                }
                            }
                            cnt++;
                        }
                        if(aCalc[2] != 0) {
//						    gpaDao.updateGpa03(ruser,gakuseki.getgSysno(),aCalc,today,goal);
                            gpaDao.insertGpa03(ruser, gakuseki.getgSysno(), goal, aCalc, today);
                            logger.info(">>>>> DEBUG:"+ruser+", "+gakuseki.getgSysno()+", "+goal+", "+aCalc[0]+"/"+aCalc[1]+"/"+aCalc[2]+", "+today);
                            //	元 insertGpa03(String ruser, String uid, int goal, double[] aCalc, Date today)
                        }
                    }
                }
            }
            logger.info("Accepted : ruser="+ruser+", shozoku="+shozoku+", nyugakuNendo="+nyugakuNendo);
        } else {
            logger.info("Denied : ruser="+ruser+", shozoku="+shozoku+", nyugakuNendo="+nyugakuNendo);
        }
    }

    /**
     * グループの学生についての平均を計算 (sub_calcStudent(String ruser, String uid)を人数分足して人数で割る)
     *
     * 以下がものすごく遅いので、この方法にした
     * SELECT * from kakutei_seiseki where G_SYSNO in (SELECT G_SYSNO FROM `gakuseki` where G_SHOZOKUCD like '0513%' and NYUGAKU_NENDO='2007')
     *
     * select distinct NINTEI_NENDO,semester FROM `gpa` where goal=-1 order by NINTEI_NENDO asc, semester asc
     *
     * @param : ruser : remoteUser (熊大ID),
     * 					 	shozoku : 所属部局コード(0513か051しか対応できないであろう。051310とは書かれていない。)
     * 						nyugakuNendo : 入学年度(2007とか)
     * @return : boolean true/false

     */
    public static boolean sub_calcAvr(String ruser, String shozoku, int nyugakuNendo) {
        boolean ret = false;
        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
        if(rst.allow(ruser, ruser, 0, 1)) { // rid = 0, read/write : 一元アクセス管理
            GakusekiDao gakusekiDao = new GakusekiDao();
            ArrayList<Gakuseki> uidS = gakusekiDao.getGakusekiList05(ruser,shozoku,nyugakuNendo);
            double users = (double)uidS.size();

            if(uidS.size() > 0) {
                // 必要な全データ取得
                ArrayList<Gpa> gpaAllList = new ArrayList<Gpa>();

                for(Gakuseki gakuseki :uidS) {

                    GpaDao gpaDao = new GpaDao();
                    ArrayList<Gpa> gpaS = gpaDao.getGpaList06(ruser,gakuseki.getgSysno());

                    gpaAllList.addAll(gpaS);

                }
                // 全平均 : 学習成果、年度、学期　(-1,-1,-1)
                int allN = 0; double allGpa = 0, allCredit = 0, allGpt = 0;
                for(Gpa gpa : gpaAllList) {

                    if((gpa.getGoal().intValue() == -1)
                            && (gpa.getNinteiNendo().intValue() == -1)
                            && (gpa.getSemester().intValue() == -1)) {
                        allN += 1;
                        allGpa += gpa.getGpa().doubleValue();
                        allCredit += gpa.getCredit().doubleValue();
                        allGpt += gpa.getGpt().doubleValue();
                    }
                }

                GpaAvrDao gpaAvrDao = new GpaAvrDao();
                if(allN > 0) {
                    // 修正:学生数で割るべき！ 2014-02-05

                    allGpa /= (double)users;
                    allCredit /= (double)users;
                    allGpt /= (double)users;
                    // データがgpa_avrに存在するか
                    int recCnt1 = gpaAvrDao.getGpaAvrCount01(ruser,nyugakuNendo,shozoku);
                    if(recCnt1 == 0) {

                        gpaAvrDao.insertGpaAvr01(ruser,nyugakuNendo,shozoku,allGpa,allCredit,allGpt,users,today);

                    } else if(recCnt1 == 1) {

                        gpaAvrDao.updateGpaAvr01(ruser,nyugakuNendo,shozoku,allGpa,allCredit,allGpt,users,today);

                    } else if(recCnt1 > 1) {
                        logger.error("gpa_avrで同じデータが2つあります(NYUGAKU_NENDO="+nyugakuNendo+" and SHOZOKUCD='"+shozoku
                                +"' and NINTEI_NENDO=-1 and semester=-1 and goal=-1)。");
                    }
                    logger.info("年度、所属毎("+users+"の学生のうち"+allN+"名)の平均のGPA、取得単位数、GPT(NYUGAKU_NENDO="+nyugakuNendo
                            +" and SHOZOKUCD='"+shozoku+"')。");
                }
                // 学習成果別平均 : 学習成果、年度、学期　(from 0 to 7,-1,-1)
                for(int g = 0; g <= 7; g++) {
                    int goalN = 0; double goalGpa = 0, goalCredit = 0, goalGpt = 0;
                    for(Gpa gpa : gpaAllList) {
                        if((gpa.getGoal().intValue() == g)
                                && (gpa.getNinteiNendo().intValue() == -1)
                                && (gpa.getSemester().intValue() == -1)) {
                            goalN += 1;
                            goalGpa += gpa.getGpa().doubleValue();
                            goalCredit += gpa.getCredit().doubleValue();
                            goalGpt += gpa.getGpt().doubleValue();
                        }
                    }
                    if(goalN > 0) {
                        // 修正:学生数で割るべき！ 2014-02-05
                        goalGpa /= (double)users;
                        goalCredit /= (double)users;
                        goalGpt /= (double)users;
                        // データがgpa_avrに存在するか
                        int recCnt2 = gpaAvrDao.getGpaAvrCount02(ruser,nyugakuNendo,shozoku,g);
                        if(recCnt2 == 0) {

                            gpaAvrDao.insertGpaAvr02(ruser,nyugakuNendo,shozoku,g,goalGpa,goalCredit,goalGpt,users,today);

                        } else if(recCnt2 == 1) {

                            gpaAvrDao.updateGpaAvr02(ruser,nyugakuNendo,shozoku,goalGpa,goalCredit,goalGpt,users,today,g);

                        } else if(recCnt2 > 1) {
                            logger.error("gpa_avrで同じデータが2つあります(NYUGAKU_NENDO="+nyugakuNendo+" and SHOZOKUCD='"+shozoku
                                    +"' and NINTEI_NENDO=-1 and semester=-1 and goal="+g+")。");
                        }
                        logger.info("学習成果("+g+")に関する、年度、所属毎("+users+"の学生のうち"+goalN
                                +"名)の平均のGPA、取得単位数、GPT(NYUGAKU_NENDO="+nyugakuNendo+" and SHOZOKUCD='"+shozoku+"')。");
                    }
                }
                // 年度、学期別平均 : 学習成果、年度、学期　(-1,例:from 2007 to 2012,from -1 to 2)
                int year_min = 100000, year_max = 0;
                for(Gpa gpa : gpaAllList) {
                    int year = gpa.getNinteiNendo().intValue();
                    if(year_max < year) year_max = year;
                    if(year_min > year) year_min = year;
                }
                int sem_min = 100000, sem_max = 0;
                for(Gpa gpa : gpaAllList) {
                    int sem = gpa.getSemester().intValue();
                    if(sem_max < sem) sem_max = sem;
                    if(sem_min > sem) sem_min = sem;
                }
                for(int y = year_min; y <= year_max; y++) {
                    for(int s = sem_min; s <= sem_max; s++) {
                        int yearN = 0; double yearGpa = 0, yearCredit = 0, yearGpt = 0;
                        for(Gpa gpa : gpaAllList) {
                            if((gpa.getGoal().intValue() == -1)
                                    && (gpa.getNinteiNendo().intValue() == y)
                                    && (gpa.getSemester().intValue() == s)) {
                                yearN += 1;
                                yearGpa += gpa.getGpa().doubleValue();
                                yearCredit += gpa.getCredit().doubleValue();
                                yearGpt += gpa.getGpt().doubleValue();
                            }
                        }
                        if(yearN > 0) {
                            // 修正:学生数で割るべき！ 2014-02-05
                            yearGpa /= (double)users;
                            yearCredit /= (double)users;
                            yearGpt /= (double)users;
                            // データがgpa_avrに存在するか
                            int recCnt3 = gpaAvrDao.getGpaAvrCount03(ruser,nyugakuNendo,shozoku,y,s);
                            if(recCnt3 == 0) {

                                gpaAvrDao.insertGpaAvr03(ruser,nyugakuNendo,shozoku,y,s,yearGpa,yearCredit,yearGpt,users,today);

                            } else if(recCnt3 == 1) {

                                gpaAvrDao.updateGpaAvr03(ruser,nyugakuNendo,shozoku,yearGpa,yearCredit,yearGpt,users,today,y,s);

                            } else if(recCnt3 > 1) {
                                logger.error("gpa_avrで同じデータが2つあります(NYUGAKU_NENDO="+nyugakuNendo+" and SHOZOKUCD='"+shozoku
                                        +"' and NINTEI_NENDO="+y+" and semester="+s+" and goal=-1)。");
                            }
                            logger.info("年度("+y+")、学期("+s+")に関する、入学年度、所属毎("+users+"の学生のうち"+yearN
                                    +"名)の平均のGPA、取得単位数、GPT(NYUGAKU_NENDO="+nyugakuNendo+" and SHOZOKUCD='"+shozoku+"')。");
                        }
                    }
                }

            }
            ret = true;
            logger.info("ret="+ret+", ruser="+ruser+", shozoku="+shozoku+", nyugakuNendo="+nyugakuNendo);
        } else {
            logger.info("No data : ret="+ret+", ruser="+ruser+", shozoku="+shozoku+", nyugakuNendo="+nyugakuNendo);
        }
        return ret;
    }


    static private void subGpa(double[] ret, double credit, String score) {
        double gG = 0, gC = credit;
        if(score.equals("秀"))	{
            gG = 4;
        } else if(score.equals("優") || score.equals("優上")) {
            gG = 3;
        } else if(score.equals("良") || score.equals("良上")) {
            gG = 2;
        } else if(score.equals("可")) {
            gG = 1;
        } else if(score.equals("合格") || score.equals("認定")) {
            gG = 2.5;
        } else if(score.equals("不可")) {
            gG = 0;
        } else {
            gC = 0;
        }
        if(gG > 0) ret[0] += gC; // 取得単位数
        if(gC > 0) ret[1] += gC; // GPA用単位数
        ret[2] += gG * gC;
        logger.debug("成績="+score+", gG="+gG+", gC="+gC+", credit="+credit+", gpt="+ret[2]);
    }

}
