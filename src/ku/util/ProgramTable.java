package ku.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import net.arnx.jsonic.JSON;

import org.apache.log4j.Logger;

import util.Tools;
import constants.CommonConst;
import dao.Dao;
import dto.ParameterDto;

/**
 * 2018-10-22 .replaceAll("\\'", "\\\\'") を追加(10箇所) 自然科学教育部の英語に"Master's Course"が出現したためエスケープ!
 * 2015-06-23 NYUGAKU_NENDOS を nyugaku_nendos に変更
 * Unified restriction for all classes, version 2014-02-09 since 2014-01-30(ProgramsService) Copyright 2014
 * Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class ProgramTable {
    private static Logger logger = Tools.getCallerLogger();
    static private Map<String, String> prepared = new HashMap<String, String>();
    static final Map<String, Map<String, String>> graduateName = // DBの構造に入っていないのでしかたなく
        new HashMap<String, Map<String, String>>() {{
            put(CommonConst.JAPANESE, new HashMap<String, String>() {{
                put("0", "学部");
                put("1", "大学院");
            }});
            put(CommonConst.ENGLISH, new HashMap<String, String>() {{
                put("0", "undergraduate");
                put("1", "graduate");
            }});
    }};

    public ProgramTable() {
        this("admin");
    }

    public ProgramTable(String ruser) {
        if(prepared.size() == 0) {
            loadJSON(ruser);
        }
    }

    /**
     * DBからJSON形式のテキストを読み込む
     * @param params : ユーザID, 言語("ja","en")
     * @return : JSON文字列
     */
  private static void loadJSON(String ruser) {
        Restriction rst = new Restriction();
        if(rst.allow(ruser, ruser, 0, 0)) { // rid = 0, read only
            if(prepared.size() == 0) {
                Dao d = new Dao();
                ArrayList<Object> dTbl = d.query2(ruser,
                        "select distinct JSON,LOCALE from stored_programetable order by UPDATE_DATE desc");
                for(int i = 0; i < dTbl.size(); i++) {
                    String json = ((ArrayList<Object>)(dTbl.get(i))).get(0).toString();
                    String lang = ((ArrayList<Object>)(dTbl.get(i))).get(1).toString();
                    prepared.put(lang, json);
                }
                logger.info("ruser="+ruser + " set prepared="+prepared.toString());
            } else {
                logger.info("ruser="+ruser + " request set prepared, but it has already set");
            }
        } else {
            logger.info("Refused ruser="+ruser);
        }
    }

     /** JSONテキストを返す
     * @param params : ユーザID, 言語("ja","en")
     * @return : JSON文字列
     */
 public String getJSON(ParameterDto pramDto) {
     String ret = null;
    if(prepared.size() == 0) loadJSON(pramDto.getRuser());
    ret = prepared.get(pramDto.getLang());

    return ret;

 }

    /**
  * (学生が実際に存在する)年度別の学部、学科、コースをJSON形式で作成しDBに登録する。
     * @param params : ユーザID
<pre>
{"2012":{
 "graduate":{
   "0":{
     "name":"学部","school":{
         "05":{
         "name":"文学部","department":{
             "0510":{
             "name":"総合人間学科"},
           "0511":{"name":"歴史学科"},
           "0512":{"name":"文学科","division":{
             "051230":{"name":"超域言語文学コース"}}},
           "0513":{"name":"コミュニケーション情報学科"}}},{
        "07":{"name":"教育学部","department":{
           "0701":{"name":"小学校教員養成課程"},...}}},{
         .....
        "44":{"name":"薬学部","department":{
           "4404":{"name":"薬学科"},"4405":{"name":"創薬・生命薬科学科"}}}}},
   "1":{
     "name":"大学院","school":{
       "08":{"name":"教育学研究科","department":{
         "0820":{"name":"修士課程","division":{
           "082010":{"name":"学校教育実践専攻"},"082020":{"name":"教科教育実践専攻"}}}}},
       .....
       "70":{"name":"法曹養成研究科","department":{
         "7001":{"name":"法科大学院の課程","division":{
           "700101":{"name":"３年標準コース"},.....}}}}},
 "2011":{...
</pre>
*/

    public static void makeTable(String ruser) {
//		LinkedHashMap<String,LinkedHashMap<Integer,Object>> list
//		= new LinkedHashMap<String,LinkedHashMap<Integer,Object>>();
        Restriction rst = new Restriction();
        if(rst.allow(ruser, ruser, 0, 0)) { // rid = 0, read only
            java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
            String[] langs = {CommonConst.JAPANESE, CommonConst.ENGLISH};
            Dao d = new Dao();
            for(String lang : langs) {
                LinkedHashMap<Integer,Object> yMap = new LinkedHashMap<Integer,Object>();
                ArrayList<Object> dYear = d.query2(ruser,
                        "select distinct NYUGAKU_NENDO from nendobetu_shozoku order by NYUGAKU_NENDO desc");
                for(Object year : dYear) {
                    LinkedHashMap<String,Object> cYear = new LinkedHashMap<String,Object>();
                    int aYear = Integer.parseInt(((ArrayList<Object>)year).get(0).toString()); // 年の値→key
                    ArrayList<Object> dGraduate = d.query2(ruser,
                            "select distinct DAIGAKUINKBNCD from nendobetu_shozoku where NYUGAKU_NENDO="
                                    + aYear +" order by DAIGAKUINKBNCD asc");
                    if(dGraduate.size() > 0) {
                        LinkedHashMap<String,Object> graduate = new LinkedHashMap<String,Object>();
                        for(Object aGraduate : dGraduate) {
                            LinkedHashMap<String,Object> cGraduate = new LinkedHashMap<String,Object>();
                            ArrayList<String> alGraduate = (ArrayList<String>)aGraduate;
    //						cGraduate.put("name", alGraduate.get(1));
                            cGraduate.put("name", graduateName.get(lang).get(alGraduate.get(0)));
                            graduate.put(alGraduate.get(0),cGraduate);
                            ArrayList<Object> dSchool = d.query2(ruser,
                                    "select SHOZOKUCD,"+(lang.equals(CommonConst.JAPANESE)?"SHOZOKUNM":"SHOZOKUNMENG")
                                    +" from nendobetu_shozoku where NYUGAKU_NENDO="+ aYear +" and DAIGAKUINKBNCD="
                                    +alGraduate.get(0)+" and KAISOFLG=1 order by SHOZOKUCD asc");
                            if(dSchool.size() > 0) {
                                LinkedHashMap<String,Object> school = new LinkedHashMap<String,Object>();
                                for(Object aSchool : dSchool) {
                                    LinkedHashMap<String,Object> cSchool = new LinkedHashMap<String,Object>();
                                    ArrayList<String> alSchool = (ArrayList<String>)aSchool;
                                    cSchool.put("name", alSchool.get(1));
                                    school.put(alSchool.get(0),cSchool);
                                    ArrayList<Object> dDepartment = d.query2(ruser,
                                        "select SHOZOKUCD,"+(lang.equals(CommonConst.JAPANESE)?"SHOZOKUNM":"SHOZOKUNMENG")
                                        +" from nendobetu_shozoku where NYUGAKU_NENDO="+ aYear +" and DAIGAKUINKBNCD="
                                        +alGraduate.get(0)+" and KAISOFLG=2 and OYA_SHOZOKUCD="+alSchool.get(0)+" order by SHOZOKUCD asc");
                                    if(dDepartment.size() > 0) {
                                        LinkedHashMap<String,Object> department = new LinkedHashMap<String,Object>();
                                        for(Object aDepartment : dDepartment) {
                                            LinkedHashMap<String,Object> cDepartment = new LinkedHashMap<String,Object>();
                                            ArrayList<String> alDepartment = (ArrayList<String>)aDepartment;
                                            cDepartment.put("name", alDepartment.get(1));
                                            department.put(alDepartment.get(0), cDepartment);
                                            ArrayList<Object> dDivision = d.query2(ruser,
                                                "select SHOZOKUCD,"+(lang.equals(CommonConst.JAPANESE)?"SHOZOKUNM":"SHOZOKUNMENG")
                                                +" from nendobetu_shozoku where NYUGAKU_NENDO="+ aYear +" and DAIGAKUINKBNCD="
                                                +alGraduate.get(0)+" and KAISOFLG=3 and OYA_SHOZOKUCD="+alDepartment.get(0)+" order by SHOZOKUCD asc");
                                            if(dDivision.size() > 0) {
                                                LinkedHashMap<String,Object> division = new LinkedHashMap<String,Object>();
                                                for(Object aDivision : dDivision) {
                                                    LinkedHashMap<String,Object> cDivision = new LinkedHashMap<String,Object>();
                                                    ArrayList<String> alDivision = (ArrayList<String>)aDivision;
                                                    cDivision.put("name", alDivision.get(1));
                                                    division.put(alDivision.get(0), cDivision);
                                                }
                                                cDepartment.put("division", division);
                                            }
                                        }
                                        cSchool.put("department", department);
                                    }
                                }
                                cGraduate.put("school", school);
                            }
                        }
                        cYear.put("graduate", graduate);
                    }
                    yMap.put(aYear, cYear);
                }
                ArrayList<Object> chk = d.query2( ruser, "select * from stored_programetable where LOCALE='"+lang+"'");
                if(chk.size() == 0) {
                    d.insert(ruser, "insert into stored_programetable values ('"+JSON.encode(yMap).replaceAll("\\'", "\\\\'")
                            +"','"+lang+"','"+today+"','"+today+"','"+ruser+"')");
                } else if(chk.size() == 1) {
                    d.insert(ruser, "update stored_programetable set JSON='"+JSON.encode(yMap).replaceAll("\\'", "\\\\'")+"',LOCALE='"+lang
                            +"',INSERT_DATE='"+today+"',UPDATE_DATE='"+today+"',USERID='"+ruser+"' where LOCALE='"+lang+"'");
                } else if(chk.size() > 1) {
                    logger.error(lang+"のstored_programetableで同じデータが2つあります。");
                }
                logger.info("ruser="+ruser+" (re)build table at chk.size()="+chk.size()
                        +", lang="+lang+", JSON String="+JSON.encode(yMap));
            }
        } else {
            logger.info("Refused ruser="+ruser);
        }
    }

	/**
	 * 入学年度毎に、実際に学生のいる所属をリストしNENDOBETU_SHOZOKUに格納する
	 * ついでに、学生のいる入学年度のリストをnyugaku_nendosに格納する （毎回アクセスの時行うには時間がかかりすぎるし、変更も極めて少ない）
	 */
	public static void shozokuSync(String ruser) {
		Restriction rst = new Restriction();
		if (rst.allow(ruser, ruser, 0, 0)) { // rid = 0, read only
			java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
			String[] langs = { CommonConst.JAPANESE, CommonConst.ENGLISH };
			Dao d = new Dao();
			ArrayList<Integer> nendoList = new ArrayList<Integer>();
			HashMap<String, ArrayList<Object>> shozokuAll = new HashMap<String, ArrayList<Object>>();
			///// read from NYUGAKU_NENDO
			ArrayList<Object> res = d.query2(ruser,
					"SELECT DISTINCT NYUGAKU_NENDO FROM gakuseki order by NYUGAKU_NENDO DESC");
			for (int i = 0; i < res.size(); i++) {
				nendoList.add(((BigDecimal) (((ArrayList<Object>) (res.get(i))).get(0))).intValue());
//				logger.debug(i + "," + nendoList.get(i));
			}
			if (nendoList.size() > 0) {
//				d.insert( ruser, "delete from NYUGAKU_NENDOS");
				d.insert(ruser, "delete from nyugaku_nendos");
				for (int nendo : nendoList) {
//					d.insert( ruser, "insert into NYUGAKU_NENDOS values ("+nendo+")");
					d.insert(ruser, "insert into nyugaku_nendos values (" + nendo + ")");
				}
				///// read all data from SHOZOKU_MST
//				logger.info("shozokuSync(): start all data from SHOZOKU_MST");
//				ArrayList<Object> res2 = d.query2( ruser, "SELECT * FROM SHOZOKU_MST");
				ArrayList<Object> res2 = d.query2(ruser, "SELECT * FROM shozoku_mst");
				for (Object row : res2) {
					shozokuAll.put((((ArrayList<Object>) row).get(0)).toString(), (ArrayList<Object>) row);
				}
				///// read from SHOZOKU_MST and write to NENDOBETU_SHOZOKU
//				logger.info("shozokuSync(): start reading from SHOZOKU_MST and writing to NENDOBETU_SHOZOKU");
				if (shozokuAll.size() > 0) {
//					d.insert( ruser, "delete from NENDOBETU_SHOZOKU");
					d.insert(ruser, "delete from nendobetu_shozoku");
					for (int nendo : nendoList) {
//						logger.info("Starting query nendo="+nendo);
						ArrayList<Object> res3 = d.query2(ruser,
								"SELECT DISTINCT G_SHOZOKUCD FROM gakuseki where NYUGAKU_NENDO=" + nendo
										+ " order by G_SHOZOKUCD");
						ArrayList<String> scds = new ArrayList<String>();
						for (Object row : res3) {
							scds.add((((ArrayList<Object>) row).get(0)).toString());
						}
						for (String scd : scds) {
							String oya = null;
							int kaiso = ((BigDecimal) (shozokuAll.get(scd).get(22))).intValue();
							if (kaiso >= 1 && kaiso <= 5) {
								if (kaiso == 1) {
									oya = (String) (shozokuAll.get(scd).get(30)); // DAIGAKUINKBNCD
								} else if (kaiso <= 5) {
									oya = scd.substring(0, 2 * (kaiso - 1));
								}
								d.insert(ruser,
										"insert into NENDOBETU_SHOZOKU values (" + nendo + ",'" + scd + "','" + oya
												+ "'," + kaiso + ",'" + shozokuAll.get(scd).get(30) + "','"
												+ ((String)(shozokuAll.get(scd).get(1 + 4 * (kaiso - 1)))).replaceAll("\\'", "\\\\'") + "','"
												+ ((String)(shozokuAll.get(scd).get(2 + 4 * (kaiso - 1)))).replaceAll("\\'", "\\\\'") + "','"
												+ ((String)(shozokuAll.get(scd).get(3 + 4 * (kaiso - 1)))).replaceAll("\\'", "\\\\'") + "','"
												+ ((String)(shozokuAll.get(scd).get(4 + 4 * (kaiso - 1)))).replaceAll("\\'", "\\\\'") + "')");
							} else {
								logger.error("kaiso = " + kaiso + " is not allowd (1-5).");
							}
						}
//						logger.info("start correcting SHOZOKU_MST : solve no parents at nendo="+nendo);
						ArrayList<Object> res4 = d.query2(ruser,
//								"SELECT MAX(KAISOFLG) FROM NENDOBETU_SHOZOKU");
								"SELECT MAX(KAISOFLG) FROM nendobetu_shozoku");
						int kaisoMax = 1;
						for (Object row : res4) {
							kaisoMax = ((BigDecimal) ((((ArrayList<Object>) row).get(0)))).intValue();
//							logger.debug("kaisoMax = "+kaisoMax);
						}
						for (int i = kaisoMax; i > 1; i--) {
							ArrayList<Object> res5 = d.query2(ruser,
//									"SELECT DISTINCT OYA_SHOZOKUCD FROM NENDOBETU_SHOZOKU "
									"SELECT DISTINCT OYA_SHOZOKUCD FROM nendobetu_shozoku " + "where NYUGAKU_NENDO="
											+ nendo + " AND KAISOFLG=" + i + " AND OYA_SHOZOKUCD "
//											+"not in (SELECT DISTINCT SHOZOKUCD FROM NENDOBETU_SHOZOKU where KAISOFLG="
											+ "not in (SELECT DISTINCT SHOZOKUCD FROM nendobetu_shozoku where KAISOFLG="
											+ (i - 1) + " AND NYUGAKU_NENDO=" + nendo + ")");
							ArrayList<String> al = new ArrayList<String>();
							for (Object row : res5) {
//								logger.debug(row.toString());
								for (Object o : (ArrayList<Object>) row) {
									al.add((String) (o));
								}
							}
							for (String s : al) {
								String oya = s.substring(0, 2 * (i - 2));
								if (i == 2) {
									oya = (String) (shozokuAll.get(s).get(30));
								}
								d.insert(ruser,
										"insert into NENDOBETU_SHOZOKU values (" + nendo + ",'" + s + "','" + oya + "',"
												+ (i - 1) + ",'" + shozokuAll.get(s).get(30) + "','"
												+ ((String)(shozokuAll.get(s).get(1 + 4 * (i - 2)))).replaceAll("\\'", "\\\\'") + "','"
												+ ((String)(shozokuAll.get(s).get(2 + 4 * (i - 2)))).replaceAll("\\'", "\\\\'") + "','"
												+ ((String)(shozokuAll.get(s).get(3 + 4 * (i - 2)))).replaceAll("\\'", "\\\\'") + "','"
												+ ((String)(shozokuAll.get(s).get(4 + 4 * (i - 2)))).replaceAll("\\'", "\\\\'") + "')");
							}
						}
					}
//					logger.info("shozokuSync(): finish reading from SHOZOKU_MST and writing to NENDOBETU_SHOZOKU");
					logger.info("shozokuSync(): finish reading from shozoku_mst and writing to nendobetu_shozoku");
				}
			}
		}
	}

}
