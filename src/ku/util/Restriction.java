package ku.util;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import util.Tools;
import dao.AuthorityDao;
import dao.GakusekiDao;
import dao.KyokanMstDao;
import dao.NendobetuShozokuDao;
import dao.RestrictionDao;
import dao.RestrictionGroupDao;
import dao.UsersDao;
import dao.entity.Authority;
import dao.entity.Gakuseki;
import dao.entity.KyokanMst;
import dao.entity.NendobetuShozoku;
import dao.entity.RestrictionGroup;
import dto.ParameterDto;

/**
 * Unified restriction for all classes, version 2014-01-28 since 2013-01-28
 * Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class Restriction {
    private static Logger logger = Tools.getCallerLogger();


    /**
     * アクセス権の問い合わせ
     *
     * @param : ruser : remoteUser (熊大ID), kid : 対象ユーザ (熊大ID), rid : 権限ID, mode :
     *        0/読 1/読書
     * @return : boolean
     */
    public boolean allow(String ruser, String kid, int rid, int mode) {

        return this.getRid(ruser, kid, rid, mode) >= rid;
    }

    /**
     * アクセス権の問い合わせ
     *
     * @param : ruser : remoteUser (熊大ID), kid : 対象ユーザ (熊大ID), rid : 権限ID, mode :
     *        0/読 1/読書
     * @return : int 対象ユーザーのrid
     */
    private int getRid(String ruser, String kid, int rid, int mode) {
        int ret = -1;

        RestrictionDao restrictionDao = new RestrictionDao();
        ArrayList<dao.entity.Restriction> res1 = restrictionDao
            .getRestrictionList01(ruser);
//		logger.info("res1.size()=" + res1.size());
        UsersDao usersDao = new UsersDao();
        ArrayList<dao.entity.Users> use1 = usersDao.getUsersList01(ruser);
//		for(int i=0; i < use1.size(); i++) logger.info("==B== use1.get(i).getUid()="+use1.get(i).getUid());

        if (res1.size() != 0) {

            RestrictionGroupDao restrictionGroupDao = new RestrictionGroupDao();

        // kid -> uid 変換 20151013 by nakano
            ArrayList<String> uids = new ArrayList<String>();
            for(dao.entity.Users users : use1) {
                if(kid.equals(users.getKid())) {
                    uids.add(users.getUid());
                }
            }

            int i = 0; // for debug // adminTmpというIDを入れたらそこで止まった！もしかして、存在しない職員番号を入れるととまる？ 2015-12-02 by nakano
            for (dao.entity.Restriction restriction : res1) {
                logger.debug("==0== i="+(++i)+"/"+res1.size());
                logger.debug("==1== ruser="+ruser+", kid="+kid+",rid="+rid+",mode="+mode);
                logger.debug("==2== "+restriction.getRid()+","+restriction.getMode()+","+restriction.getFlg()+","+restriction.getId());
                if (restriction.getMode().intValue() >= mode) {
                    int flg = restriction.getFlg().intValue();
                    if (flg == 0) { // 個人指定（熊大ID) => 学生/職員番号 20151013 by nakano
//						if (kid.equals(restriction.getId())) {
//							logger.debug(restriction.getId());
//							ret = restriction.getRid();
//							break;
//						}
                        if(uids.contains(restriction.getId())) {
                            logger.debug(restriction.getId());
                            ret = restriction.getRid();
                            break;
                        }
                    } else if (flg == -1) { // 学生番号パターン指定 => 学生/職員番号パターン 20151013 by nakano
//						// 2015.02.23 SVC 追加 >>
//						Pattern p = Pattern.compile(restriction.getId());
//						Matcher m = p.matcher(ruser);
//						if (m.matches()){
//							ret = restriction.getRid();
//							break;
//						}
                        Pattern p = Pattern.compile(restriction.getId());
                        for(String uid : uids) {
                            Matcher m = p.matcher(uid);
                            if (m.matches()){
//								logger.info("==A== uid="+uid+",restriction.getId())="+restriction.getId()+",restriction.getRid()="+restriction.getRid());
                                ret = restriction.getRid();
                                break;
                            }
                        }
                        // 2015.02.23 SVC 追加 <<
                    } else if (flg == 1) { // グループ指定
                        // 2015.02.26 SVC 追加 >>
                        // ここでの処理はグループIDでRestrictionGroupを紐付ける処理のみ行います
                        // flgが1のとき、idはグループIDであると解釈します
//						if ("0".equals(restriction.getId())) { // 誰でも可
//							ret = true;
//							break;
//						} else if ("1".equals(restriction.getId())) { // 本人のみ可
//							ret = ruser.equals(kid);
//							break;
//						} else { // グループID指定
//
//							ArrayList<RestrictionGroup> res2 = restrictionGroupDao
//							    .getRestrictionGroupList01(ruser, restriction.getId());
//							logger.debug(res2);
//							for (RestrictionGroup restrictionGroup : res2) {
//								if (kid.equals(restrictionGroup.getKid())) {
//									logger.debug(restrictionGroup.getKid());
//									ret = true;
//									break;
//								}
//							}
//						}
                        ArrayList<RestrictionGroup> res2 = restrictionGroupDao
                                .getRestrictionGroupList01(ruser, restriction.getId());
                        logger.debug(res2);
                        for (RestrictionGroup restrictionGroup : res2) {
                            if (kid.equals(restrictionGroup.getKid())) {
                                logger.debug(restrictionGroup.getKid());
                                ret = restriction.getRid();
                                break;
                            }
                        }
                        if (ret > -1){
                            break;
                        }
                        // 2015.02.26 SVC 追加 <<
                    }
                }
            }
        }

        logger.info("ruser=" + ruser + ", kid=" + kid + ", rid=" + rid + ", mode="
            + mode + ", ret=" + ret);
        return ret;
    }

    /**
     * アクセス権の問い合わせ（ページ生成時のajax処理）
     * @param pramDto ajax処理時のパラメータ
     * @param rid
     * @param mode
     * @return
     */
    public boolean allow(ParameterDto pramDto, int rid, int mode) {

        // allowメソッドのオーバーロードを呼ぶ
        int userRid = this.getRid(pramDto.getRuser(), pramDto.getRuser(), rid, mode);
        // 要求されたridを下回っている場合はNG
        if (userRid < rid){
            return false;
        }

        // ridが2：管理者以上ならOK
        if (userRid >= 2){
            return true;
        }

        // パラメータにuidがなければ無条件でOK ==> 2016-010-16 とりあえずどれかの学籍番号で在籍していることを条件に加える
        if (pramDto.getUid() == null || pramDto.getUid().equals("")){
//			return true; // 無条件のコード
            // ここから: 在籍していないとアクセス不許可のコード (2017-01-16) by nakano@cc.kumamoto-u.ac.jp
            String[] uids = new UserID().getUID(pramDto.getRuser());
            boolean shokuinF = false;
            boolean zaisekiF = false;
            for(int i = 0; i < uids.length; i++) {
                shokuinF = false;
                zaisekiF = false;
//				logger.info("==F== uids["+i+"]"+uids[i]);
                if(uids[i].matches("[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]")) shokuinF = true;
                ParameterDto np = new ParameterDto();
                np.setUid(uids[i]);
                if(shokuinF == true) { // 職員番号の場合
                    ArrayList<KyokanMst> nk01 = new KyokanMstDao().getKyokanMstList01(np);
                    if(nk01.size() > 0)
                        if(nk01.get(0).getZAISHOKUCD().equals("1") && nk01.get(0).getKINMUKBNCD().equals("1")) zaisekiF = true; // 在職かつ常勤に限定
//					logger.info("==G=="+nk01.get(0).getKYOKANCD()+", "+nk01.get(0).getKYOKANNM()+", "+nk01.get(0).getZAISHOKUCD()+", "+nk01.get(0).getKINMUKBNCD());
                } else { // 学生番号の場合
                    ArrayList<Gakuseki> nr01 = new GakusekiDao().getGakusekiList04(np);
                    if(nr01.size() > 0)
                        if(nr01.get(0).getSotsugyoymd() == null || nr01.get(0).getgShozokucd().equals("null") || nr01.get(0).getgShozokucd().equals("0000-00-00")) zaisekiF = true;
//					logger.info("==E== nr01.getUid()="+nr01.get(0).getgSysno()+", nr01.get(0).getSotsugyoymd()="+nr01.get(0).getSotsugyoymd()+", ret="+ret);
                }
                if(zaisekiF == true) break;
            }
            return zaisekiF;
//			if(shokuinF != true) {
//				return zaisekiF;
//			} else {
//				return true; // 暫定
//			}
            // ここまで: 在籍していないとアクセス不許可のコード (2017-01-16) by nakano@cc.kumamoto-u.ac.jp
        }

        // uidで学籍データを取得しておく
        ArrayList<Gakuseki> res01 = new GakusekiDao().getGakusekiList04(pramDto);
//		for(int i = 0; i < res01.size(); i++) logger.info("***** "+res01.get(i).getgSysno()+", getGenkyokbncd="+res01.get(i).getGenkyokbncd()+", getSotsugyoymd="+res01.get(i).getSotsugyoymd());
        // ruserからuidの配列を取得
        String[] uids = new UserID().getUID(pramDto.getRuser());

        // 取得できなければNG
        if (uids == null){
            return false;
        }

        NendobetuShozokuDao nendobetushozokuDao = new NendobetuShozokuDao();
        AuthorityDao authorityDao = new AuthorityDao();

        for(int i = 0; i < uids.length; i++){

            // パラメータのuidと一致したら自分自身なのでOK
            if (pramDto.getUid().equals(uids[i])){
                return true;
            }
            // 参照権限を取得
            ArrayList<Authority> aut = authorityDao.getAuthority01(uids[i]);
            // uidから取得した学籍データが参照できるか判定
            for(Gakuseki gakuseki :res01){
                // 年度別所属データを取得
                ArrayList<NendobetuShozoku> res03 = nendobetushozokuDao.getDaigakuinKbn01(pramDto.getUid(),gakuseki.getYokenNendo(),gakuseki.getgShozokucd());
                for(Authority authority :aut) {

                    // 要件年度が異なる場合は次へ
                    if(authority.getYokenNendo().equals(0) == false && gakuseki.getYokenNendo().equals(authority.getYokenNendo()) == false ){
                        continue;
                    }
                    // 大学院区分コードが異なる場合は次へ
                    if(res03.isEmpty() == false){
                        if(authority.getDaigakuinKbncd().equals("") == false && res03.get(0).getDaigakuinkbncd().equals(authority.getDaigakuinKbncd()) == false){
                            continue;
                        }
                    }
                    // 所属コードが異なる場合は次へ
                    if(authority.getShozokucd().equals("") == false && gakuseki.getgShozokucd().startsWith(authority.getShozokucd()) == false){
                        continue;
                    }
                    // 学籍番号が異なる場合は次へ
                    if(authority.getSysno().equals("") == false && gakuseki.getgSysno().equals(authority.getSysno()) == false){
                        continue;
                    }
                    // ここまで通過したらOK
                    return true;
                }
            }
        }

        // ここまできたらFalse
        return false;
    }


}
