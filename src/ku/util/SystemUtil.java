package ku.util;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SystemUtil {

    /**
     * 日付から年度を取得する
     * @param Date型
     * @return 年度(Integer)
     */
    public Integer getFiscalYear(Date date){
        Integer fiscalYear = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("MM");

        try {
            // 月の取得
            String dateStr = sdf.format(date);
            int JanFebMarCheck = Integer.parseInt(dateStr);

            // 1～3月の場合は前年度
            if(JanFebMarCheck < 4){
                Calendar nextCal = Calendar.getInstance();
                nextCal.setTime(date);
                nextCal.add(Calendar.YEAR, -1);
                fiscalYear = nextCal.get(Calendar.YEAR);
            }else{
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                fiscalYear = cal.get(Calendar.YEAR);
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return fiscalYear;
    }

    /**
     * 日付から学期を取得する
     * @param Date型
     * @return 学期(String) ※前期：1、後期：2
     */
    public String getGakkiKbnCD(Date date){
        String gakkiKbnCD = "1";
        SimpleDateFormat sdf = new SimpleDateFormat("MM");

        try {
            // 月の取得
            String dateStr = sdf.format(date);
            int afterCheck = Integer.parseInt(dateStr);

            // 10～3月の場合は後期
            if(afterCheck >= 10 || afterCheck <= 3){
                gakkiKbnCD = "2";
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return gakkiKbnCD;
    }
}
