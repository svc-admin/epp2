package ku.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import util.Tools;
import dao.AuthorityDao;
import dao.entity.Authority;

/**
 * Unified restriction for all classes, version 2014-01-28 since 2013-01-28 Copyright 2014
 * Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class AuthorityTable {
    private static Logger logger = Tools.getCallerLogger();


   /**
     * ユーザIDから参照権限を取得する。
     *
     * @param : uid : 学生番号
     * @return : boolean true/false
     */
    public ArrayList<Object> getAuthority(String uid) {
        LinkedHashMap<String,Object> ret = new LinkedHashMap<String,Object>();

        AuthorityDao authorityDao = new AuthorityDao();
//        ArrayList<Authority> res = authorityDao.getAuthority01(uid); // commented out by KU)Nakano 2018.11.28
        ArrayList<Authority> res = authorityDao.getAuthority03(uid);  // add by KU)Nakano 2018.11.28

        ArrayList<Object> retData = new ArrayList<Object>();
        ArrayList<Object> obj = new ArrayList<Object>();
        if(res.size() > 0){
            for(Authority authority : res){
                obj.add(authority.getYokenNendo());
                obj.add(authority.getDaigakuinKbncd());
                obj.add(authority.getShozokucd());
                obj.add(authority.getSysno());
                retData.add(obj);
            }
        }
        logger.info("uid=" + uid+", ret=" + res);
        ret.put("aaData", retData);
        return retData;
    }

}
