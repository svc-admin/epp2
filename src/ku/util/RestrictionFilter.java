package ku.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import util.PropertiesUtil;
import util.Tools;
//2018.06.04 SVC)S.Furuta ADD START >> 利用実績収集機構の拡充
import dao.AccessLogDao;
//2018.06.04 SVC)S.Furuta ADD END <<
import dto.ParameterDto;
import dto.SysPropertiesDto;

/**
 * 2015-04-30 dataディレクトリ(WebCT等の巨大エビデンスデータ)へのsymbolic linkがない場合作成する by nakano
 * 2015-01-16 filterの初期パラメータのトークンを増やして、複数行で書けるようにした。また、logフォーマットを変更。
 * Unified restriction Filter Class, version 2014-01-28 since 2013-01-28 Copyright 2014
 * Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class RestrictionFilter implements Filter {

//	public HttpServletRequest request;
    private static Logger logger = Tools.getCallerLogger();
    private String errPage = "";
    int defaultRestriction = 2;
    HashMap<String, Integer> restriction = new HashMap<String, Integer>();

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest arg0, ServletResponse arg1,
        FilterChain arg2) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest)arg0;
        HttpServletResponse httpResponse = (HttpServletResponse)arg1;
        HttpSession session = httpRequest.getSession(false);
        Restriction rst = new Restriction();
        String ruser = httpRequest.getRemoteUser();
        String ip = httpRequest.getRemoteAddr();
        String cPath = httpRequest.getContextPath();

        // Restrictionのパラメータを取得
        ParameterDto paramDto = new ParameterDto();
        setParameter(httpRequest, paramDto);

//2018.06.04 SVC)S.Furuta ADD START >> 利用実績収集機構の拡充
        String sPath = httpRequest.getServletPath();
        // 古田PCだとなぜか個人ページ（pf3）への遷移時にhtmlファイルのServletPathが取れてこない。。（宮本さんPCだと正常にとれる）キャッシュ？ブラウザ依存？
        //if (sPath.indexOf(".html") >= 0){ // 2018.06.13 06/12の打合せ結果より、htmlファイルに限定せずログ出力を行う
            AccessLogDao accessLogDao = new AccessLogDao();
            int makeLog = accessLogDao.addAccessLog01(ruser,paramDto.getUid(),ip,sPath);
            logger.info("=== Make AccessLog === : ruser="+ruser+", uid="+paramDto.getUid()+"ip="+ip+", requested page="+sPath+", session="+session+", makeLog="+makeLog);
        //}
//2018.06.04 SVC)S.Furuta ADD END <<
        if(session == null) {
            logger.info("session is null!!!");
        } else {
            String path = httpRequest.getServletPath();
            path.replaceFirst(cPath, "");
            boolean matched = false;
            for(String patt : restriction.keySet()) {
                if( path.matches(patt)) {
                    matched = true;
                    int rid = restriction.get(patt);
                    logger.debug("-----patt=" + patt+",pramDto.getRuser()="+paramDto.getRuser()+",rid="+rid+",mode="+0);
                    logger.info("=== RestrictionFilter Matched === : ruser="+ruser+", uid="+paramDto.getUid()+",rid="+rid+" , ip="+ip+", requested page="+path+", session="+session);
                    if(rst.allow(paramDto, rid, 0)) {
                        logger.info("=== RestrictionFilter Allowed === : ruser="+ruser+", uid="+paramDto.getUid()+",rid="+rid+" , ip="+ip+", requested page="+path+", session="+session);
                        arg2.doFilter(httpRequest, httpResponse);
                        return;
                    } else {
                        logger.info("=== RestrictionFilter NotAllowed === : ruser="+ruser+", uid="+paramDto.getUid()+",rid="+rid+" , ip="+ip+", requested page="+path+", session="+session);
                    }
                }
            }
            if(!matched) logger.info("=== RestrictionFilter Unmatched === : ruser=" + ruser + " , ip=" + ip + ", requested page=" + path + ", session="+session);
        }
        arg0.getRequestDispatcher(errPage).forward(arg0, arg1);

    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        String restrictionList = null;
        try {
             errPage = arg0.getInitParameter("errPage");
             logger.info("errPage=" + errPage);
             // (ここから) data ディレクトリへのsymbolic linkがない場合作成する 2015-04-30 by nakano
             String realPath = arg0.getServletContext().getRealPath("");
             if(! realPath.endsWith("/"))	realPath = realPath + "/";
             logger.info("LinkDataDir.makeLink(\""+realPath+"\")");
             LinkDataDir.makeLink(realPath);
             // (ここまで) data ディレクトリへのsymbolic linkがない場合作成する 2015-04-30 by nakano
             defaultRestriction = Integer.parseInt(arg0.getInitParameter("defaultRestriction"));
             logger.info("defaultRestriction="+defaultRestriction);
             restrictionList = arg0.getInitParameter("restriction");
             StringTokenizer st = new StringTokenizer(restrictionList, ",\n\t ");
             restriction.clear();
             while (st.hasMoreTokens()) {
                 String [] pair = st.nextToken().split(":");
                 restriction.put(pair[0], Integer.parseInt(pair[1]));
                 logger.info("add restriction ("+pair[0]+", "+pair[1]+")");
             }
        }catch(Exception e) {
            logger.error(e);
        }
    }

    private void setParameter(HttpServletRequest httpRequest, ParameterDto paramDto){

        String ruser = httpRequest.getRemoteUser();
        Map<String, String[]> paramMap = httpRequest.getParameterMap();
        String url = httpRequest.getRequestURI();

        PropertiesUtil propUtil = new PropertiesUtil();
        SysPropertiesDto propDto = propUtil.getSysProperties();

        paramDto.setRuser(ruser);

        // パラメータにuidが指定してある場合
        if (paramMap.containsKey("uid")){
            paramDto.setUid(paramMap.get("uid")[0]);
        }
        else {

            Pattern pat = null;
            Matcher matcher = null;
            // URLの中にuidが指定してある場合
            String uid = "";
            for (String pattern : propDto.getUidPatterns()) {

                pat = Pattern.compile(pattern);
                matcher = pat.matcher(url);

                if (matcher.matches()) {
                    pat = Pattern.compile(propDto.getUidRegex());
                    matcher = pat.matcher(url);
                    if (matcher.find()){
                        uid = matcher.group();
                        break;
                    }
                }

                pat = null;
                matcher = null;

            }

            // uidをセット
            if (uid.equals("") == false) {
                // 学生番号の形式（999-X9999）でなければ、形式を戻して設定
                if(uid.indexOf("-") < 0){
                    paramDto.setUid(uid.substring(0, 3) + "-" + uid.substring(3, 4).toUpperCase() + uid.substring(4));
                } else {
                    paramDto.setUid(uid);
                }
            }
        }
    }

}
