package ku.util;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import util.Tools;
import dao.UsersDao;
import dao.entity.Users;

/**
 * Unified restriction for all classes, version 2014-01-28 since 2013-01-28 Copyright 2014
 * Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class UserID {
    private static Logger logger = Tools.getCallerLogger();

    /**
     * 熊大IDから学生番号、職員番号を取得する。
     *
     * @param : ruser : remoteUser (熊大ID)
     * @return : boolean true/false
     */
    public String[] getUID(String ruser) {
        String[] ret = null;

        UsersDao usersDao = new UsersDao();
        ArrayList<Users> res = usersDao.getUsersList01(ruser);

        if(res.size() > 0){
            ret = new String[res.size()];;

            int i = 0;
            for(Users users:res){
                ret[i] = users.getUid();
                i++;
            }
        }

        logger.info("ruser=" + ruser + ", ret=" + ret);
        return ret;
    }

    /**
     * 熊大IDから入学年度の新しい順で学生番号を取得する。
     *
     * @param : ruser : remoteUser (熊大ID)
     * @return : boolean true/false
     */
    public String[] getGUID(String ruser) {
        String[] ret = null;

        UsersDao usersDao = new UsersDao();
        ArrayList<Users> res = usersDao.getUsersList03(ruser);

        if(res.size() > 0){
            ret = new String[res.size()];;

            int i = 0;
            for(Users users:res){
                ret[i] = users.getUid();
                i++;
            }
        }

        logger.info("ruser=" + ruser + ", ret=" + ret);
        return ret;
    }

       /**
     * 学生番号、職員番号から熊大IDを取得する。
     *
     * @param : ruser : remoteUser (熊大ID),uid : 学生番号、職員番号
     * @return : boolean true/false
     */
    public String getKID(String ruser, String uid) {
        String ret = "";

        UsersDao usersDao = new UsersDao();
        ArrayList<Users> res = usersDao.getUsersList02(ruser,uid);

        if(res.size() > 0){
            ret = res.get(0).getKid();
        }
        logger.info("ruser=" + ruser + ", uid=" + uid+", ret=" + ret);
        return ret;
    }


}
