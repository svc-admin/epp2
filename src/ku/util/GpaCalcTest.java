package ku.util;

public class GpaCalcTest {

    /**
     * Test Program for GpaCalc.java. version 2014-01-31 since 2013-01-31 Copyright 2014
     * Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
     */
    public static void main(String[] args) {

//		2015-06-23 ==>
//		/home/nakano/projects/ePortfolio/R4ePf/epf/20150623/makeGpaTable02_20150623.Rmd
//		/home/nakano/projects/ePortfolio/R4ePf/epf/20150623/makeGpaAvr01_20150623.Rmd

//		GpaCalc.calc("hm5317735", 1988, 2009, 4);
//		GpaCalc.calc("hm5317735", 2010, 2013, 4);
//		GpaCalc.calc("hm5317735", 2014, 2014, 4);


//		GpaCalc.sub_calcStudent("admin", "070-L4008");
//		GpaCalc.sub_calcStudent("admin", "070-T2703");
//		for(int year = 2007; year < 2012; year++) {
//			GpaCalc.sub_calcAll("admin", "0513", year); // 文・コミ情報
//			GpaCalc.sub_calcAll("admin", "2582", year); // 工・マテリアル
//			GpaCalc.sub_calcAll("admin", "2587", year); // 工・数理工学
//		}
//		for(int year = 2007; year < 2012; year++) {
//			GpaCalc.sub_calcAvr("admin", "0513", year); // 文・コミ情報
//			GpaCalc.sub_calcAvr("admin", "2582", year); // 工・マテリアル
//			GpaCalc.sub_calcAvr("admin", "2587", year); // 工・数理工学
//		}
//		GpaCalc.calc("admin", 2013, 10000, 4);
//		GpaCalc.calc("admin", 2012, 2012, 4);
//	GpaCalc.calc("admin", 2009, 2011, 4);
//		GpaCalc.calc("admin", 1999, 1999, 4);
//		GpaCalc.calc("admin", 2000, 2008, 4);
//		GpaCalc.calc("admin", 1988, 2014, 4);
//			GpaCalc.calc("admin", 1988, 1988, 4);
//			GpaCalc.calc("admin", 2007, 2007, 4);
//		GpaCalc.calc("hm5317735", 1988, 2014, 4);


//		GpaCalc.calc("hm5317735", 1998, 2014, 4); // <<<<<<<<<<--------------

//		GpaCalc.sub_calcAll("hm5317735", "1511", 2012); // 法学部法学科
//		GpaCalc.sub_calcAvr("hm5317735", "1511", 2012); // 法学部法学科
//			GpaCalc.sub_calcAvr("hm5317735", "2582", 2013); // 工・マテリアル

//		GpaCalc.sub_calcAll("hm5317735", "1511", 2012); // 法学部法学科
//		GpaCalc.sub_calcAvr("hm5317735", "1511", 2012); // 法学部法学科
//...

//		GpaCalc.sub_calcAll("hm5317735", "22", 2010); // 理学部理学科
//		GpaCalc.sub_calcAvr("hm5317735", "22", 2010); // 理学部理学科
//		GpaCalc.sub_calcAll("hm5317735", "2214", 2010); // 理学部理学科
//		GpaCalc.sub_calcAvr("hm5317735", "2214", 2010); // 理学部理学科

//		GpaCalc.sub_calcAll("hm5317735", "2581", 2011); // 工学部物質生命化学科
//		GpaCalc.sub_calcAvr("hm5317735", "2581", 2011); // 工学部物質生命化学科
//		GpaCalc.sub_calcAll("hm5317735", "2581", 2010); // 工学部物質生命化学科
//		GpaCalc.sub_calcAvr("hm5317735", "2581", 2010); // 工学部物質生命化学科

//	GpaCalc.calc("hm5317735", 2013, 2013, 4);

//		GpaCalc.sub_calcAll("hm5317735", "0701", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "0701", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "221492", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "221492", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "221493", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "221493", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "221494", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "221494", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "221495", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "221495", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "2583", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "2583", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "2586", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "2586", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "4203", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "4203", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "420401", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "420401", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "420402", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "420402", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "420403", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "420403", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "4405", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "4405", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "6121", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "6121", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "612110", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "612110", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "612115", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "612115", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "612120", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "612120", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "612130", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "612130", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "612140", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "612140", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "612150", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "612150", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "612160", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "612160", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "612170", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "612170", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "612180", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "612180", 2012);

//		GpaCalc.sub_calcAll("hm5317735", "0701", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "0701", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "070218", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "070218", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "221492", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "221492", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "221493", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "221493", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "221494", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "221494", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "221495", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "221495", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "2583", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "2583", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "2586", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "2586", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "4203", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "4203", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "420401", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "420401", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "420402", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "420402", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "420403", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "420403", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "6121", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "6121", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "612110", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "612110", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "612115", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "612115", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "612130", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "612130", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "612140", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "612140", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "612150", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "612150", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "612160", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "612160", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "612170", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "612170", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "612180", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "612180", 2011);

//		GpaCalc.sub_calcAll("hm5317735", "051020", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "051020", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "051030", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "051030", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "0701", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "0701", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "070212", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "070212", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "070213", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "070213", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "070216", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "070216", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "070217", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "070217", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "070218", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "070218", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "221404", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "221404", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "221405", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "221405", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "221406", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "221406", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "2583", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "2583", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "2584", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "2584", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "2585", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "2585", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "2586", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "2586", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "4203", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "4203", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "420401", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "420401", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "420402", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "420402", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "420403", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "420403", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "6121", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "6121", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "612110", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "612110", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "612115", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "612115", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "612130", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "612130", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "612140", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "612140", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "612150", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "612150", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "612160", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "612160", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "612170", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "612170", 2010);
//		GpaCalc.sub_calcAll("hm5317735", "612180", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "612180", 2010);

//		GpaCalc.sub_calcAll("hm5317735", "4204", 2012);
//		GpaCalc.sub_calcAvr("hm5317735", "4204", 2012);
//		GpaCalc.sub_calcAll("hm5317735", "4204", 2011);
//		GpaCalc.sub_calcAvr("hm5317735", "4204", 2011);
//		GpaCalc.sub_calcAll("hm5317735", "4204", 2010);
//		GpaCalc.sub_calcAvr("hm5317735", "4204", 2010);


        }

}
