-- 58だけのkakutei_seiseki
insert into kakutei_seiseki_ge
SELECT * FROM kakutei_seiseki
where JIKANWARI_SHOZOKUCD = 58

-- 58以外のkakutei_seiseki
insert into kakutei_seiseki_sp
SELECT * FROM kakutei_seiseki
where JIKANWARI_SHOZOKUCD != 58

-- (1) ある年度(2011)の所属コードの長さがある桁(4)の所属コード
select SHOZOKUCD from nendobetu_shozoku
where NYUGAKU_NENDO=2011 and length(SHOZOKUCD)=4
order by SHOZOKUCD asc;
-- 37

-- (2) (1)をgakusekiから取得しようとする
select G_SHOZOKUCD from gakuseki
where NYUGAKU_NENDO=2011 and length(G_SHOZOKUCD)=4
group by G_SHOZOKUCD
order by  G_SHOZOKUCD asc;
-- 28

-- なぜ、上記(1)と(2)が異なるか => 例えば、後者に0510はなく、051010,051020,051030しかない

select distinct left(G_SHOZOKUCD,4) from gakuseki
where NYUGAKU_NENDO=2011 and length(G_SHOZOKUCD) >= 4
group by G_SHOZOKUCD
order by G_SHOZOKUCD asc;

-- 以下でも同じ

--★ (2)の世界版 gakusekiから直接、ある年度(2011)の所属コードの長さがある桁(4)の所属コード
select left(G_SHOZOKUCD,4) as a from gakuseki
where NYUGAKU_NENDO=2011 and length(G_SHOZOKUCD) >= 4
group by a order by a asc;
-- 37


