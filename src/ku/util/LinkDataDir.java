package ku.util;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;

import org.apache.log4j.Logger;

import dto.SysPropertiesDto;
import util.PropertiesUtil;
import util.Tools;

/**
 * @author nakano@cc.kumamoto-u.acjp
 * @version 2015-03-30
 *
 */
public class LinkDataDir {
    private static Logger logger = Tools.getCallerLogger();
    private static PropertiesUtil props = new PropertiesUtil();

    public static boolean makeLink(String realPath) {
        boolean ret = false;
        String evDir = props.getSysProperties().getEvdir();
        String lcDir = realPath + props.getSysProperties().getEvuri().replaceFirst(".*/", "").replaceFirst("\\{.*\\}","");
        if(evDir == null) {
            logger.error("evDir is null !!!");
        } else {
            try {
                File evd = new File(evDir);
                if(!evd.exists()) {
                    logger.error("evDir ["+evDir+"] does not exist !!!");
                } else {
                    File lcd = new File(lcDir);
                    if(lcd.exists()) {
                        if(lcd.getAbsolutePath().equals(evd)) {
                            logger.error("lcDir ["+lcDir+"] is not a link of evDir["+evDir+"] !!!");
                        } else {
                            logger.info("Already lcDir ["+lcDir+"] exists as a link of evDir["+evDir+"] !!!");
                        }
                    } else {
                        Files.createSymbolicLink(Paths.get(lcDir), Paths.get(evDir));
                        logger.info("Symboliclink lcDir ["+lcDir+"] is created as a link of evDir["+evDir+"] !!!");
                    }
                }

            } catch(Exception e) {
                logger.fatal("try-catch's Error : " , e);;
            }
        }

        return ret;
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        LinkDataDir.makeLink("/home/nakano/workspace7/.metadata/.plugins/org.eclipse.wst.server.core/tmp3/wtpwebapps/epp2/");
    }

}
