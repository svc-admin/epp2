package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.GpaShugakuninteiGpMst;
import dto.ParameterDto;

public class GpaShugakuninteiGpMstDao extends AbstractDao<GpaShugakuninteiGpMst> {

    private static Logger logger = null;

    public GpaShugakuninteiGpMstDao() {
        super();
    }

    @Override
    ArrayList<GpaShugakuninteiGpMst> setEntityList(ResultSet rs) {
        GpaShugakuninteiGpMst entity = new GpaShugakuninteiGpMst();
        return entity.setRsToList(rs);
    }

    public ArrayList<GpaShugakuninteiGpMst> getGpaShugakuninteiGpMst(ParameterDto pramDto) {

    	String strSql = getSql("GpaShugakuninteiGPMstDao_getGpaShugakuninteiGPMst02.sql");
    	List<Object> valueList = new ArrayList<Object>();
    	valueList.add(pramDto.getGpaShugakuninteiGpMst().getgBukyokucd());
    	valueList.add(pramDto.getGpaShugakuninteiGpMst().getShori_start());
    	valueList.add(pramDto.getGpaShugakuninteiGpMst().getShori_end());
    	return get("",strSql,valueList);
    }

    public Boolean deleteGpaShugakuninteiGpMst01(ParameterDto pramDto) {

        Boolean res = false;

        Connection conn = null;
        PreparedStatement pstmt = null;

        String strSql = "delete from gpa_shugakunintei_gp_mst where ";
        String strWhere = "";

        if(pramDto.getGpaShugakuninteiBukyokuMst().getgBukyokucd().equals("") == false){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "G_BUKYOKUCD = '" + pramDto.getGpaShugakuninteiBukyokuMst().getgBukyokucd() + "' ";
        }

        if(pramDto.getGpaShugakuninteiBukyokuMst().getShori_start() != 0){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "SHORI_START = '" + pramDto.getGpaShugakuninteiBukyokuMst().getShori_start() + "' ";
        }

        if(pramDto.getGpaShugakuninteiBukyokuMst().getShori_end() != 0){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "SHORI_END = '" + pramDto.getGpaShugakuninteiBukyokuMst().getShori_end() + "' ";
        }

        if(pramDto.getGpaShugakuninteiGpMst().getHyogonm() != null && pramDto.getGpaShugakuninteiGpMst().getHyogonm().equals("") == false){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "HYOGONM = '" + pramDto.getGpaShugakuninteiGpMst().getHyogonm() + "' ";
        }
        // queryの生成
        strSql = strSql + strWhere;
        System.out.println(strSql);

        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            int num = pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return true;
    }

    public Boolean addGpaShugakuninteiGpMst01(ParameterDto pramDto) {

        Boolean res = false;

        Connection conn = null;
        PreparedStatement pstmt = null;
        System.out.println("gp:" + pramDto.getGpaShugakuninteiGpMst().getGp());

        String strSql = "insert into gpa_shugakunintei_gp_mst (";
        String strPram = ")values(" ;
        strSql = strSql + "G_BUKYOKUCD";
        strPram = strPram + "'" + pramDto.getGpaShugakuninteiBukyokuMst().getgBukyokucd() + "'";

        strSql = strSql + ",SHORI_START";
        strPram = strPram + ",'" + pramDto.getGpaShugakuninteiBukyokuMst().getShori_start() + "'";

        strSql = strSql + ",SHORI_END";
        strPram = strPram + ",'" + pramDto.getGpaShugakuninteiBukyokuMst().getShori_end() + "'";

        if(pramDto.getGpaShugakuninteiGpMst().getHyogonm().equals("") == false){
            strSql = strSql + ",HYOGONM";
            strPram = strPram + ",'" + pramDto.getGpaShugakuninteiGpMst().getHyogonm() + "'";
        }

        strSql = strSql + ",gp";
        strPram = strPram + "," + pramDto.getGpaShugakuninteiGpMst().getGp();

        if(pramDto.getGpaShugakuninteiGpMst().getGpaTaisho() != -1){
            strSql = strSql + ",gpa_taisho";
            strPram = strPram + "," + pramDto.getGpaShugakuninteiGpMst().getGpaTaisho();
        }

        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
        strSql = strSql + ",INSERT_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",UPDATE_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",USERID";
        strPram = strPram + ",'" + pramDto.getRuser() + "'";

        strSql = strSql + strPram + ")";
        System.out.println(strSql);

        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            int num = pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return true;
    }
}