package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.ShugakusienHanteiRireki;
import dto.ParameterDto;

public class ShugakusienHanteiRirekiDao extends AbstractDao<ShugakusienHanteiRireki> {

	private static Logger logger = null;

	public ShugakusienHanteiRirekiDao() {
		super();
	}

	@Override
	ArrayList<ShugakusienHanteiRireki> setEntityList(ResultSet rs) {
		ShugakusienHanteiRireki entity = new ShugakusienHanteiRireki();
		return entity.setRsToList(rs);
	}

	/**
	 * 判定履歴データを取得する
	 * @param paramDto
	 * @return
	 */
	public ArrayList<ShugakusienHanteiRireki> getRirekiData(ParameterDto paramDto) {

		// SQL文の作成
		String strSql = "";
		strSql += " SELECT";
		strSql += "     A.SHORI_NENDO";
		strSql += "    ,A.GAKKIKBNCD";
		strSql += "    ,A.KIJUN_DATE";
		strSql += "    ,A.G_BUKYOKUCD";
		strSql += "    ,RTRIM(CONCAT(B.SHOZOKUNM1,' ',B.SHOZOKUNM2,' ',B.SHOZOKUNM3,' ',B.SHOZOKUNM4,' ',B.SHOZOKUNM5)) AS SHOZOKUNM";
		strSql += "    ,A.GAKUNEN";
		strSql += "    ,A.NUM";
		strSql += " FROM";
		strSql += "     shugakusien_hantei_rireki A";
		strSql += "     INNER JOIN shozoku_mst B";
		strSql += "             ON (    A.G_BUKYOKUCD = B.SHOZOKUCD";
		strSql += "                )";
		strSql += " WHERE";
		strSql += "     A.SHORI_NENDO = ?";
		strSql += " AND A.GAKKIKBNCD = ?";
		strSql += " AND A.KIJUN_DATE = ?";
		strSql += " ORDER BY";
		strSql += "     A.SHORI_NENDO DESC";
		strSql += "    ,A.GAKKIKBNCD DESC";
		strSql += "    ,A.KIJUN_DATE DESC";
		strSql += "    ,A.G_BUKYOKUCD DESC";
		strSql += "    ,A.GAKUNEN DESC";
		strSql += "    ,A.NUM DESC";

		// パラメータの設定
		List<Object> valueList = new ArrayList<Object>();
		valueList.add(paramDto.getShoriJokenMst().getShorinendo());
		valueList.add(paramDto.getShoriJokenMst().getGakkikbncd());
		valueList.add(paramDto.getShoriJokenMst().getKijundate());

		// SQLの実行
		return get(paramDto.getRuser(), strSql, valueList);
	}
}