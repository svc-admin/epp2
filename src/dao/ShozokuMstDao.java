package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.ShozokuMst;
import dto.ParameterDto;

public class ShozokuMstDao extends AbstractDao<ShozokuMst> {

    public ShozokuMstDao() {
        super();
    }

    @Override
    ArrayList<ShozokuMst> setEntityList(ResultSet rs) {
        ShozokuMst entity = new ShozokuMst();
        return entity.setRsToList(rs);
    }

    public ArrayList<ShozokuMst> getShozokuMstList01(ParameterDto pramDto,String shozokucd) {

        //外だしSQLの取得
        String strSql = getSql("ShozokuMstDao_getShozokuMstList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.所属課コード
        valueList.add(shozokucd);

        return get(pramDto.getRuser(),strSql,valueList);
    }
// 2018.06.19 SVC)S.Furuta ADD START >> 部局計算GPAの表示対応
    public ArrayList<ShozokuMst> getShozokuMstList02(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("ShozokuMstDao_getShozokuMstList02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();

        return get(pramDto.getRuser(),strSql,valueList);
    }
// 2018.06.19 SVC)S.Furuta ADD END <<
}
