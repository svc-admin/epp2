package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsGpaShugakuninteiGpMst;

public class GpaShugakuninteiGpMst extends BsGpaShugakuninteiGpMst {

    public ArrayList<GpaShugakuninteiGpMst> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<GpaShugakuninteiGpMst> list = new ArrayList<GpaShugakuninteiGpMst>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                GpaShugakuninteiGpMst entity = new GpaShugakuninteiGpMst();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "G_BUKYOKUCD":
                            entity.setgBukyokucd(rs.getString(sColName));
                            break;
                        case "SHORI_START":
                        	entity.setShori_start(rs.getInt(sColName));
                        	break;
                        case "SHORI_END":
                        	entity.setShori_end(rs.getInt(sColName));
                        	break;
                        case "HYOGONM":
                            entity.setHyogonm(rs.getString(sColName));
                            break;
                        case "gp":
                            entity.setGp(rs.getBigDecimal(sColName));
                            break;
                        case "gpa_taisho":
                            entity.setGpaTaisho(rs.getInt(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
