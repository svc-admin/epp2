package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsJugyoKoma;

public class JugyoKoma extends BsJugyoKoma {

    public ArrayList<JugyoKoma> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<JugyoKoma> list = new ArrayList<JugyoKoma>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                JugyoKoma entity = new JugyoKoma();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "NENDO":
                            entity.setNendo((Integer)rs.getInt(sColName));
                            break;
                        case "JIKANWARI_SHOZOKUCD":
                            entity.setJikanwariShozokucd(rs.getString(sColName));
                            break;
                        case "JIKANWARICD":
                            entity.setJikanwaricd(rs.getString(sColName));
                            break;
                        case "YOBI":
                            entity.setYobi((Integer)rs.getInt(sColName));
                            break;
                        case "JIGEN":
                            entity.setJigen((Integer)rs.getInt(sColName));
                            break;
                        case "TEKIYO_ST_YMD":
                            entity.setTekiyoStYmd(rs.getDate(sColName));
                            break;
                        case "GAKKIKBNCD":
                            entity.setGakkikbncd(rs.getString(sColName));
                            break;
                        case "TEKIYO_END_YMD":
                            entity.setTekiyoEndYmd(rs.getDate(sColName));
                            break;
                        case "ST_TIME":
                            entity.setStTime(rs.getString(sColName));
                            break;
                        case "END_TIME":
                            entity.setEndTime(rs.getString(sColName));
                            break;
                        case "SHISETSUCD":
                            entity.setShisetsucd(rs.getString(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
