package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsGakuseki;

public class Gakuseki extends BsGakuseki {

    public ArrayList<Gakuseki> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<Gakuseki> list = new ArrayList<Gakuseki>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                Gakuseki entity = new Gakuseki();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "G_BUKYOKUCD":
                            entity.setgBukyokucd(rs.getString(sColName));
                            break;
                        case "G_GAKUNO":
                            entity.setgGakuno(rs.getString(sColName));
                            break;
                        case "G_SORT":
                            entity.setgSort(rs.getString(sColName));
                            break;
                        case "G_SYSNO":
                            entity.setgSysno(rs.getString(sColName));
                            break;
                        case "G_SHOZOKUCD":
                            entity.setgShozokucd(rs.getString(sColName));
                            break;
                        case "G_MIBUNCD":
                            entity.setgMibuncd(rs.getString(sColName));
                            break;
                        case "GENKYOKBNCD":
                            entity.setGenkyokbncd(rs.getString(sColName));
                            break;
                        case "GAKUNEN":
                            entity.setGakunen((Integer)rs.getInt(sColName));
                            break;
                        case "ZAIGAKU_TSUKISU":
                            entity.setZaigakuTsukisu((Integer)rs.getInt(sColName));
                            break;
                        case "NYUGAKUYMD":
                            entity.setNyugakuymd(rs.getDate(sColName));
                            break;
                        case "SOTSUGYOYMD":
                            entity.setSotsugyoymd(rs.getDate(sColName));
                            break;
                        case "TOKKI":
                            entity.setTokki(rs.getString(sColName));
                            break;
                        case "RYUGAKUKBNCD":
                            entity.setRyugakukbncd(rs.getString(sColName));
                            break;
                        case "ZAIRYUSHIKAKUCD":
                            entity.setZairyushikakucd(rs.getString(sColName));
                            break;
                        case "SHAKAIJINFLG":
                            entity.setShakaijinflg(rs.getString(sColName));
                            break;
                        case "GAIKOKUJINFLG":
                            entity.setGaikokujinflg(rs.getString(sColName));
                            break;
                        case "JUKENNO":
                            entity.setJukenno(rs.getString(sColName));
                            break;
                        case "NYUGAKUKBNCD":
                            entity.setNyugakukbncd(rs.getString(sColName));
                            break;
                        case "NYUGAKU_NENDO":
                            entity.setNyugakuNendo((Integer)rs.getInt(sColName));
                            break;
                        case "NYUGAKU_GAKUNEN":
                            entity.setNyugakuGakunen((Integer)rs.getInt(sColName));
                            break;
                        case "AKINYUGAKUFLG":
                            entity.setAkinyugakuflg((Integer)rs.getInt(sColName));
                            break;
                        case "KENCD":
                            entity.setKencd(rs.getString(sColName));
                            break;
                        case "GAKUICD":
                            entity.setGakuicd(rs.getString(sColName));
                            break;
                        case "GAKUIKINO":
                            entity.setGakuikino(rs.getString(sColName));
                            break;
                        case "SUB_SHOZOKUCD":
                            entity.setSubShozokucd(rs.getString(sColName));
                            break;
                        case "YOKEN_NENDO":
                            entity.setYokenNendo((Integer)rs.getInt(sColName));
                            break;
                        case "YOKEN_MONTH":
                            entity.setYokenMonth(rs.getString(sColName));
                            break;
                        case "MIKOMIFLG":
                            entity.setMikomiflg(rs.getString(sColName));
                            break;
                        case "IMAGEFILENM":
                            entity.setImagefilenm(rs.getString(sColName));
                            break;
                        case "HAKKO_CNT":
                            entity.setHakkoCnt((Integer)rs.getInt(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
