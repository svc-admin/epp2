package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsDirectory;

public class Directory extends BsDirectory {

    public ArrayList<Directory> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<Directory> list = new ArrayList<Directory>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                Directory entity = new Directory();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "UID":
                            entity.setUid(rs.getString(sColName));
                            break;
                        case "CID":
                            entity.setCid(rs.getString(sColName));
                            break;
                        case "DEPTH":
                            entity.setDepth((Integer)rs.getInt(sColName));
                            break;
                        case "NAME":
                            entity.setName(rs.getString(sColName));
                            break;
                        case "DATE":
                            entity.setDate(rs.getTimestamp(sColName));
                            break;
                        case "ISDIR":
                            entity.setIsdir((Integer)rs.getInt(sColName));
                            break;
                        case "PARENT":
                            entity.setParent(rs.getString(sColName));
                            break;
                        case "ABSDIR":
                            entity.setAbsdir(rs.getString(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
