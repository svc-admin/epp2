package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsKussShinro;

public class KussShinro extends BsKussShinro {

    public ArrayList<KussShinro> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<KussShinro> list = new ArrayList<KussShinro>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                KussShinro entity = new KussShinro();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "NENDO":
                            entity.setNendo((Integer)rs.getInt(sColName));
                            break;
                        case "G_BUKYOKUCD":
                            entity.setgBukyokucd(rs.getString(sColName));
                            break;
                        case "G_GAKUNO":
                            entity.setgGakuno(rs.getString(sColName));
                            break;
                        case "G_SYSNO":
                            entity.setgSysno(rs.getString(sColName));
                            break;
                        case "G_SHOZOKUCD":
                            entity.setgShozokucd(rs.getString(sColName));
                            break;
                        case "G_SEX":
                            entity.setgSex((Integer)rs.getInt(sColName));
                            break;
                        case "SHINROKBN":
                            entity.setShinrokbn((Integer)rs.getInt(sColName));
                            break;
                        case "GYOSHUCD":
                            entity.setGyoshucd(rs.getString(sColName));
                            break;
                        case "SHOKUSHUCD":
                            entity.setShokushucd(rs.getString(sColName));
                            break;
                        case "KINMUCHICD":
                            entity.setKinmuchicd(rs.getString(sColName));
                            break;
                        case "SHINROCD":
                            entity.setShinrocd(rs.getString(sColName));
                            break;
                        case "SHINRONM":
                            entity.setShinronm(rs.getString(sColName));
                            break;
                        case "KANNAIFLG":
                            entity.setKannaiflg((Integer)rs.getInt(sColName));
                            break;
                        case "NAITEIYMD":
                            entity.setNaiteiymd(rs.getDate(sColName));
                            break;
                        case "KOKAIFLG":
                            entity.setKokaiflg((Integer)rs.getInt(sColName));
                            break;
                        case "GAKKOSECCHICD":
                            entity.setGakkosecchicd(rs.getString(sColName));
                            break;
                        case "KOYOKEITAICD":
                            entity.setKoyokeitaicd(rs.getString(sColName));
                            break;
                        case "BIKO":
                            entity.setBiko(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
