package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsKakuteiSeiseki;

public class KakuteiSeiseki extends BsKakuteiSeiseki {

    public ArrayList<KakuteiSeiseki> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<KakuteiSeiseki> list = new ArrayList<KakuteiSeiseki>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                KakuteiSeiseki entity = new KakuteiSeiseki();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "G_BUKYOKUCD":
                            entity.setgBukyokucd(rs.getString(sColName));
                            break;
                        case "G_GAKUNO":
                            entity.setgGakuno(rs.getString(sColName));
                            break;
                        case "KAMOKUCD":
                            entity.setKamokucd(rs.getString(sColName));
                            break;
                        case "SEQ_NO":
                            entity.setSeqNo((Integer)rs.getInt(sColName));
                            break;
                        case "G_SYSNO":
                            entity.setgSysno(rs.getString(sColName));
                            break;
                        case "NINTEI_NENDO":
                            entity.setNinteiNendo((Integer)rs.getInt(sColName));
                            break;
                        case "NINTEI_GAKKIKBNCD":
                            entity.setNinteiGakkikbncd(rs.getString(sColName));
                            break;
                        case "NINTEI_KAIKOKBNCD":
                            entity.setNinteiKaikokbncd(rs.getString(sColName));
                            break;
                        case "CUR_NENDO":
                            entity.setCurNendo(rs.getString(sColName));
                            break;
                        case "CUR_SHOZOKUCD":
                            entity.setCurShozokucd(rs.getString(sColName));
                            break;
                        case "KAMOKUD_SHOZOKUCD":
                            entity.setKamokudShozokucd(rs.getString(sColName));
                            break;
                        case "KAMOKUDKBNCD":
                            entity.setKamokudkbncd(rs.getString(sColName));
                            break;
                        case "KAMOKUM_SHOZOKUCD":
                            entity.setKamokumShozokucd(rs.getString(sColName));
                            break;
                        case "KAMOKUMKBNCD":
                            entity.setKamokumkbncd(rs.getString(sColName));
                            break;
                        case "KAMOKUS_SHOZOKUCD":
                            entity.setKamokusShozokucd(rs.getString(sColName));
                            break;
                        case "KAMOKUSKBNCD":
                            entity.setKamokuskbncd(rs.getString(sColName));
                            break;
                        case "KAMOKUSS_SHOZOKUCD":
                            entity.setKamokussShozokucd(rs.getString(sColName));
                            break;
                        case "KAMOKUSSKBNCD":
                            entity.setKamokusskbncd(rs.getString(sColName));
                            break;
                        case "HYOGOCD":
                            entity.setHyogocd(rs.getString(sColName));
                            break;
                        case "HYOTEN":
                            entity.setHyoten(rs.getString(sColName));
                            break;
                        case "HYOGONM":
                            entity.setHyogonm(rs.getString(sColName));
                            break;
                        case "TANISU":
                            entity.setTanisu(rs.getBigDecimal(sColName));
                            break;
                        case "GOHIKBN":
                            entity.setGohikbn((Integer)rs.getInt(sColName));
                            break;
                        case "SAITENYMD":
                            entity.setSaitenymd(rs.getDate(sColName));
                            break;
                        case "SAITENSHA_SHOZOKUCD":
                            entity.setSaitenshaShozokucd(rs.getString(sColName));
                            break;
                        case "SAITENSHACD":
                            entity.setSaitenshacd(rs.getString(sColName));
                            break;
                        case "JIKANWARI_NENDO":
                            entity.setJikanwariNendo((Integer)rs.getInt(sColName));
                            break;
                        case "JIKANWARI_SHOZOKUCD":
                            entity.setJikanwariShozokucd(rs.getString(sColName));
                            break;
                        case "JIKANWARICD":
                            entity.setJikanwaricd(rs.getString(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
