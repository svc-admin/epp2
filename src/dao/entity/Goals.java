package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsGoals;

public class Goals extends BsGoals {

    public ArrayList<Goals> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<Goals> list = new ArrayList<Goals>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                Goals entity = new Goals();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "NENDO":
                            entity.setNendo((Integer)rs.getInt(sColName));
                            break;
                        case "JIKANWARI_SHOZOKUCD":
                            entity.setJikanwariShozokucd(rs.getString(sColName));
                            break;
                        case "JIKANWARICD":
                            entity.setJikanwaricd(rs.getString(sColName));
                            break;
                        case "ENTERD_FLG":
                            entity.setEnterdFlg((Integer)rs.getInt(sColName));
                            break;
                        case "CODE1":
                            entity.setCode1((Integer)rs.getInt(sColName));
                            break;
                        case "CODE2":
                            entity.setCode2((Integer)rs.getInt(sColName));
                            break;
                        case "CODE3":
                            entity.setCode3((Integer)rs.getInt(sColName));
                            break;
                        case "CODE4":
                            entity.setCode4((Integer)rs.getInt(sColName));
                            break;
                        case "CODE5":
                            entity.setCode5((Integer)rs.getInt(sColName));
                            break;
                        case "CODE6":
                            entity.setCode6((Integer)rs.getInt(sColName));
                            break;
                        case "CODE7":
                            entity.setCode7((Integer)rs.getInt(sColName));
                            break;
                        case "COMMENT":
                            entity.setComment(rs.getString(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
