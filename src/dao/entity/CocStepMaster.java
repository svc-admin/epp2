package dao.entity;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsCocStepMaster;

public class CocStepMaster extends BsCocStepMaster {

    public ArrayList<CocStepMaster> setEntityList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<CocStepMaster> list = new ArrayList<CocStepMaster>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                CocStepMaster entity = new CocStepMaster();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "COC_KBN":
                            entity.setCocKbn(rs.getString(sColName));
                            break;
                        case "NENDO":
                            entity.setNendo(rs.getInt(sColName));
                            break;
                        case "G_SYSNO":
                            entity.setGSysNo(rs.getString(sColName));
                            break;
                        case "STEP":
                            entity.setStep(rs.getString(sColName));
                            break;
                        case "TANISU":
                            entity.setTanisu((BigDecimal)rs.getBigDecimal(sColName));
                            break;
                        case "DISP_FLG":
                            entity.setDispFlg((Integer)rs.getInt(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
