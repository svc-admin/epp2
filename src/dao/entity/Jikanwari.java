package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsJikanwari;

public class Jikanwari extends BsJikanwari {

    public ArrayList<Jikanwari> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<Jikanwari> list = new ArrayList<Jikanwari>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                Jikanwari entity = new Jikanwari();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "NENDO":
                            entity.setNendo((Integer)rs.getInt(sColName));
                            break;
                        case "JIKANWARI_SHOZOKUCD":
                            entity.setJikanwariShozokucd(rs.getString(sColName));
                            break;
                        case "JIKANWARICD":
                            entity.setJikanwaricd(rs.getString(sColName));
                            break;
                        case "CUR_NENDO":
                            entity.setCurNendo(rs.getString(sColName));
                            break;
                        case "CUR_SHOZOKUCD":
                            entity.setCurShozokucd(rs.getString(sColName));
                            break;
                        case "GAKKIKBNCD":
                            entity.setGakkikbncd(rs.getString(sColName));
                            break;
                        case "KAMOKUCD":
                            entity.setKamokucd(rs.getString(sColName));
                            break;
                        case "KAIKOKBNCD":
                            entity.setKaikokbncd(rs.getString(sColName));
                            break;
                        case "ST_YMD":
                            entity.setStYmd(rs.getDate(sColName));
                            break;
                        case "END_YMD":
                            entity.setEndYmd(rs.getDate(sColName));
                            break;
                        case "K_SHOZOKUCD":
                            entity.setkShozokucd(rs.getString(sColName));
                            break;
                        case "SHUKYOKANCD":
                            entity.setShukyokancd(rs.getString(sColName));
                            break;
                        case "TANISU":
                            entity.setTanisu(rs.getBigDecimal(sColName));
                            break;
                        case "JUGYOKEITAICD":
                            entity.setJugyokeitaicd(rs.getString(sColName));
                            break;
                        case "YOBICHOFUKU":
                            entity.setYobichofuku((Integer)rs.getInt(sColName));
                            break;
                        case "TAISHO_NENJI":
                            entity.setTaishoNenji(rs.getString(sColName));
                            break;
                        case "KAIKO_KAMOKUNM":
                            entity.setKaikoKamokunm(rs.getString(sColName));
                            break;
                        case "KAIKO_KAMOKUNMENG":
                            entity.setKaikoKamokunmeng(rs.getString(sColName));
                            break;
                        case "BIKO":
                            entity.setBiko(rs.getString(sColName));
                            break;
                        case "JIGAKUBU_GOHIIPTFLG":
                            entity.setJigakubuGohiiptflg((Integer)rs.getInt(sColName));
                            break;
                        case "KYOTSUKAMOKU_KBNCD":
                            entity.setKyotsukamokuKbncd((Integer)rs.getInt(sColName));
                            break;
                        case "SETTEIKBNCD":
                            entity.setSetteikbncd((Integer)rs.getInt(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
