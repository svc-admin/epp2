package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsNendobetuShozoku;

public class NendobetuShozoku extends BsNendobetuShozoku {

    public ArrayList<NendobetuShozoku> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<NendobetuShozoku> list = new ArrayList<NendobetuShozoku>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                NendobetuShozoku entity = new NendobetuShozoku();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "NYUGAKU_NENDO":
                            entity.setNyugakuNendo((Integer)rs.getInt(sColName));
                            break;
                        case "SHOZOKUCD":
                            entity.setShozokucd(rs.getString(sColName));
                            break;
                        case "OYA_SHOZOKUCD":
                            entity.setOyaShozokucd(rs.getString(sColName));
                            break;
                        case "KAISOFLG":
                            entity.setKaisoflg((Integer)rs.getInt(sColName));
                            break;
                        case "DAIGAKUINKBNCD":
                            entity.setDaigakuinkbncd(rs.getString(sColName));
                            break;
                        case "SHOZOKUNM":
                            entity.setShozokunm(rs.getString(sColName));
                            break;
                        case "SHOZOKUKN":
                            entity.setShozokukn(rs.getString(sColName));
                            break;
                        case "SHOZOKUNMENG":
                            entity.setShozokunmeng(rs.getString(sColName));
                            break;
                        case "SHOZOKURKNM":
                            entity.setShozokurknm(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
