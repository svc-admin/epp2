package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsShutokutaniTaishoMst;

public class ShutokutaniTaishoMst extends BsShutokutaniTaishoMst {

    public ArrayList<ShutokutaniTaishoMst> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<ShutokutaniTaishoMst> list = new ArrayList<ShutokutaniTaishoMst>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                ShutokutaniTaishoMst entity = new ShutokutaniTaishoMst();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "KAMOKUDKBNCD":
                            entity.setKamokudkbncd(rs.getString(sColName));
                            break;
                        case "KAMOKUM_SHOZOKUCD":
                            entity.setKamokum_shozokucd(rs.getString(sColName));
                            break;
                        case "KAMOKUMKBNCD":
                            entity.setKamokumkbncd(rs.getString(sColName));
                            break;
                        case "KAMOKUS_SHOZOKUCD":
                            entity.setKamokus_shozokucd(rs.getString(sColName));
                            break;
                        case "KAMOKUSKBNCD":
                            entity.setKamokuskbncd(rs.getString(sColName));
                            break;
                        case "KAMOKUSS_SHOZOKUCD":
                            entity.setKamokuss_shozokucd(rs.getString(sColName));
                            break;
                        case "KAMOKUSSKBNCD":
                            entity.setKamokusskbncd(rs.getString(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return list;

    }

}
