package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsStudyingAbroadExperiences;

public class StudyingAbroadExperieces extends BsStudyingAbroadExperiences {

    public ArrayList<StudyingAbroadExperieces> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<StudyingAbroadExperieces> list = new ArrayList<StudyingAbroadExperieces>();

        try {
            // メタデータ取得
            rsm = rs.getMetaData();

            // 取得件数分繰り返す
            while(rs.next()){
                StudyingAbroadExperieces entity = new StudyingAbroadExperieces();

                // 取得項目分繰り返す
                for(int i = 0; i < rsm.getColumnCount(); i++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "id":
                            entity.setId((Integer)rs.getInt(sColName));
                            break;
                        case "G_SYSNO":
                            entity.setgSysno(rs.getString(sColName));
                            break;
                        case "KAMOKUCD":
                            entity.setKamokucd(rs.getString(sColName));
                            break;
                        case "purpose_type":
                            entity.setPurposeType(rs.getString(sColName));
                            break;
                        case "nation":
                            entity.setNation(rs.getString(sColName));
                            break;
                        case "date_from":
                            entity.setDateFrom(rs.getTimestamp(sColName));
                            break;
                        case "date_to":
                            entity.setDateTo(rs.getTimestamp(sColName));
                            break;
                        case "destination":
                            entity.setDestination(rs.getString(sColName));
                            break;
                        case "insert_date":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "update_date":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "userid":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }
                list.add(entity);
            }
        } catch (SQLException e) {

        }

        return list;
    }

}
