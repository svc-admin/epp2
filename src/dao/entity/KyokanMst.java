package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsKyokanMst;

public class KyokanMst extends BsKyokanMst {

    public ArrayList<KyokanMst> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<KyokanMst> list = new ArrayList<KyokanMst>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                KyokanMst entity = new KyokanMst();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                    case "KYOKANCD":
                        entity.setKYOKANCD(rs.getString(sColName));
                        break;
                        case "KYOKANNM":
                        entity.setKYOKANNM (rs.getString(sColName));
                        break;
                        case "KYOKANKN":
                        entity.setKYOKANKN (rs.getString(sColName));
                        break;
                        case "KYOKANENG":
                        entity.setKYOKANENG (rs.getString(sColName));
                        break;
                        case "KYOKANRK":
                        entity.setKYOKANRK (rs.getString(sColName));
                        break;
                        case "KYOKANSEX":
                        entity.setKYOKANSEX (rs.getInt(sColName));
                        break;
                        case "KYOKANYUBIN":
                        entity.setKYOKANYUBIN (rs.getString(sColName));
                        break;
                        case "KYOKANADD1":
                        entity.setKYOKANADD1 (rs.getString(sColName));
                        break;
                        case "KYOKANADD2":
                        entity.setKYOKANADD2 (rs.getString(sColName));
                        break;
                        case "KYOKANADD3":
                        entity.setKYOKANADD3 (rs.getString(sColName));
                        break;
                        case "KYOKANTEL":
                        entity.setKYOKANTEL (rs.getString(sColName));
                        break;
                        case "K_SHOZOKUCD":
                        entity.setK_SHOZOKUCD (rs.getString(sColName));
                        break;
                        case "YAKUSHOKUCD":
                        entity.setYAKUSHOKUCD (rs.getString(sColName));
                        break;
                        case "KYOKANKBNCD":
                        entity.setKYOKANKBNCD (rs.getString(sColName));
                        break;
                        case "ZAISHOKUCD":
                        entity.setZAISHOKUCD (rs.getString(sColName));
                        break;
                        case "KINMUKBNCD":
                        entity.setKINMUKBNCD (rs.getString(sColName));
                        break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
