package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsShugakusienJokyo;

public class ShugakusienJokyo extends BsShugakusienJokyo {

    public ArrayList<ShugakusienJokyo> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<ShugakusienJokyo> list = new ArrayList<ShugakusienJokyo>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                ShugakusienJokyo entity = new ShugakusienJokyo();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "NINTEI_NENDO":
                            entity.setNinteiNendo(rs.getInt(sColName));
                            break;
                        case "G_GAKUNO":
                            entity.setgGakuno(rs.getString(sColName));
                            break;
                        case "sien_jokyo":
                            entity.setSienJokyo(rs.getString(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
