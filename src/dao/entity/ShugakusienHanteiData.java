package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsShugakusienHanteiData;

public class ShugakusienHanteiData extends BsShugakusienHanteiData {

	public ArrayList<ShugakusienHanteiData> setRsToList(ResultSet rs){

		ResultSetMetaData rsm;
		ArrayList<ShugakusienHanteiData> list = new ArrayList<ShugakusienHanteiData>();

		try {
			//メタデータ取得
			rsm = rs.getMetaData();

			//取得件数分繰り返す
			while (rs.next()){

				ShugakusienHanteiData entity = new ShugakusienHanteiData();

				//取得項目分繰り返す
				for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
					String sColName = rsm.getColumnName(i + 1);

					if(sColName == null){
						//System.out.println("Null!");
					}else switch(sColName){
						case "SHORI_NENDO":
							entity.setShorinendo(rs.getInt(sColName));
							break;
						case "GAKKIKBNCD":
							entity.setGakkikbncd(rs.getString(sColName));
							break;
						case "KIJUN_DATE":
							entity.setKijundate(rs.getString(sColName));
							break;
						case "G_BUKYOKUCD":
							entity.setG_bukyokucd(rs.getString(sColName));
							break;
						case "GAKUNEN":
							entity.setGakunen(rs.getInt(sColName));
							break;
						case "NUM":
							entity.setNum(rs.getInt(sColName));
							break;
						case "G_SYSNO":
							entity.setG_gakuno(rs.getString(sColName));
							break;
						case "G_NAME":
							entity.setG_name(rs.getString(sColName));
							break;
						case "JUDGE_TYPE":
							entity.setJudge_type(rs.getString(sColName));
							break;
						case "Z_JUDGE":
							entity.setZ_judge(rs.getString(sColName));
							break;
						case "T_JUDGE":
							entity.setT_judge(rs.getString(sColName));
							break;
						case "Z_GPA":
							entity.setZ_gpa(rs.getString(sColName));
							break;
						case "Z_GPA_B":
							entity.setZ_gpa_b(rs.getString(sColName));
							break;
						case "Z_TANI":
							entity.setZ_tani(rs.getString(sColName));
							break;
						case "Z_TANI_B":
							entity.setZ_tani_b(rs.getString(sColName));
							break;
						case "H_SOTSU":
							entity.setH_sotsu(rs.getString(sColName));
							break;
						case "H_SOTSU_B":
							entity.setH_sotsu_b(rs.getString(sColName));
							break;
						case "H_TANI":
							entity.setH_tani(rs.getString(sColName));
							break;
						case "H_TANI_B":
							entity.setH_tani_b(rs.getString(sColName));
							break;
						case "H_SHU":
							entity.setH_shu(rs.getString(sColName));
							break;
						case "H_SHU_B":
							entity.setH_shu_b(rs.getString(sColName));
							break;
						case "H_ZEN":
							entity.setH_zen(rs.getString(sColName));
							break;
						case "H_ZEN_B":
							entity.setH_zen_b(rs.getString(sColName));
							break;
						case "K_TANI":
							entity.setK_tani(rs.getString(sColName));
							break;
						case "K_TANI_B":
							entity.setK_tani_b(rs.getString(sColName));
							break;
						case "K_GPA":
							entity.setK_gpa(rs.getString(sColName));
							break;
						case "K_GPA_B":
							entity.setK_gpa_b(rs.getString(sColName));
							break;
						case "K_SHU":
							entity.setK_shu(rs.getString(sColName));
							break;
						case "K_SHU_B":
							entity.setK_shu_b(rs.getString(sColName));
							break;
						case "S_TANI":
							entity.setS_tani(rs.getString(sColName));
							break;
						case "S_TANI_B":
							entity.setS_tani_b(rs.getString(sColName));
							break;
						case "S_SHU":
							entity.setS_shu(rs.getString(sColName));
							break;
						case "S_SHU_B":
							entity.setS_shu_b(rs.getString(sColName));
							break;
						case "BIKOU":
							entity.setBikou(rs.getString(sColName));
							break;
						case "T_GPT":
							entity.setT_gpt(rs.getBigDecimal(sColName));
							break;
						case "T_GP_TANISU":
							entity.setT_gp_tanisu(rs.getBigDecimal(sColName));
							break;
						case "T_GPA":
							entity.setT_gpa(rs.getBigDecimal(sColName));
							break;
						case "T_GPA_B":
							entity.setT_gpa_b(rs.getBigDecimal(sColName));
							break;
						case "T_GPA_JUNI":
							entity.setT_gpa_juni(rs.getInt(sColName));
							break;
						case "N_GPT":
							entity.setN_gpt(rs.getBigDecimal(sColName));
							break;
						case "N_GP_TANISU":
							entity.setN_gp_tanisu(rs.getBigDecimal(sColName));
							break;
						case "N_GPA":
							entity.setN_gpa(rs.getBigDecimal(sColName));
							break;
						case "N_GPA_B":
							entity.setN_gpa_b(rs.getBigDecimal(sColName));
							break;
						case "N_GPA_JUNI":
							entity.setN_gpa_juni(rs.getInt(sColName));
							break;
						case "GPA_PARAM":
							entity.setGpa_param(rs.getInt(sColName));
							break;
						case "ZAISEKI_MONTH":
							entity.setZaiseki_month(rs.getInt(sColName));
							break;
						case "KYUGAKU_MONTH":
							entity.setKyugaku_month(rs.getInt(sColName));
							break;
						case "ZAIGAKU_NENSU":
							entity.setZaigaku_nensu(rs.getBigDecimal(sColName));
							break;
						case "HITSUYO_TANISU":
							entity.setHitsuyo_tanisu(rs.getBigDecimal(sColName));
							break;
						case "SHUGYO_NENGEN":
							entity.setShugyo_nengen(rs.getInt(sColName));
							break;
						case "HYOJUN_TANISU":
							entity.setHyojun_tanisu(rs.getBigDecimal(sColName));
							break;
						case "SHUTOKU_TANISU":
							entity.setShutoku_tanisu(rs.getBigDecimal(sColName));
							break;
						case "SHUTOKU_RITSU":
							entity.setShutoku_ritsu(rs.getBigDecimal(sColName));
							break;
						case "RISHU_TANISU":
							entity.setRishu_tanisu(rs.getBigDecimal(sColName));
							break;
						case "KESSEKI_TANISU":
							entity.setKesseki_tanisu(rs.getBigDecimal(sColName));
							break;
						case "SHUSSEKI_RITSU":
							entity.setShusseki_ritsu(rs.getBigDecimal(sColName));
							break;
						case "RYUNENDATA":
							entity.setRyunendata(rs.getString(sColName));
							break;
						case "KEIKAKUSHO":
							entity.setKeikakusho(rs.getString(sColName));
							break;
						case "INSERT_DATE":
							entity.setInsertDate(rs.getDate(sColName));
							break;
						case "UPDATE_DATE":
							entity.setUpdateDate(rs.getDate(sColName));
							break;
						case "USERID":
							entity.setUserid(rs.getString(sColName));
							break;
						case "SHOZOKUNM":
							entity.setShozokunm(rs.getString(sColName));
							break;
						case "SIEN_JOKYO":
							entity.setShien_jokyo(rs.getString(sColName));
							break;
						case "HITSUYOTANISU":
							entity.setHitsuyo_tanisu(rs.getBigDecimal(sColName));
							break;
						case "SYUGYONENGEN":
							entity.setShugyo_nengen(rs.getInt(sColName));
							break;
						default:
							;
					}
				}

				list.add(entity);
			}
		} catch (SQLException e) {
			System.out.println(e);
		}

		return list;

	}

}
