package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsHanteiShubetsuData;

public class HanteiShubetsuData extends BsHanteiShubetsuData {

	public ArrayList<HanteiShubetsuData> setRsToList(ResultSet rs){

		ResultSetMetaData rsm;
		ArrayList<HanteiShubetsuData> list = new ArrayList<HanteiShubetsuData>();

		try {
			//メタデータ取得
			rsm = rs.getMetaData();

			//取得件数分繰り返す
			while (rs.next()){

				HanteiShubetsuData entity = new HanteiShubetsuData();

				//取得項目分繰り返す
				for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
					String sColName = rsm.getColumnName(i + 1);

					if(sColName == null){
						//System.out.println("Null!");
					}else switch(sColName){
						case "SHORI_NENDO":
							entity.setShori_nendo(Integer.parseInt(rs.getString(sColName)));
							break;
						case "G_SYSNO":
							entity.setG_sysno(rs.getString(sColName));
							break;
						case "HANTEI_SHUBETSU":
							entity.setHantei_shubetsu(rs.getString(sColName));
							break;
						default:
							;
					}
				}

				list.add(entity);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return list;

	}

}
