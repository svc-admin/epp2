package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsGoalsSub;

public class GoalsSub extends BsGoalsSub {

    public ArrayList<GoalsSub> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<GoalsSub> list = new ArrayList<GoalsSub>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                GoalsSub entity = new GoalsSub();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "NENDO":
                            entity.setNendo((Integer)rs.getInt(sColName));
                            break;
                        case "JIKANWARI_SHOZOKUCD":
                            entity.setJikanwariShozokucd(rs.getString(sColName));
                            break;
                        case "KAISOFLG":
                            entity.setKaisoflg((Integer)rs.getInt(sColName));
                            break;
                        case "GOALCD0":
                            entity.setGoalcd0((Integer)rs.getInt(sColName));
                            break;
                        case "GOALCD1":
                            entity.setGoalcd1((Integer)rs.getInt(sColName));
                            break;
                        case "GOALCD2":
                            entity.setGoalcd2((Integer)rs.getInt(sColName));
                            break;
                        case "NAME":
                            entity.setName(rs.getString(sColName));
                            break;
                        case "NAME_ENG":
                            entity.setNameEng(rs.getString(sColName));
                            break;
                        case "EXPL":
                            entity.setExpl(rs.getString(sColName));
                            break;
                        case "EXPL_ENG":
                            entity.setExplEng(rs.getString(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
