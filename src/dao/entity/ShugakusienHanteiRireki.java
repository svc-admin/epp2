package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsShugakusienHanteiRireki;

public class ShugakusienHanteiRireki extends BsShugakusienHanteiRireki {

	public ArrayList<ShugakusienHanteiRireki> setRsToList(ResultSet rs){

		ResultSetMetaData rsm;
		ArrayList<ShugakusienHanteiRireki> list = new ArrayList<ShugakusienHanteiRireki>();

		try {
			//メタデータ取得
			rsm = rs.getMetaData();

			//取得件数分繰り返す
			while (rs.next()){

				ShugakusienHanteiRireki entity = new ShugakusienHanteiRireki();

				//取得項目分繰り返す
				for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
					String sColName = rsm.getColumnName(i + 1);

					if(sColName == null){
						//System.out.println("Null!");
					}else switch(sColName){
						case "SHORI_NENDO":
							entity.setShorinendo(rs.getInt(sColName));
							break;
						case "GAKKIKBNCD":
							entity.setGakkikbncd(rs.getString(sColName));
							break;
						case "KIJUN_DATE":
							entity.setKijundate(rs.getString(sColName));
							break;
						case "G_BUKYOKUCD":
							entity.setG_bukyokucd(rs.getString(sColName));
							break;
						case "SHOZOKUNM":
							entity.setShozokunm(rs.getString(sColName));
							break;
						case "GAKUNEN":
							entity.setGakunen(rs.getInt(sColName));
							break;
						case "NUM":
							entity.setNum(rs.getInt(sColName));
							break;
						case "INSERT_DATE":
							entity.setInsertDate(rs.getDate(sColName));
							break;
						case "UPDATE_DATE":
							entity.setUpdateDate(rs.getDate(sColName));
							break;
						case "USERID":
							entity.setUserid(rs.getString(sColName));
							break;
						default:
							;
					}
				}

				list.add(entity);
			}
		} catch (SQLException e) {
		}

		return list;

	}

}
