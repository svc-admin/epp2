package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsResults;

public class Results extends BsResults {

    public ArrayList<Results> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<Results> list = new ArrayList<Results>();

        try {
            // メタデータ取得
            rsm = rs.getMetaData();

            // 取得件数分繰り返す
            while(rs.next()){
                Results entity = new Results();

                // 取得項目分繰り返す
                for(int i = 0; i < rsm.getColumnCount(); i++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "G_SYSNO":
                            entity.setgSysno(rs.getString(sColName));
                            break;
                        case "KAMOKUCD":
                            entity.setKamokucd(rs.getString(sColName));
                            break;
                        case "date":
                            entity.setDate(rs.getTimestamp(sColName));
                            break;
                        case "classification":
                            entity.setClassification(rs.getString(sColName));
                            break;
                        case "classification_custom":
                            entity.setClassificationCustom(rs.getString(sColName));
                            break;
                        case "title":
                            entity.setTitle(rs.getString(sColName));
                            break;
                        case "comment":
                            entity.setComment(rs.getString(sColName));
                            break;
                        case "author_name":
                            entity.setAuthorName(rs.getString(sColName));
                            break;
                        case "author_type":
                            entity.setAuthorType(rs.getString(sColName));
                            break;
                        case "presentation_format":
                            entity.setPresentationFormat(rs.getString(sColName));
                            break;
                        case "medium_title":
                            entity.setMediumTitle(rs.getString(sColName));
                            break;
                        case "medium_class":
                            entity.setMediumClass(rs.getString(sColName));
                            break;
                        case "has_impact_factor":
                            entity.setHasImpactFactor(rs.getString(sColName));
                            break;
                        case "impact_factor":
                            entity.setImpactFactor(rs.getString(sColName));
                            break;
                        case "location":
                            entity.setLocation(rs.getString(sColName));
                            break;
                        case "publisher":
                            entity.setPublisher(rs.getString(sColName));
                            break;
                        case "isbn":
                            entity.setIsbn(rs.getString(sColName));
                            break;
                        case "journal_volume":
                            entity.setJournalVolume(rs.getString(sColName));
                            break;
                        case "journal_number":
                            entity.setJournalNumber(rs.getString(sColName));
                            break;
                        case "page_from":
                            entity.setPageFrom((Integer)rs.getInt(sColName));
                            break;
                        case "page_to":
                            entity.setPageTo((Integer)rs.getInt(sColName));
                            break;
                        case "published_year":
                            entity.setPublishedYear((Integer)rs.getInt(sColName));
                            break;
                        case "published_month":
                            entity.setPublishedMonth((Integer)rs.getInt(sColName));
                            break;
                        case "has_reviewed":
                            entity.setHasReviewed(rs.getString(sColName));
                            break;
                        case "original_medium_name":
                            entity.setOriginalMediumName(rs.getString(sColName));
                            break;
                        case "original_author_name":
                            entity.setOriginalAuthorName(rs.getString(sColName));
                            break;
                        case "original_publisher":
                            entity.setOriginalPublisher(rs.getString(sColName));
                            break;
                        case "original_page_from":
                            entity.setOriginalPageFrom((Integer)rs.getInt(sColName));
                            break;
                        case "original_page_to":
                            entity.setOriginalPageTo((Integer)rs.getInt(sColName));
                            break;
                        case "original_published_year":
                            entity.setOriginalPublishedYear((Integer)rs.getInt(sColName));
                            break;
                        case "original_published_month":
                            entity.setOriginalPublishedMonth((Integer)rs.getInt(sColName));
                            break;
                        case "insert_date":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "update_date":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "userid":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }
                list.add(entity);
            }
        } catch (SQLException e) {

        }

        return list;
    }

}
