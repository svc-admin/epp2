package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsCocAdminister;

public class CocAdminister extends BsCocAdminister {

    public ArrayList<CocAdminister> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<CocAdminister> list = new ArrayList<CocAdminister>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                CocAdminister entity = new CocAdminister();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "GAKUNO":
                            entity.setgGakuno(rs.getString(sColName));
                            break;
                        case "G_SORT":
                            entity.setgSort(rs.getString(sColName));
                            break;
                        case "G_SYSNO":
                            entity.setgSysno(rs.getString(sColName));
                            break;
                        case "G_SHOZOKUCD":
                            entity.setgShozokucd(rs.getString(sColName));
                            break;
                        case "GAKUNEN":
                            entity.setGakunen((Integer)rs.getInt(sColName));
                            break;
                        case "KUBUN":
                            entity.setKubun((String)rs.getString(sColName));
                            break;
                        case "BIKOU":
                            entity.setBikou((String)rs.getString(sColName));
                            break;
                        case "IMAGEFILENM":
                            entity.setImagefilenm(rs.getString(sColName));
                            break;
                        case "HAKKO_CNT":
                            entity.setHakkoCnt((Integer)rs.getInt(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        case "SHOZOKUNM":
                            entity.setShozokunm(rs.getString(sColName));
                            break;
                        case "SHOZOKUNMENG":
                            entity.setShozokunmeng(rs.getString(sColName));
                            break;
                        case "G_NAME":
                            entity.setgName(rs.getString(sColName));
                            break;
                        case "G_NAMEENG":
                            entity.setgNameeng(rs.getString(sColName));
                            break;
                        case "STEP1":
                            entity.setStep1((Integer)rs.getInt(sColName));
                            break;
                        case "STEP2":
                            entity.setStep2((Integer)rs.getInt(sColName));
                            break;
                        case "STEP3":
                            entity.setStep3((Integer)rs.getInt(sColName));
                            break;
                        case "STEP4":
                            entity.setStep4((Integer)rs.getInt(sColName));
                            break;
                        case "STEP5":
                            entity.setStep5((Integer)rs.getInt(sColName));
                            break;
                        case "STEP6":
                            entity.setStep6((Integer)rs.getInt(sColName));
                            break;
                        case "STEP7":
                            entity.setStep7((Integer)rs.getInt(sColName));
                            break;
                        case "STEP8":
                            entity.setStep8((Integer)rs.getInt(sColName));
                            break;
                        case "STEP9":
                            entity.setStep9((Integer)rs.getInt(sColName));
                            break;
                        case "STEP10":
                            entity.setStep10((Integer)rs.getInt(sColName));
                            break;
                        // csv入力時のメッセージはBikouパラメータに仮代入
                        case "MES":
                            entity.setBikou((String)rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
