package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsToeicAvr;

public class ToeicAvr extends BsToeicAvr {

    public ArrayList<ToeicAvr> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<ToeicAvr> list = new ArrayList<ToeicAvr>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                ToeicAvr entity = new ToeicAvr();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "NYUGAKU_NENDO":
                            entity.setNyugakuNendo((Integer)rs.getInt(sColName));
                            break;
                        case "SHOZOKUCD":
                            entity.setShozokucd(rs.getString(sColName));
                            break;
                        case "JYUKEN_NENDO":
                            entity.setJyukenNendo((Integer)rs.getInt(sColName));
                            break;
                        case "score":
                            entity.setScore((Integer)rs.getInt(sColName));
                            break;
                        case "member":
                            entity.setMember((Integer)rs.getInt(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
