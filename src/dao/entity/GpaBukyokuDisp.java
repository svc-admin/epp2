package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsGpaBukyokuDisp;

public class GpaBukyokuDisp extends BsGpaBukyokuDisp {

    public ArrayList<GpaBukyokuDisp> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<GpaBukyokuDisp> list = new ArrayList<GpaBukyokuDisp>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                GpaBukyokuDisp entity = new GpaBukyokuDisp();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "g_bukyokucd":
                            entity.setBukyokucd(rs.getString(sColName));
                            break;
                        case "disp_flg":
                            entity.setDispFlg((Integer)rs.getInt(sColName));
                            break;
                        case "insert_date":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "update_date":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "userid":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
