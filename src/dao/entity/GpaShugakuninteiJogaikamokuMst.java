package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsGpaShugakuninteiJogaikamokuMst;

public class GpaShugakuninteiJogaikamokuMst extends BsGpaShugakuninteiJogaikamokuMst {

    public ArrayList<GpaShugakuninteiJogaikamokuMst> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<GpaShugakuninteiJogaikamokuMst> list = new ArrayList<GpaShugakuninteiJogaikamokuMst>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                GpaShugakuninteiJogaikamokuMst entity = new GpaShugakuninteiJogaikamokuMst();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "YOKEN_NENDO":
                            entity.setYokenNendo(rs.getInt(sColName));
                            break;
                        case "G_BUKYOKUCD":
                            entity.setgBukyokucd(rs.getString(sColName));
                            break;
                        case "KAMOKUCD":
                            entity.setKamokucd(rs.getString(sColName));
                            break;
                        case "KAIKO_KAMOKUNM":
                            entity.setKamokunm(rs.getString(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
