package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsAuthority;

public class Authority extends BsAuthority {

    public ArrayList<Authority> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<Authority> list = new ArrayList<Authority>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                Authority entity = new Authority();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "uid":
                            entity.setUid(rs.getString(sColName));
                            break;
                        case "YOKEN_NENDO":
                            entity.setYokenNendo(rs.getInt(sColName));
                            break;
                        case "DAIGAKUINKBNCD":
                            entity.setDaigakuinKbncd(rs.getString(sColName));
                            break;
                        case "SHOZOKUCD":
                            entity.setShozokucd(rs.getString(sColName));
                            break;
                        case "SYSNO":
                            entity.setSysno(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
