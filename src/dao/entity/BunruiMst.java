package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsBunruiMst;

public class BunruiMst extends BsBunruiMst {

    public ArrayList<BunruiMst> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<BunruiMst> list = new ArrayList<BunruiMst>();

        try {
            // メタデータ取得
            rsm = rs.getMetaData();

            // 取得件数分繰り返す
            while(rs.next()){
                BunruiMst entity = new BunruiMst();

                // 取得項目分繰り返す
                for(int i = 0; i < rsm.getColumnCount(); i++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){

                    }else switch(sColName){
                        case "BUNRUICD":
                            entity.setBunruicd(rs.getString(sColName));
                            break;
                        case "SHOZOKUCD":
                            entity.setShozokucd(rs.getString(sColName));
                            break;
                        case "BUNRUINM":
                            entity.setBunruinm(rs.getString(sColName));
                            break;
                        case "BUNRUINMENG":
                            entity.setBunruinmeng(rs.getString(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }
                list.add(entity);
            }
        } catch (SQLException e) {

        }

        return list;
    }

}
