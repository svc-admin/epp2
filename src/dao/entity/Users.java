package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsUsers;

public class Users extends BsUsers {

    public ArrayList<Users> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<Users> list = new ArrayList<Users>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                Users entity = new Users();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "kid":
                            entity.setKid(rs.getString(sColName));
                            break;
                        case "uid":
                            entity.setUid(rs.getString(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));;
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));;
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
