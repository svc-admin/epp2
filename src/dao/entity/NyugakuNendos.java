package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsNyugakuNendos;

public class NyugakuNendos extends BsNyugakuNendos {

    public ArrayList<NyugakuNendos> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<NyugakuNendos> list = new ArrayList<NyugakuNendos>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                NyugakuNendos entity = new NyugakuNendos();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "NYUGAKU_NENDO":
                            entity.setNyugakuNendo((Integer)rs.getInt(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
