package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsAttachments;

public class Attachments extends BsAttachments {

    public ArrayList<Attachments> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<Attachments> list = new ArrayList<Attachments>();

        try {
            // メタデータ取得
            rsm = rs.getMetaData();

            // 取得件数分繰り返す
            while(rs.next()){
                Attachments entity = new Attachments();

                // 取得項目分繰り返す
                for(int i = 0; i < rsm.getColumnCount(); i++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "id":
                            entity.setId((Integer)rs.getInt(sColName));
                            break;
                        case "G_SYSNO":
                            entity.setgSysno(rs.getString(sColName));
                            break;
                        case "KAMOKUCD":
                            entity.setKamokucd(rs.getString(sColName));
                            break;
                        case "filename":
                            entity.setFilename(rs.getString(sColName));
                            break;
                        case "filepath":
                            entity.setFilepath(rs.getString(sColName));
                            break;
                        case "filesize":
                            entity.setFilesize((Integer)rs.getInt(sColName));
                            break;
                        case "insert_date":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "update_date":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "userid":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }
                list.add(entity);
            }
        } catch (SQLException e) {

        }

        return list;
    }

}
