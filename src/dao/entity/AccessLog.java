package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsAccessLog;

public class AccessLog extends BsAccessLog {

    public ArrayList<AccessLog> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<AccessLog> list = new ArrayList<AccessLog>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                AccessLog entity = new AccessLog();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "kid":
                            entity.setKid(rs.getString(sColName));
                            break;
                        case "targetid":
                            entity.setTargetid(rs.getString(sColName));
                            break;
                        case "ip":
                            entity.setIp(rs.getString(sColName));
                            break;
                        case "pagenm":
                            entity.setPagenm(rs.getString(sColName));
                            break;
                        case "access_date":
                            entity.setAccessDate(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
