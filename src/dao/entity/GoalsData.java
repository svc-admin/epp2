package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsGoalsData;

public class GoalsData extends BsGoalsData {

    public ArrayList<GoalsData> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<GoalsData> list = new ArrayList<GoalsData>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                GoalsData entity = new GoalsData();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "NENDO":
                            entity.setNendo((Integer)rs.getInt(sColName));
                            break;
                        case "JIKANWARI_SHOZOKUCD":
                            entity.setJikanwariShozokucd(rs.getString(sColName));
                            break;
                        case "JIKANWARICD":
                            entity.setJikanwaricd(rs.getString(sColName));
                            break;
                        case "KAISOFLG":
                            entity.setKaisoflg((Integer)rs.getInt(sColName));
                            break;
                        case "GOALCD0":
                            entity.setGoalcd0((Integer)rs.getInt(sColName));
                            break;
                        case "GOALCD1":
                            entity.setGoalcd1((Integer)rs.getInt(sColName));
                            break;
                        case "GOALCD2":
                            entity.setGoalcd2((Integer)rs.getInt(sColName));
                            break;
                        case "VALUE":
                            entity.setValue((Integer)rs.getInt(sColName));
                            break;
                        case "TANISU":
                            entity.setTanisu((Integer)rs.getInt(sColName));
                            break;
                        case "TAISHO_NENJI_ONE":
                            entity.setTaishoNenjiOne((Integer)rs.getInt(sColName));
                            break;
                        case "KAIKOKBNCD":
                            entity.setKaikokbncd((Integer)rs.getInt(sColName));
                            break;
                        case "HISSHUFLG":
                            entity.setHisshuflg((Integer)rs.getInt(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
