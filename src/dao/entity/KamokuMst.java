package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsKamokuMst;

public class KamokuMst extends BsKamokuMst {

    public ArrayList<KamokuMst> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<KamokuMst> list = new ArrayList<KamokuMst>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                KamokuMst entity = new KamokuMst();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "KAMOKUCD":
                            entity.setKamokucd(rs.getString(sColName));
                            break;
                        case "KAMOKUNM":
                            entity.setKamokunm(rs.getString(sColName));
                            break;
                        case "KAMOKUKN":
                            entity.setKamokukn(rs.getString(sColName));
                            break;
                        case "KAMOKUNMENG":
                            entity.setKamokunmeng(rs.getString(sColName));
                            break;
                        case "KAMOKURKNM":
                            entity.setKamokurknm(rs.getString(sColName));
                            break;
                        case "KAMOKU_TANISU":
                            entity.setKamokuTanisu(rs.getBigDecimal(sColName));
                            break;
                        case "KAMOKU_SHOZOKUCD":
                            entity.setKamokuShozokucd(rs.getString(sColName));
                            break;
                        case "NENDO":
                            entity.setNendo((Integer)rs.getInt(sColName));
                            break;
                        case "TANI_NINTEI_KBNCD":
                            entity.setTaniNinteiKbncd((Integer)rs.getInt(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
