package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsShugakusienHanteiShusei;

public class ShugakusienHanteiShusei extends BsShugakusienHanteiShusei {

	public ArrayList<ShugakusienHanteiShusei> setRsToList(ResultSet rs){

		ResultSetMetaData rsm;
		ArrayList<ShugakusienHanteiShusei> list = new ArrayList<ShugakusienHanteiShusei>();

		try {
			//メタデータ取得
			rsm = rs.getMetaData();

			//取得件数分繰り返す
			while (rs.next()){

				ShugakusienHanteiShusei entity = new ShugakusienHanteiShusei();

				//取得項目分繰り返す
				for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
					String sColName = rsm.getColumnName(i + 1);

					if(sColName == null){
						//System.out.println("Null!");
					}else switch(sColName){
						case "G_SYSNO":
							entity.setG_sysno(rs.getString(sColName));
							break;
						case "Z_GPA_RESULT":
							entity.setZ_gpa_result(rs.getString(sColName));
							break;
						case "Z_TANI_RESULT":
							entity.setZ_tani_result(rs.getString(sColName));
							break;
						case "H_SOTSU_RESULT":
							entity.setH_sotsu_result(rs.getString(sColName));
							break;
						case "H_TANI_RESULT":
							entity.setH_tani_result(rs.getString(sColName));
							break;
						case "H_SHU_RESULT":
							entity.setH_shu_result(rs.getString(sColName));
							break;
						case "H_ZEN_RESULT":
							entity.setH_zen_result(rs.getString(sColName));
							break;
						case "K_TANI_RESULT":
							entity.setK_tani_result(rs.getString(sColName));
							break;
						case "K_GPA_RESULT":
							entity.setK_gpa_result(rs.getString(sColName));
							break;
						case "K_SHU_RESULT":
							entity.setK_shu_result(rs.getString(sColName));
							break;
						case "S_TANI_RESULT":
							entity.setS_tani_result(rs.getString(sColName));
							break;
						case "S_SHU_RESULT":
							entity.setS_shu_result(rs.getString(sColName));
							break;
						case "BIKOU":
							entity.setBikou(rs.getString(sColName));
							break;
						case "T_GPA":
							entity.setT_gpa(rs.getBigDecimal(sColName));
							break;
						case "N_GPA":
							entity.setN_gpa(rs.getBigDecimal(sColName));
							break;
						case "INSERT_DATE":
							entity.setInsertDate(rs.getDate(sColName));
							break;
						case "UPDATE_DATE":
							entity.setUpdateDate(rs.getDate(sColName));
							break;
						case "USERID":
							entity.setUserid(rs.getString(sColName));
							break;
						default:
							;
					}
				}

				list.add(entity);
			}
		} catch (SQLException e) {
		}

		return list;

	}

}
