package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsHyojunTanisu;

public class HyojunTanisu extends BsHyojunTanisu {

    public ArrayList<HyojunTanisu> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<HyojunTanisu> list = new ArrayList<HyojunTanisu>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                HyojunTanisu entity = new HyojunTanisu();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "NYUGAKU_NENDO":
                            entity.setNyugakuNendo(rs.getString(sColName));
                            break;
                        case "G_BUKYOKUCD":
                            entity.setgBukyokucd(rs.getString(sColName));
                            break;
                        case "SHOZOKUNM":
                            entity.setgBukyokunm(rs.getString(sColName));
                            break;
                        case "HITSUYOTANISU":
                            entity.setHitsuyotanisu(rs.getBigDecimal(sColName));
                            break;
                        case "SYUGYONENGEN":
                            entity.setSyugyonengen(rs.getInt(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
