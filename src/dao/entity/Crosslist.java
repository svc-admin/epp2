package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsCrosslist;

public class Crosslist extends BsCrosslist {

    public ArrayList<Crosslist> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<Crosslist> list = new ArrayList<Crosslist>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                Crosslist entity = new Crosslist();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "ID":
                            entity.setId(rs.getString(sColName));
                            break;
                        case "SHORT_NAME":
                            entity.setShortName(rs.getString(sColName));
                            break;
                        case "LONG_NAME":
                            entity.setLongName(rs.getString(sColName));
                            break;
                        case "LEVEL":
                            entity.setLevel((Integer)rs.getInt(sColName));
                            break;
                        case "GAKKI_ID":
                            entity.setGakkiId(rs.getString(sColName));
                            break;
                        case "CROSSLIST_ID":
                            entity.setCrosslistId(rs.getString(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
