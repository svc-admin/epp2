package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsShozokuMst;

public class ShozokuMst extends BsShozokuMst {

    public ArrayList<ShozokuMst> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<ShozokuMst> list = new ArrayList<ShozokuMst>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                ShozokuMst entity = new ShozokuMst();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "SHOZOKUCD":
                            entity.setShozokucd(rs.getString(sColName));
                            break;
                        case "SHOZOKUNM1":
                            entity.setShozokunm1(rs.getString(sColName));
                            break;
                        case "SHOZOKUKN1":
                            entity.setShozokukn1(rs.getString(sColName));
                            break;
                        case "SHOZOKUNMENG1":
                            entity.setShozokunmeng1(rs.getString(sColName));
                            break;
                        case "SHOZOKURKNM1":
                            entity.setShozokurknm1(rs.getString(sColName));
                            break;
                        case "SHOZOKUNM2":
                            entity.setShozokunm2(rs.getString(sColName));
                            break;
                        case "SHOZOKUKN2":
                            entity.setShozokukn2(rs.getString(sColName));
                            break;
                        case "SHOZOKUNMENG2":
                            entity.setShozokunmeng2(rs.getString(sColName));
                            break;
                        case "SHOZOKURKNM2":
                            entity.setShozokurknm2(rs.getString(sColName));
                            break;
                        case "SHOZOKUNM3":
                            entity.setShozokunm3(rs.getString(sColName));
                            break;
                        case "SHOZOKUKN3":
                            entity.setShozokukn3(rs.getString(sColName));
                            break;
                        case "SHOZOKUNMENG3":
                            entity.setShozokunmeng3(rs.getString(sColName));
                            break;
                        case "SHOZOKURKNM3":
                            entity.setShozokurknm3(rs.getString(sColName));
                            break;
                        case "SHOZOKUNM4":
                            entity.setShozokunm4(rs.getString(sColName));
                            break;
                        case "SHOZOKUKN4":
                            entity.setShozokukn4(rs.getString(sColName));
                            break;
                        case "SHOZOKUNMENG4":
                            entity.setShozokunmeng4(rs.getString(sColName));
                            break;
                        case "SHOZOKURKNM4":
                            entity.setShozokurknm4(rs.getString(sColName));
                            break;
                        case "SHOZOKUNM5":
                            entity.setShozokunm5(rs.getString(sColName));
                            break;
                        case "SHOZOKUKN5":
                            entity.setShozokukn5(rs.getString(sColName));
                            break;
                        case "SHOZOKUNMENG5":
                            entity.setShozokunmeng5(rs.getString(sColName));
                            break;
                        case "SHOZOKURKNM5":
                            entity.setShozokurknm5(rs.getString(sColName));
                            break;
                        case "ZAIGAKUNENGEN":
                            entity.setZaigakunengen((Integer)rs.getInt(sColName));
                            break;
                        case "KAISOFLG":
                            entity.setKaisoflg((Integer)rs.getInt(sColName));
                            break;
                        case "NYUDISPGRPCD":
                            entity.setNyudispgrpcd(rs.getString(sColName));
                            break;
                        case "KYODISPGRPCD":
                            entity.setKyodispgrpcd(rs.getString(sColName));
                            break;
                        case "SHORTRKNM":
                            entity.setShortrknm(rs.getString(sColName));
                            break;
                        case "BUKYOKUKBNCD":
                            entity.setBukyokukbncd(rs.getString(sColName));
                            break;
                        case "GAKUSEIFLG":
                            entity.setGakuseiflg((Integer)rs.getInt(sColName));
                            break;
                        case "KYOKANFLG":
                            entity.setKyokanflg((Integer)rs.getInt(sColName));
                            break;
                        case "SHOKUINFLG":
                            entity.setShokuinflg((Integer)rs.getInt(sColName));
                            break;
                        case "DAIGAKUINKBNCD":
                            entity.setDaigakuinkbncd(rs.getString(sColName));
                            break;
// 2018.06.19 SVC)S.Furuta ADD START >> 部局計算GPAの表示対応
                            // gpa_bukyoku_disp の表示On/Offフラグをjoinするために追記
                        case "disp_flg":
                            entity.setDispflg(rs.getInt(sColName));
                            break;
// 2018.06.19 SVC)S.Furuta ADD END <<
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
