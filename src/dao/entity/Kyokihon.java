package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsKyokihon;

public class Kyokihon extends BsKyokihon {

    public ArrayList<Kyokihon> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<Kyokihon> list = new ArrayList<Kyokihon>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                Kyokihon entity = new Kyokihon();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "G_SYSNO":
                            entity.setgSysno(rs.getString(sColName));
                            break;
                        case "G_NAME":
                            entity.setgName(rs.getString(sColName));
                            break;
                        case "G_NAMERK":
                            entity.setgNamerk(rs.getString(sColName));
                            break;
                        case "G_KANA":
                            entity.setgKana(rs.getString(sColName));
                            break;
                        case "G_NAMEENG":
                            entity.setgNameeng(rs.getString(sColName));
                            break;
                        case "G_SEX":
                            entity.setgSex(rs.getString(sColName));
                            break;
                        case "G_BIRTH":
                            entity.setgBirth(rs.getDate(sColName));
                            break;
                        case "G_YUBIN":
                            entity.setgYubin(rs.getString(sColName));
                            break;
                        case "G_ADD1":
                            entity.setgAdd1(rs.getString(sColName));
                            break;
                        case "G_ADD2":
                            entity.setgAdd2(rs.getString(sColName));
                            break;
                        case "G_ADD3":
                            entity.setgAdd3(rs.getString(sColName));
                            break;
                        case "G_TEL":
                            entity.setgTel(rs.getString(sColName));
                            break;
                        case "G_TELKBNCD":
                            entity.setgTelkbncd(rs.getString(sColName));
                            break;
                        case "G_KEITAITEL":
                            entity.setgKeitaitel(rs.getString(sColName));
                            break;
                        case "G_EMAIL":
                            entity.setgEmail(rs.getString(sColName));
                            break;
                        case "G_KENCD":
                            entity.setgKencd(rs.getString(sColName));
                            break;
                        case "JUKYOKBNCD":
                            entity.setJukyokbncd(rs.getString(sColName));
                            break;
                        case "KOKUSEKICD":
                            entity.setKokusekicd(rs.getString(sColName));
                            break;
                        case "HONSEKICD":
                            entity.setHonsekicd(rs.getString(sColName));
                            break;
                        case "HONSEKIADD1":
                            entity.setHonsekiadd1(rs.getString(sColName));
                            break;
                        case "HONSEKIADD2":
                            entity.setHonsekiadd2(rs.getString(sColName));
                            break;
                        case "HONSEKIADD3":
                            entity.setHonsekiadd3(rs.getString(sColName));
                            break;
                        case "BIKO":
                            entity.setBiko(rs.getString(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
