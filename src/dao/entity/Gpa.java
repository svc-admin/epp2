package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsGpa;

public class Gpa extends BsGpa {

    public ArrayList<Gpa> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<Gpa> list = new ArrayList<Gpa>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                Gpa entity = new Gpa();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "uid":
                            entity.setUid(rs.getString(sColName));
                            break;
                        case "NINTEI_NENDO":
                            entity.setNinteiNendo((Integer)rs.getInt(sColName));
                            break;
                        case "semester":
                            entity.setSemester((Integer)rs.getInt(sColName));
                            break;
                        case "goal":
                            entity.setGoal((Integer)rs.getInt(sColName));
                            break;
                        case "gpa":
                            entity.setGpa(rs.getBigDecimal(sColName));
                            break;
                        case "credit":
                            entity.setCredit(rs.getBigDecimal(sColName));
                            break;
                        case "gpt":
                            entity.setGpt(rs.getBigDecimal(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
