package dao.entity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsRestriction;

public class Restriction extends BsRestriction {

    public ArrayList<Restriction> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<Restriction> list = new ArrayList<Restriction>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){

                Restriction entity = new Restriction();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "rid":
                            entity.setRid((Integer)rs.getInt(sColName));
                            break;
                        case "mode":
                            entity.setMode((Integer)rs.getInt(sColName));
                            break;
                        case "flg":
                            entity.setFlg((Integer)rs.getInt(sColName));
                            break;
                        case "id":
                            entity.setId(rs.getString(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
        }

        return list;

    }

}
