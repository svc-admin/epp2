package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.StudyingAbroadExperieces;
import dto.ParameterDto;

public class StudyingAbroadExperiencesDao extends AbstractDao<StudyingAbroadExperieces> {

    private static Logger logger = null;

    public StudyingAbroadExperiencesDao() {
        super();
    }

    @Override
    ArrayList<StudyingAbroadExperieces> setEntityList(ResultSet rs) {
    	StudyingAbroadExperieces entity = new StudyingAbroadExperieces();
        return entity.setRsToList(rs);
    }

    public Boolean addStudyingAbroadExperieces01(StudyingAbroadExperieces dto, String gSysno, String kamokucd, String ruser) {

        Connection conn = null;
        PreparedStatement pstmt = null;

        String strSql = "insert into studying_abroad_experiences (G_SYSNO,KAMOKUCD";

        String strPram = ")values('" + gSysno + "'" + ",'" + kamokucd + "'";

        strSql = strSql + ",purpose_type";
        strPram = strPram + ",'" + dto.getPurposeType() + "'";
        strSql = strSql + ",nation";
        strPram = strPram + ",'" + dto.getNation() + "'";
        strSql = strSql + ",date_from";
        strPram = strPram + ",'" + dto.getDateFrom() + "'";
        strSql = strSql + ",date_to";
        strPram = strPram + ",'" + dto.getDateTo() + "'";
        strSql = strSql + ",destination";
        strPram = strPram + ",'" + dto.getDestination() + "'";

        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
        strSql = strSql + ",INSERT_DATE";
// 2019.02.01 s.furuta chg start 2.学修成果修正
//        strPram = strPram + ",'" + today + "'";
        strPram = strPram + ",'" + dto.getInsertDate() + "'";
// 2019.02.01 s.furuta chg end
        strSql = strSql + ",UPDATE_DATE";
        strPram = strPram + ",'" + today + "'";
        strSql = strSql + ",USERID";
        strPram = strPram + ",'" + ruser + "'";

        strSql = strSql + strPram + ")";

        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return true;
    }

// 2019.01.17 s.furuta add start 2.学修成果修正
    public ArrayList<StudyingAbroadExperieces> getStudyingAbroadExperiences01(ParameterDto paramDto) {

        //外だしSQLの取得
        String strSql = getSql("StudyingAbroadExperiencesDao_getStudyingAbroadExperiences01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(new String(paramDto.getUid().replace("-", "").toLowerCase()));
        //2.科目コード
        valueList.add(new String(paramDto.getKamokuCd()));
        return get(paramDto.getRuser(),strSql,valueList);
    }
    public ArrayList<StudyingAbroadExperieces> getStudyingAbroadExperiences02(ParameterDto paramDto) {

        //外だしSQLの取得
        String strSql = getSql("StudyingAbroadExperiencesDao_getStudyingAbroadExperiences02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(new String(paramDto.getUid().replace("-", "").toLowerCase()));
        return get(paramDto.getRuser(),strSql,valueList);
    }
    /**
     * ParameterDtoを引数として1件削除
     * @param paramDto
     * @return
     */
    public int deleteStudyingAbroadExperiences01(ParameterDto paramDto){
        // SQLの取得
        String strSql = getSql("StudyingAbroadExperiencesDao_deleteStudyingAbroadExperiences01.sql");

        // SQLの?に設定する値をListに順に設定する
        List<Object> valueList = new ArrayList<Object>();
        // 1.学生番号
        valueList.add(paramDto.getUid().replace("-", "").toLowerCase());
        // 2.科目コード
        valueList.add(paramDto.getKamokuCd());

        return update(paramDto.getRuser(),strSql,valueList);
    }
// 2019.01.17 s.furuta add end

}
