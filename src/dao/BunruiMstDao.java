package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.BunruiMst;
import dto.ParameterDto;

/**
 * @author nakano@cc.kumamoto-u.ac.jp
 * 2015-03-17 nakano@cc.kumamoto-u.ac.jp
 *
 */
public class BunruiMstDao extends AbstractDao<BunruiMst> {

    public BunruiMstDao() {
        super();
    }

    @Override
    ArrayList<BunruiMst> setEntityList(ResultSet rs){
        BunruiMst entity = new BunruiMst();
        return entity.setRsToList(rs);
    }

    /**
     * 登録されている情報からリスト用のデータ取得
     * @param ruser
     * @return
     */
    public ArrayList<BunruiMst> getBunruiMst01(ParameterDto paramDto){
        // SQLの取得
        String strSql = getSql("BunruiMstDao_getBunruiMst01.sql");

        // SQLの?に設定する値をListに設定
        List<Object> valueList = new ArrayList<Object>();
        valueList.add("00");	// 所属コード"00"は必ず出す
        valueList.add(paramDto.getShozokucd());

        return get(paramDto.getRuser(), strSql, valueList);
    }

    /**
     * 分類名からコードを取得
     * @param ruser
     * @param bunruinm
     * @return
     */
    public ArrayList<BunruiMst> getBunruiMst02(String ruser, String bunruinm){
        // SQLの取得
        String strSql = getSql("BunruiMstDao_getBunruiMst02.sql");

        // SQLの?に設定する値をListに設定
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(bunruinm);
        valueList.add(bunruinm);

        return get(ruser, strSql, valueList);
    }

}
