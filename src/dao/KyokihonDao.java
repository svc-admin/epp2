package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.Kyokihon;
import dto.ParameterDto;

public class KyokihonDao extends AbstractDao<Kyokihon> {

    public KyokihonDao() {
        super();
    }

    @Override
    ArrayList<Kyokihon> setEntityList(ResultSet rs) {
        Kyokihon entity = new Kyokihon();
        return entity.setRsToList(rs);
    }

    public ArrayList<Kyokihon> getKyokihonList01(ParameterDto pramDto,String uid) {

        //外だしSQLの取得
        String strSql = getSql("KyokihonDao_getKyokihonList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(uid);

        return get(pramDto.getRuser(),strSql,valueList);
    }
}
