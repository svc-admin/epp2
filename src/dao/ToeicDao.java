package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.Toeic;
import dto.ParameterDto;

public class ToeicDao extends AbstractDao<Toeic> {

    public ToeicDao() {
        super();
    }

    @Override
    ArrayList<Toeic> setEntityList(ResultSet rs) {
        Toeic entity = new Toeic();
        return entity.setRsToList(rs);
    }

    public ArrayList<Toeic> getToeicList01(ParameterDto pramDto) {
        //外だしSQLの取得
        String strSql = getSql("ToeicDao_getToeicList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生ID
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<Toeic> getToeicList02(ParameterDto pramDto) {
        //外だしSQLの取得
        String strSql = getSql("ToeicDao_getToeicList02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生ID
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    /**
     * 年が新しい順(最も最近の順)に指定した学生のデータ取得
     * @param pramDto
     * @return
     */
    public ArrayList<Toeic> getToeicList3(ParameterDto pramDto, String uid) {
      //外だしSQLの取得
      String strSql = getSql("ToeicDao_getToeicList03.sql");

      //SQLの?に設定する値をListに順に設定する。
      List<Object> valueList = new ArrayList<Object>();
      //1.学生ID
      valueList.add(uid);

      return get(pramDto.getRuser(),strSql,valueList);
  }

}
