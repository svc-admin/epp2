package dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import dao.entity.Crosslist;

public class CrosslistDao extends AbstractDao<Crosslist> {

    public CrosslistDao() {
        super();
    }

    @Override
    ArrayList<Crosslist> setEntityList(ResultSet rs) {
        Crosslist entity = new Crosslist();
        return entity.setRsToList(rs);
    }

}
