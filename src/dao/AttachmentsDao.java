package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.Attachments;
import dto.ParameterDto;

public class AttachmentsDao extends AbstractDao<Attachments> {

    private static Logger logger = null;

    public AttachmentsDao() {
        super();
    }

    @Override
    ArrayList<Attachments> setEntityList(ResultSet rs) {
    	Attachments entity = new Attachments();
        return entity.setRsToList(rs);
    }

    public Boolean addAttachments01(ParameterDto pramDto) {

        Boolean res = false;

        Connection conn = null;
        PreparedStatement pstmt = null;

        String strSql = "insert into attachments (G_SYSNO,KAMOKUCD";

        String strPram = ")values('" + pramDto.getAttachments().getgSysno() + "'" + ",'" + pramDto.getAttachments().getKamokucd() + "'";

        if(pramDto.getAttachments().getFilename().equals("") == false){
            strSql = strSql + ",filename";
            strPram = strPram + ",'" + pramDto.getAttachments().getFilename() + "'";
        }
        if(pramDto.getAttachments().getFilepath().equals("") == false){
            strSql = strSql + ",filepath";
            strPram = strPram + ",'" + pramDto.getAttachments().getFilepath() + "'";
        }
        strSql = strSql + ",filesize";
        strPram = strPram + "," + pramDto.getAttachments().getFilesize() + "";

        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
        strSql = strSql + ",INSERT_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",UPDATE_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",USERID";
        strPram = strPram + ",'" + pramDto.getRuser() + "'";

        strSql = strSql + strPram + ")";

        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            int num = pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return true;
    }
// 2019.01.17 s.furuta add start 2.学修成果修正
    public ArrayList<Attachments> getAttachments01(ParameterDto paramDto) {

        //外だしSQLの取得
        String strSql = getSql("AttachmentsDao_getAttachments01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(new String(paramDto.getUid().replace("-", "").toLowerCase()));
        //2.科目コード
        valueList.add(new String(paramDto.getKamokuCd()));
        return get(paramDto.getRuser(),strSql,valueList);
    }
    /**
     * ParameterDtoを引数として1件削除
     * @param paramDto
     * @return
     */
    public int deleteAttachments01(ParameterDto paramDto){
        // SQLの取得
        String strSql = getSql("AttachmentsDao_deleteAttachments01.sql");

        // SQLの?に設定する値をListに順に設定する
        List<Object> valueList = new ArrayList<Object>();
        // 1.ID
        valueList.add(paramDto.getAttachments().getId());
        // 2.学生番号
        valueList.add(paramDto.getUid().replace("-", "").toLowerCase());
        // 3.科目コード
        valueList.add(paramDto.getKamokuCd());

        return update(paramDto.getRuser(),strSql,valueList);
    }
    /**
     * ParameterDtoを引数としてファイルパス、更新日、ユーザを更新
     * @param paramDto
     * @return
     */
    public int updateAttachments01(ParameterDto paramDto){
        // SQLの取得
        String strSql = getSql("AttachmentsDao_updateAttachments01.sql");

        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());

        // SQLの?に設定する値をListに順に設定する
        List<Object> valueList = new ArrayList<Object>();
        // 1.ファイルパス
        valueList.add(paramDto.getAttachments().getFilepath());
        // 2.更新日
        valueList.add(today);
        // 3.ユーザ
        valueList.add(paramDto.getRuser());
        // 4.ID
        valueList.add(paramDto.getAttachments().getId());

        return update(paramDto.getRuser(),strSql,valueList);
    }
// 2019.01.17 s.furuta add end
}
