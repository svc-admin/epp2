package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.CocAdminister;
import dto.ParameterDto;

public class CocAdministerDao extends AbstractDao<CocAdminister> {

    private static Logger logger = null;

    public CocAdministerDao() {
        super();
    }

    @Override
    ArrayList<CocAdminister> setEntityList(ResultSet rs) {
        CocAdminister entity = new CocAdminister();
        return entity.setRsToList(rs);
    }

    public ArrayList<CocAdminister> cocTargetList01(ParameterDto pramDto){

        //外だしSQLの取得
        String strSql = getSql("CocAdministerDao_getCoc01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();

        return get(pramDto.getRuser(),strSql,valueList);
    }
    public ArrayList<CocAdminister> cocStatusList01(ParameterDto pramDto, ArrayList<String> stepList){
        //SQLの取得
        String strSql = "select * from (select distinct E.GAKUNO, A.G_SYSNO, C.G_NAME, C.G_NAMEENG, D.SHOZOKUNM, D.SHOZOKUNMENG, A.COC_KBN ";
        for(int i =0; i < stepList.size(); i++){
            strSql += ",(select TANISU from coc_status where COC_KBN = '" + pramDto.getCocOrPlus() + "' and STEP = '" + stepList.get(i) + "' and G_SYSNO = A.G_SYSNO ) as STEP" + stepList.get(i) + " ";
        }
        strSql += "from coc_status A ";
        strSql += "left join gakuseki B on A.G_SYSNO = B.G_SYSNO ";
        strSql += "left join kyokihon C on A.G_SYSNO = C.G_SYSNO ";
        strSql += "left join nendobetu_shozoku D on B.G_SHOZOKUCD = D.SHOZOKUCD and B.NYUGAKU_NENDO = D.NYUGAKU_NENDO ";
        strSql += "left join coc_target E on A.G_SYSNO = E.GAKUNO and A.COC_KBN = E.KUBUN ";
        strSql += ") T where T.COC_KBN = " + pramDto.getCocOrPlus() + " ";
        //対象ステップが存在しない場合は1件も取得しない
        if(stepList.isEmpty()){
            strSql += "and T.STEP = 0 ";
        }
        strSql += pramDto.getSearchCon();

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();

        return get(pramDto.getRuser(),strSql,valueList);
    }
    public int addCoc01(ParameterDto pramDto){

        String gakuno =pramDto.getUid();
        String ind = pramDto.getCocOrPlus();
        //外だしSQLの取得
        String strSql = getSql("CocAdministerDao_insertCoc01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
            valueList.add(gakuno);
        //2.COC/COC+区分
            valueList.add(ind);
        //3.備考
            valueList.add("");
        //4.ユーザーID
            valueList.add(pramDto.getRuser());

        return update(pramDto.getRuser(),strSql,valueList);
    }
    public int deleteCoc01(ParameterDto pramDto){

        //外だしSQLの取得
        String strSql = getSql("CocAdministerDao_deleteCoc01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
            valueList.add(pramDto.getUid());
        //2.COCまたはCOC+対象学生
            valueList.add(pramDto.getCocOrPlus());

        return update(pramDto.getRuser(),strSql,valueList);
    }
    public int editCoc01(ParameterDto pramDto){

        String setKbn = pramDto.getShozokucd();
        String bikou = pramDto.getSearchCon();
        String gakuno = pramDto.getUid();
        String whereKbn = pramDto.getCocOrPlus();

        //外だしSQLの取得
        String strSql = getSql("CocAdministerDao_updateCoc01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.変更後COC/COC+区分（set）
            valueList.add(setKbn);
        //2.備考
            valueList.add(bikou);
        //3.学生番号
            valueList.add(gakuno);
        //4.変更前COC/COC+区分（where）
            valueList.add(whereKbn);

        return update(pramDto.getRuser(),strSql,valueList);
    }
    public ArrayList<CocAdminister> verificationCoc01(ParameterDto pramDto){

        //外だしSQLの取得
        String strSql = getSql("CocAdministerDao_verificationCoc01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
            valueList.add(pramDto.getUid());

        ArrayList<CocAdminister> getList = get(pramDto.getRuser(),strSql,valueList);

        if(getList.isEmpty()){
            return null;
        }
        return getList;
    }
    public ArrayList<CocAdminister> verificationCoc02(ParameterDto pramDto){

        //外だしSQLの取得
        String strSql = getSql("CocAdministerDao_verificationCoc02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
            valueList.add(pramDto.getUid());
        //2.COC区分
            valueList.add(pramDto.getCocOrPlus());

        ArrayList<CocAdminister> getList = get(pramDto.getRuser(),strSql,valueList);

        if(getList.isEmpty()){
            return null;
        }
        return getList;
    }
    public int addMes01(ParameterDto pramDto){

        //外だしSQLの取得
        String strSql = getSql("CocAdministerDao_insertMes01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.メッセージ
            valueList.add(pramDto.getSearchCon());
        //2.ユーザーID
            valueList.add(pramDto.getRuser());

        return update(pramDto.getRuser(),strSql,valueList);
    }
    public int deleteMes01(ParameterDto pramDto){

        //外だしSQLの取得
        String strSql = getSql("CocAdministerDao_deleteMes01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.ユーザーID
            valueList.add(pramDto.getRuser());

        return update(pramDto.getRuser(),strSql,valueList);
    }
    public ArrayList<CocAdminister> getMes01(ParameterDto pramDto){

        //外だしSQLの取得
        String strSql = getSql("CocAdministerDao_getMes01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.ユーザーID
            valueList.add(pramDto.getRuser());

        return get(pramDto.getRuser(),strSql,valueList);
    }
}