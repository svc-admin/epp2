package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.Gakuseki;
import dto.ParameterDto;

/**
 *　2014-11-07: add getGakusekiList07(ParameterDto pramDto) by nakano@cc.kumamoto-u.ac.jp
 */
public class GakusekiDao extends AbstractDao<Gakuseki> {

    public GakusekiDao() {
        super();
    }

    @Override
    ArrayList<Gakuseki> setEntityList(ResultSet rs) {
        Gakuseki entity = new Gakuseki();
        return entity.setRsToList(rs);
    }

    public ArrayList<Gakuseki> getGakusekiList01(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GakusekiDao_getGakusekiList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<Gakuseki> getGakusekiList02(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GakusekiDao_getGakusekiList02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.所属課コード(後方一致)
        valueList.add(likeEscape(pramDto.getShozokucd())+"%");
        //2.入学年度
        valueList.add(pramDto.getNyugakunendo());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<Gakuseki> getGakusekiList03(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GakusekiDao_getGakusekiList03.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<Gakuseki> getGakusekiList04(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GakusekiDao_getGakusekiList04.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<Gakuseki> getGakusekiList05(String ruser, String shozoku,
            int nyugakuNendo) {

        //外だしSQLの取得
        String strSql = getSql("GakusekiDao_getGakusekiList05.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.所属課コード(後方一致)
        valueList.add(likeEscape(shozoku)+"%");
        //2.入学年度
        valueList.add(new Integer(nyugakuNendo));

        return get(ruser,strSql,valueList);
    }

    public ArrayList<Gakuseki> getGakusekiList06(ParameterDto pramDto,String uid) {

        //外だしSQLの取得
        String strSql = getSql("GakusekiDao_getGakusekiList06.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(uid);

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<Gakuseki> getGakusekiList07(ParameterDto pramDto) {

      //外だしSQLの取得 全ての G_SYSNO を取得
      String strSql = getSql("GakusekiDao_getGakusekiList07.sql");
      List<Object> valueList = new ArrayList<Object>();

      return get(pramDto.getRuser(),strSql,valueList);
  }

// 2018.06.04 SVC)S.Furuta ADD START >> アクセス制限設定機構の改善対応
    public ArrayList<Gakuseki> getGakusekiList08(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GakusekiDao_getGakusekiList08.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.所属課コード(後方一致)
        valueList.add(likeEscape(pramDto.getShozokucd())+"%");
        //2.入学年度
        valueList.add(pramDto.getNyugakunendo());

        return get(pramDto.getRuser(),strSql,valueList);
    }
// 2018.06.04 SVC)S.Furuta ADD END

// 2019.01.09 s.furuta add start 1.学生番号検索機能追加
    public ArrayList<Gakuseki> getGakusekiList09(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GakusekiDao_getGakusekiList09.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.検索学生番号
        valueList.add(pramDto.getSearchId().replace("-", "").toLowerCase());

        return get(pramDto.getRuser(),strSql,valueList);
    }
    public ArrayList<Gakuseki> getGakusekiList10(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GakusekiDao_getGakusekiList10.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.検索学生番号
        valueList.add(pramDto.getSearchId().replace("-", "").toLowerCase());

        return get(pramDto.getRuser(),strSql,valueList);
    }
// 2019.01.09 s.furuta add end

}
