package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.RestrictionGroup;

public class RestrictionGroupDao extends AbstractDao<RestrictionGroup> {

    public RestrictionGroupDao() {
        super();
    }

    @Override
    ArrayList<RestrictionGroup> setEntityList(ResultSet rs) {
        RestrictionGroup entity = new RestrictionGroup();
        return entity.setRsToList(rs);
    }

    public ArrayList<RestrictionGroup> getRestrictionGroupList01(String ruser, String id) {
        //外だしSQLの取得
        String strSql = getSql("RestrictionGroupDao_getRestrictionGroupList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.グループID
        valueList.add(id);

        return get(ruser,strSql,valueList);
    }

// 2018.06.04 SVC)S.Furuta ADD START >> アクセス制限設定機構の改善対応
    public ArrayList<RestrictionGroup> getRestrictionGroupList02(String ruser) {
        //外だしSQLの取得
        String strSql = getSql("RestrictionGroupDao_getRestrictionGroupList02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.ユーザID（管理者ID）
        valueList.add(ruser);

        return get(ruser,strSql,valueList);
    }
// 2018.06.04 SVC)S.Furuta ADD END <<

}
