package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.ShugakusienHanteiData;
import dao.entity.ShugakusienJokyo;
import dto.ParameterDto;

public class ShugakusienJokyoDao extends AbstractDao<ShugakusienJokyo> {

	private static Logger logger = null;

	public ShugakusienJokyoDao() {
		super();
	}

	@Override
	ArrayList<ShugakusienJokyo> setEntityList(ResultSet rs) {
		ShugakusienJokyo entity = new ShugakusienJokyo();
		return entity.setRsToList(rs);
	}

	/**
	 * 支援状況テーブルの削除/登録を行う
	 * @param paramDto
	 * @param dataList
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public Boolean registSupportStatus(ParameterDto paramDto, ArrayList<ShugakusienHanteiData> dataList, Connection conn) throws Exception {
		Boolean ret = false;

		// 支援状況の削除を実施
		for (ShugakusienHanteiData data : dataList) {
			if ((data.getZ_judge() != null && !"".equals(data.getZ_judge())) ||
					(data.getT_judge() != null && !"".equals(data.getT_judge()))) {
				delSupportStatus(paramDto, data, conn);
			}
		}

		// 支援状況の削除を実施
		for (ShugakusienHanteiData data : dataList) {
			if ((data.getZ_judge() != null && !"".equals(data.getZ_judge())) ||
					(data.getT_judge() != null && !"".equals(data.getT_judge()))) {
				addSupportStatus(paramDto, data, conn);
			}
		}

		ret = true;

		return ret;
	}

	/**
	 * 支援状況テーブルからデータを削除する
	 * @param paramDto
	 * @param data
	 * @param conn
	 * @return
	 */
	public Boolean delSupportStatus(ParameterDto paramDto, ShugakusienHanteiData data, Connection conn) {

		Boolean ret = false;
		PreparedStatement pstmt = null;

		String strSql = "";
		strSql += " DELETE FROM shugakusien_jokyo";
		strSql += " WHERE";
		strSql += "     SHORI_NENDO = ?";
		strSql += " AND G_GAKUNO = ?";

		try {
			pstmt = conn.prepareStatement(strSql);
			List<Object> valueList = new ArrayList<Object>();
			valueList.add(paramDto.getShoriJokenMst().getShorinendo());
			valueList.add(data.getG_gakuno());

			// パラメータを設定
			setPreparedStatement(pstmt, valueList);

			// SQL実行
			pstmt.executeUpdate();

			ret = true;
		} catch (Exception ex) {
			logger.error(ex);
		}
		return ret;
	}

	/**
	 * 支援状況テーブルにデータを登録する
	 * @param paramDto
	 * @param data
	 * @param conn
	 * @return
	 */
	public Boolean addSupportStatus(ParameterDto paramDto, ShugakusienHanteiData data, Connection conn) {

		Boolean ret = false;
		PreparedStatement pstmt = null;
		java.sql.Date today = new java.sql.Date(System.currentTimeMillis());

		String strSql = "";
		strSql += " INSERT INTO shugakusien_jokyo (";
		strSql += "  SHORI_NENDO";
		strSql += " ,G_GAKUNO";
		strSql += " ,SIEN_JOKYO";
		strSql += " ,INSERT_DATE";
		strSql += " ,UPDATE_DATE";
		strSql += " ,USERID";
		strSql += " ) VALUES (";
		strSql += "  ?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " )";

		try {
			pstmt = conn.prepareStatement(strSql);
			List<Object> valueList = new ArrayList<Object>();
			valueList.add(paramDto.getShoriJokenMst().getShorinendo());
			valueList.add(data.getG_gakuno());
			if (data.getZ_judge() != null && !"".equals(data.getZ_judge())) {
				valueList.add(data.getZ_judge());
			} else {
				valueList.add(data.getT_judge());
			}
			valueList.add(today);
			valueList.add(today);
			valueList.add(paramDto.getRuser());

			// パラメータを設定
			setPreparedStatement(pstmt, valueList);

			// SQL実行
			pstmt.executeUpdate();

			ret = true;
		} catch (Exception ex) {
			logger.error(ex);
		}
		return ret;
	}
}