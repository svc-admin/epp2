package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.KussShinro;
import dto.ParameterDto;

public class KussShinroDao extends AbstractDao<KussShinro> {

    public KussShinroDao() {
        super();
    }

    @Override
    ArrayList<KussShinro> setEntityList(ResultSet rs) {
        KussShinro entity = new KussShinro();
        return entity.setRsToList(rs);
    }

    public ArrayList<KussShinro> getKussShinroList01(ParameterDto pramDto,String uid) {

        //外だしSQLの取得
        String strSql = getSql("KussShinroDao_getKussShinroList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(uid);

        return get(pramDto.getRuser(),strSql,valueList);
    }

}
