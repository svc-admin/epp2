package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.ShutokutaniTaishoMst;
import dto.ParameterDto;

public class ShutokutaniTaishoMstDao extends AbstractDao<ShutokutaniTaishoMst> {

    private static Logger logger = null;

    public ShutokutaniTaishoMstDao() {
        super();
    }

    @Override
    ArrayList<ShutokutaniTaishoMst> setEntityList(ResultSet rs) {
    	ShutokutaniTaishoMst entity = new ShutokutaniTaishoMst();
        return entity.setRsToList(rs);
    }

    public ArrayList<ShutokutaniTaishoMst> getShutokutaniTaishoMst() {

    	String strSql = getSql("ShutokutaniTaishoMstDao_getShutokutaniTaishoMst.sql");
    	List<Object> valueList = new ArrayList<Object>();
    	return get("",strSql,valueList);
    }


    public Boolean deleteShutokutaniTaishoMstDao01(ParameterDto pramDto) {

        Connection conn = null;
        PreparedStatement pstmt = null;

        String strSql = "delete from shutokutani_taisho_mst";
        String strWhere = "";

        if(pramDto.getShutokutaniTaishoMst().getKamokudkbncd() != null){
            strWhere = strWhere + " where KAMOKUDKBNCD = '" + pramDto.getShutokutaniTaishoMst().getKamokudkbncd() + "' ";
        }
        if(pramDto.getShutokutaniTaishoMst().getKamokum_shozokucd() != null){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "KAMOKUM_SHOZOKUCD = '" + pramDto.getShutokutaniTaishoMst().getKamokum_shozokucd() + "' ";
        }
        if(pramDto.getShutokutaniTaishoMst().getKamokumkbncd() != null){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "KAMOKUMKBNCD = '" + pramDto.getShutokutaniTaishoMst().getKamokumkbncd() + "' ";
        }
        if(pramDto.getShutokutaniTaishoMst().getKamokus_shozokucd() != null){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "KAMOKUS_SHOZOKUCD = '" + pramDto.getShutokutaniTaishoMst().getKamokus_shozokucd() + "' ";
        }
        if(pramDto.getShutokutaniTaishoMst().getKamokuskbncd() != null){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "KAMOKUSKBNCD = '" + pramDto.getShutokutaniTaishoMst().getKamokuskbncd() + "' ";
        }
        if(pramDto.getShutokutaniTaishoMst().getKamokuss_shozokucd() != null){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "KAMOKUSS_SHOZOKUCD = '" + pramDto.getShutokutaniTaishoMst().getKamokuss_shozokucd() + "' ";
        }
        if(pramDto.getShutokutaniTaishoMst().getKamokusskbncd() != null){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "KAMOKUSSKBNCD = '" + pramDto.getShutokutaniTaishoMst().getKamokusskbncd() + "' ";
        }
        // queryの生成
        strSql = strSql + strWhere;

        System.out.println(strSql);

        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return true;
    }

    public Boolean addShutokoutaniTaishoMstDao01(ParameterDto pramDto) {

        Connection conn = null;
        PreparedStatement pstmt = null;

        String strSql = "insert into shutokutani_taisho_mst (KAMOKUDKBNCD";

        String strPram = ")values(" ;

        strPram = strPram + "'" + pramDto.getShutokutaniTaishoMst().getKamokudkbncd() + "'";
        if(pramDto.getShutokutaniTaishoMst().getKamokum_shozokucd() != null){
            strSql = strSql + ",KAMOKUM_SHOZOKUCD";
            strPram = strPram + ",'" + pramDto.getShutokutaniTaishoMst().getKamokum_shozokucd() + "'";
        }
        if(pramDto.getShutokutaniTaishoMst().getKamokumkbncd() != null){
            strSql = strSql + ",KAMOKUMKBNCD";
            strPram = strPram + ",'" + pramDto.getShutokutaniTaishoMst().getKamokumkbncd() + "'";
        }
        if(pramDto.getShutokutaniTaishoMst().getKamokus_shozokucd() != null){
            strSql = strSql + ",KAMOKUS_SHOZOKUCD";
            strPram = strPram + ",'" + pramDto.getShutokutaniTaishoMst().getKamokus_shozokucd() + "'";
        }
        if(pramDto.getShutokutaniTaishoMst().getKamokuskbncd() != null){
            strSql = strSql + ",KAMOKUSKBNCD";
            strPram = strPram + ",'" + pramDto.getShutokutaniTaishoMst().getKamokuskbncd() + "'";
        }
        if(pramDto.getShutokutaniTaishoMst().getKamokuss_shozokucd() != null){
            strSql = strSql + ",KAMOKUSS_SHOZOKUCD";
            strPram = strPram + ",'" + pramDto.getShutokutaniTaishoMst().getKamokuss_shozokucd() + "'";
        }
        if(pramDto.getShutokutaniTaishoMst().getKamokusskbncd() != null){
            strSql = strSql + ",KAMOKUSSKBNCD";
            strPram = strPram + ",'" + pramDto.getShutokutaniTaishoMst().getKamokusskbncd() + "'";
        }

        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
        strSql = strSql + ",INSERT_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",UPDATE_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",USERID";
        strPram = strPram + ",'" + pramDto.getRuser() + "'";

        strSql = strSql + strPram + ")";


        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return true;
    }

}
