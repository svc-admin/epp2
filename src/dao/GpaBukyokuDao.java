package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.GpaBukyoku;
import dto.ParameterDto;

public class GpaBukyokuDao extends AbstractDao<GpaBukyoku> {

    public GpaBukyokuDao() {
        super();
    }

    @Override
    ArrayList<GpaBukyoku> setEntityList(ResultSet rs) {
        GpaBukyoku entity = new GpaBukyoku();
        return entity.setRsToList(rs);
    }

    public ArrayList<GpaBukyoku> getGpaBukyokuList01(ParameterDto pramDto, String uid) {

        //外だしSQLの取得
        String strSql = getSql("GpaBukyokuDao_getGpaBukyokuList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(uid);

        return get(pramDto.getRuser(),strSql,valueList);
    }
}
