package dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import dao.entity.StoredProgrametable;

public class StoredProgrametableDao extends AbstractDao<StoredProgrametable> {

    public StoredProgrametableDao() {
        super();
    }

    @Override
    ArrayList<StoredProgrametable> setEntityList(ResultSet rs) {
        StoredProgrametable entity = new StoredProgrametable();
        return entity.setRsToList(rs);
    }

}
