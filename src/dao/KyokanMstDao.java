package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.KyokanMst;
import dto.ParameterDto;

public class KyokanMstDao extends AbstractDao<KyokanMst> {

    public KyokanMstDao() {
        super();
    }

    @Override
    ArrayList<KyokanMst> setEntityList(ResultSet rs) {
        KyokanMst entity = new KyokanMst();
        return entity.setRsToList(rs);
    }

    public ArrayList<KyokanMst> getKyokanMstList01(ParameterDto pramDto) {
        //外だしSQLの取得
        String strSql = getSql("KyokanMstDao_getKyokanMstList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.職員番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }
}
