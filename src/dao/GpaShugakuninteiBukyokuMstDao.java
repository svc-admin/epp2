package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.GpaShugakuninteiBukyokuMst;
import dto.ParameterDto;

public class GpaShugakuninteiBukyokuMstDao extends AbstractDao<GpaShugakuninteiBukyokuMst> {

    private static Logger logger = null;

    public GpaShugakuninteiBukyokuMstDao() {
        super();
    }

    @Override
    ArrayList<GpaShugakuninteiBukyokuMst> setEntityList(ResultSet rs) {
        GpaShugakuninteiBukyokuMst entity = new GpaShugakuninteiBukyokuMst();
        return entity.setRsToList(rs);
    }

    public ArrayList<GpaShugakuninteiBukyokuMst> getGpaShugakuninteiBukyokuMst() {

    	String strSql = getSql("GpaShugakuninteiBukyokuMstDao_getGpaShugakuninteiBukyokuMst01.sql");
    	List<Object> valueList = new ArrayList<Object>();
    	return get("",strSql,valueList);
    }

    public ArrayList<GpaShugakuninteiBukyokuMst> InputCheck(ParameterDto pramDto) {

        String strSql = getSql("GpaShugakuninteiBukyokuMstDao_getGpaShugakuninteiBukyokuMst02.sql");
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(pramDto.getGpaShugakuninteiBukyokuMst().getgBukyokucd());
        valueList.add(pramDto.getGpaShugakuninteiBukyokuMst().getgBukyokucd() + "%");
        return get("",strSql,valueList);
    }

    public Boolean deleteGpaShugakuninteiBukyokuMst01(ParameterDto pramDto) {

        Boolean res = false;

        Connection conn = null;
        PreparedStatement pstmt = null;

        String strSql = "delete from gpa_shugakunintei_bukyoku_mst where ";
        String strWhere = "";

        if(pramDto.getGpaShugakuninteiBukyokuMst().getgBukyokucd().equals("") == false){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "G_BUKYOKUCD = '" + pramDto.getGpaShugakuninteiBukyokuMst().getgBukyokucd() + "' ";
        }

        if(pramDto.getGpaShugakuninteiBukyokuMst().getShori_start() != 0){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "SHORI_START = '" + pramDto.getGpaShugakuninteiBukyokuMst().getShori_start() + "' ";
        }

        if(pramDto.getGpaShugakuninteiBukyokuMst().getShori_end() != 0){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "SHORI_END = '" + pramDto.getGpaShugakuninteiBukyokuMst().getShori_end() + "' ";
        }
        // queryの生成
        strSql = strSql + strWhere;

        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            int num = pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return true;
    }

    public Boolean addGpaShugakuninteiBukyokuMst01(ParameterDto pramDto) {

        Boolean res = false;

        Connection conn = null;
        PreparedStatement pstmt = null;

        String strSql = "insert into gpa_shugakunintei_bukyoku_mst (";
        String strPram = ")values(" ;

        strSql = strSql + "G_BUKYOKUCD";
        strPram = strPram + "'" + pramDto.getGpaShugakuninteiBukyokuMst().getgBukyokucd() + "'";

        strSql = strSql + ",SHORI_START";
        strPram = strPram + ",'" + pramDto.getGpaShugakuninteiBukyokuMst().getShori_start() + "'";

        strSql = strSql + ",SHORI_END";
        strPram = strPram + ",'" + pramDto.getGpaShugakuninteiBukyokuMst().getShori_end() + "'";

        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
        strSql = strSql + ",INSERT_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",UPDATE_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",USERID";
        strPram = strPram + ",'" + pramDto.getRuser() + "'";

        strSql = strSql + strPram + ")";

        System.out.println(strSql);

        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            int num = pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return true;
    }

    /**
     * GPA集計用部局管理テーブル内にデータが存在するかチェック
     * @param paramDto
     * @return データが存在する場合はtrue
     */
    public boolean getGpaBukyokuJoken(ParameterDto paramDto) {

        // SQL文の作成
        String strSql = "";
        strSql += " SELECT";
        strSql += "     COUNT(0) AS DATACNT";
        strSql += " FROM";
        strSql += "     gpa_shugakunintei_bukyoku_mst C";
        strSql += " WHERE";
        strSql += "     ? BETWEEN C.SHORI_START AND C.SHORI_END";
        strSql += " AND ? LIKE CONCAT(C.G_BUKYOKUCD, '%')";

        // パラメータの設定
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(paramDto.getShoriJokenMst().getShorinendo());
        valueList.add(paramDto.getgBukyokucd());

        // SQLの実行
        ArrayList<GpaShugakuninteiBukyokuMst> getList = new ArrayList<GpaShugakuninteiBukyokuMst>();
        getList = get(paramDto.getRuser(), strSql, valueList);
        if (getList.size() > 0 && getList.get(0).getDataCnt() > 0) {
            return true;
        }
        return false;
    }
}