package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.HanteiShubetsuData;
import dto.ParameterDto;

public class HanteiShubetsuDataDao extends AbstractDao<HanteiShubetsuData> {

	private static Logger logger = null;

	public HanteiShubetsuDataDao() {
		super();
	}

	@Override
	ArrayList<HanteiShubetsuData> setEntityList(ResultSet rs) {
		HanteiShubetsuData entity = new HanteiShubetsuData();
		return entity.setRsToList(rs);
	}

	/**
	 * 判定種別データを取得する
	 * @param paramDto
	 * @return
	 */
	public ArrayList<HanteiShubetsuData> getTypeData(ParameterDto paramDto) {

		String strSql = getSql("HanteiShubetsuDataDao_getTypeData01.sql");

		List<Object> valueList = new ArrayList<Object>();
		valueList.add(paramDto.getShoriJokenMst().getShorinendo());
		valueList.add(paramDto.getShoriJokenMst().getGakkikbncd());
		return get(paramDto.getRuser(), strSql, valueList);
	}

	/**
	 * 判定種別データを削除する
	 * @param paramDto
	 * @return
	 */
	public Boolean delTypeData(ParameterDto paramDto) {

		Boolean ret = false;

		Connection conn = null;
		PreparedStatement pstmt = null;

		String strSql = "";
		strSql += " DELETE FROM hantei_shubetsu_data";
		strSql += " WHERE";
		strSql += "     SHORI_NENDO = " + paramDto.getShoriJokenMst().getShorinendo();
		strSql += " AND GAKKIKBNCD = " + paramDto.getShoriJokenMst().getGakkikbncd();

		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(strSql);
			pstmt.executeUpdate();

			ret = true;
		} catch (Exception ex) {
			logger.error(ex);
		} finally {
			try {
				if (pstmt != null) pstmt.close();
				if (conn != null) conn.close();
			} catch (Exception ex) {
				logger.error(ex);
			}
		}
		return ret;
	}

	/**
	 * 判定種別データを登録する
	 * @param paramDto
	 * @return
	 */
	public Boolean addTypeData(ParameterDto paramDto) {

		Boolean ret = false;

		Connection conn = null;
		PreparedStatement pstmt = null;
		java.sql.Date today = new java.sql.Date(System.currentTimeMillis());

		String strSql = "";
		strSql += " INSERT IGNORE INTO hantei_shubetsu_data (";
		strSql += "  SHORI_NENDO";
		strSql += " ,GAKKIKBNCD";
		strSql += " ,G_SYSNO";
		strSql += " ,HANTEI_SHUBETSU";
		strSql += " ,INSERT_DATE";
		strSql += " ,UPDATE_DATE";
		strSql += " ,USERID";
		strSql += " ) VALUES (";
		strSql += "  " + paramDto.getShoriJokenMst().getShorinendo();
		strSql += " ,'" + paramDto.getShoriJokenMst().getGakkikbncd() + "'";
		strSql += " ,'" + paramDto.getHanteiShubetsuData().getG_sysno() + "'";
		strSql += " ,'" + paramDto.getHanteiShubetsuData().getHantei_shubetsu() + "'";
		strSql += " ,'" + today + "'";
		strSql += " ,'" + today + "'";
		strSql += " ,'" + paramDto.getRuser() + "'";
		strSql += " )";

		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(strSql);
			pstmt.executeUpdate();

			ret = true;
		} catch (Exception ex) {
			logger.error(ex);
		} finally {
			try {
				if (pstmt != null) pstmt.close();
				if (conn != null) conn.close();
			} catch (Exception ex) {
				logger.error(ex);
			}
		}
		return ret;
	}
}
