package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import dao.entity.Results;
import dto.ParameterDto;

/**
 * @author nakano@cc.kumamoto-u.ac.jp
 * 2015-02-26 nakano@cc.kumamoto-u.ac.jp
 *
 */
public class ResultsDao extends AbstractDao<Results> {

    public ResultsDao() {
        super();
    }

    @Override
    ArrayList<Results> setEntityList(ResultSet rs){
        Results entity = new Results();
        return entity.setRsToList(rs);
    }

    /**
     * ParameterDtoを引数として1件登録
     * @param paramDto
     * @return
     */
    public int insertResults01(ParameterDto paramDto){
        // SQLの取得
        String strSql = getSql("ResultsDao_insertResults01.sql");

        // SQLの?に設定する値をListに順に設定する
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(emptyOr(paramDto.getResults().getgSysno()));
        valueList.add(emptyOr(paramDto.getResults().getKamokucd()));
        valueList.add(emptyOr(paramDto.getResults().getDate()));
        valueList.add(emptyOr(paramDto.getResults().getClassification()));
        valueList.add(emptyOr(paramDto.getResults().getTitle()));
        valueList.add(emptyOr(paramDto.getResults().getComment()));
        valueList.add(emptyOr(paramDto.getResults().getAuthorName()));
        valueList.add(emptyOr(paramDto.getResults().getAuthorType()));
        valueList.add(emptyOr(paramDto.getResults().getPresentationFormat()));
        valueList.add(emptyOr(paramDto.getResults().getMediumTitle()));
        valueList.add(emptyOr(paramDto.getResults().getMediumClass()));
        valueList.add(emptyOr(paramDto.getResults().getHasImpactFactor()));
        valueList.add(emptyOr(paramDto.getResults().getImpactFactor()));
        valueList.add(emptyOr(paramDto.getResults().getLocation()));
        valueList.add(emptyOr(paramDto.getResults().getPublisher()));
        valueList.add(emptyOr(paramDto.getResults().getIsbn()));
        valueList.add(emptyOr(paramDto.getResults().getJournalVolume()));
        valueList.add(emptyOr(paramDto.getResults().getJournalNumber()));
        valueList.add( zeroOr(paramDto.getResults().getPageFrom()));
        valueList.add( zeroOr(paramDto.getResults().getPageTo()));
        valueList.add( zeroOr(paramDto.getResults().getPublishedYear()));
        valueList.add( zeroOr(paramDto.getResults().getPublishedMonth()));
        valueList.add(emptyOr(paramDto.getResults().getHasReviewed()));
        valueList.add(emptyOr(paramDto.getResults().getOriginalMediumName()));
        valueList.add(emptyOr(paramDto.getResults().getOriginalAuthorName()));
        valueList.add(emptyOr(paramDto.getResults().getOriginalPublisher()));
        valueList.add( zeroOr(paramDto.getResults().getOriginalPageFrom()));
        valueList.add( zeroOr(paramDto.getResults().getOriginalPageTo()));
        valueList.add( zeroOr(paramDto.getResults().getOriginalPublishedYear()));
        valueList.add( zeroOr(paramDto.getResults().getOriginalPublishedMonth()));
        java.sql.Date today = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
// 2019.02.01 s.furuta chg start 2.学修成果修正
//        valueList.add(today);
        valueList.add(paramDto.getResults().getInsertDate());
//2019.02.01 s.furuta chg end
        valueList.add(today);
        valueList.add(paramDto.getRuser());

        return update(paramDto.getRuser(),strSql,valueList);
    }
 // 2019.01.17 s.furuta add start 2.学修成果修正
    /**
     * 学生番号、科目コードを引数にデータを取得
     * @param paramDto
     * @return
     */
    public ArrayList<Results> getResults01(ParameterDto paramDto) {

        //外だしSQLの取得
        String strSql = getSql("ResultsDao_getResults01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(new String(paramDto.getUid().replace("-", "").toLowerCase()));
        //2.科目コード
        valueList.add(new String(paramDto.getKamokuCd()));
        return get(paramDto.getRuser(),strSql,valueList);
    }
    /**
     * 学生番号、分類、タイトルを引数にデータを取得
     * @param paramDto
     * @return
     */
    public ArrayList<Results> getResults02(ParameterDto paramDto) {

        //外だしSQLの取得
        String strSql = getSql("ResultsDao_getResults02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(new String(paramDto.getUid().replace("-", "").toLowerCase()));
        //2.科目コード
        valueList.add(new String(paramDto.getKamokuCd()));

        return get(paramDto.getRuser(),strSql,valueList);
    }
    /**
     * ParameterDtoを引数として1件削除
     * @param paramDto
     * @return
     */
    public int deleteResults01(ParameterDto paramDto){
        // SQLの取得
        String strSql = getSql("ResultsDao_deleteResults01.sql");

        // SQLの?に設定する値をListに順に設定する
        List<Object> valueList = new ArrayList<Object>();
        // 1.学生番号
        valueList.add(paramDto.getUid().replace("-", "").toLowerCase());
        // 2.科目コード
        valueList.add(paramDto.getKamokuCd());

        return update(paramDto.getRuser(),strSql,valueList);
    }
// 2019.01.17 s.furuta add end
// 2019.02.15 s.furuta add start 3.各種出力
    /**
     * 学生番号を引数にデータを取得
     * @param paramDto
     * @return
     */
    public ArrayList<Results> getResults03(ParameterDto paramDto) {

        //外だしSQLの取得
        String strSql = getSql("ResultsDao_getResults03.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(new String(paramDto.getUid().replace("-", "").toLowerCase()));

        return get(paramDto.getRuser(),strSql,valueList);
    }
    /**
     * 学生番号を引数にデータを取得(学生番号はParameterDtoとは別で受け渡し)
     * @param paramDto
     * @return
     */
    public ArrayList<Results> getResults04(ParameterDto paramDto, String uid) {

        //外だしSQLの取得
        String strSql = getSql("ResultsDao_getResults04.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(new String(uid.replace("-", "").toLowerCase()));

        return get(paramDto.getRuser(),strSql,valueList);
    }
// 2019.02.15 s.furuta add end
    private Object emptyOr(Object object) {
        return object == null ? "" : object;
    }

    private Object zeroOr(Object object) {
        return object == null ? 0 : object;
    }
}
