package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import dao.entity.Evidence;
import dto.ParameterDto;

/**
 * @author nakano@cc.kumamoto-u.ac.jp
 * 2014-11-10 nakano@cc.kumamoto-u.ac.jp
 *
 */
public class EvidenceDao extends AbstractDao<Evidence> {

    public EvidenceDao() {
        super();
    }

    @Override
    ArrayList<Evidence> setEntityList(ResultSet rs) {
        Evidence entity = new Evidence();
        return entity.setRsToList(rs);
    }

    /**
     * uidで指定されるユーザの全ての学習成果の取得 (チェックしていない)
 * 2014-11-10 nakano@cc.kumamoto-u.ac.jp
     * @param pramDto
     * @return
     */
    public ArrayList<Evidence> getEvidencList01(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("EvidenceDao_getEvidenceList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(pramDto.getUid());
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    /**
     * 1つの学習成果を取得(二重には登録されていないはず) (チェックしていない)
 * 2014-11-10 nakano@cc.kumamoto-u.ac.jp
     * @param pramDto
     * @return
     */
    public ArrayList<Evidence> getEvidencList02(ParameterDto pramDto) {

      //外だしSQLの取得
      String strSql = getSql("EvidenceDao_getEvidenceList02.sql");

      //SQLの?に設定する値をListに順に設定する。
      List<Object> valueList = new ArrayList<Object>();
      valueList.add(pramDto.getEvidence().getUid());
      valueList.add(pramDto.getEvidence().getNendo());
      valueList.add(pramDto.getEvidence().getJikanwariShozokucd());
      valueList.add(pramDto.getEvidence().getJikanwaricd());
      valueList.add(pramDto.getEvidence().getDir());
      valueList.add(pramDto.getEvidence().getEvidence());
      valueList.add(pramDto.getEvidence().getSize());
      valueList.add(pramDto.getEvidence().getLastUpdate());
      valueList.add(pramDto.getEvidence().getUid());
      valueList.add(pramDto.getEvidence().getNendo());
      valueList.add(pramDto.getEvidence().getJikanwariShozokucd());
      valueList.add(pramDto.getEvidence().getJikanwaricd());
      valueList.add(pramDto.getEvidence().getDir());
      valueList.add(pramDto.getEvidence().getEvidence());
      valueList.add(pramDto.getEvidence().getSize());
      valueList.add(pramDto.getEvidence().getLastUpdate());

      return get(pramDto.getRuser(),strSql,valueList);
  }

    /**
     * 1つの学習成果を登録 ==> 遅い！ 数時間試したが、多分１日位はかかりそう！
 * 2014-11-10 nakano@cc.kumamoto-u.ac.jp
     * @param ruser
     * @param uid
     * @param nendo
     * @param jikanwariShozokucd
     * @param jikanwaricd
     * @param dir
     * @param evidence
     * @param size
     * @param lastUpdate
     * @return
     */
    public int insertEvidence01(String ruser, String uid, int nendo, String jikanwariShozokucd,
            String jikanwaricd, String dir, String evidence, long size, java.util.Date lastUpdate) {

        //外だしSQLの取得
        String strSql = getSql("EvidenceDao_insertEvidence01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(uid);
        valueList.add(new Integer(nendo));
        valueList.add(jikanwariShozokucd);
        valueList.add(jikanwaricd);
        valueList.add(dir);
        valueList.add(evidence);
        valueList.add(new Long(size));
        valueList.add(new java.sql.Timestamp(lastUpdate.getTime()));
        java.sql.Date today = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
        valueList.add(today);
        valueList.add(today);
        valueList.add(ruser);

        return update(ruser,strSql,valueList);

    }

/**
 * 上記の高速版 (autocommit = off) 数10分で終わる (1,831,644 件登録)
 * 2014-11-10 nakano@cc.kumamoto-u.ac.jp
 * @param ruser
 * @param paramList
 * @return
 */
    @SuppressWarnings("unchecked")
    public int insertEvidence01all(String ruser, List<List<Object>> paramList) {

        //外だしSQLの取得
        String strSql = getSql("EvidenceDao_insertEvidence01.sql");
        //SQLの?に設定する値をListに順に設定する。
        java.sql.Date today = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
        for(List<Object> valueList : paramList) {
          valueList.add(today);
          valueList.add(today);
          valueList.add(ruser);
//					System.out.println("***=========== valueList.size() = "+valueList.size());

        }

        return updateAll(ruser,strSql,paramList);

    }

    /**
     * 1つアップデート (チェックしていない)
 * 2014-11-10 nakano@cc.kumamoto-u.ac.jp
     * @param ruser
     * @param uid
     * @param nendo
     * @param jikanwariShozokucd
     * @param jikanwaricd
     * @param dir
     * @param evidence
     * @param size
     * @param lastUpdate
     * @return
     */
    public int updateGpa01(String ruser, String uid, int nendo, String jikanwariShozokucd,
            String jikanwaricd, String dir, String evidence, long size, java.util.Date lastUpdate) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_updateGpa01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(uid);
        valueList.add(new Integer(nendo));
        valueList.add(jikanwariShozokucd);
        valueList.add(jikanwaricd);
        valueList.add(dir);
        valueList.add(evidence);
        valueList.add(new Long(size));
        valueList.add(new java.sql.Timestamp(lastUpdate.getTime()));
        java.sql.Date today = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
        valueList.add(today);
        valueList.add(ruser);

        return update(ruser,strSql,valueList);
    }

}
