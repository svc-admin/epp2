package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.ShugakusienHanteiData;
import dto.ParameterDto;

public class ShugakusienHanteiDataDao extends AbstractDao<ShugakusienHanteiData> {

	private static Logger logger = null;
	private static final String END_OF_YEAR = "3";

	public ShugakusienHanteiDataDao() {
		super();
	}

	@Override
	ArrayList<ShugakusienHanteiData> setEntityList(ResultSet rs) {
		ShugakusienHanteiData entity = new ShugakusienHanteiData();
		return entity.setRsToList(rs);
	}

	/**
	 * 修学支援判定のベースとなるデータを取得する
	 * @param hasGpaDivData
	 * @param paramDto
	 * @return
	 */
	public ArrayList<ShugakusienHanteiData> getJudgeBaseData(boolean hasGpaDivData, ParameterDto paramDto) {

		// SQL文の作成とパラメータの設定
		List<Object> valueList = new ArrayList<Object>();
		String strSql = "";
// 2020.06.25 S.Miyamoto DEL START >> 基準日の1日前という条件は使わなくなったので削除
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//		Date kijunDate = null;
//		String beforKijunDate = paramDto.getShoriJokenMst().getKijundate();
//		try {
//			kijunDate = sdf.parse(paramDto.getShoriJokenMst().getKijundate());
//			Calendar calendar = Calendar.getInstance();
//			calendar.setTime(kijunDate);
//			calendar.add(Calendar.DATE, -1);
//			beforKijunDate = sdf.format(calendar.getTime());
//		} catch(Exception e) {
//		}
// 2020.06.25 S.Miyamoto DEL END   <<

		strSql += " SELECT";
		strSql += "     A.G_SYSNO";
		strSql += "    ,K.G_NAME";
		strSql += "    ,IFNULL(SUM(CASE WHEN (IFNULL(D.gpa_taisho,0) = 0 OR F.YOKEN_NENDO IS NOT NULL) THEN 0 ELSE B.TANISU END), 0.0) AS T_GP_TANISU";
		strSql += "    ,IFNULL(SUM((CASE WHEN (IFNULL(D.gpa_taisho,0) = 0 OR F.YOKEN_NENDO IS NOT NULL) THEN 0 ELSE B.TANISU END) * D.gp), 0.0) AS T_GPT";
		strSql += "    ,IFNULL(ROUND((SUM((CASE WHEN (IFNULL(D.gpa_taisho,0) = 0 OR F.YOKEN_NENDO IS NOT NULL) THEN 0 ELSE B.TANISU END) * D.gp) / SUM(CASE WHEN (IFNULL(D.gpa_taisho,0) = 0 OR F.YOKEN_NENDO IS NOT NULL) THEN 0 ELSE B.TANISU END)), 2), 0.0) AS T_GPA";
		strSql += "    ,E.HITSUYOTANISU";
		strSql += "    ,E.SYUGYONENGEN";
		strSql += "    ,IFNULL(SUM(CASE WHEN (B.GOHIKBN = 0 OR (TRIM(G.KAMOKUM_SHOZOKUCD) <> '') OR (B.KAMOKUDKBNCD NOT IN (SELECT KAMOKUDKBNCD FROM shutokutani_taisho_mst WHERE TRIM(KAMOKUM_SHOZOKUCD) = '' GROUP BY KAMOKUDKBNCD))) THEN 0 ELSE B.TANISU END), 0.0) AS SHUTOKU_TANISU";
		strSql += "    ,IFNULL(SUM(CASE WHEN (IFNULL(B.JIKANWARI_NENDO,0) > 0 AND (CASE WHEN B.JIKANWARI_NENDO <> B.NINTEI_NENDO THEN B.NINTEI_NENDO ELSE B.JIKANWARI_NENDO END) = ?) THEN B.TANISU ELSE 0 END), 0.0) AS RISHU_TANISU";
		valueList.add(getNendo(paramDto.getShoriJokenMst().getKijundate()));
		strSql += "    ,IFNULL(SUM(CASE WHEN B.HYOGONM IN ('X','Ｘ') AND IFNULL(B.JIKANWARI_NENDO,0) > 0 AND (CASE WHEN B.JIKANWARI_NENDO <> B.NINTEI_NENDO THEN B.NINTEI_NENDO ELSE B.JIKANWARI_NENDO END) = ? THEN B.TANISU ELSE 0 END), 0.0) AS KESSEKI_TANISU";
		valueList.add(getNendo(paramDto.getShoriJokenMst().getKijundate()));
		strSql += "    ,H.SIEN_JOKYO";
		strSql += "    ,IFNULL((CASE WHEN I.GETSUSU IS NULL THEN 0 ELSE I.GETSUSU END), 0) AS KYUGAKU_MONTH";
		strSql += "    ,(PERIOD_DIFF(SUBSTRING(?, 1, 6), DATE_FORMAT(CONCAT(A.YOKEN_NENDO,'/',A.YOKEN_MONTH,'/01'), '%Y%m')) + 1) AS ZAISEKI_MONTH";
		valueList.add(paramDto.getShoriJokenMst().getKijundate());
		strSql += "    ,J.RYUNENDATA";
		strSql += "    ,IFNULL(SUM(CASE WHEN (IFNULL(D.gpa_taisho,0) = 0 OR F.YOKEN_NENDO IS NOT NULL OR B.NINTEI_NENDO <> ?) THEN 0 ELSE B.TANISU END), 0.0) AS N_GP_TANISU";
		valueList.add(getNendo(paramDto.getShoriJokenMst().getKijundate()));
		strSql += "    ,IFNULL(SUM((CASE WHEN (IFNULL(D.gpa_taisho,0) = 0 OR F.YOKEN_NENDO IS NOT NULL OR B.NINTEI_NENDO <> ?) THEN 0 ELSE B.TANISU END) * D.gp), 0.0) AS N_GPT";
		valueList.add(getNendo(paramDto.getShoriJokenMst().getKijundate()));
		strSql += "    ,IFNULL(ROUND((SUM((CASE WHEN (IFNULL(D.gpa_taisho,0) = 0 OR F.YOKEN_NENDO IS NOT NULL OR B.NINTEI_NENDO <> ?) THEN 0 ELSE B.TANISU END) * D.gp) / SUM(CASE WHEN (IFNULL(D.gpa_taisho,0) = 0 OR F.YOKEN_NENDO IS NOT NULL OR B.NINTEI_NENDO <> ?) THEN 0 ELSE B.TANISU END)), 2), 0.0) AS N_GPA";
		valueList.add(getNendo(paramDto.getShoriJokenMst().getKijundate()));
		valueList.add(getNendo(paramDto.getShoriJokenMst().getKijundate()));
		strSql += " FROM";
		strSql += "     (";
		strSql += "      SELECT";
		strSql += "          A.G_BUKYOKUCD,A.G_GAKUNO,A.G_SORT,A.G_SYSNO,A.G_SHOZOKUCD,A.G_MIBUNCD,A.GENKYOKBNCD,A.GAKUNEN,A.ZAIGAKU_TSUKISU";
		strSql += "         ,A.NYUGAKUYMD,A.SOTSUGYOYMD,A.TOKKI,A.RYUGAKUKBNCD,A.ZAIRYUSHIKAKUCD,A.SHAKAIJINFLG,A.GAIKOKUJINFLG,A.JUKENNO";
		strSql += "         ,A.NYUGAKUKBNCD,A.NYUGAKU_NENDO,A.NYUGAKU_GAKUNEN,A.AKINYUGAKUFLG,A.KENCD,A.GAKUICD,A.GAKUIKINO,A.SUB_SHOZOKUCD";
		strSql += "         ,A.YOKEN_NENDO,A.YOKEN_MONTH,A.MIKOMIFLG,A.IMAGEFILENM,A.HAKKO_CNT,A.INSERT_DATE,A.UPDATE_DATE,A.USERID";
		strSql += "      FROM";
		strSql += "          gakuseki A";
		strSql += "      WHERE";
// 2020.06.25 S.Miyamoto CHG START >> 基準日の1日前という条件は外す（処理条件入力で対応）
//		strSql += "          A.UPDATE_DATE <= ? AND A.INSERT_DATE <= ?";
//		valueList.add(beforKijunDate);
//		valueList.add(beforKijunDate);
		strSql += "          A.UPDATE_DATE <= ?";
		valueList.add(paramDto.getShoriJokenMst().getKijundate());
// 2020.06.25 S.Miyamoto CHG END   <<
		strSql += "      UNION";
		strSql += "      SELECT";
		strSql += "          GH.G_BUKYOKUCD,GH.G_GAKUNO,GH.G_SORT,GH.G_SYSNO,GH.G_SHOZOKUCD,GH.G_MIBUNCD,GH.GENKYOKBNCD,GH.GAKUNEN,GH.ZAIGAKU_TSUKISU";
		strSql += "         ,GH.NYUGAKUYMD,GH.SOTSUGYOYMD,GH.TOKKI,GH.RYUGAKUKBNCD,GH.ZAIRYUSHIKAKUCD,GH.SHAKAIJINFLG,GH.GAIKOKUJINFLG,GH.JUKENNO";
		strSql += "         ,GH.NYUGAKUKBNCD,GH.NYUGAKU_NENDO,GH.NYUGAKU_GAKUNEN,GH.AKINYUGAKUFLG,GH.KENCD,GH.GAKUICD,GH.GAKUIKINO,GH.SUB_SHOZOKUCD";
		strSql += "         ,GH.YOKEN_NENDO,GH.YOKEN_MONTH,GH.MIKOMIFLG,GH.IMAGEFILENM,GH.HAKKO_CNT,GH.INSERT_DATE,GH.UPDATE_DATE,GH.USERID";
		strSql += "      FROM";
		strSql += "          gakuseki_history GH";
		strSql += "          INNER JOIN (SELECT G_SYSNO, MAX(RIREKI_DATE) AS MAX_DATE FROM gakuseki_history WHERE RIREKI_DATE <= ? GROUP BY G_SYSNO) GH_MAX";
// 2020.06.25 S.Miyamoto CHG START >> 基準日の1日前という条件は外す（処理条件入力で対応）
//		valueList.add(beforKijunDate);
		valueList.add(paramDto.getShoriJokenMst().getKijundate());
// 2020.06.25 S.Miyamoto CHG END   <<
		strSql += "                  ON (    GH.G_SYSNO = GH_MAX.G_SYSNO";
		strSql += "                      AND GH.RIREKI_DATE = GH_MAX.MAX_DATE";
		strSql += "                     )";
		strSql += "          INNER JOIN gakuseki A";
		strSql += "                  ON (    GH.G_SYSNO = A.G_SYSNO";
		strSql += "                     )";
		strSql += "      WHERE";
// 2020.06.25 S.Miyamoto CHG START >> 基準日以降に学籍が更新された学生を履歴データの母数とする
//		strSql += "          A.UPDATE_DATE > ? AND A.INSERT_DATE <= ?";
//		valueList.add(beforKijunDate);
//		valueList.add(beforKijunDate);
		strSql += "          A.UPDATE_DATE > ?";
		valueList.add(paramDto.getShoriJokenMst().getKijundate());
// 2020.06.25 S.Miyamoto CHG END   <<
		strSql += "     ) A";
		strSql += "     LEFT  JOIN shugakusien_jokyo H";
		strSql += "             ON (    A.G_SYSNO = H.G_GAKUNO";
		strSql += "                 AND H.SHORI_NENDO = ?";
		valueList.add(paramDto.getShoriJokenMst().getShorinendo() - 1);
		strSql += "                )";
		strSql += "     LEFT  JOIN kakutei_seiseki B";
		strSql += "             ON (    A.G_SYSNO = B.G_SYSNO";
// 2020.07.21 S.Miyamoto CHG START >> 基準日の年度以降の成績は参照しない
//		strSql += "                 AND B.NINTEI_NENDO <= 9999";
		strSql += "                 AND B.NINTEI_NENDO <= ?";
		valueList.add(getNendo(paramDto.getShoriJokenMst().getKijundate()));
// 2020.07.21 S.Miyamoto CHG END   <<
		strSql += "                )";
		strSql += "     INNER JOIN gpa_shugakunintei_bukyoku_mst C";
		if (hasGpaDivData) {
			strSql += "             ON (    A.G_BUKYOKUCD LIKE CONCAT(C.G_BUKYOKUCD, '%')";
		} else {
			strSql += "             ON (    C.G_BUKYOKUCD = '-1'";
		}
		strSql += "                )";
		strSql += "     LEFT  JOIN gpa_shugakunintei_gp_mst D";
		strSql += "             ON (    C.G_BUKYOKUCD = D.G_BUKYOKUCD";
		strSql += "                 AND C.SHORI_START = D.SHORI_START";
		strSql += "                 AND C.SHORI_END = D.SHORI_END";
		strSql += "                 AND B.HYOGONM = D.HYOGONM";
		strSql += "                )";
		strSql += "     LEFT  JOIN hyojun_tanisu E";
		strSql += "             ON (    A.YOKEN_NENDO = E.NYUGAKU_NENDO";
		strSql += "                 AND A.G_SHOZOKUCD LIKE CONCAT(E.G_BUKYOKUCD, '%')";
		strSql += "                )";
		strSql += "     LEFT  JOIN gpa_shugakunintei_jogaikamoku_mst F";
		strSql += "             ON (    A.YOKEN_NENDO = F.YOKEN_NENDO";
		strSql += "                 AND A.G_BUKYOKUCD = F.G_BUKYOKUCD";
		strSql += "                 AND B.KAMOKUCD = F.KAMOKUCD";
		strSql += "                )";
		strSql += "     LEFT  JOIN shutokutani_taisho_mst G";
		strSql += "             ON (    B.KAMOKUDKBNCD = G.KAMOKUDKBNCD";
		strSql += "                 AND (TRIM(G.KAMOKUM_SHOZOKUCD) <> '' AND CONCAT(IFNULL(TRIM(B.KAMOKUM_SHOZOKUCD),''),IFNULL(TRIM(B.KAMOKUMKBNCD),''),IFNULL(TRIM(B.KAMOKUS_SHOZOKUCD),''),IFNULL(TRIM(B.KAMOKUSKBNCD),''),IFNULL(TRIM(B.KAMOKUSS_SHOZOKUCD),''),IFNULL(TRIM(B.KAMOKUSSKBNCD),''))";
		strSql += "                   LIKE CONCAT(IFNULL(TRIM(G.KAMOKUM_SHOZOKUCD),''),IFNULL(TRIM(G.KAMOKUMKBNCD),''),IFNULL(TRIM(G.KAMOKUS_SHOZOKUCD),''),IFNULL(TRIM(G.KAMOKUSKBNCD),''),IFNULL(TRIM(G.KAMOKUSS_SHOZOKUCD),''),IFNULL(TRIM(G.KAMOKUSSKBNCD),''),'%'))";
		strSql += "                )";
		strSql += "     LEFT  JOIN (";
		strSql += "                 SELECT";
		strSql += "                     IDO.G_GAKUNO";
		strSql += "                    ,IDO.G_BUKYOKUCD";
		strSql += "                    ,SUM(CASE WHEN (IDO.GETSUSU < 0 ) THEN 0 ELSE IDO.GETSUSU END) AS GETSUSU";
		strSql += "                 FROM (";
		strSql += "                   SELECT";
		strSql += "                       A.G_GAKUNO";
		strSql += "                      ,A.G_BUKYOKUCD";
		strSql += "                      ,(CASE";
		strSql += "                          WHEN ((A.J_ENDYMD IS NOT NULL AND A.J_ENDYMD <> '0000-00-00') AND CAST(DATE_FORMAT(A.J_ENDYMD, '%Y%m') AS SIGNED) <= CAST(SUBSTRING(?, 1, 6) AS SIGNED)) THEN PERIOD_DIFF(DATE_FORMAT(A.J_ENDYMD, '%Y%m'), DATE_FORMAT((CASE WHEN IFNULL(A.J_STYMD,'0000-00-00') <> '0000-00-00' THEN A.J_STYMD ELSE A.Y_STYMD END), '%Y%m')) + 1";
		valueList.add(paramDto.getShoriJokenMst().getKijundate());
		strSql += "                          ELSE";
		strSql += "                            CASE";
		strSql += "                              WHEN (CAST(DATE_FORMAT(A.Y_ENDYMD, '%Y%m') AS SIGNED) <= CAST(SUBSTRING(?, 1, 6) AS SIGNED)) THEN PERIOD_DIFF(DATE_FORMAT(A.Y_ENDYMD, '%Y%m'), DATE_FORMAT((CASE WHEN IFNULL(A.J_STYMD,'0000-00-00') <> '0000-00-00' THEN A.J_STYMD ELSE A.Y_STYMD END), '%Y%m')) + 1";
		valueList.add(paramDto.getShoriJokenMst().getKijundate());
		strSql += "                              ELSE PERIOD_DIFF(SUBSTRING(?, 1, 6), DATE_FORMAT((CASE WHEN IFNULL(A.J_STYMD,'0000-00-00') <> '0000-00-00' THEN A.J_STYMD ELSE A.Y_STYMD END), '%Y%m')) + 1";
		valueList.add(paramDto.getShoriJokenMst().getKijundate());
		strSql += "                            END";
		strSql += "                        END) AS GETSUSU";
		strSql += "                   FROM";
		strSql += "                       gk_gakusekiido A";
		strSql += "                   WHERE";
		strSql += "                       A.IDOKBNCD = '03'";
		strSql += "                   AND NOT (IFNULL(A.J_STYMD,'0000-00-00') = '0000-00-00' AND DATE_FORMAT(A.Y_STYMD, '%Y%m%d') > ?)";
		valueList.add(paramDto.getShoriJokenMst().getKijundate());
		strSql += "                   AND NOT (A.J_ENDYMD IS NOT NULL AND A.J_ENDYMD <> '0000-00-00' AND A.J_ENDYMD < (A.INSERT_DATE + INTERVAL 7 DAY))";
		strSql += "                 ) IDO";
		strSql += "                 GROUP BY";
		strSql += "                     IDO.G_GAKUNO";
		strSql += "                    ,IDO.G_BUKYOKUCD";
		strSql += "                ) I";
		strSql += "             ON (    A.G_SYSNO = I.G_GAKUNO";
		strSql += "                 AND A.G_BUKYOKUCD = I.G_BUKYOKUCD";
		strSql += "                )";
		strSql += "     LEFT  JOIN (";
		strSql += "                 SELECT";
		strSql += "                     A.G_GAKUNO";
		strSql += "                    ,GROUP_CONCAT(DISTINCT CAST((NENDO - 1) AS CHAR) ORDER BY NENDO SEPARATOR '、') AS RYUNENDATA";
		strSql += "                 FROM";
		strSql += "                     gk_shinkyu_info A";
		strSql += "                 WHERE";
		strSql += "                     A.SHINKYUKBNCD = '3'";
		strSql += "                 AND A.RYUNENFLG = 1";
		strSql += "                 AND A.KAKUTEIFLG = 1";
		strSql += "                 GROUP BY";
		strSql += "                     A.G_GAKUNO";
		strSql += "                ) J";
		strSql += "             ON (    A.G_SYSNO = J.G_GAKUNO";
		strSql += "                )";
		strSql += "     LEFT  JOIN kyokihon K";
		strSql += "             ON (    A.G_SYSNO = K.G_SYSNO";
		strSql += "                )";
		strSql += " WHERE";
		strSql += "     A.G_BUKYOKUCD LIKE ?";
		valueList.add(paramDto.getgBukyokucd() + "%");
		strSql += " AND A.GAKUNEN = ?";
		valueList.add(paramDto.getGakunen());
		strSql += " AND (    ((A.SOTSUGYOYMD = '0000-00-00' OR A.SOTSUGYOYMD IS NULL) AND A.GENKYOKBNCD IN ('1','2','3','4','8'))";
		strSql += "      OR  ((A.SOTSUGYOYMD BETWEEN ? AND ?) AND A.GENKYOKBNCD IN ('5','6','7','9','A','D'))";
		valueList.add(paramDto.getNendoStart());
		valueList.add(paramDto.getNendoEnd());
		strSql += "     )";
		strSql += " AND ? BETWEEN C.SHORI_START AND C.SHORI_END";
		valueList.add(paramDto.getShoriJokenMst().getShorinendo());
		strSql += " AND A.G_MIBUNCD = '1'";
// 2020.06.25 S.Miyamoto ADD START >> 学籍の抽出条件に入学年度を追加（基準日から算出した年度までに入学した学生を対象とする)
		strSql += " AND A.NYUGAKU_NENDO <= ?";
		valueList.add(getNendo(paramDto.getShoriJokenMst().getKijundate()));
// 2020.06.25 S.Miyamoto ADD END   <<
		strSql += " GROUP BY";
		strSql += "     A.G_SYSNO";
		strSql += " ORDER BY";
		strSql += "     A.G_SYSNO";
		strSql += "    ,D.gp";

		// SQLの実行
		return get(paramDto.getRuser(), strSql, valueList);
	}

	/**
	 * CSV出力用のデータを取得する
	 * @param paramDto
	 * @return
	 */
	public ArrayList<ShugakusienHanteiData> getHanteiDataForCsv(ParameterDto paramDto) {

		// SQL文の作成
		String strSql = "";
		strSql += " SELECT";
		strSql += "     A.SHORI_NENDO";
		strSql += "    ,A.GAKKIKBNCD";
		strSql += "    ,A.KIJUN_DATE";
		strSql += "    ,A.G_BUKYOKUCD";
		strSql += "    ,RTRIM(CONCAT(B.SHOZOKUNM1,' ',B.SHOZOKUNM2,' ',B.SHOZOKUNM3,' ',B.SHOZOKUNM4,' ',B.SHOZOKUNM5)) AS SHOZOKUNM";
		strSql += "    ,A.GAKUNEN";
		strSql += "    ,A.NUM";
		strSql += "    ,A.G_SYSNO";
		strSql += "    ,A.G_NAME";
		strSql += "    ,A.JUDGE_TYPE";
		strSql += "    ,A.Z_JUDGE";
		strSql += "    ,A.T_JUDGE";
		strSql += "    ,A.Z_GPA";
		strSql += "    ,A.Z_GPA_B";
		strSql += "    ,A.Z_TANI";
		strSql += "    ,A.Z_TANI_B";
		strSql += "    ,A.H_SOTSU";
		strSql += "    ,A.H_SOTSU_B";
		strSql += "    ,A.H_TANI";
		strSql += "    ,A.H_TANI_B";
		strSql += "    ,A.H_SHU";
		strSql += "    ,A.H_SHU_B";
		strSql += "    ,A.H_ZEN";
		strSql += "    ,A.H_ZEN_B";
		strSql += "    ,A.K_TANI";
		strSql += "    ,A.K_TANI_B";
		strSql += "    ,A.K_GPA";
		strSql += "    ,A.K_GPA_B";
		strSql += "    ,A.K_SHU";
		strSql += "    ,A.K_SHU_B";
		strSql += "    ,A.S_TANI";
		strSql += "    ,A.S_TANI_B";
		strSql += "    ,A.S_SHU";
		strSql += "    ,A.S_SHU_B";
		strSql += "    ,A.BIKOU";
		strSql += "    ,A.T_GPT";
		strSql += "    ,A.T_GP_TANISU";
		strSql += "    ,A.T_GPA";
		strSql += "    ,A.T_GPA_B";
		strSql += "    ,A.T_GPA_JUNI";
		strSql += "    ,A.N_GPT";
		strSql += "    ,A.N_GP_TANISU";
		strSql += "    ,A.N_GPA";
		strSql += "    ,A.N_GPA_B";
		strSql += "    ,A.N_GPA_JUNI";
		strSql += "    ,A.GPA_PARAM";
		strSql += "    ,A.ZAISEKI_MONTH";
		strSql += "    ,A.KYUGAKU_MONTH";
		strSql += "    ,A.ZAIGAKU_NENSU";
		strSql += "    ,A.HITSUYO_TANISU";
		strSql += "    ,A.SHUGYO_NENGEN";
		strSql += "    ,A.HYOJUN_TANISU";
		strSql += "    ,A.SHUTOKU_TANISU";
		strSql += "    ,A.SHUTOKU_RITSU";
		strSql += "    ,A.RISHU_TANISU";
		strSql += "    ,A.KESSEKI_TANISU";
		strSql += "    ,A.SHUSSEKI_RITSU";
		strSql += "    ,A.RYUNENDATA";
		strSql += "    ,A.KEIKAKUSHO";
		strSql += " FROM";
		strSql += "     shugakusien_hantei_data A";
		strSql += "     INNER JOIN shozoku_mst B";
		strSql += "             ON (    A.G_BUKYOKUCD = B.SHOZOKUCD";
		strSql += "                )";
		strSql += " WHERE";
		strSql += "     A.SHORI_NENDO = ?";
		strSql += " AND A.GAKKIKBNCD = ?";
		strSql += " AND A.KIJUN_DATE = ?";
		strSql += " AND A.G_BUKYOKUCD = ?";
		strSql += " AND A.GAKUNEN = ?";
		strSql += " AND A.NUM = ?";
		strSql += " ORDER BY";
		strSql += "     A.G_SYSNO";

		// パラメータの設定
		List<Object> valueList = new ArrayList<Object>();
		valueList.add(paramDto.getShoriJokenMst().getShorinendo());
		valueList.add(paramDto.getShoriJokenMst().getGakkikbncd());
		valueList.add(paramDto.getShoriJokenMst().getKijundate());
		valueList.add(paramDto.getgBukyokucd());
		valueList.add(paramDto.getGakunen());
		valueList.add(paramDto.getNum());

		// SQLの実行
		return get(paramDto.getRuser(), strSql, valueList);
	}

	/**
	 * 最大連番を採番する
	 * @param paramDto
	 * @return
	 */
	public ArrayList<ShugakusienHanteiData> getMaxNum(ParameterDto paramDto) {

		// SQL文の作成
		String strSql = "";
		strSql += " SELECT";
		strSql += "     IFNULL(MAX(A.NUM), 0) + 1 AS NUM";
		strSql += " FROM";
		strSql += "     shugakusien_hantei_data A";
		strSql += " WHERE";
		strSql += "     A.SHORI_NENDO = ?";
		strSql += " AND A.GAKKIKBNCD = ?";
		strSql += " AND A.KIJUN_DATE = ?";
		strSql += " AND A.G_BUKYOKUCD = ?";
		strSql += " AND A.GAKUNEN = ?";

		// パラメータの設定
		List<Object> valueList = new ArrayList<Object>();
		valueList.add(paramDto.getShoriJokenMst().getShorinendo());
		valueList.add(paramDto.getShoriJokenMst().getGakkikbncd());
		valueList.add(paramDto.getShoriJokenMst().getKijundate());
		valueList.add(paramDto.getgBukyokucd());
		valueList.add(paramDto.getGakunen());

		// SQLの実行
		return get(paramDto.getRuser(), strSql, valueList);
	}

	/**
	 * 対象の日付(yyyyMMdd)が含まれる年度を取得する
	 * @param taishoYMD
	 * @return String
	 */
	private String getNendo(String taishoYMD) {
		if (taishoYMD.length() != 8) {
			return "9999";
		}

		Integer nen = Integer.parseInt(taishoYMD.substring(0, 4));
		Integer mon = Integer.parseInt(taishoYMD.substring(4, 6));

		if (mon >= 1 && mon <= 3) {
			nen = nen - 1;
		}
		return nen.toString();
	}

	/**
	 * 判定データを登録する
	 * @param paramDto
	 * @return
	 */
	public Boolean addJudgeData(ParameterDto paramDto, ArrayList<ShugakusienHanteiData> dataList) throws Exception {

		Boolean ret = false;

		Connection conn = null;
		PreparedStatement pstmt = null;
		java.sql.Date today = new java.sql.Date(System.currentTimeMillis());

		String strSql = "";
		strSql += " INSERT IGNORE INTO shugakusien_hantei_data (";
		strSql += "  SHORI_NENDO";
		strSql += " ,GAKKIKBNCD";
		strSql += " ,KIJUN_DATE";
		strSql += " ,G_BUKYOKUCD";
		strSql += " ,GAKUNEN";
		strSql += " ,NUM";
		strSql += " ,G_SYSNO";
		strSql += " ,G_NAME";
		strSql += " ,JUDGE_TYPE";
		strSql += " ,Z_JUDGE";
		strSql += " ,T_JUDGE";
		strSql += " ,Z_GPA";
		strSql += " ,Z_GPA_B";
		strSql += " ,Z_TANI";
		strSql += " ,Z_TANI_B";
		strSql += " ,H_SOTSU";
		strSql += " ,H_SOTSU_B";
		strSql += " ,H_TANI";
		strSql += " ,H_TANI_B";
		strSql += " ,H_SHU";
		strSql += " ,H_SHU_B";
		strSql += " ,H_ZEN";
		strSql += " ,H_ZEN_B";
		strSql += " ,K_TANI";
		strSql += " ,K_TANI_B";
		strSql += " ,K_GPA";
		strSql += " ,K_GPA_B";
		strSql += " ,K_SHU";
		strSql += " ,K_SHU_B";
		strSql += " ,S_TANI";
		strSql += " ,S_TANI_B";
		strSql += " ,S_SHU";
		strSql += " ,S_SHU_B";
		strSql += " ,BIKOU";
		strSql += " ,T_GPT";
		strSql += " ,T_GP_TANISU";
		strSql += " ,T_GPA";
		strSql += " ,T_GPA_B";
		strSql += " ,T_GPA_JUNI";
		strSql += " ,N_GPT";
		strSql += " ,N_GP_TANISU";
		strSql += " ,N_GPA";
		strSql += " ,N_GPA_B";
		strSql += " ,N_GPA_JUNI";
		strSql += " ,GPA_PARAM";
		strSql += " ,ZAISEKI_MONTH";
		strSql += " ,KYUGAKU_MONTH";
		strSql += " ,ZAIGAKU_NENSU";
		strSql += " ,HITSUYO_TANISU";
		strSql += " ,SHUGYO_NENGEN";
		strSql += " ,HYOJUN_TANISU";
		strSql += " ,SHUTOKU_TANISU";
		strSql += " ,SHUTOKU_RITSU";
		strSql += " ,RISHU_TANISU";
		strSql += " ,KESSEKI_TANISU";
		strSql += " ,SHUSSEKI_RITSU";
		strSql += " ,RYUNENDATA";
		strSql += " ,KEIKAKUSHO";
		strSql += " ,INSERT_DATE";
		strSql += " ,UPDATE_DATE";
		strSql += " ,USERID";
		strSql += " ) VALUES (";
		strSql += "  ?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " )";

		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(strSql);

			for (ShugakusienHanteiData data : dataList) {
				List<Object> valueList = new ArrayList<Object>();
				valueList.add(paramDto.getShoriJokenMst().getShorinendo());
				valueList.add(paramDto.getShoriJokenMst().getGakkikbncd());
				valueList.add(paramDto.getShoriJokenMst().getKijundate());
				valueList.add(paramDto.getgBukyokucd());
				valueList.add(paramDto.getGakunen());
				valueList.add(paramDto.getNum());
				valueList.add(data.getG_gakuno());
				valueList.add(data.getG_name());
				valueList.add(data.getJudge_type());
				valueList.add(data.getZ_judge());
				valueList.add(data.getT_judge());
				valueList.add(data.getZ_gpa());
				valueList.add(data.getZ_gpa_b());
				valueList.add(data.getZ_tani());
				valueList.add(data.getZ_tani_b());
				valueList.add(data.getH_sotsu());
				valueList.add(data.getH_sotsu_b());
				valueList.add(data.getH_tani());
				valueList.add(data.getH_tani_b());
				valueList.add(data.getH_shu());
				valueList.add(data.getH_shu_b());
				valueList.add(data.getH_zen());
				valueList.add(data.getH_zen_b());
				valueList.add(data.getK_tani());
				valueList.add(data.getK_tani_b());
				valueList.add(data.getK_gpa());
				valueList.add(data.getK_gpa_b());
				valueList.add(data.getK_shu());
				valueList.add(data.getK_shu_b());
				valueList.add(data.getS_tani());
				valueList.add(data.getS_tani_b());
				valueList.add(data.getS_shu());
				valueList.add(data.getS_shu_b());
				valueList.add(data.getBikou());
				valueList.add(data.getT_gpt());
				valueList.add(data.getT_gp_tanisu());
				valueList.add(data.getT_gpa());
				valueList.add(data.getT_gpa_b());
				valueList.add(data.getT_gpa_juni());
				valueList.add(data.getN_gpt());
				valueList.add(data.getN_gp_tanisu());
				valueList.add(data.getN_gpa());
				valueList.add(data.getN_gpa_b());
				valueList.add(data.getN_gpa_juni());
				valueList.add(data.getGpa_param());
				valueList.add(data.getZaiseki_month());
				valueList.add(data.getKyugaku_month());
				valueList.add(data.getZaigaku_nensu());
				valueList.add(data.getHitsuyo_tanisu());
				valueList.add(data.getShugyo_nengen());
				valueList.add(data.getHyojun_tanisu());
				valueList.add(data.getShutoku_tanisu());
				valueList.add(data.getShutoku_ritsu());
				valueList.add(data.getRishu_tanisu());
				valueList.add(data.getKesseki_tanisu());
				valueList.add(data.getShusseki_ritsu());
				valueList.add(data.getRyunendata());
				valueList.add(data.getKeikakusho());
				valueList.add(today);
				valueList.add(today);
				valueList.add(paramDto.getRuser());

				// パラメータを設定
				setPreparedStatement(pstmt, valueList);

				// SQL実行
				pstmt.executeUpdate();
			}

			// 同じconnectionで履歴も追加する
			addJudgeHistory(paramDto, conn);

			// 年度末判定の場合は支援状況を登録する
			if (END_OF_YEAR.equals(paramDto.getShoriJokenMst().getGakkikbncd())) {
				ShugakusienJokyoDao sienDao = new ShugakusienJokyoDao();
				sienDao.registSupportStatus(paramDto, dataList, conn);
			}
			conn.commit();

			ret = true;
		} catch (Exception ex) {
			try {
				conn.rollback();
			} catch (Exception e) {
				logger.error(e);
			}
			throw ex;
		} finally {
			try {
				if (pstmt != null) pstmt.close();
				if (conn != null) conn.close();
			} catch (Exception ex) {
				logger.error(ex);
				throw ex;
			}
		}
		return ret;
	}

	/**
	 * 判定結果履歴データを登録する
	 * @param paramDto
	 * @param conn
	 * @return
	 */
	public Boolean addJudgeHistory(ParameterDto paramDto, Connection conn) throws Exception {

		Boolean ret = false;

		PreparedStatement pstmt = null;
		java.sql.Date today = new java.sql.Date(System.currentTimeMillis());

		String strSql = "";
		strSql += " INSERT IGNORE INTO shugakusien_hantei_rireki (";
		strSql += "  SHORI_NENDO";
		strSql += " ,GAKKIKBNCD";
		strSql += " ,KIJUN_DATE";
		strSql += " ,G_BUKYOKUCD";
		strSql += " ,GAKUNEN";
		strSql += " ,NUM";
		strSql += " ,INSERT_DATE";
		strSql += " ,UPDATE_DATE";
		strSql += " ,USERID";
		strSql += " ) VALUES (";
		strSql += "  " + paramDto.getShoriJokenMst().getShorinendo();
		strSql += " ,'" + paramDto.getShoriJokenMst().getGakkikbncd() + "'";
		strSql += " ,'" + paramDto.getShoriJokenMst().getKijundate() + "'";
		strSql += " ,'" + paramDto.getgBukyokucd() + "'";
		strSql += " , " + paramDto.getGakunen();
		strSql += " , " + paramDto.getNum();
		strSql += " ,'" + today + "'";
		strSql += " ,'" + today + "'";
		strSql += " ,'" + paramDto.getRuser() + "'";
		strSql += " )";

		try {
			pstmt = conn.prepareStatement(strSql);
			pstmt.executeUpdate();

			ret = true;
		} catch (Exception ex) {
			logger.error(ex);
			throw ex;
		}
		return ret;
	}
}