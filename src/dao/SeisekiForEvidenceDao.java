package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import dao.entity.SeisekiForEvidence;
import dto.ParameterDto;

/**
 * @author nakano@cc.kumamoto-u.ac.jp
 * 2015-02-26 nakano@cc.kumamoto-u.ac.jp
 *
 */
public class SeisekiForEvidenceDao extends AbstractDao<SeisekiForEvidence> {

    public SeisekiForEvidenceDao() {
        super();
    }

    @Override
    ArrayList<SeisekiForEvidence> setEntityList(ResultSet rs){
        SeisekiForEvidence entity = new SeisekiForEvidence();
        return entity.setRsToList(rs);
    }

    /**
     * G_SYSNOを指定し、最大のKAMOKUCDを採番して返す
     * @param ParameterDto
     * @return String(code)
     */
    public String checkGSysNoMakeCode(ParameterDto paramDto){
        String code = "00001";

        // 検索用SQLの取得
        // 2019.03.26 s.furuta chg start 顧客検証対応No2
        //   削除時、seiseki_for_evidence以外の4テーブル(results,studying_abroad_experiences,external_english_tests,attachments)はデータが削除されないため
        //   削除後の登録時、seiseki_for_evidenceからkamokucdのMaxを取ると他テーブルでキーが被る
        //   この場合、Insertが主キー制約で通らず、削除したはずのデータが残るような形となる
        //   ⇒5テーブルを通してのMax値を取得する
        //String strSql = getSql("SeisekiForEvidenceDao_getMaxKamokucdForGSysNo.sql");
        String strSql = getSql("SeisekiForEvidenceDao_getMaxKamokucdForGSysNo02.sql");
        // 2019.03.26 s.furuta chg end

        // SQLの?に設定する値をListに設定
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(paramDto.getSeisekiForEvidence().getgSysno());
        // 2019.03.26 s.furuta add start 検証対応No2
        //   5テーブルそれぞれで学生番号が必要なので、パラメータ4つ追加
        valueList.add(paramDto.getSeisekiForEvidence().getgSysno());
        valueList.add(paramDto.getSeisekiForEvidence().getgSysno());
        valueList.add(paramDto.getSeisekiForEvidence().getgSysno());
        valueList.add(paramDto.getSeisekiForEvidence().getgSysno());
        // 2019.03.26 s.furuta add end

        ArrayList<SeisekiForEvidence> res = null;
        res = get(paramDto.getRuser(),strSql,valueList);

        for(SeisekiForEvidence evi : res){
            // 見つかったらそのコード+1を返す
            if(evi.getKamokucd() != null){
                int intCode = Integer.parseInt(evi.getKamokucd());
                intCode++;
                return String.format("%05d", intCode);
            }
        }

        return code;
    }

    /**
     * ParameterDtoを引数として1件登録
     * @param paramDto
     * @return
     */
    public int insertSeisekiForEvidence01(ParameterDto paramDto){
        // SQLの取得
        String strSql = getSql("SeisekiForEvidenceDao_insertSeisekiForEvidence01.sql");

        // SQLの?に設定する値をListに順に設定する
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(paramDto.getSeisekiForEvidence().getgSysno());
        valueList.add(paramDto.getSeisekiForEvidence().getKamokucd());
        valueList.add(paramDto.getSeisekiForEvidence().getKamokunm());
        valueList.add(paramDto.getSeisekiForEvidence().getKamokunmeng());
        valueList.add(paramDto.getSeisekiForEvidence().getHyogocd());
        valueList.add(paramDto.getSeisekiForEvidence().getHyogonm());
        valueList.add(paramDto.getSeisekiForEvidence().getTanisu());
        valueList.add(paramDto.getSeisekiForEvidence().getNinteiNendo());
        valueList.add(paramDto.getSeisekiForEvidence().getNinteiGakkikbncd());
        valueList.add(paramDto.getSeisekiForEvidence().getHitsusenkbncd());
        valueList.add(paramDto.getSeisekiForEvidence().getJikanwariNendo());
        valueList.add(paramDto.getSeisekiForEvidence().getJikanwariShozokucd());
        valueList.add(paramDto.getSeisekiForEvidence().getJikanwaricd());
        valueList.add(paramDto.getSeisekiForEvidence().getEnterdFlg());
        valueList.add(paramDto.getSeisekiForEvidence().getCode1());
        valueList.add(paramDto.getSeisekiForEvidence().getCode2());
        valueList.add(paramDto.getSeisekiForEvidence().getCode3());
        valueList.add(paramDto.getSeisekiForEvidence().getCode4());
        valueList.add(paramDto.getSeisekiForEvidence().getCode5());
        valueList.add(paramDto.getSeisekiForEvidence().getCode6());
        valueList.add(paramDto.getSeisekiForEvidence().getCode7());
        valueList.add(paramDto.getSeisekiForEvidence().getComment());
        valueList.add(paramDto.getSeisekiForEvidence().getDir());
        valueList.add(paramDto.getSeisekiForEvidence().getEvidence());
        valueList.add(paramDto.getSeisekiForEvidence().getSize());
        valueList.add(paramDto.getSeisekiForEvidence().getLastUpdate());
        java.sql.Date today = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
// 2019.02.01 s.furuta chg start 2.学修成果修正
//        valueList.add(today);
        valueList.add(paramDto.getSeisekiForEvidence().getInsertDate());
// 2019.02.01 s.furuta chg end
        valueList.add(today);
        valueList.add(paramDto.getRuser());

        return update(paramDto.getRuser(),strSql,valueList);
    }

    /**
     * ParameterDtoを引数として1件削除
     * @param paramDto
     * @return
     */
    public int deleteSeisekiForEvidence01(ParameterDto paramDto){
        // SQLの取得
        String strSql = getSql("SeisekiForEvidenceDao_deleteSeisekiForEvidence01.sql");

        // SQLの?に設定する値をListに順に設定する
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(paramDto.getUid());
        valueList.add(paramDto.getSysNo());

        return update(paramDto.getRuser(),strSql,valueList);
    }

// 2019.01.17 s.furuta add start 2.学修成果修正
    public ArrayList<SeisekiForEvidence> getSeisekiForEvidence01(ParameterDto paramDto) {

        //外だしSQLの取得
        String strSql = getSql("SeisekiForEvidenceDao_getSeisekiForEvidence01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(new String(paramDto.getUid().replace("-", "").toLowerCase()));
        //2.科目コード
        valueList.add(new String(paramDto.getKamokuCd()));
        return get(paramDto.getRuser(),strSql,valueList);
    }
    public int deleteSeisekiForEvidence02(ParameterDto paramDto){
        // SQLの取得
        String strSql = getSql("SeisekiForEvidenceDao_deleteSeisekiForEvidence02.sql");

        // SQLの?に設定する値をListに順に設定する
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(paramDto.getUid().replace("-", "").toLowerCase());
        valueList.add(paramDto.getSeisekiForEvidence().getKamokucd());

        return update(paramDto.getRuser(),strSql,valueList);
    }

// 2019.01.17 s.furuta add end
}
