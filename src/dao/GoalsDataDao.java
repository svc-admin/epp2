package dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import dao.entity.GoalsData;

public class GoalsDataDao extends AbstractDao<GoalsData> {

    public GoalsDataDao() {
        super();
    }

    @Override
    ArrayList<GoalsData> setEntityList(ResultSet rs) {
        GoalsData entity = new GoalsData();
        return entity.setRsToList(rs);
    }

}
