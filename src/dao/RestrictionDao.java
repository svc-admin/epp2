package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.Restriction;

public class RestrictionDao extends AbstractDao<Restriction> {

    public RestrictionDao() {
        super();
    }

    @Override
    ArrayList<Restriction> setEntityList(ResultSet rs) {
        Restriction entity = new Restriction();
        return entity.setRsToList(rs);
    }

    public ArrayList<Restriction> getRestrictionList01(String ruser) {
        //外だしSQLの取得
        String strSql = getSql("RestrictionDao_getRestrictionList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
//        //1.対象ユーザ (熊大ID)
//        valueList.add(new Integer(rid));

        return get(ruser,strSql,valueList);
    }

}
