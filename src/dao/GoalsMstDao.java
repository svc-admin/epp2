package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.GoalsMst;
import dto.ParameterDto;

public class GoalsMstDao extends AbstractDao<GoalsMst> {

    public GoalsMstDao() {
        super();
    }

    @Override
    ArrayList<GoalsMst> setEntityList(ResultSet rs) {
        GoalsMst entity = new GoalsMst();
        return entity.setRsToList(rs);
    }

    public ArrayList<GoalsMst> getGoalsMstList01(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GoalsMstDao_getGoalsMstList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.num
        valueList.add(pramDto.getNum());
        // 2015.02.16 SVC 追加 >>
        //2.DAIGAKUINKBNCD
        if (pramDto.getDaigakuinKbncd() == null)
            pramDto.setDaigakuinKbncd("0");
        valueList.add(pramDto.getDaigakuinKbncd());
        // 2015.02.16 SVC 追加 <<

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<GoalsMst> getGoalsMstList02(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GoalsMstDao_getGoalsMstList02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();

        // 2015.02.16 SVC 追加 >>
        //1.DAIGAKUINKBNCD
        if (pramDto.getDaigakuinKbncd() == null)
            pramDto.setDaigakuinKbncd("0");
        valueList.add(pramDto.getDaigakuinKbncd());
        // 2015.02.16 SVC 追加 <<

        return get(pramDto.getRuser(),strSql,valueList);
    }

//    public ArrayList<GoalsMst> getGoalsMstList03(ParameterDto pramDto) {
//
//        //外だしSQLの取得
//        String strSql = getSql("GoalsMstDao_getGoalsMstList03.sql");
//
//        //SQLの?に設定する値をListに順に設定する。
//        List<Object> valueList = new ArrayList<Object>();
//
//        return get(pramDto.getRuser(),strSql,valueList);
//    }

//    public ArrayList<GoalsMst> getGoalsMstList04(ParameterDto pramDto) {
//
////        //外だしSQLの取得
////        String strSql = getSql("GoalsMstDao_getGoalsMstList04.sql");
////
////        //SQLの?に設定する値をListに順に設定する。
////       List<Object> valueList = new ArrayList<Object>();
////
////        return get(pramDto.getRuser(),strSql,valueList);
//    	ArrayList<GoalsMst> list = new ArrayList<GoalsMst>();
//    	if(pramDto.getDaigakuinKbncd() == null || pramDto.getDaigakuinKbncd().equals("0")){
//    		list.add(new GoalsMst());
//        	list.add(new GoalsMst());
//        	list.add(new GoalsMst());
//        	list.add(new GoalsMst());
//        	list.add(new GoalsMst());
//        	list.add(new GoalsMst());
//        	list.add(new GoalsMst());
//    	}
//    	else{
//    		list.add(new GoalsMst());
//        	list.add(new GoalsMst());
//        	list.add(new GoalsMst());
//        	list.add(new GoalsMst());
//    	}
//
//    	return list;
//    }

}
