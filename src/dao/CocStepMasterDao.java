package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.CocStepMaster;
import dto.ParameterDto;

public class CocStepMasterDao extends AbstractDao<CocStepMaster>{

    @Override
    ArrayList<CocStepMaster> setEntityList(ResultSet rs) {
        CocStepMaster entity = new CocStepMaster();
        return entity.setEntityList(rs);
    }

    /**
     * COC関連事業の各ステップ情報の取得
     * @param pramDto
     * @return
     */
    public ArrayList<CocStepMaster> getStepInfoList01(ParameterDto pramDto){
        //外だしSQLの取得
        String strSql = getSql("CocStepMasterDao_getStepInfoList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.COC区分：1=COC,2=COC+
        valueList.add("1");

        return get(pramDto.getRuser(),strSql,valueList);
    }

    /**
     * COC+関連事業の各ステップの取得
     * @param pramDto
     * @return
     */
    public ArrayList<CocStepMaster> getStepInfoList02(ParameterDto pramDto){
        //外だしSQLの取得
        String strSql = getSql("CocStepMasterDao_getStepInfoList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.COC区分：1=COC,2=COC+
        valueList.add("2");

       return get(pramDto.getRuser(),strSql,valueList);
    }
// 2017.01.20 SVC ADD START >> COC機能拡張
    /**
     * COC+関連事業の各ステップの取得
     * @param pramDto
     * @return
     */
    public ArrayList<CocStepMaster> getStepInfoList03(ParameterDto pramDto){
        //外だしSQLの取得
        String strSql = getSql("CocStepMasterDao_getStepInfoList02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.COC区分：1=COC,2=COC+
        valueList.add(pramDto.getCocOrPlus());

        return get(pramDto.getRuser(),strSql,valueList);
    }
    /**
     * COC+関連事業の各ステップにおける必要単位数の取得
     * @param pramDto
     * @return
     */
    public ArrayList<CocStepMaster> getStepInfoList04(ParameterDto pramDto){
        //外だしSQLの取得
        String strSql = getSql("CocStepMasterDao_getStepInfoList03.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.COC区分：1=COC,2=COC+
        valueList.add(pramDto.getCocOrPlus());

        return get(pramDto.getRuser(),strSql,valueList);
    }
// 2017.01.20 SVC ADD END <<
}
