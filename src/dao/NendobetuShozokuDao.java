package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.NendobetuShozoku;

public class NendobetuShozokuDao extends AbstractDao<NendobetuShozoku> {

    public NendobetuShozokuDao() {
        super();
    }

    @Override
    ArrayList<NendobetuShozoku> setEntityList(ResultSet rs) {
        NendobetuShozoku entity = new NendobetuShozoku();
        return entity.setRsToList(rs);
    }

    public ArrayList<NendobetuShozoku> getNendobetuShozokuList01(String ruser,
            int startYear, int endYear, int cdLength) {
        //外だしSQLの取得
        String strSql = getSql("NendobetuShozokuDao_getNendobetuShozokuList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.入学年度の開始年
        valueList.add(new Integer(startYear));
        //2.終了年
        valueList.add(new Integer(endYear));
        //3.部局コード長
        valueList.add(new Integer(cdLength));

        return get(ruser,strSql,valueList);
    }

    public ArrayList<NendobetuShozoku> getDaigakuinKbn01(String ruser,
            int yokennendo, String shozokucd) {
        //外だしSQLの取得
        String strSql = getSql("NendobetuShozokuDao_getDaigakuinKbn01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.要件年度
        valueList.add(new Integer(yokennendo));
        //2.所属コード
        valueList.add(new Integer(shozokucd));

        return get(ruser,strSql,valueList);
    }

//  2019.12.02 SVC 修学支援判定 add start
    public ArrayList<NendobetuShozoku> getShozokunm01( String shozokucd) {
        //外だしSQLの取得
        String strSql = getSql("NendobetuShozokuDao_getShozokunm01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.要件年度
        valueList.add(shozokucd);

        return get("",strSql,valueList);
    }
//  2019.12.02 SVC 修学支援判定 add end
}
