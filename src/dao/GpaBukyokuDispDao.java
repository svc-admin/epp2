package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.GpaBukyokuDisp;
import dto.ParameterDto;

public class GpaBukyokuDispDao extends AbstractDao<GpaBukyokuDisp> {

    public GpaBukyokuDispDao() {
        super();
    }

    @Override
    ArrayList<GpaBukyokuDisp> setEntityList(ResultSet rs) {
        GpaBukyokuDisp entity = new GpaBukyokuDisp();
        return entity.setRsToList(rs);
    }

    public ArrayList<GpaBukyokuDisp> getGpaBukyokuDispList01(ParameterDto pramDto, String bukyokucd) {

        //外だしSQLの取得
        String strSql = getSql("GpaBukyokuDispDao_getGpaBukyokuDispList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.部局コード
        valueList.add(bukyokucd);

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public int delBukyokuDisp01(ParameterDto pramDto, String bukyokucd){

        String bukyokuCd = bukyokucd;

        //外だしSQLの取得
        String strSql = getSql("GpaBukyokuDispDao_deleteGpaBukyokuDispList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.削除部局コード
            valueList.add(bukyokuCd);

        return update(pramDto.getRuser(),strSql,valueList);
    }

    public int addBukyokuDisp01(ParameterDto pramDto, String bukyokucd, Integer dispflg){

        String bukyokuCd = bukyokucd;
        Integer dispFlg = dispflg;

        //外だしSQLの取得
        String strSql = getSql("GpaBukyokuDispDao_insertGpaBukyokuDispList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.追加部局コード
            valueList.add(bukyokuCd);
        //2.表示フラグ
            valueList.add(dispFlg);
        //3.ユーザID
            valueList.add(pramDto.getRuser());

        return update(pramDto.getRuser(),strSql,valueList);
    }

    public int editBukyokuDisp01(ParameterDto pramDto, String bukyokucd, Integer dispflg){

        Integer ret = 0;

        ret = this.delBukyokuDisp01(pramDto,bukyokucd);
        if(ret >= 0){
             ret = this.addBukyokuDisp01(pramDto, bukyokucd, dispflg);
        }

        return ret;
    }

}
