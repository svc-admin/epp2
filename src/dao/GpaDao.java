package dao;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.Gpa;
import dto.ParameterDto;

public class GpaDao extends AbstractDao<Gpa> {

    public GpaDao() {
        super();
    }

    @Override
    ArrayList<Gpa> setEntityList(ResultSet rs) {
        Gpa entity = new Gpa();
        return entity.setRsToList(rs);
    }

    public ArrayList<Gpa> getGpaList01(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_getGpaList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<Gpa> getGpaList02(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_getGpaList02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<Gpa> getGpaList03(ParameterDto pramDto,String uid) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_getGpaList03.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(uid);

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<Gpa> getGpaList04(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_getGpaList04.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<Gpa> getGpaList05(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_getGpaList05.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<Gpa> getGpaList06(String ruser, String uid) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_getGpaList06.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(uid);

        return get(ruser,strSql,valueList);
    }

    public ArrayList<Gpa> getGpaList07(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_getGpaList07.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<Gpa> getGpaList08(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_getGpaList08.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

// 2018.06.04 SVC)S.Furuta ADD START >> 累積GPA・GPT表示対応
    public ArrayList<Gpa> getGpaList09(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_getGpaList09.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }
//2018.06.04 SVC)S.Furuta ADD END <<

    public int getGpaCount01(String ruser, String uid) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_getGpaCount01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(uid);

        return get(ruser,strSql,valueList).size();
    }

    public int getGpaCount02(String ruser, String uid, int year, int sem) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_getGpaCount02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(uid);
        //2.
        valueList.add(new Integer(year));
        //3.
        valueList.add(new Integer(sem));


        return get(ruser,strSql,valueList).size();
    }

    public int getGpaCount03(String ruser, String uid, int goal) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_getGpaCount03.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(uid);
        //2.
        valueList.add(new Integer(goal));

        return get(ruser,strSql,valueList).size();
    }

    public int insertGpa01(String ruser, String uid, double[] aCalc, Date today) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_insertGpa01.sql");

        //SQLの?に設定する値をListに順に設定する。aCalc = {0, 0, 0}; // 途中計算用配列 : 取得単位数, GPA計算用単位数, GPT
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(uid);
        valueList.add(new BigDecimal(aCalc[1]!=0 ? aCalc[2]/aCalc[1] : 0));
        valueList.add(new BigDecimal(aCalc[0]));
        valueList.add(new BigDecimal(aCalc[2]));
        valueList.add(today);
        valueList.add(today);
        valueList.add(ruser);

        return update(ruser,strSql,valueList);

    }

    public int insertGpa02(String ruser, String uid, int year, int sem, double[] aCalc, Date today) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_insertGpa02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(uid);
        valueList.add(new Integer(year));
        valueList.add(new Integer(sem));
        valueList.add(new BigDecimal(aCalc[1]!=0 ? aCalc[2]/aCalc[1] : 0));
        valueList.add(new BigDecimal(aCalc[0]));
        valueList.add(new BigDecimal(aCalc[2]));
        valueList.add(today);
        valueList.add(today);
        valueList.add(ruser);

        return update(ruser,strSql,valueList);

    }

    public int insertGpa03(String ruser, String uid, int goal, double[] aCalc,
            Date today) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_insertGpa03.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(uid);
        valueList.add(new Integer(goal));
        valueList.add(new BigDecimal(aCalc[1]!=0 ? aCalc[2]/aCalc[1] : 0));
        valueList.add(new BigDecimal(aCalc[0]));
        valueList.add(new BigDecimal(aCalc[2]));
        valueList.add(today);
        valueList.add(today);
        valueList.add(ruser);

        return update(ruser,strSql,valueList);

    }

    public int deleteGpa01(String ruser, List<List<Object>> paramList) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_deleteGpa01.sql");

        return updateAll(ruser,strSql,paramList);

    }

    public int updateGpa01(String ruser, String uid, double[] aCalc, Date today) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_updateGpa01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(uid);
        valueList.add(new BigDecimal(aCalc[1]!=0 ? aCalc[2]/aCalc[1] : 0));
        valueList.add(new BigDecimal(aCalc[0]));
        valueList.add(new BigDecimal(aCalc[2]));
        valueList.add(today);
        valueList.add(today);
        valueList.add(ruser);
        valueList.add(uid);

        return update(ruser,strSql,valueList);

    }

    public int updateGpa02(String ruser, String uid, double[] aCalc,
            Date today, int year, int sem) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_updateGpa02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(uid);
        valueList.add(new BigDecimal(aCalc[1]!=0 ? aCalc[2]/aCalc[1] : 0));
        valueList.add(new BigDecimal(aCalc[0]));
        valueList.add(new BigDecimal(aCalc[2]));
        valueList.add(today);
        valueList.add(today);
        valueList.add(ruser);
        valueList.add(uid);
        valueList.add(new Integer(year));
        valueList.add(new Integer(sem));

        return update(ruser,strSql,valueList);

    }

    public int updateGpa03(String ruser, String uid, double[] aCalc,
            Date today, int goal) {

        //外だしSQLの取得
        String strSql = getSql("GpaDao_updateGpa03.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(uid);
        valueList.add(new BigDecimal(aCalc[1]!=0 ? aCalc[2]/aCalc[1] : 0));
        valueList.add(new BigDecimal(aCalc[0]));
        valueList.add(new BigDecimal(aCalc[2]));
        valueList.add(today);
        valueList.add(today);
        valueList.add(ruser);
        valueList.add(uid);
        valueList.add(new Integer(goal));

        return update(ruser,strSql,valueList);

    }




}
