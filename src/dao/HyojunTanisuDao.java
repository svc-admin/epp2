package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.HyojunTanisu;
import dto.ParameterDto;

public class HyojunTanisuDao extends AbstractDao<HyojunTanisu> {

    private static Logger logger = null;

    public HyojunTanisuDao() {
        super();
    }

    @Override
    ArrayList<HyojunTanisu> setEntityList(ResultSet rs) {
        HyojunTanisu entity = new HyojunTanisu();
        return entity.setRsToList(rs);
    }

    public ArrayList<HyojunTanisu> getHyojunTanisu() {

    	String strSql = getSql("HyojunTanisuDao_getHyojunTanisu.sql");
    	List<Object> valueList = new ArrayList<Object>();
    	return get("",strSql,valueList);
    }

    public Boolean deleteHyojunTanisu01(ParameterDto pramDto) {

        Boolean res = false;

        Connection conn = null;
        PreparedStatement pstmt = null;

        String strSql = "delete from hyojun_tanisu where ";
        String strWhere = "";

        if(pramDto.getNyugakunendo().equals("") == false){
            strWhere = strWhere + "NYUGAKU_NENDO = " + pramDto.getNyugakunendo();
        }
        if(pramDto.getHyojunTanisu().getgBukyokucd().equals("") == false){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "G_BUKYOKUCD = '" + pramDto.getHyojunTanisu().getgBukyokucd() + "' ";
        }
        // queryの生成
        strSql = strSql + strWhere;

        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            int num = pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return true;
    }

    public Boolean addHyojunTanisu01(ParameterDto pramDto) {

        Boolean res = false;
        System.out.println(pramDto.getNyugakunendo());
        System.out.println(pramDto.getHyojunTanisu().getgBukyokucd());
        System.out.println(pramDto.getHyojunTanisu().getHitsuyotanisu());
        System.out.println(pramDto.getHyojunTanisu().getSyugyonengen());

        Connection conn = null;
        PreparedStatement pstmt = null;

        String strSql = "insert into hyojun_tanisu (NYUGAKU_NENDO";

        String strPram = ")values(" ;

        if(pramDto.getNyugakunendo().equals("") == false){
            strPram = strPram + pramDto.getNyugakunendo();
        }

        strSql = strSql + ",G_BUKYOKUCD";
        strPram = strPram + ",'" + pramDto.getHyojunTanisu().getgBukyokucd() + "'";

        if(pramDto.getHyojunTanisu().getHitsuyotanisu() != null){
            strSql = strSql + ",HITSUYOTANISU";
            strPram = strPram + ",'" + pramDto.getHyojunTanisu().getHitsuyotanisu() + "'";
        }
        if(pramDto.getHyojunTanisu().getSyugyonengen() != 0){
            strSql = strSql + ",SYUGYONENGEN";
            strPram = strPram + ",'" + pramDto.getHyojunTanisu().getSyugyonengen() + "'";
        }

        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
        strSql = strSql + ",INSERT_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",UPDATE_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",USERID";
        strPram = strPram + ",'" + pramDto.getRuser() + "'";

        strSql = strSql + strPram + ")";

        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            int num = pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return true;
    }

}
