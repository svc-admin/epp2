package dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import dao.entity.NyugakuNendos;

public class NyugakuNendosDao extends AbstractDao<NyugakuNendos> {

    public NyugakuNendosDao() {
        super();
    }

    @Override
    ArrayList<NyugakuNendos> setEntityList(ResultSet rs) {
        NyugakuNendos entity = new NyugakuNendos();
        return entity.setRsToList(rs);
    }

}
