package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.cstmentity.GoalSeiseki;
import dto.ParameterDto;

public class GoalSeisekiDao extends AbstractDao<GoalSeiseki> {

    public GoalSeisekiDao() {
        super();
    }

    @Override
    ArrayList<GoalSeiseki> setEntityList(ResultSet rs) {
        GoalSeiseki entity = new GoalSeiseki();
        return entity.setRsToList(rs);
    }

    public ArrayList<GoalSeiseki> getGoalSeisekiList01(ParameterDto pramDto) {
        //外だしSQLの取得
        String strSql = getSql("GoalSeisekiDao_getGoalSeisekiList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生ID
        valueList.add(pramDto.getUid());
        valueList.add(pramDto.getUid());
// 2017.03.08 SVC DEL START >> 速度改善対応
//        valueList.add(pramDto.getUid());
// 2017.03.08 SVC DEL END   <<
       return get(pramDto.getRuser(),strSql,valueList);
    }

}
