package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.Authority;
import dto.ParameterDto;

public class AuthorityDao extends AbstractDao<Authority> {

    private static Logger logger = null;

    public AuthorityDao() {
        super();
    }

    @Override
    ArrayList<Authority> setEntityList(ResultSet rs) {
        Authority entity = new Authority();
        return entity.setRsToList(rs);
    }

    public ArrayList<Authority> getAuthority01(String ruser) {

        //外だしSQLの取得
        String strSql = getSql("AuthorityDao_getAuthority01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.ユーザID
        valueList.add(new String(ruser));
// 2017/03/09 SVC)Furuta ADD START >> ポートフォリオ改善
        valueList.add(new String(ruser));
        valueList.add(new String(ruser));
// 2017/03/09 SVC)Furuta ADD END   <<
        return get(ruser,strSql,valueList);
    }

// 2018.06.14 SVC)S.Furuta ADD START >> アクセス制限設定機構の改善対応
    public ArrayList<Authority> getAuthority02(String ruser, String ruid) {

        //外だしSQLの取得
        String strSql = getSql("AuthorityDao_getAuthority02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.ユーザID
        valueList.add(new String(ruid));
        return get(ruser,strSql,valueList);
    }
// 2018.06.14 SVC)S.Furuta ADD START <<

 // 2018.11.28 KU)Nakano ADD START >> ku.util.AuthorityTable.java用 (権限リストにデフォルトの権限を表示しないため)
    public ArrayList<Authority> getAuthority03(String ruser) {

        //外だしSQLの取得
        String strSql = getSql("AuthorityDao_getAuthority03.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.ユーザID
        valueList.add(new String(ruser));
        return get(ruser,strSql,valueList);
    }
    // 2018.11.28 KU)Nakano ADD END <<

    public Boolean deleteAuthority01(ParameterDto pramDto) {

        Boolean res = false;

        Connection conn = null;
        PreparedStatement pstmt = null;

        String strSql = "delete from authority where uid = '" + pramDto.getUid() + "' ";

        String strWhere = "";

        if(pramDto.getYokenNendo().equals("") == false) {
            strWhere = strWhere + "and YOKEN_NENDO = '" + pramDto.getYokenNendo() + "' ";
        }
        if(pramDto.getDaigakuinKbncd().equals("") == false) {
            strWhere = strWhere + "and DAIGAKUINKBNCD = '" + pramDto.getDaigakuinKbncd() + "' ";
        }
        if(pramDto.getShozokucd().equals("") == false) {
            strWhere = strWhere + "and SHOZOKUCD = '" + pramDto.getShozokucd() + "' ";
        }
        if(pramDto.getSysNo().equals("") == false) {
            strWhere = strWhere + "and SYSNO = '" + pramDto.getSysNo() + "' ";
        }
        // queryの生成
        strSql = strSql + strWhere;

        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            int num = pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return true;
    }

    public Boolean addAuthority01(ParameterDto pramDto) {

        Boolean res = false;

        Connection conn = null;
        PreparedStatement pstmt = null;

        String strSql = "insert into authority (uid";

        String strPram = ")values('" + pramDto.getUid() + "'";

        if(pramDto.getYokenNendo().equals("") == false){
            strSql = strSql + ",YOKEN_NENDO";
            strPram = strPram + "," + pramDto.getYokenNendo();
        }
        if(pramDto.getDaigakuinKbncd().equals("") == false){
            strSql = strSql + ",DAIGAKUINKBNCD";
            strPram = strPram + ",'" + pramDto.getDaigakuinKbncd() + "'";
        }
        if(pramDto.getShozokucd().equals("") == false){
            strSql = strSql + ",SHOZOKUCD";
            strPram = strPram + ",'" + pramDto.getShozokucd() + "'";
        }
        if(pramDto.getSysNo().equals("") == false){
            strSql = strSql + ",SYSNO";
            strPram = strPram + ",'" + pramDto.getSysNo() + "'";
        }

        // 2015.02.27 SVC 追加 >>
        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
        strSql = strSql + ",INSERT_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",UPDATE_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",USERID";
        strPram = strPram + ",'" + pramDto.getRuser() + "'";
        // 2015.02.27 SVC 追加 <<

        strSql = strSql + strPram + ")";

        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            int num = pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
}
        return true;
    }

}
