package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.Directory;
import dto.ParameterDto;

public class DirectoryDao extends AbstractDao<Directory> {

    public DirectoryDao() {
        super();
    }

    @Override
    ArrayList<Directory> setEntityList(ResultSet rs) {
        Directory entity = new Directory();
        return entity.setRsToList(rs);
    }

    public ArrayList<Directory> getDirectoryList01(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("DirectoryDao_getDirectoryList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生番号
        valueList.add(pramDto.getUid());
        //2.学生番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }
}
