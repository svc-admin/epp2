package dao;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import util.Tools;
import dao.entity.AccessLog;

public class AccessLogDao extends AbstractDao<AccessLog> {

    private static Logger logger = Tools.getCallerLogger();

    public AccessLogDao() {
        super();
    }

    @Override
    ArrayList<AccessLog> setEntityList(ResultSet rs) {
        AccessLog entity = new AccessLog();
        return entity.setRsToList(rs);
    }

    public int addAccessLog01(String kid, String gakuno, String ip, String pagenm) {
        Date date = new Date();
        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        //外だしSQLの取得
        String strSql = getSql("AccessLogDao_insertAccessLog01.sql");
        String uid = "";
        if (gakuno != null) uid = gakuno;
        logger.info("########## AccessLogDao ########## : kid="+kid+", uid="+uid+", ip="+ip+", pagenm="+pagenm+", ACCESS_DATE="+sdFormat.format(date).toString());
        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.熊大ID
        valueList.add(kid);
        //2.学生ID
        valueList.add(uid);
        //3.IP
        valueList.add(ip);
        //4.ページ
        valueList.add(pagenm);
        //5.アクセス日時
        valueList.add(sdFormat.format(date).toString());

        return update(kid,strSql,valueList);
    }
}
