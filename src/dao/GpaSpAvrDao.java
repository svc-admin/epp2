package dao;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.GpaAvr;
import dto.ParameterDto;

public class GpaSpAvrDao extends AbstractDao<GpaAvr> {

    public GpaSpAvrDao() {
        super();
    }

    @Override
    ArrayList<GpaAvr> setEntityList(ResultSet rs) {
        GpaAvr entity = new GpaAvr();
        return entity.setRsToList(rs);
    }

    public ArrayList<GpaAvr> getGpaAvrList01(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GpaSpAvrDao_getGpaAvrList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.所属課コード
        valueList.add(pramDto.getShozokucd());
        //2.所属課コード(後方一致)
        valueList.add(likeEscape(pramDto.getShozokucd())+"%");
        //3.学生番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<GpaAvr> getGpaAvrList02(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GpaSpAvrDao_getGpaAvrList02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.所属課コード
        valueList.add(pramDto.getShozokucd());
        //2.所属課コード(後方一致)
        valueList.add(likeEscape(pramDto.getShozokucd())+"%");
        //3.学生番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<GpaAvr> getGpaAvrList03(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GpaSpAvrDao_getGpaAvrList03.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.所属課コード
        valueList.add(pramDto.getShozokucd());
        //2.所属課コード(後方一致)
        valueList.add(likeEscape(pramDto.getShozokucd())+"%");
        //3.学生番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<GpaAvr> getGpaAvrList04(ParameterDto pramDto) {

        //外だしSQLの取得
        String strSql = getSql("GpaSpAvrDao_getGpaAvrList04.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.所属課コード
        valueList.add(pramDto.getShozokucd());
        //2.所属課コード(後方一致)
        valueList.add(likeEscape(pramDto.getShozokucd())+"%");
        //3.学生番号
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<GpaAvr> getGpaAvrList05(ParameterDto pramDto,Integer goal) {

        //外だしSQLの取得
        String strSql = getSql("GpaSpAvrDao_getGpaAvrList05.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.goal
        valueList.add(goal);
        //2.所属課コード
        valueList.add(pramDto.getGakuseki().getgBukyokucd());
        //3.入学年度
        valueList.add(pramDto.getGakuseki().getNyugakuNendo());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public int getGpaAvrCount01(String ruser, int nyugakuNendo, String shozoku) {

        //外だしSQLの取得
        String strSql = getSql("GpaSpAvrDao_getGpaAvrCount01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.入学年度
        valueList.add(new Integer(nyugakuNendo));
        //2.所属コード
        valueList.add(shozoku);

        return get(ruser,strSql,valueList).size();
    }

    public int getGpaAvrCount02(String ruser, int nyugakuNendo, String shozoku,int goal) {

        //外だしSQLの取得
        String strSql = getSql("GpaSpAvrDao_getGpaAvrCount02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.入学年度
        valueList.add(new Integer(nyugakuNendo));
        //2.所属コード
        valueList.add(shozoku);
        //3.goal
        valueList.add(new Integer(goal));

        return get(ruser,strSql,valueList).size();
    }

    public int getGpaAvrCount03(String ruser, int nyugakuNendo, String shozoku,
            int year, int sem) {

        //外だしSQLの取得
        String strSql = getSql("GpaSpAvrDao_getGpaAvrCount03.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.入学年度
        valueList.add(new Integer(nyugakuNendo));
        //2.所属コード
        valueList.add(shozoku);
        //3.認定年度
        valueList.add(new Integer(year));
        //4.semester
        valueList.add(new Integer(sem));

        return get(ruser,strSql,valueList).size();
    }

    public int insertGpaAvr01(String ruser, int nyugakuNendo, String shozoku,
            double allGpa, double allCredit, double allGpt, double users,
            Date today) {

        //外だしSQLの取得
        String strSql = getSql("GpaSpAvrDao_insertGpaAvr01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(new Integer(nyugakuNendo));
        valueList.add(shozoku);
        valueList.add(new BigDecimal(allGpa));
        valueList.add(new BigDecimal(allCredit));
        valueList.add(new BigDecimal(allGpt));
        valueList.add(new BigDecimal(users));
        valueList.add(today);
        valueList.add(today);
        valueList.add(ruser);

        return update(ruser,strSql,valueList);

    }

    public int insertGpaAvr02(String ruser, int nyugakuNendo, String shozoku,
            int goal, double goalGpa, double goalCredit, double goalGpt,
            double users, Date today) {

        //外だしSQLの取得
        String strSql = getSql("GpaSpAvrDao_insertGpaAvr02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(new Integer(nyugakuNendo));
        valueList.add(shozoku);
        valueList.add(new Integer(goal));
        valueList.add(new BigDecimal(goalGpa));
        valueList.add(new BigDecimal(goalCredit));
        valueList.add(new BigDecimal(goalGpt));
        valueList.add(new BigDecimal(users));
        valueList.add(today);
        valueList.add(today);
        valueList.add(ruser);

        return update(ruser,strSql,valueList);

    }

    public int insertGpaAvr03(String ruser, int nyugakuNendo, String shozoku,
            int year, int sem, double yearGpa, double yearCredit, double yearGpt,
            double users, Date today) {

        //外だしSQLの取得
        String strSql = getSql("GpaSpAvrDao_insertGpaAvr03.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(new Integer(nyugakuNendo));
        valueList.add(shozoku);
        valueList.add(new Integer(year));
        valueList.add(new Integer(sem));
        valueList.add(new BigDecimal(yearGpa));
        valueList.add(new BigDecimal(yearCredit));
        valueList.add(new BigDecimal(yearGpt));
        valueList.add(new BigDecimal(users));
        valueList.add(today);
        valueList.add(today);
        valueList.add(ruser);

        return update(ruser,strSql,valueList);

    }

    public int updateGpaAvr01(String ruser, int nyugakuNendo, String shozoku,
            double allGpa, double allCredit, double allGpt, double users,
            Date today) {

        //外だしSQLの取得
        String strSql = getSql("GpaSpAvrDao_updateGpaAvr01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(new BigDecimal(allGpa));
        valueList.add(new BigDecimal(allCredit));
        valueList.add(new BigDecimal(allGpt));
        valueList.add(new BigDecimal(users));
        valueList.add(today);
        valueList.add(today);
        valueList.add(ruser);
        valueList.add(new Integer(nyugakuNendo));
        valueList.add(shozoku);

        return update(ruser,strSql,valueList);

    }

    public int updateGpaAvr02(String ruser, int nyugakuNendo, String shozoku,
            double goalGpa, double goalCredit, double goalGpt, double users,
            Date today, int goal) {

        //外だしSQLの取得
        String strSql = getSql("GpaSpAvrDao_updateGpaAvr02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(new BigDecimal(goalGpa));
        valueList.add(new BigDecimal(goalCredit));
        valueList.add(new BigDecimal(goalGpt));
        valueList.add(new BigDecimal(users));
        valueList.add(today);
        valueList.add(today);
        valueList.add(ruser);
        valueList.add(new Integer(nyugakuNendo));
        valueList.add(shozoku);
        valueList.add(new Integer(goal));

        return update(ruser,strSql,valueList);

    }

    public int updateGpaAvr03(String ruser, int nyugakuNendo, String shozoku,
            double yearGpa, double yearCredit, double yearGpt, double users,
            Date today, int year, int sem) {

        //外だしSQLの取得
        String strSql = getSql("GpaSpAvrDao_updateGpaAvr03.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        valueList.add(new BigDecimal(yearGpa));
        valueList.add(new BigDecimal(yearCredit));
        valueList.add(new BigDecimal(yearGpt));
        valueList.add(new BigDecimal(users));
        valueList.add(today);
        valueList.add(today);
        valueList.add(ruser);
        valueList.add(new Integer(nyugakuNendo));
        valueList.add(shozoku);
        valueList.add(new Integer(year));
        valueList.add(new Integer(sem));

        return update(ruser,strSql,valueList);

    }

}
