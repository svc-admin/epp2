package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.Goals;

public class GoalsDao extends AbstractDao<Goals> {

    public GoalsDao() {
        super();
    }

    @Override
    ArrayList<Goals> setEntityList(ResultSet rs) {
        Goals entity = new Goals();
        return entity.setRsToList(rs);
    }

    public ArrayList<Goals> getGoalsList01(String ruser,
            Integer jikanwariNendo, String jikanwariShozokucd,
            String jikannaricd) {

        //外だしSQLの取得
        String strSql = getSql("GoalsDao_getGoalsList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.
        valueList.add(jikanwariNendo);
        //2.
        valueList.add(jikanwariShozokucd);
        //3.
        valueList.add(jikannaricd);

        return get(ruser,strSql,valueList);
    }

}
