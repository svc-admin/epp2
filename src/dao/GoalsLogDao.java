package dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import dao.entity.GoalsLog;

public class GoalsLogDao extends AbstractDao<GoalsLog> {

    public GoalsLogDao() {
        super();
    }

    @Override
    ArrayList<GoalsLog> setEntityList(ResultSet rs) {
        GoalsLog entity = new GoalsLog();
        return entity.setRsToList(rs);
    }

}
