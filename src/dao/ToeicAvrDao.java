package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.ToeicAvr;
import dto.ParameterDto;

public class ToeicAvrDao extends AbstractDao<ToeicAvr> {

    public ToeicAvrDao() {
        super();
    }

    @Override
    ArrayList<ToeicAvr> setEntityList(ResultSet rs) {
        ToeicAvr entity = new ToeicAvr();
        return entity.setRsToList(rs);
    }

    public ArrayList<ToeicAvr> getToeicAvrList01(ParameterDto pramDto) {
        //外だしSQLの取得
        String strSql = getSql("ToeicAvrDao_getToeicAvrList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.入学年度
        valueList.add(pramDto.getGakuseki().getNyugakuNendo());
        //2.所属課コード
        valueList.add(pramDto.getGakuseki().getgShozokucd());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<ToeicAvr> getToeicAvrList02(ParameterDto pramDto) {
        //外だしSQLの取得
        String strSql = getSql("ToeicAvrDao_getToeicAvrList02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.入学年度
        valueList.add(pramDto.getGakuseki().getNyugakuNendo());
        //2.所属課コード
// これでは、渡したパラメータが上書きされてしまう！！！ 2014-11-05 修正 by nakano@cc.kumamoto-u.ac.jp
//        valueList.add(pramDto.getGakuseki().getgShozokucd());
        valueList.add(pramDto.getShozokucd());

        return get(pramDto.getRuser(),strSql,valueList);
    }
}
