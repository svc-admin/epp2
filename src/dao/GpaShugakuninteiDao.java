package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.GpaShugakunintei;
import dto.ParameterDto;

public class GpaShugakuninteiDao extends AbstractDao<GpaShugakunintei> {

    private static Logger logger = null;

    public GpaShugakuninteiDao() {
        super();
    }

    @Override
    ArrayList<GpaShugakunintei> setEntityList(ResultSet rs) {
        GpaShugakunintei entity = new GpaShugakunintei();
        return entity.setRsToList(rs);
    }

}