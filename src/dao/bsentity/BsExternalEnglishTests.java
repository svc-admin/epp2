package dao.bsentity;

import java.sql.Timestamp;


public abstract class BsExternalEnglishTests {

    /** id: {PK, NotNull, int(11)} */
    protected Integer id;

    /** G_SYSNO: {PK, NotNull, VARCHAR(10)} */
    protected String gSysno;

    /** KAMOKUCD: {PK, NotNull, VARCHAR(10)} */
    protected String kamokucd;

    /** test_type: {VARCHAR(255)} */
/*    protected String testType;*/

    /** test_name: {VARCHAR(255)} */
    protected String testName;

    /** score: {VARCHAR(255)} */
    protected String score;

    /** date: {NotNull, timestamp} */
    protected Timestamp date;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getgSysno() {
		return gSysno;
	}

	public void setgSysno(String gSysno) {
		this.gSysno = gSysno;
	}

	public String getKamokucd() {
		return kamokucd;
	}

	public void setKamokucd(String kamokucd) {
		this.kamokucd = kamokucd;
	}

/*	public String getTestType() {
		return testType;
	}

	public void setTestType(String testType) {
		this.testType = testType;
	}
*/
	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public java.util.Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(java.util.Date insertDate) {
		this.insertDate = insertDate;
	}

	public java.util.Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(java.util.Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}
}
