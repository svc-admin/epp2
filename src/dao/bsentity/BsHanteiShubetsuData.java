package dao.bsentity;

public abstract class BsHanteiShubetsuData {

	protected Integer shori_nendo;
	protected String g_sysno;
	protected String hantei_shubetsu;

	public Integer getShori_nendo() {
		return shori_nendo;
	}
	public void setShori_nendo(Integer shori_nendo) {
		this.shori_nendo = shori_nendo;
	}
	public String getG_sysno() {
		return g_sysno;
	}
	public void setG_sysno(String g_sysno) {
		this.g_sysno = g_sysno;
	}
	public String getHantei_shubetsu() {
		return hantei_shubetsu;
	}
	public void setHantei_shubetsu(String hantei_shubetsu) {
		this.hantei_shubetsu = hantei_shubetsu;
	}

}
