package dao.bsentity;

public abstract class BsInputForEvidence {

    /** uid: {IX, NotNull, VARCHAR(10)} */
    protected String uid;

    /** NENDO: {IX, NotNull, DECIMAL(4)} */
    protected Integer Nendo;

    /** JIKANWARI_SHOZOKUCD: {NotNull, varchar(14)} */
    protected String jikanwariShozokucd;

    /** JIKANWARICD: {NotNull, varchar(8)} */
    protected String jikanwaricd;

    /** dir: {NotNull, varchar(256)} */
    protected String dir;

    /** evidence: {NotNull, varchar(256)} */
    protected String evidence;

    /** size: {NotNull, bigint(20)} */
    protected Long size;

    /** lastUpdate: {NotNull, TIMESTAMP} */
    protected java.sql.Timestamp lastUpdate;

    /** INSERT_DATE: {NotNull, DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {NotNull, DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {NotNull, VARCHAR(10)} */
    protected String userid;

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public Integer getNendo() {
            return Nendo;
        }

        public void setNendo(Integer nendo) {
            Nendo = nendo;
        }

        public String getJikanwariShozokucd() {
            return jikanwariShozokucd;
        }

        public void setJikanwariShozokucd(String jikanwariShozokucd) {
            this.jikanwariShozokucd = jikanwariShozokucd;
        }

        public String getJikanwaricd() {
            return jikanwaricd;
        }

        public void setJikanwaricd(String jikanwaricd) {
            this.jikanwaricd = jikanwaricd;
        }

        public String getDir() {
            return dir;
        }

        public void setDir(String dir) {
            this.dir = dir;
        }

        public String getEvidence() {
            return evidence;
        }

        public void setEvidence(String evidence) {
            this.evidence = evidence;
        }

        public Long getSize() {
            return size;
        }

        public void setSize(Long size) {
            this.size = size;
        }

        public java.sql.Timestamp getLastUpdate() {
            return lastUpdate;
        }

        public void setLastUpdate(java.sql.Timestamp lastUpdate) {
            this.lastUpdate = lastUpdate;
        }

        public java.util.Date getInsertDate() {
            return insertDate;
        }

        public void setInsertDate(java.util.Date insertDate) {
            this.insertDate = insertDate;
        }

        public java.util.Date getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(java.util.Date updateDate) {
            this.updateDate = updateDate;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }



}
