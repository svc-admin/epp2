package dao.bsentity;

public abstract class BsGpaBukyokuDisp {

    /** G_BUKYOKUCD: {IX, NotNull, VARCHAR(14)} */
    protected String g_bukyokucd;

    /** DISP_FLG: {NotNull, DECIMAL(4)} */
    protected Integer dispFlg;

    /** INSERT_DATE: {NotNull, DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {NotNull, DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {NotNull, VARCHAR(10)} */
    protected String userid;

    public String getBukyokucd() {
        return g_bukyokucd;
    }

    public void setBukyokucd(String g_bukyokucd) {
        this.g_bukyokucd = g_bukyokucd;
    }

    public Integer getDispFlg() {
        return dispFlg;
    }

    public void setDispFlg(Integer dispFlg) {
        this.dispFlg = dispFlg;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
