package dao.bsentity;

public abstract class BsGoalsMst {

    /** CODE: {DECIMAL(2)} */
    protected Integer code;

    /** NAME: {VARCHAR(100)} */
    protected String name;

    /** NAME_ENG: {VARCHAR(100)} */
    protected String nameEng;

    /** EXPL: {VARCHAR(500)} */
    protected String expl;

    /** EXPL_ENG: {VARCHAR(500)} */
    protected String explEng;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public String getExpl() {
        return expl;
    }

    public void setExpl(String expl) {
        this.expl = expl;
    }

    public String getExplEng() {
        return explEng;
    }

    public void setExplEng(String explEng) {
        this.explEng = explEng;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
