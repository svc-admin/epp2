package dao.bsentity;

public abstract class BsBunruiMst {

    /** BUNRUICD: {PK, NotNull, VARCHAR(10)} */
    protected String bunruicd;

    /** SHOZOKUCD: {PK, NotNull, VARCHAR(14)} */
    protected String shozokucd;

    /** BUNRUINM: {PK, NotNull, VARCHAR(150)} */
    protected String bunruinm;

    /** BUNRUINMENG: {PK, NotNull, VARCHAR(150) */
    protected String bunruinmeng;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    public String getBunruicd() {
        return bunruicd;
    }

    public void setBunruicd(String bunruicd) {
        this.bunruicd = bunruicd;
    }

    public String getShozokucd() {
        return shozokucd;
    }

    public void setShozokucd(String shozokucd) {
        this.shozokucd = shozokucd;
    }

    public String getBunruinm() {
        return bunruinm;
    }

    public void setBunruinm(String bunruinm) {
        this.bunruinm = bunruinm;
    }

    public String getBunruinmeng() {
        return bunruinmeng;
    }

    public void setBunruinmeng(String bunruinmeng) {
        this.bunruinmeng = bunruinmeng;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
