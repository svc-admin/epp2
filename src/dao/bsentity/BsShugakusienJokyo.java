package dao.bsentity;

public abstract class BsShugakusienJokyo {

    /** NINTEI_NENDO: {DECIMAL(4)} */
    protected Integer ninteinendo;

    /** G_GAKUNO: {VARCHAR(10)} */
    protected String gGakuno;

    /** sien_jokyo: {VARCHAR(10)} */
    protected String sienJokyo;

    /** INSERT_DATE: {NotNull, DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {NotNull, DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {NotNull, VARCHAR(10)} */
    protected String userid;

    public Integer getNinteiNendo() {
        return ninteinendo;
    }

    public void setNinteiNendo(Integer ninteinendo) {
        this.ninteinendo = ninteinendo;
    }

    public String getgGakuno() {
        return gGakuno;
    }

    public void setgGakuno(String gGakuno) {
        this.gGakuno = gGakuno;
    }

    public String getSienJokyo() {
        return sienJokyo;
    }

    public void setSienJokyo(String sienJokyo) {
        this.sienJokyo = sienJokyo;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
