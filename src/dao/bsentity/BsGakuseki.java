package dao.bsentity;

public abstract class BsGakuseki {

    /** G_BUKYOKUCD: {VARCHAR(14)} */
    protected String gBukyokucd;

    /** G_GAKUNO: {VARCHAR(10)} */
    protected String gGakuno;

    /** G_SORT: {VARCHAR(10)} */
    protected String gSort;

    /** G_SYSNO: {VARCHAR(10)} */
    protected String gSysno;

    /** G_SHOZOKUCD: {VARCHAR(14)} */
    protected String gShozokucd;

    /** G_MIBUNCD: {VARCHAR(2)} */
    protected String gMibuncd;

    /** GENKYOKBNCD: {VARCHAR(1)} */
    protected String genkyokbncd;

    /** GAKUNEN: {DECIMAL(2)} */
    protected Integer gakunen;

    /** ZAIGAKU_TSUKISU: {DECIMAL(3)} */
    protected Integer zaigakuTsukisu;

    /** NYUGAKUYMD: {DATE(10)} */
    protected java.util.Date nyugakuymd;

    /** SOTSUGYOYMD: {DATE(10)} */
    protected java.util.Date sotsugyoymd;

    /** TOKKI: {VARCHAR(100)} */
    protected String tokki;

    /** RYUGAKUKBNCD: {VARCHAR(1)} */
    protected String ryugakukbncd;

    /** ZAIRYUSHIKAKUCD: {VARCHAR(1)} */
    protected String zairyushikakucd;

    /** SHAKAIJINFLG: {VARCHAR(1)} */
    protected String shakaijinflg;

    /** GAIKOKUJINFLG: {VARCHAR(1)} */
    protected String gaikokujinflg;

    /** JUKENNO: {VARCHAR(10)} */
    protected String jukenno;

    /** NYUGAKUKBNCD: {VARCHAR(1)} */
    protected String nyugakukbncd;

    /** NYUGAKU_NENDO: {DECIMAL(4)} */
    protected Integer nyugakuNendo;

    /** NYUGAKU_GAKUNEN: {DECIMAL(2)} */
    protected Integer nyugakuGakunen;

    /** AKINYUGAKUFLG: {DECIMAL(1)} */
    protected Integer akinyugakuflg;

    /** KENCD: {VARCHAR(2)} */
    protected String kencd;

    /** GAKUICD: {VARCHAR(3)} */
    protected String gakuicd;

    /** GAKUIKINO: {VARCHAR(10)} */
    protected String gakuikino;

    /** SUB_SHOZOKUCD: {VARCHAR(14)} */
    protected String subShozokucd;

    /** YOKEN_NENDO: {DECIMAL(4)} */
    protected Integer yokenNendo;

    /** YOKEN_MONTH: {VARCHAR(2)} */
    protected String yokenMonth;

    /** MIKOMIFLG: {VARCHAR(1)} */
    protected String mikomiflg;

    /** IMAGEFILENM: {VARCHAR(20)} */
    protected String imagefilenm;

    /** HAKKO_CNT: {DECIMAL(2)} */
    protected Integer hakkoCnt;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    public String getgBukyokucd() {
        return gBukyokucd;
    }

    public void setgBukyokucd(String gBukyokucd) {
        this.gBukyokucd = gBukyokucd;
    }

    public String getgGakuno() {
        return gGakuno;
    }

    public void setgGakuno(String gGakuno) {
        this.gGakuno = gGakuno;
    }

    public String getgSort() {
        return gSort;
    }

    public void setgSort(String gSort) {
        this.gSort = gSort;
    }

    public String getgSysno() {
        return gSysno;
    }

    public void setgSysno(String gSysno) {
        this.gSysno = gSysno;
    }

    public String getgShozokucd() {
        return gShozokucd;
    }

    public void setgShozokucd(String gShozokucd) {
        this.gShozokucd = gShozokucd;
    }

    public String getgMibuncd() {
        return gMibuncd;
    }

    public void setgMibuncd(String gMibuncd) {
        this.gMibuncd = gMibuncd;
    }

    public String getGenkyokbncd() {
        return genkyokbncd;
    }

    public void setGenkyokbncd(String genkyokbncd) {
        this.genkyokbncd = genkyokbncd;
    }

    public Integer getGakunen() {
        return gakunen;
    }

    public void setGakunen(Integer gakunen) {
        this.gakunen = gakunen;
    }

    public Integer getZaigakuTsukisu() {
        return zaigakuTsukisu;
    }

    public void setZaigakuTsukisu(Integer zaigakuTsukisu) {
        this.zaigakuTsukisu = zaigakuTsukisu;
    }

    public java.util.Date getNyugakuymd() {
        return nyugakuymd;
    }

    public void setNyugakuymd(java.util.Date nyugakuymd) {
        this.nyugakuymd = nyugakuymd;
    }

    public java.util.Date getSotsugyoymd() {
        return sotsugyoymd;
    }

    public void setSotsugyoymd(java.util.Date sotsugyoymd) {
        this.sotsugyoymd = sotsugyoymd;
    }

    public String getTokki() {
        return tokki;
    }

    public void setTokki(String tokki) {
        this.tokki = tokki;
    }

    public String getRyugakukbncd() {
        return ryugakukbncd;
    }

    public void setRyugakukbncd(String ryugakukbncd) {
        this.ryugakukbncd = ryugakukbncd;
    }

    public String getZairyushikakucd() {
        return zairyushikakucd;
    }

    public void setZairyushikakucd(String zairyushikakucd) {
        this.zairyushikakucd = zairyushikakucd;
    }

    public String getShakaijinflg() {
        return shakaijinflg;
    }

    public void setShakaijinflg(String shakaijinflg) {
        this.shakaijinflg = shakaijinflg;
    }

    public String getGaikokujinflg() {
        return gaikokujinflg;
    }

    public void setGaikokujinflg(String gaikokujinflg) {
        this.gaikokujinflg = gaikokujinflg;
    }

    public String getJukenno() {
        return jukenno;
    }

    public void setJukenno(String jukenno) {
        this.jukenno = jukenno;
    }

    public String getNyugakukbncd() {
        return nyugakukbncd;
    }

    public void setNyugakukbncd(String nyugakukbncd) {
        this.nyugakukbncd = nyugakukbncd;
    }

    public Integer getNyugakuNendo() {
        return nyugakuNendo;
    }

    public void setNyugakuNendo(Integer nyugakuNendo) {
        this.nyugakuNendo = nyugakuNendo;
    }

    public Integer getNyugakuGakunen() {
        return nyugakuGakunen;
    }

    public void setNyugakuGakunen(Integer nyugakuGakunen) {
        this.nyugakuGakunen = nyugakuGakunen;
    }

    public Integer getAkinyugakuflg() {
        return akinyugakuflg;
    }

    public void setAkinyugakuflg(Integer akinyugakuflg) {
        this.akinyugakuflg = akinyugakuflg;
    }

    public String getKencd() {
        return kencd;
    }

    public void setKencd(String kencd) {
        this.kencd = kencd;
    }

    public String getGakuicd() {
        return gakuicd;
    }

    public void setGakuicd(String gakuicd) {
        this.gakuicd = gakuicd;
    }

    public String getGakuikino() {
        return gakuikino;
    }

    public void setGakuikino(String gakuikino) {
        this.gakuikino = gakuikino;
    }

    public String getSubShozokucd() {
        return subShozokucd;
    }

    public void setSubShozokucd(String subShozokucd) {
        this.subShozokucd = subShozokucd;
    }

    public Integer getYokenNendo() {
        return yokenNendo;
    }

    public void setYokenNendo(Integer yokenNendo) {
        this.yokenNendo = yokenNendo;
    }

    public String getYokenMonth() {
        return yokenMonth;
    }

    public void setYokenMonth(String yokenMonth) {
        this.yokenMonth = yokenMonth;
    }

    public String getMikomiflg() {
        return mikomiflg;
    }

    public void setMikomiflg(String mikomiflg) {
        this.mikomiflg = mikomiflg;
    }

    public String getImagefilenm() {
        return imagefilenm;
    }

    public void setImagefilenm(String imagefilenm) {
        this.imagefilenm = imagefilenm;
    }

    public Integer getHakkoCnt() {
        return hakkoCnt;
    }

    public void setHakkoCnt(Integer hakkoCnt) {
        this.hakkoCnt = hakkoCnt;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
