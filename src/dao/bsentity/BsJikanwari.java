package dao.bsentity;

public abstract class BsJikanwari {

    /** NENDO: {DECIMAL(4)} */
    protected Integer nendo;

    /** JIKANWARI_SHOZOKUCD: {VARCHAR(14)} */
    protected String jikanwariShozokucd;

    /** JIKANWARICD: {VARCHAR(8)} */
    protected String jikanwaricd;

    /** CUR_NENDO: {VARCHAR(6)} */
    protected String curNendo;

    /** CUR_SHOZOKUCD: {VARCHAR(14)} */
    protected String curShozokucd;

    /** GAKKIKBNCD: {VARCHAR(2)} */
    protected String gakkikbncd;

    /** KAMOKUCD: {VARCHAR(10)} */
    protected String kamokucd;

    /** KAIKOKBNCD: {VARCHAR(1)} */
    protected String kaikokbncd;

    /** ST_YMD: {DATE(10)} */
    protected java.util.Date stYmd;

    /** END_YMD: {DATE(10)} */
    protected java.util.Date endYmd;

    /** K_SHOZOKUCD: {VARCHAR(14)} */
    protected String kShozokucd;

    /** SHUKYOKANCD: {VARCHAR(8)} */
    protected String shukyokancd;

    /** TANISU: {DECIMAL(4, 1)} */
    protected java.math.BigDecimal tanisu;

    /** JUGYOKEITAICD: {VARCHAR(1)} */
    protected String jugyokeitaicd;

    /** YOBICHOFUKU: {DECIMAL(1)} */
    protected Integer yobichofuku;

    /** TAISHO_NENJI: {VARCHAR(6)} */
    protected String taishoNenji;

    /** KAIKO_KAMOKUNM: {VARCHAR(150)} */
    protected String kaikoKamokunm;

    /** KAIKO_KAMOKUNMENG: {VARCHAR(300)} */
    protected String kaikoKamokunmeng;

    /** BIKO: {VARCHAR(100)} */
    protected String biko;

    /** JIGAKUBU_GOHIIPTFLG: {DECIMAL(1)} */
    protected Integer jigakubuGohiiptflg;

    /** KYOTSUKAMOKU_KBNCD: {DECIMAL(1)} */
    protected Integer kyotsukamokuKbncd;

    /** SETTEIKBNCD: {DECIMAL(1)} */
    protected Integer setteikbncd;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    public Integer getNendo() {
        return nendo;
    }

    public void setNendo(Integer nendo) {
        this.nendo = nendo;
    }

    public String getJikanwariShozokucd() {
        return jikanwariShozokucd;
    }

    public void setJikanwariShozokucd(String jikanwariShozokucd) {
        this.jikanwariShozokucd = jikanwariShozokucd;
    }

    public String getJikanwaricd() {
        return jikanwaricd;
    }

    public void setJikanwaricd(String jikanwaricd) {
        this.jikanwaricd = jikanwaricd;
    }

    public String getCurNendo() {
        return curNendo;
    }

    public void setCurNendo(String curNendo) {
        this.curNendo = curNendo;
    }

    public String getCurShozokucd() {
        return curShozokucd;
    }

    public void setCurShozokucd(String curShozokucd) {
        this.curShozokucd = curShozokucd;
    }

    public String getGakkikbncd() {
        return gakkikbncd;
    }

    public void setGakkikbncd(String gakkikbncd) {
        this.gakkikbncd = gakkikbncd;
    }

    public String getKamokucd() {
        return kamokucd;
    }

    public void setKamokucd(String kamokucd) {
        this.kamokucd = kamokucd;
    }

    public String getKaikokbncd() {
        return kaikokbncd;
    }

    public void setKaikokbncd(String kaikokbncd) {
        this.kaikokbncd = kaikokbncd;
    }

    public java.util.Date getStYmd() {
        return stYmd;
    }

    public void setStYmd(java.util.Date stYmd) {
        this.stYmd = stYmd;
    }

    public java.util.Date getEndYmd() {
        return endYmd;
    }

    public void setEndYmd(java.util.Date endYmd) {
        this.endYmd = endYmd;
    }

    public String getkShozokucd() {
        return kShozokucd;
    }

    public void setkShozokucd(String kShozokucd) {
        this.kShozokucd = kShozokucd;
    }

    public String getShukyokancd() {
        return shukyokancd;
    }

    public void setShukyokancd(String shukyokancd) {
        this.shukyokancd = shukyokancd;
    }

    public java.math.BigDecimal getTanisu() {
        return tanisu;
    }

    public void setTanisu(java.math.BigDecimal tanisu) {
        this.tanisu = tanisu;
    }

    public String getJugyokeitaicd() {
        return jugyokeitaicd;
    }

    public void setJugyokeitaicd(String jugyokeitaicd) {
        this.jugyokeitaicd = jugyokeitaicd;
    }

    public Integer getYobichofuku() {
        return yobichofuku;
    }

    public void setYobichofuku(Integer yobichofuku) {
        this.yobichofuku = yobichofuku;
    }

    public String getTaishoNenji() {
        return taishoNenji;
    }

    public void setTaishoNenji(String taishoNenji) {
        this.taishoNenji = taishoNenji;
    }

    public String getKaikoKamokunm() {
        return kaikoKamokunm;
    }

    public void setKaikoKamokunm(String kaikoKamokunm) {
        this.kaikoKamokunm = kaikoKamokunm;
    }

    public String getKaikoKamokunmeng() {
        return kaikoKamokunmeng;
    }

    public void setKaikoKamokunmeng(String kaikoKamokunmeng) {
        this.kaikoKamokunmeng = kaikoKamokunmeng;
    }

    public String getBiko() {
        return biko;
    }

    public void setBiko(String biko) {
        this.biko = biko;
    }

    public Integer getJigakubuGohiiptflg() {
        return jigakubuGohiiptflg;
    }

    public void setJigakubuGohiiptflg(Integer jigakubuGohiiptflg) {
        this.jigakubuGohiiptflg = jigakubuGohiiptflg;
    }

    public Integer getKyotsukamokuKbncd() {
        return kyotsukamokuKbncd;
    }

    public void setKyotsukamokuKbncd(Integer kyotsukamokuKbncd) {
        this.kyotsukamokuKbncd = kyotsukamokuKbncd;
    }

    public Integer getSetteikbncd() {
        return setteikbncd;
    }

    public void setSetteikbncd(Integer setteikbncd) {
        this.setteikbncd = setteikbncd;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
