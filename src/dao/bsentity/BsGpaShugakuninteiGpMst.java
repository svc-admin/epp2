package dao.bsentity;

public abstract class BsGpaShugakuninteiGpMst {

    /** G_BUKYOKUCD: {IX, VARCHAR(14)} */
    protected String gBukyokucd;

    /** SHORI_NENDO: {DECIMAL(4,0)} */
    protected Integer shori_start;

    /** SHORI_END: {DECIMAL(4,0)} */
    protected Integer shori_end;

    /** HYOGONM: {VARCHAR(10)} */
    protected String hyogonm;

    /** gp: [decimal(2,0)] */
    protected java.math.BigDecimal gp;

    /** gpa_taisho: [decimal(2,0)] */
    protected Integer gpaTaisho;

    /** INSERT_DATE: {NotNull, DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {NotNull, DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {NotNull, VARCHAR(10)} */
    protected String userid;

    public String getgBukyokucd() {
        return gBukyokucd;
    }

    public void setgBukyokucd(String gBukyokucd) {
        this.gBukyokucd = gBukyokucd;
    }

	public Integer getShori_start() {
		return shori_start;
	}

	public void setShori_start(Integer shori_start) {
		this.shori_start = shori_start;
	}

	public Integer getShori_end() {
		return shori_end;
	}

	public void setShori_end(Integer shori_end) {
		this.shori_end = shori_end;
	}

    public String getHyogonm() {
        return hyogonm;
    }

    public void setHyogonm(String hyogonm) {
        this.hyogonm = hyogonm;
    }

    public java.math.BigDecimal getGp() {
        return gp;
    }

    public void setGp(java.math.BigDecimal gp) {
        this.gp = gp;
    }

    public Integer getGpaTaisho() {
        return gpaTaisho;
    }

    public void setGpaTaisho(Integer gpaTaisho) {
        this.gpaTaisho = gpaTaisho;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
