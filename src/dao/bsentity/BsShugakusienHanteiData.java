package dao.bsentity;

import java.math.BigDecimal;

public abstract class BsShugakusienHanteiData extends BsShugakusienHanteiRireki {

	/**
	 * テーブル：shugakusien_hantei_dataの項目
	 */
	protected String g_gakuno;
	protected String g_name;
	protected String judge_type;
	protected String z_judge;
	protected String t_judge;
	protected String bikou;
	protected BigDecimal t_gpt;
	protected BigDecimal t_gp_tanisu;
	protected Integer t_gpa_juni;
	protected BigDecimal n_gpt;
	protected BigDecimal n_gp_tanisu;
	protected Integer n_gpa_juni;
	protected Integer gpa_param;
	protected Integer zaiseki_month;
	protected Integer kyugaku_month;
	protected BigDecimal zaigaku_nensu;
	protected BigDecimal hitsuyo_tanisu;
	protected Integer shugyo_nengen;
	protected BigDecimal hyojun_tanisu;
	protected BigDecimal shutoku_tanisu;
	protected BigDecimal shutoku_ritsu;
	protected BigDecimal rishu_tanisu;
	protected BigDecimal kesseki_tanisu;
	protected BigDecimal shusseki_ritsu;
	protected String ryunendata;
	protected String keikakusho;
	protected String shien_jokyo;
	// 在学採用判定の項目
	protected String z_gpa;
	protected String z_gpa_b;
	protected String z_tani;
	protected String z_tani_b;
	protected String z_sotsu_b;
	protected String t_sotsu_b;
	// 廃止判定の項目
	protected String h_sotsu;
	protected String h_sotsu_b;
	protected String h_tani;
	protected String h_tani_b;
	protected String h_shu;
	protected String h_shu_b;
	protected String h_zen;
	protected String h_zen_b;
	// 警告判定の項目
	protected String k_tani;
	protected String k_tani_b;
	protected String k_gpa;
	protected String k_gpa_b;
	protected String k_shu;
	protected String k_shu_b;
	// 遡及取消判定の項目
	protected String s_tani;
	protected String s_tani_b;
	protected String s_shu;
	protected String s_shu_b;
	// GPAの修正データ格納用
	protected BigDecimal t_gpa;
	protected BigDecimal t_gpa_b;
	protected BigDecimal n_gpa;
	protected BigDecimal n_gpa_b;
	// GPAの修正有無格納用
	protected String t_gpa_ch;
	protected String n_gpa_ch;

	public String getG_gakuno() {
		return g_gakuno;
	}
	public void setG_gakuno(String g_gakuno) {
		this.g_gakuno = g_gakuno;
	}
	public Integer getGpa_param() {
		return gpa_param;
	}
	public void setGpa_param(Integer gpa_param) {
		this.gpa_param = gpa_param;
	}
	public BigDecimal getHitsuyo_tanisu() {
		return hitsuyo_tanisu;
	}
	public void setHitsuyo_tanisu(BigDecimal hitsuyo_tanisu) {
		this.hitsuyo_tanisu = hitsuyo_tanisu;
	}
	public Integer getShugyo_nengen() {
		return shugyo_nengen;
	}
	public void setShugyo_nengen(Integer shugyo_nengen) {
		this.shugyo_nengen = shugyo_nengen;
	}
	public Integer getZaiseki_month() {
		return zaiseki_month;
	}
	public void setZaiseki_month(Integer zaiseki_month) {
		this.zaiseki_month = zaiseki_month;
	}
	public Integer getKyugaku_month() {
		return kyugaku_month;
	}
	public void setKyugaku_month(Integer kyugaku_month) {
		this.kyugaku_month = kyugaku_month;
	}
	public BigDecimal getZaigaku_nensu() {
		return zaigaku_nensu;
	}
	public void setZaigaku_nensu(BigDecimal zaigaku_nensu) {
		this.zaigaku_nensu = zaigaku_nensu;
	}
	public BigDecimal getHyojun_tanisu() {
		return hyojun_tanisu;
	}
	public void setHyojun_tanisu(BigDecimal hyojun_tanisu) {
		this.hyojun_tanisu = hyojun_tanisu;
	}
	public BigDecimal getShutoku_tanisu() {
		return shutoku_tanisu;
	}
	public void setShutoku_tanisu(BigDecimal shutoku_tanisu) {
		this.shutoku_tanisu = shutoku_tanisu;
	}
	public BigDecimal getShutoku_ritsu() {
		return shutoku_ritsu;
	}
	public void setShutoku_ritsu(BigDecimal shutoku_ritsu) {
		this.shutoku_ritsu = shutoku_ritsu;
	}
	public BigDecimal getRishu_tanisu() {
		return rishu_tanisu;
	}
	public void setRishu_tanisu(BigDecimal rishu_tanisu) {
		this.rishu_tanisu = rishu_tanisu;
	}
	public BigDecimal getKesseki_tanisu() {
		return kesseki_tanisu;
	}
	public void setKesseki_tanisu(BigDecimal kesseki_tanisu) {
		this.kesseki_tanisu = kesseki_tanisu;
	}
	public BigDecimal getShusseki_ritsu() {
		return shusseki_ritsu;
	}
	public void setShusseki_ritsu(BigDecimal shusseki_ritsu) {
		this.shusseki_ritsu = shusseki_ritsu;
	}
	public String getRyunendata() {
		return ryunendata;
	}
	public void setRyunendata(String ryunendata) {
		this.ryunendata = ryunendata;
	}
	public String getKeikakusho() {
		return keikakusho;
	}
	public void setKeikakusho(String keikakusho) {
		this.keikakusho = keikakusho;
	}
	public String getG_name() {
		return g_name;
	}
	public void setG_name(String g_name) {
		this.g_name = g_name;
	}
	public BigDecimal getT_gp_tanisu() {
		return t_gp_tanisu;
	}
	public void setT_gp_tanisu(BigDecimal t_gp_tanisu) {
		this.t_gp_tanisu = t_gp_tanisu;
	}
	public BigDecimal getT_gpt() {
		return t_gpt;
	}
	public void setT_gpt(BigDecimal t_gpt) {
		this.t_gpt = t_gpt;
	}
	public BigDecimal getT_gpa() {
		return t_gpa;
	}
	public void setT_gpa(BigDecimal t_gpa) {
		this.t_gpa = t_gpa;
	}
	public Integer getT_gpa_juni() {
		return t_gpa_juni;
	}
	public void setT_gpa_juni(Integer t_gpa_juni) {
		this.t_gpa_juni = t_gpa_juni;
	}
	public BigDecimal getN_gp_tanisu() {
		return n_gp_tanisu;
	}
	public void setN_gp_tanisu(BigDecimal n_gp_tanisu) {
		this.n_gp_tanisu = n_gp_tanisu;
	}
	public BigDecimal getN_gpt() {
		return n_gpt;
	}
	public void setN_gpt(BigDecimal n_gpt) {
		this.n_gpt = n_gpt;
	}
	public BigDecimal getN_gpa() {
		return n_gpa;
	}
	public void setN_gpa(BigDecimal n_gpa) {
		this.n_gpa = n_gpa;
	}
	public Integer getN_gpa_juni() {
		return n_gpa_juni;
	}
	public void setN_gpa_juni(Integer n_gpa_juni) {
		this.n_gpa_juni = n_gpa_juni;
	}
	public String getJudge_type() {
		return judge_type;
	}
	public void setJudge_type(String judge_type) {
		this.judge_type = judge_type;
	}
	public String getZ_judge() {
		return z_judge;
	}
	public void setZ_judge(String z_judge) {
		this.z_judge = z_judge;
	}
	public String getT_judge() {
		return t_judge;
	}
	public void setT_judge(String t_judge) {
		this.t_judge = t_judge;
	}
	public String getBikou() {
		return bikou;
	}
	public void setBikou(String bikou) {
		this.bikou = bikou;
	}
	public String getZ_gpa() {
		return z_gpa;
	}
	public void setZ_gpa(String z_gpa) {
		this.z_gpa = z_gpa;
	}
	public String getZ_gpa_b() {
		return z_gpa_b;
	}
	public void setZ_gpa_b(String z_gpa_b) {
		this.z_gpa_b = z_gpa_b;
	}
	public String getZ_tani() {
		return z_tani;
	}
	public void setZ_tani(String z_tani) {
		this.z_tani = z_tani;
	}
	public String getZ_tani_b() {
		return z_tani_b;
	}
	public void setZ_tani_b(String z_tani_b) {
		this.z_tani_b = z_tani_b;
	}
	public String getH_sotsu() {
		return h_sotsu;
	}
	public void setH_sotsu(String h_sotsu) {
		this.h_sotsu = h_sotsu;
	}
	public String getH_sotsu_b() {
		return h_sotsu_b;
	}
	public void setH_sotsu_b(String h_sotsu_b) {
		this.h_sotsu_b = h_sotsu_b;
	}
	public String getH_tani() {
		return h_tani;
	}
	public void setH_tani(String h_tani) {
		this.h_tani = h_tani;
	}
	public String getH_tani_b() {
		return h_tani_b;
	}
	public void setH_tani_b(String h_tani_b) {
		this.h_tani_b = h_tani_b;
	}
	public String getH_shu() {
		return h_shu;
	}
	public void setH_shu(String h_shu) {
		this.h_shu = h_shu;
	}
	public String getH_shu_b() {
		return h_shu_b;
	}
	public void setH_shu_b(String h_shu_b) {
		this.h_shu_b = h_shu_b;
	}
	public String getH_zen() {
		return h_zen;
	}
	public void setH_zen(String h_zen) {
		this.h_zen = h_zen;
	}
	public String getH_zen_b() {
		return h_zen_b;
	}
	public void setH_zen_b(String h_zen_b) {
		this.h_zen_b = h_zen_b;
	}
	public String getK_tani() {
		return k_tani;
	}
	public void setK_tani(String k_tani) {
		this.k_tani = k_tani;
	}
	public String getK_tani_b() {
		return k_tani_b;
	}
	public void setK_tani_b(String k_tani_b) {
		this.k_tani_b = k_tani_b;
	}
	public String getK_gpa() {
		return k_gpa;
	}
	public void setK_gpa(String k_gpa) {
		this.k_gpa = k_gpa;
	}
	public String getK_gpa_b() {
		return k_gpa_b;
	}
	public void setK_gpa_b(String k_gpa_b) {
		this.k_gpa_b = k_gpa_b;
	}
	public String getK_shu() {
		return k_shu;
	}
	public void setK_shu(String k_shu) {
		this.k_shu = k_shu;
	}
	public String getK_shu_b() {
		return k_shu_b;
	}
	public void setK_shu_b(String k_shu_b) {
		this.k_shu_b = k_shu_b;
	}
	public String getS_tani() {
		return s_tani;
	}
	public void setS_tani(String s_tani) {
		this.s_tani = s_tani;
	}
	public String getS_tani_b() {
		return s_tani_b;
	}
	public void setS_tani_b(String s_tani_b) {
		this.s_tani_b = s_tani_b;
	}
	public String getS_shu() {
		return s_shu;
	}
	public void setS_shu(String s_shu) {
		this.s_shu = s_shu;
	}
	public String getS_shu_b() {
		return s_shu_b;
	}
	public void setS_shu_b(String s_shu_b) {
		this.s_shu_b = s_shu_b;
	}
	public BigDecimal getT_gpa_b() {
		return t_gpa_b;
	}
	public void setT_gpa_b(BigDecimal t_gpa_b) {
		this.t_gpa_b = t_gpa_b;
	}
	public BigDecimal getN_gpa_b() {
		return n_gpa_b;
	}
	public void setN_gpa_b(BigDecimal n_gpa_b) {
		this.n_gpa_b = n_gpa_b;
	}
	public String getZ_sotsu_b() {
		return z_sotsu_b;
	}
	public void setZ_sotsu_b(String z_sotsu_b) {
		this.z_sotsu_b = z_sotsu_b;
	}
	public String getShien_jokyo() {
		return shien_jokyo;
	}
	public void setShien_jokyo(String shien_jokyo) {
		this.shien_jokyo = shien_jokyo;
	}
	public String getT_sotsu_b() {
		return t_sotsu_b;
	}
	public void setT_sotsu_b(String t_sotsu_b) {
		this.t_sotsu_b = t_sotsu_b;
	}
	public String getT_gpa_ch() {
		return t_gpa_ch;
	}
	public void setT_gpa_ch(String t_gpa_ch) {
		this.t_gpa_ch = t_gpa_ch;
	}
	public String getN_gpa_ch() {
		return n_gpa_ch;
	}
	public void setN_gpa_ch(String n_gpa_ch) {
		this.n_gpa_ch = n_gpa_ch;
	}
}
