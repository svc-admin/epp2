package dao.bsentity;

import java.sql.Timestamp;

public abstract class BsResults {

    /** G_SYSNO: {PK, NotNull, VARCHAR(10)} */
    protected String gSysno;

    /** KAMOKUCD: {PK, NotNull, VARCHAR(10)} */
    protected String kamokucd;

    /** date: {NotNull, timestamp} */
    protected Timestamp date;

    /** classification: {VARCHAR(255)} */
    protected String classification;

    /** classification_custom: {VARCHAR(255)} */
    protected String classificationCustom;

    /** title: {VARCHAR(255)} */
    protected String title;

    /** comment: {VARCHAR(500)} */
    protected String comment;

    /** author_name: {VARCHAR(255)} */
    protected String authorName;

    /** author_type: {VARCHAR(255)} */
    protected String authorType;

    /** presentation_format: {VARCHAR(255)} */
    protected String presentationFormat;

    /** medium_title: {VARCHAR(255)} */
    protected String mediumTitle;

    /** medium_class: {VARCHAR(255)} */
    protected String mediumClass;

    /** has_impact_factor: {VARCHAR(255)} */
    protected String hasImpactFactor;

    /** impact_factor: {VARCHAR255} */
    protected String impactFactor;

    /** location: {VARCHAR(255)} */
    protected String location;

    /** publisher: {VARCHAR(255)} */
    protected String publisher;

    /** isbn: {VARCHAR(255)} */
    protected String isbn;

    /** journal_volume: {VARCHAR(255)} */
    protected String journalVolume;

    /** journal_number: {VARCHAR(255)} */
    protected String journalNumber;

    /** page_from: {int(11)} */
    protected Integer pageFrom;

    /** page_to: {int(11)} */
    protected Integer pageTo;

    /** published_year: {int(11)} */
    protected Integer publishedYear;

    /** published_month: {int(11)} */
    protected Integer publishedMonth;

    /** has_reviewed: {VARCHAR(255)} */
    protected String hasReviewed;

    /** original_medium_name: {VARCHAR(255)} */
    protected String originalMediumName;

    /** original_author_name: {VARCHAR(255)} */
    protected String originalAuthorName;

    /** original_publisher: {VARCHAR(255)} */
    protected String originalPublisher;

    /** original_page_from: {int(11)} */
    protected Integer originalPageFrom;

    /** original_page_to: {int(11)} */
    protected Integer originalPageTo;

    /** original_published_year: {int(11)} */
    protected Integer originalPublishedYear;

    /** original_published_month: {int(11)} */
    protected Integer originalPublishedMonth;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

	public String getgSysno() {
		return gSysno;
	}

	public void setgSysno(String gSysno) {
		this.gSysno = gSysno;
	}

	public String getKamokucd() {
		return kamokucd;
	}

	public void setKamokucd(String kamokucd) {
		this.kamokucd = kamokucd;
	}

	public String getClassificationCustom() {
		return classificationCustom;
	}

	public void setClassificationCustom(String classificationCustom) {
		this.classificationCustom = classificationCustom;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getAuthorType() {
		return authorType;
	}

	public void setAuthorType(String authorType) {
		this.authorType = authorType;
	}

	public String getPresentationFormat() {
		return presentationFormat;
	}

	public void setPresentationFormat(String presentationFormat) {
		this.presentationFormat = presentationFormat;
	}

	public String getMediumTitle() {
		return mediumTitle;
	}

	public void setMediumTitle(String mediumTitle) {
		this.mediumTitle = mediumTitle;
	}

	public String getMediumClass() {
		return mediumClass;
	}

	public void setMediumClass(String mediumClass) {
		this.mediumClass = mediumClass;
	}

	public String getHasImpactFactor() {
		return hasImpactFactor;
	}

	public void setHasImpactFactor(String hasImpactFactor) {
		this.hasImpactFactor = hasImpactFactor;
	}

	public String getImpactFactor() {
		return impactFactor;
	}

	public void setImpactFactor(String impactFactor) {
		this.impactFactor = impactFactor;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getJournalVolume() {
		return journalVolume;
	}

	public void setJournalVolume(String journalVolume) {
		this.journalVolume = journalVolume;
	}

	public String getJournalNumber() {
		return journalNumber;
	}

	public void setJournalNumber(String journalNumber) {
		this.journalNumber = journalNumber;
	}

	public Integer getPageFrom() {
		return pageFrom;
	}

	public void setPageFrom(Integer pageFrom) {
		this.pageFrom = pageFrom;
	}

	public Integer getPageTo() {
		return pageTo;
	}

	public void setPageTo(Integer pageTo) {
		this.pageTo = pageTo;
	}

	public Integer getPublishedYear() {
		return publishedYear;
	}

	public void setPublishedYear(Integer publishedYear) {
		this.publishedYear = publishedYear;
	}

	public Integer getPublishedMonth() {
		return publishedMonth;
	}

	public void setPublishedMonth(Integer publishedMonth) {
		this.publishedMonth = publishedMonth;
	}

	public String getHasReviewed() {
		return hasReviewed;
	}

	public void setHasReviewed(String hasReviewed) {
		this.hasReviewed = hasReviewed;
	}

	public String getOriginalMediumName() {
		return originalMediumName;
	}

	public void setOriginalMediumName(String originalMediumName) {
		this.originalMediumName = originalMediumName;
	}

	public String getOriginalAuthorName() {
		return originalAuthorName;
	}

	public void setOriginalAuthorName(String originalAuthorName) {
		this.originalAuthorName = originalAuthorName;
	}

	public String getOriginalPublisher() {
		return originalPublisher;
	}

	public void setOriginalPublisher(String originalPublisher) {
		this.originalPublisher = originalPublisher;
	}

	public Integer getOriginalPageFrom() {
		return originalPageFrom;
	}

	public void setOriginalPageFrom(Integer originalPageFrom) {
		this.originalPageFrom = originalPageFrom;
	}

	public Integer getOriginalPageTo() {
		return originalPageTo;
	}

	public void setOriginalPageTo(Integer originalPageTo) {
		this.originalPageTo = originalPageTo;
	}

	public Integer getOriginalPublishedYear() {
		return originalPublishedYear;
	}

	public void setOriginalPublishedYear(Integer originalPublishedYear) {
		this.originalPublishedYear = originalPublishedYear;
	}

	public Integer getOriginalPublishedMonth() {
		return originalPublishedMonth;
	}

	public void setOriginalPublishedMonth(Integer originalPublishedMonth) {
		this.originalPublishedMonth = originalPublishedMonth;
	}

	public java.util.Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(java.util.Date insertDate) {
		this.insertDate = insertDate;
	}

	public java.util.Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(java.util.Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

}
