package dao.bsentity;

public abstract class BsShutokutaniTaishoMst {

    protected String kamokudkbncd;
    protected String kamokum_shozokucd;
    protected String kamokumkbncd;
    protected String kamokus_shozokucd;
    protected String kamokuskbncd;
    protected String kamokuss_shozokucd;
    protected String kamokusskbncd;

    protected java.util.Date insertDate;
    protected java.util.Date updateDate;
    protected String userid;

    public String getKamokudkbncd() {
		return kamokudkbncd;
	}

	public void setKamokudkbncd(String kamokudkbncd) {
		this.kamokudkbncd = kamokudkbncd;
	}

	public String getKamokum_shozokucd() {
		return kamokum_shozokucd;
	}

	public void setKamokum_shozokucd(String kamokum_shozokucd) {
		this.kamokum_shozokucd = kamokum_shozokucd;
	}

	public String getKamokumkbncd() {
		return kamokumkbncd;
	}

	public void setKamokumkbncd(String kamokumkbncd) {
		this.kamokumkbncd = kamokumkbncd;
	}

	public String getKamokus_shozokucd() {
		return kamokus_shozokucd;
	}

	public void setKamokus_shozokucd(String kamokus_shozokucd) {
		this.kamokus_shozokucd = kamokus_shozokucd;
	}

	public String getKamokuskbncd() {
		return kamokuskbncd;
	}

	public void setKamokuskbncd(String kamokuskbncd) {
		this.kamokuskbncd = kamokuskbncd;
	}

	public String getKamokuss_shozokucd() {
		return kamokuss_shozokucd;
	}

	public void setKamokuss_shozokucd(String kamokuss_shozokucd) {
		this.kamokuss_shozokucd = kamokuss_shozokucd;
	}

	public String getKamokusskbncd() {
		return kamokusskbncd;
	}

	public void setKamokusskbncd(String kamokusskbncd) {
		this.kamokusskbncd = kamokusskbncd;
	}

	public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
