package dao.bsentity;

public abstract class BsNyugakuNendos {

    /** NYUGAKU_NENDO: {DECIMAL(4)} */
    protected Integer nyugakuNendo;

    public Integer getNyugakuNendo() {
        return nyugakuNendo;
    }

    public void setNyugakuNendo(Integer nyugakuNendo) {
        this.nyugakuNendo = nyugakuNendo;
    }

}
