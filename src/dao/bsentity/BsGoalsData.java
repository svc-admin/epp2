package dao.bsentity;

public abstract class BsGoalsData {

    /** NENDO: {DECIMAL(4)} */
    protected Integer nendo;

    /** JIKANWARI_SHOZOKUCD: {VARCHAR(14)} */
    protected String jikanwariShozokucd;

    /** JIKANWARICD: {VARCHAR(8)} */
    protected String jikanwaricd;

    /** KAISOFLG: {DECIMAL(1)} */
    protected Integer kaisoflg;

    /** GOALCD0: {DECIMAL(2)} */
    protected Integer goalcd0;

    /** GOALCD1: {DECIMAL(2)} */
    protected Integer goalcd1;

    /** GOALCD2: {DECIMAL(2)} */
    protected Integer goalcd2;

    /** VALUE: {DECIMAL(1)} */
    protected Integer value;

    /** TANISU: {DECIMAL(2)} */
    protected Integer tanisu;

    /** TAISHO_NENJI_ONE: {DECIMAL(2)} */
    protected Integer taishoNenjiOne;

    /** KAIKOKBNCD: {DECIMAL(1)} */
    protected Integer kaikokbncd;

    /** HISSHUFLG: {DECIMAL(1)} */
    protected Integer hisshuflg;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    public Integer getNendo() {
        return nendo;
    }

    public void setNendo(Integer nendo) {
        this.nendo = nendo;
    }

    public String getJikanwariShozokucd() {
        return jikanwariShozokucd;
    }

    public void setJikanwariShozokucd(String jikanwariShozokucd) {
        this.jikanwariShozokucd = jikanwariShozokucd;
    }

    public String getJikanwaricd() {
        return jikanwaricd;
    }

    public void setJikanwaricd(String jikanwaricd) {
        this.jikanwaricd = jikanwaricd;
    }

    public Integer getKaisoflg() {
        return kaisoflg;
    }

    public void setKaisoflg(Integer kaisoflg) {
        this.kaisoflg = kaisoflg;
    }

    public Integer getGoalcd0() {
        return goalcd0;
    }

    public void setGoalcd0(Integer goalcd0) {
        this.goalcd0 = goalcd0;
    }

    public Integer getGoalcd1() {
        return goalcd1;
    }

    public void setGoalcd1(Integer goalcd1) {
        this.goalcd1 = goalcd1;
    }

    public Integer getGoalcd2() {
        return goalcd2;
    }

    public void setGoalcd2(Integer goalcd2) {
        this.goalcd2 = goalcd2;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getTanisu() {
        return tanisu;
    }

    public void setTanisu(Integer tanisu) {
        this.tanisu = tanisu;
    }

    public Integer getTaishoNenjiOne() {
        return taishoNenjiOne;
    }

    public void setTaishoNenjiOne(Integer taishoNenjiOne) {
        this.taishoNenjiOne = taishoNenjiOne;
    }

    public Integer getKaikokbncd() {
        return kaikokbncd;
    }

    public void setKaikokbncd(Integer kaikokbncd) {
        this.kaikokbncd = kaikokbncd;
    }

    public Integer getHisshuflg() {
        return hisshuflg;
    }

    public void setHisshuflg(Integer hisshuflg) {
        this.hisshuflg = hisshuflg;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
