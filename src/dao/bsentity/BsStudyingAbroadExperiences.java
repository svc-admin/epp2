package dao.bsentity;

import java.sql.Timestamp;


public abstract class BsStudyingAbroadExperiences {

    /** id: {PK, NotNull, int(11)} */
    protected Integer id;

    /** G_SYSNO: {PK, NotNull, VARCHAR(10)} */
    protected String gSysno;

    /** KAMOKUCD: {PK, NotNull, VARCHAR(10)} */
    protected String kamokucd;

    /** purpose_type: {VARCHAR(255)} */
    protected String purposeType;

    /** nation: {VARCHAR(255)} */
    protected String nation;

    /** date_from: {timestamp} */
    protected Timestamp dateFrom;

    /** date_to: {timestamp} */
    protected Timestamp dateTo;

    /** destination: {VARCHAR(255)} */
    protected String destination;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getgSysno() {
		return gSysno;
	}

	public void setgSysno(String gSysno) {
		this.gSysno = gSysno;
	}

	public String getKamokucd() {
		return kamokucd;
	}

	public void setKamokucd(String kamokucd) {
		this.kamokucd = kamokucd;
	}

	public String getPurposeType() {
		return purposeType;
	}

	public void setPurposeType(String purposeType) {
		this.purposeType = purposeType;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public Timestamp getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Timestamp dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Timestamp getDateTo() {
		return dateTo;
	}

	public void setDateTo(Timestamp dateTo) {
		this.dateTo = dateTo;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public java.util.Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(java.util.Date insertDate) {
		this.insertDate = insertDate;
	}

	public java.util.Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(java.util.Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

}
