package dao.bsentity;

public abstract class BsKakuteiSeiseki {

    /** G_BUKYOKUCD: {IX, VARCHAR(14)} */
    protected String gBukyokucd;

    /** G_GAKUNO: {VARCHAR(10)} */
    protected String gGakuno;

    /** KAMOKUCD: {VARCHAR(10)} */
    protected String kamokucd;

    /** SEQ_NO: {DECIMAL(3)} */
    protected Integer seqNo;

    /** G_SYSNO: {IX, VARCHAR(10)} */
    protected String gSysno;

    /** NINTEI_NENDO: {DECIMAL(4)} */
    protected Integer ninteiNendo;

    /** NINTEI_GAKKIKBNCD: {VARCHAR(2)} */
    protected String ninteiGakkikbncd;

    /** NINTEI_KAIKOKBNCD: {VARCHAR(1)} */
    protected String ninteiKaikokbncd;

    /** CUR_NENDO: {VARCHAR(6)} */
    protected String curNendo;

    /** CUR_SHOZOKUCD: {VARCHAR(14)} */
    protected String curShozokucd;

    /** KAMOKUD_SHOZOKUCD: {VARCHAR(14)} */
    protected String kamokudShozokucd;

    /** KAMOKUDKBNCD: {VARCHAR(3)} */
    protected String kamokudkbncd;

    /** KAMOKUM_SHOZOKUCD: {VARCHAR(14)} */
    protected String kamokumShozokucd;

    /** KAMOKUMKBNCD: {VARCHAR(3)} */
    protected String kamokumkbncd;

    /** KAMOKUS_SHOZOKUCD: {VARCHAR(14)} */
    protected String kamokusShozokucd;

    /** KAMOKUSKBNCD: {VARCHAR(3)} */
    protected String kamokuskbncd;

    /** KAMOKUSS_SHOZOKUCD: {VARCHAR(14)} */
    protected String kamokussShozokucd;

    /** KAMOKUSSKBNCD: {VARCHAR(3)} */
    protected String kamokusskbncd;

    /** HYOGOCD: {VARCHAR(2)} */
    protected String hyogocd;

    /** HYOTEN: {VARCHAR(3)} */
    protected String hyoten;

    /** HYOGONM: {VARCHAR(10)} */
    protected String hyogonm;

    /** TANISU: {DECIMAL(4, 1)} */
    protected java.math.BigDecimal tanisu;

    /** GOHIKBN: {DECIMAL(1)} */
    protected Integer gohikbn;

    /** SAITENYMD: {DATE(10)} */
    protected java.util.Date saitenymd;

    /** SAITENSHA_SHOZOKUCD: {VARCHAR(14)} */
    protected String saitenshaShozokucd;

    /** SAITENSHACD: {VARCHAR(8)} */
    protected String saitenshacd;

    /** JIKANWARI_NENDO: {DECIMAL(4)} */
    protected Integer jikanwariNendo;

    /** JIKANWARI_SHOZOKUCD: {VARCHAR(14)} */
    protected String jikanwariShozokucd;

    /** JIKANWARICD: {VARCHAR(8)} */
    protected String jikanwaricd;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    public String getgBukyokucd() {
        return gBukyokucd;
    }

    public void setgBukyokucd(String gBukyokucd) {
        this.gBukyokucd = gBukyokucd;
    }

    public String getgGakuno() {
        return gGakuno;
    }

    public void setgGakuno(String gGakuno) {
        this.gGakuno = gGakuno;
    }

    public String getKamokucd() {
        return kamokucd;
    }

    public void setKamokucd(String kamokucd) {
        this.kamokucd = kamokucd;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    public String getgSysno() {
        return gSysno;
    }

    public void setgSysno(String gSysno) {
        this.gSysno = gSysno;
    }

    public Integer getNinteiNendo() {
        return ninteiNendo;
    }

    public void setNinteiNendo(Integer ninteiNendo) {
        this.ninteiNendo = ninteiNendo;
    }

    public String getNinteiGakkikbncd() {
        return ninteiGakkikbncd;
    }

    public void setNinteiGakkikbncd(String ninteiGakkikbncd) {
        this.ninteiGakkikbncd = ninteiGakkikbncd;
    }

    public String getNinteiKaikokbncd() {
        return ninteiKaikokbncd;
    }

    public void setNinteiKaikokbncd(String ninteiKaikokbncd) {
        this.ninteiKaikokbncd = ninteiKaikokbncd;
    }

    public String getCurNendo() {
        return curNendo;
    }

    public void setCurNendo(String curNendo) {
        this.curNendo = curNendo;
    }

    public String getCurShozokucd() {
        return curShozokucd;
    }

    public void setCurShozokucd(String curShozokucd) {
        this.curShozokucd = curShozokucd;
    }

    public String getKamokudShozokucd() {
        return kamokudShozokucd;
    }

    public void setKamokudShozokucd(String kamokudShozokucd) {
        this.kamokudShozokucd = kamokudShozokucd;
    }

    public String getKamokudkbncd() {
        return kamokudkbncd;
    }

    public void setKamokudkbncd(String kamokudkbncd) {
        this.kamokudkbncd = kamokudkbncd;
    }

    public String getKamokumShozokucd() {
        return kamokumShozokucd;
    }

    public void setKamokumShozokucd(String kamokumShozokucd) {
        this.kamokumShozokucd = kamokumShozokucd;
    }

    public String getKamokumkbncd() {
        return kamokumkbncd;
    }

    public void setKamokumkbncd(String kamokumkbncd) {
        this.kamokumkbncd = kamokumkbncd;
    }

    public String getKamokusShozokucd() {
        return kamokusShozokucd;
    }

    public void setKamokusShozokucd(String kamokusShozokucd) {
        this.kamokusShozokucd = kamokusShozokucd;
    }

    public String getKamokuskbncd() {
        return kamokuskbncd;
    }

    public void setKamokuskbncd(String kamokuskbncd) {
        this.kamokuskbncd = kamokuskbncd;
    }

    public String getKamokussShozokucd() {
        return kamokussShozokucd;
    }

    public void setKamokussShozokucd(String kamokussShozokucd) {
        this.kamokussShozokucd = kamokussShozokucd;
    }

    public String getKamokusskbncd() {
        return kamokusskbncd;
    }

    public void setKamokusskbncd(String kamokusskbncd) {
        this.kamokusskbncd = kamokusskbncd;
    }

    public String getHyogocd() {
        return hyogocd;
    }

    public void setHyogocd(String hyogocd) {
        this.hyogocd = hyogocd;
    }

    public String getHyoten() {
        return hyoten;
    }

    public void setHyoten(String hyoten) {
        this.hyoten = hyoten;
    }

    public String getHyogonm() {
        return hyogonm;
    }

    public void setHyogonm(String hyogonm) {
        this.hyogonm = hyogonm;
    }

    public java.math.BigDecimal getTanisu() {
        return tanisu;
    }

    public void setTanisu(java.math.BigDecimal tanisu) {
        this.tanisu = tanisu;
    }

    public Integer getGohikbn() {
        return gohikbn;
    }

    public void setGohikbn(Integer gohikbn) {
        this.gohikbn = gohikbn;
    }

    public java.util.Date getSaitenymd() {
        return saitenymd;
    }

    public void setSaitenymd(java.util.Date saitenymd) {
        this.saitenymd = saitenymd;
    }

    public String getSaitenshaShozokucd() {
        return saitenshaShozokucd;
    }

    public void setSaitenshaShozokucd(String saitenshaShozokucd) {
        this.saitenshaShozokucd = saitenshaShozokucd;
    }

    public String getSaitenshacd() {
        return saitenshacd;
    }

    public void setSaitenshacd(String saitenshacd) {
        this.saitenshacd = saitenshacd;
    }

    public Integer getJikanwariNendo() {
        return jikanwariNendo;
    }

    public void setJikanwariNendo(Integer jikanwariNendo) {
        this.jikanwariNendo = jikanwariNendo;
    }

    public String getJikanwariShozokucd() {
        return jikanwariShozokucd;
    }

    public void setJikanwariShozokucd(String jikanwariShozokucd) {
        this.jikanwariShozokucd = jikanwariShozokucd;
    }

    public String getJikanwaricd() {
        return jikanwaricd;
    }

    public void setJikanwaricd(String jikanwaricd) {
        this.jikanwaricd = jikanwaricd;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
