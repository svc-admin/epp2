package dao.bsentity;

public abstract class BsGpaShugakuninteiJogaikamokuMst {

    /** NINTEI_NENDO: {DECIMAL(4)} */
    protected Integer yokennendo;

    /** G_BUKYOKUCD: {IX, VARCHAR(14)} */
    protected String gBukyokucd;

    /** KAMOKUCD: {VARCHAR(10)} */
    protected String kamokucd;

    /** KAMOKUNM */
    protected String kamokunm;

    /** INSERT_DATE: {NotNull, DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {NotNull, DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {NotNull, VARCHAR(10)} */
    protected String userid;

    public Integer getYokenNendo() {
        return yokennendo;
    }

    public void setYokenNendo(Integer yokennendo) {
        this.yokennendo = yokennendo;
    }

    public String getgBukyokucd() {
        return gBukyokucd;
    }

    public void setgBukyokucd(String gBukyokucd) {
        this.gBukyokucd = gBukyokucd;
    }

    public String getKamokucd() {
        return kamokucd;
    }

    public void setKamokucd(String kamokucd) {
        this.kamokucd = kamokucd;
    }

    public String getKamokunm() {
        return kamokunm;
    }

    public void setKamokunm(String kamokunm) {
        this.kamokunm = kamokunm;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
