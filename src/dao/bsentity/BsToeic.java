package dao.bsentity;

public abstract class BsToeic {

    /** uid: {NotNull, VARCHAR(10)} */
    protected String uid;

    /** year: {NotNull, DECIMAL(4)} */
    protected Integer year;

    /** score: {NotNull, DECIMAL(3)} */
    protected Integer score;

    /** INSERT_DATE: {NotNull, DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {NotNull, DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {NotNull, VARCHAR(10)} */
    protected String userid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
