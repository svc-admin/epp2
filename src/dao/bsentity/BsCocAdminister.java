package dao.bsentity;

public abstract class BsCocAdminister {

    /** G_GAKUNO: {VARCHAR(10)} */
    protected String gGakuno;

    /** G_SORT: {VARCHAR(10)} */
    protected String gSort;

    /** G_SYSNO: {VARCHAR(10)} */
    protected String gSysno;

    /** G_SHOZOKUCD: {VARCHAR(14)} */
    protected String gShozokucd;

    /** GAKUNEN: {DECIMAL(2)} */
    protected Integer gakunen;

    /** KUBUN: {VARCHAR(10)} */
    protected String kubun;

    /** BIKOU: {VARCHAR(10)} */
    protected String bikou;

    /** IMAGEFILENM: {VARCHAR(20)} */
    protected String imagefilenm;

    /** HAKKO_CNT: {DECIMAL(2)} */
    protected Integer hakkoCnt;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    /** SHOZOKUNM: {VARCHAR(120)} */
    protected String shozokunm;

    /** SHOZOKUNMENG: {VARCHAR(60)} */
    protected String shozokunmeng;

    /** G_NAME: {VARCHAR(40)} */
    protected String gName;

    /** G_NAMEENG: {VARCHAR(40)} */
    protected String gNameeng;

    /** STEP1: {DECIMAL(2)} */
    protected Integer Step1;

    /** STEP2: {DECIMAL(2)} */
    protected Integer Step2;

    /** STEP3: {DECIMAL(2)} */
    protected Integer Step3;

    /** STEP4: {DECIMAL(2)} */
    protected Integer Step4;

    /** STEP5: {DECIMAL(2)} */
    protected Integer Step5;

    /** STEP6: {DECIMAL(2)} */
    protected Integer Step6;

    /** STEP7: {DECIMAL(2)} */
    protected Integer Step7;

    /** STEP8: {DECIMAL(2)} */
    protected Integer Step8;

    /** STEP9: {DECIMAL(2)} */
    protected Integer Step9;

    /** STEP10: {DECIMAL(2)} */
    protected Integer Step10;

    public String getgGakuno() {
        return gGakuno;
    }

    public void setgGakuno(String gGakuno) {
        this.gGakuno = gGakuno;
    }

    public String getgSort() {
        return gSort;
    }

    public void setgSort(String gSort) {
        this.gSort = gSort;
    }

    public String getgSysno() {
        return gSysno;
    }

    public void setgSysno(String gSysno) {
        this.gSysno = gSysno;
    }

    public String getgShozokucd() {
        return gShozokucd;
    }

    public void setgShozokucd(String gShozokucd) {
        this.gShozokucd = gShozokucd;
    }

    public Integer getGakunen() {
        return gakunen;
    }

    public void setGakunen(Integer gakunen) {
        this.gakunen = gakunen;
    }

    public String getKubun() {
        return kubun;
    }

    public void setKubun(String Kubun) {
        this.kubun = Kubun;
    }

    public String getBikou() {
        return bikou;
    }

    public void setBikou(String Bikou) {
        this.bikou = Bikou;
    }

    public String getImagefilenm() {
        return imagefilenm;
    }

    public void setImagefilenm(String imagefilenm) {
        this.imagefilenm = imagefilenm;
    }

    public Integer getHakkoCnt() {
        return hakkoCnt;
    }

    public void setHakkoCnt(Integer hakkoCnt) {
        this.hakkoCnt = hakkoCnt;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getShozokunm() {
        return shozokunm;
    }

    public void setShozokunm(String shozokunm) {
        this.shozokunm = shozokunm;
    }

    public String getShozokunmeng() {
        return shozokunmeng;
    }

    public void setShozokunmeng(String shozokunmeng) {
        this.shozokunmeng = shozokunmeng;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public String getgNameeng() {
        return gNameeng;
    }

    public void setgNameeng(String gNameeng) {
        this.gNameeng = gNameeng;
    }

    public Integer getStep1() {
        return Step1;
    }

    public void setStep1(Integer cocStep1) {
        this.Step1 = cocStep1;
    }

    public Integer getStep2() {
        return Step2;
    }

    public void setStep2(Integer Step2) {
        this.Step2 =Step2;
    }

    public Integer getStep3() {
        return Step3;
    }

    public void setStep3(Integer Step3) {
        this.Step3 = Step3;
    }

    public Integer getStep4() {
        return Step4;
    }

    public void setStep4(Integer Step4) {
        this.Step4 = Step4;
    }

    public Integer getStep5() {
        return Step5;
    }

    public void setStep5(Integer Step5) {
        this.Step5 = Step5;
    }

    public Integer getStep6() {
        return Step6;
    }

    public void setStep6(Integer Step6) {
        this.Step6 = Step6;
    }

    public Integer getStep7() {
        return Step7;
    }

    public void setStep7(Integer Step7) {
        this.Step7 = Step7;
    }

    public Integer getStep8() {
        return Step8;
    }

    public void setStep8(Integer Step8) {
        this.Step8 = Step8;
    }

    public Integer getStep9() {
        return Step9;
    }

    public void setStep9(Integer Step9) {
        this.Step9 = Step9;
    }

    public Integer getStep10() {
        return Step10;
    }

    public void setStep10(Integer Step10) {
        this.Step10 = Step10;
    }
}