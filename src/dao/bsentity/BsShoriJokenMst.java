package dao.bsentity;

public abstract class BsShoriJokenMst {

    /** SHORI_NENDO: {DECIMAL(4)} */
    protected Integer shori_nendo;

    /** GAKKIKBNCD: {VARCHAR(2)} */
    protected String gakkikbncd;

    /** KIJUN_DATE: {VARCHAR(8)} */
    protected String kijundate;

    /** INSERT_DATE: {NotNull, DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {NotNull, DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {NotNull, VARCHAR(10)} */
    protected String userid;

    public Integer getShorinendo() {
        return shori_nendo;
    }

    public void setShorinendo(Integer shori_nendo) {
        this.shori_nendo = shori_nendo;
    }

    public String getGakkikbncd() {
        return gakkikbncd;
    }

    public void setGakkikbncd(String gakkikbncd) {
        this.gakkikbncd = gakkikbncd;
    }

    public String getKijundate() {
        return kijundate;
    }

    public void setKijundate(String kijundate) {
        this.kijundate = kijundate;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
