package dao.bsentity;

public abstract class BsAccessLog {

    /** kid: {VARCHAR(10)} */
    protected String kid;

    /** targetid: {VARCHAR(10)} */
    protected String targetid;

    /** ip: {VARCHAR(15)} */
    protected String ip;

    /** pagenm: {VARCHAR(100)} */
    protected String pagenm;

    /** ACCESS_DATE: {NotNull, VARCHAR(19)} */
    protected String accessDate;

    public String getKid() {
        return kid;
    }

    public void setKid(String kid) {
        this.kid = kid;
    }

    public String getTargetid() {
        return targetid;
    }

    public void setTargetid(String targetid) {
        this.targetid = targetid;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPagenm() {
        return pagenm;
    }

    public void setPagenm(String pagenm) {
        this.pagenm = pagenm;
    }

    public String getAccessDate() {
        return accessDate;
    }

    public void setAccessDate(String accessDate) {
        this.accessDate = accessDate;
    }
}
