package dao.bsentity;

public abstract class BsGoalsSub {

    /** NENDO: {DECIMAL(4)} */
    protected Integer nendo;

    /** JIKANWARI_SHOZOKUCD: {VARCHAR(14)} */
    protected String jikanwariShozokucd;

    /** KAISOFLG: {DECIMAL(1)} */
    protected Integer kaisoflg;

    /** GOALCD0: {DECIMAL(2)} */
    protected Integer goalcd0;

    /** GOALCD1: {DECIMAL(2)} */
    protected Integer goalcd1;

    /** GOALCD2: {DECIMAL(2)} */
    protected Integer goalcd2;

    /** NAME: {VARCHAR(100)} */
    protected String name;

    /** NAME_ENG: {VARCHAR(100)} */
    protected String nameEng;

    /** EXPL: {VARCHAR(500)} */
    protected String expl;

    /** EXPL_ENG: {VARCHAR(500)} */
    protected String explEng;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    public Integer getNendo() {
        return nendo;
    }

    public void setNendo(Integer nendo) {
        this.nendo = nendo;
    }

    public String getJikanwariShozokucd() {
        return jikanwariShozokucd;
    }

    public void setJikanwariShozokucd(String jikanwariShozokucd) {
        this.jikanwariShozokucd = jikanwariShozokucd;
    }

    public Integer getKaisoflg() {
        return kaisoflg;
    }

    public void setKaisoflg(Integer kaisoflg) {
        this.kaisoflg = kaisoflg;
    }

    public Integer getGoalcd0() {
        return goalcd0;
    }

    public void setGoalcd0(Integer goalcd0) {
        this.goalcd0 = goalcd0;
    }

    public Integer getGoalcd1() {
        return goalcd1;
    }

    public void setGoalcd1(Integer goalcd1) {
        this.goalcd1 = goalcd1;
    }

    public Integer getGoalcd2() {
        return goalcd2;
    }

    public void setGoalcd2(Integer goalcd2) {
        this.goalcd2 = goalcd2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public String getExpl() {
        return expl;
    }

    public void setExpl(String expl) {
        this.expl = expl;
    }

    public String getExplEng() {
        return explEng;
    }

    public void setExplEng(String explEng) {
        this.explEng = explEng;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
