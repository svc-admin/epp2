package dao.bsentity;

public abstract class BsKyokihon {

    /** G_SYSNO: {PK, NotNull, VARCHAR(10)} */
    protected String gSysno;

    /** G_NAME: {VARCHAR(40)} */
    protected String gName;

    /** G_NAMERK: {VARCHAR(40)} */
    protected String gNamerk;

    /** G_KANA: {VARCHAR(80)} */
    protected String gKana;

    /** G_NAMEENG: {VARCHAR(40)} */
    protected String gNameeng;

    /** G_SEX: {VARCHAR(1)} */
    protected String gSex;

    /** G_BIRTH: {DATE(10)} */
    protected java.util.Date gBirth;

    /** G_YUBIN: {VARCHAR(10)} */
    protected String gYubin;

    /** G_ADD1: {VARCHAR(100)} */
    protected String gAdd1;

    /** G_ADD2: {VARCHAR(100)} */
    protected String gAdd2;

    /** G_ADD3: {VARCHAR(100)} */
    protected String gAdd3;

    /** G_TEL: {VARCHAR(20)} */
    protected String gTel;

    /** G_TELKBNCD: {VARCHAR(1)} */
    protected String gTelkbncd;

    /** G_KEITAITEL: {VARCHAR(20)} */
    protected String gKeitaitel;

    /** G_EMAIL: {VARCHAR(40)} */
    protected String gEmail;

    /** G_KENCD: {VARCHAR(2)} */
    protected String gKencd;

    /** JUKYOKBNCD: {VARCHAR(2)} */
    protected String jukyokbncd;

    /** KOKUSEKICD: {VARCHAR(3)} */
    protected String kokusekicd;

    /** HONSEKICD: {VARCHAR(2)} */
    protected String honsekicd;

    /** HONSEKIADD1: {VARCHAR(40)} */
    protected String honsekiadd1;

    /** HONSEKIADD2: {VARCHAR(40)} */
    protected String honsekiadd2;

    /** HONSEKIADD3: {VARCHAR(40)} */
    protected String honsekiadd3;

    /** BIKO: {VARCHAR(100)} */
    protected String biko;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    public String getgSysno() {
        return gSysno;
    }

    public void setgSysno(String gSysno) {
        this.gSysno = gSysno;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public String getgNamerk() {
        return gNamerk;
    }

    public void setgNamerk(String gNamerk) {
        this.gNamerk = gNamerk;
    }

    public String getgKana() {
        return gKana;
    }

    public void setgKana(String gKana) {
        this.gKana = gKana;
    }

    public String getgNameeng() {
        return gNameeng;
    }

    public void setgNameeng(String gNameeng) {
        this.gNameeng = gNameeng;
    }

    public String getgSex() {
        return gSex;
    }

    public void setgSex(String gSex) {
        this.gSex = gSex;
    }

    public java.util.Date getgBirth() {
        return gBirth;
    }

    public void setgBirth(java.util.Date gBirth) {
        this.gBirth = gBirth;
    }

    public String getgYubin() {
        return gYubin;
    }

    public void setgYubin(String gYubin) {
        this.gYubin = gYubin;
    }

    public String getgAdd1() {
        return gAdd1;
    }

    public void setgAdd1(String gAdd1) {
        this.gAdd1 = gAdd1;
    }

    public String getgAdd2() {
        return gAdd2;
    }

    public void setgAdd2(String gAdd2) {
        this.gAdd2 = gAdd2;
    }

    public String getgAdd3() {
        return gAdd3;
    }

    public void setgAdd3(String gAdd3) {
        this.gAdd3 = gAdd3;
    }

    public String getgTel() {
        return gTel;
    }

    public void setgTel(String gTel) {
        this.gTel = gTel;
    }

    public String getgTelkbncd() {
        return gTelkbncd;
    }

    public void setgTelkbncd(String gTelkbncd) {
        this.gTelkbncd = gTelkbncd;
    }

    public String getgKeitaitel() {
        return gKeitaitel;
    }

    public void setgKeitaitel(String gKeitaitel) {
        this.gKeitaitel = gKeitaitel;
    }

    public String getgEmail() {
        return gEmail;
    }

    public void setgEmail(String gEmail) {
        this.gEmail = gEmail;
    }

    public String getgKencd() {
        return gKencd;
    }

    public void setgKencd(String gKencd) {
        this.gKencd = gKencd;
    }

    public String getJukyokbncd() {
        return jukyokbncd;
    }

    public void setJukyokbncd(String jukyokbncd) {
        this.jukyokbncd = jukyokbncd;
    }

    public String getKokusekicd() {
        return kokusekicd;
    }

    public void setKokusekicd(String kokusekicd) {
        this.kokusekicd = kokusekicd;
    }

    public String getHonsekicd() {
        return honsekicd;
    }

    public void setHonsekicd(String honsekicd) {
        this.honsekicd = honsekicd;
    }

    public String getHonsekiadd1() {
        return honsekiadd1;
    }

    public void setHonsekiadd1(String honsekiadd1) {
        this.honsekiadd1 = honsekiadd1;
    }

    public String getHonsekiadd2() {
        return honsekiadd2;
    }

    public void setHonsekiadd2(String honsekiadd2) {
        this.honsekiadd2 = honsekiadd2;
    }

    public String getHonsekiadd3() {
        return honsekiadd3;
    }

    public void setHonsekiadd3(String honsekiadd3) {
        this.honsekiadd3 = honsekiadd3;
    }

    public String getBiko() {
        return biko;
    }

    public void setBiko(String biko) {
        this.biko = biko;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
