package dao.bsentity;

public abstract class BsKussShinro {

    /** NENDO: {NotNull, DECIMAL(4)} */
    protected Integer nendo;

    /** G_BUKYOKUCD: {NotNull, VARCHAR(14)} */
    protected String gBukyokucd;

    /** G_GAKUNO: {NotNull, VARCHAR(9)} */
    protected String gGakuno;

    /** G_SYSNO: {VARCHAR(10)} */
    protected String gSysno;

    /** G_SHOZOKUCD: {VARCHAR(14)} */
    protected String gShozokucd;

    /** G_SEX: {DECIMAL(1)} */
    protected Integer gSex;

    /** SHINROKBN: {DECIMAL(2)} */
    protected Integer shinrokbn;

    /** GYOSHUCD: {VARCHAR(3)} */
    protected String gyoshucd;

    /** SHOKUSHUCD: {VARCHAR(4)} */
    protected String shokushucd;

    /** KINMUCHICD: {VARCHAR(2)} */
    protected String kinmuchicd;

    /** SHINROCD: {VARCHAR(10)} */
    protected String shinrocd;

    /** SHINRONM: {VARCHAR(100)} */
    protected String shinronm;

    /** KANNAIFLG: {DECIMAL(1)} */
    protected Integer kannaiflg;

    /** NAITEIYMD: {DATE(10)} */
    protected java.util.Date naiteiymd;

    /** KOKAIFLG: {DECIMAL(1)} */
    protected Integer kokaiflg;

    /** GAKKOSECCHICD: {VARCHAR(2)} */
    protected String gakkosecchicd;

    /** KOYOKEITAICD: {VARCHAR(2)} */
    protected String koyokeitaicd;

    /** BIKO: {VARCHAR(400)} */
    protected String biko;

    public Integer getNendo() {
        return nendo;
    }

    public void setNendo(Integer nendo) {
        this.nendo = nendo;
    }

    public String getgBukyokucd() {
        return gBukyokucd;
    }

    public void setgBukyokucd(String gBukyokucd) {
        this.gBukyokucd = gBukyokucd;
    }

    public String getgGakuno() {
        return gGakuno;
    }

    public void setgGakuno(String gGakuno) {
        this.gGakuno = gGakuno;
    }

    public String getgSysno() {
        return gSysno;
    }

    public void setgSysno(String gSysno) {
        this.gSysno = gSysno;
    }

    public String getgShozokucd() {
        return gShozokucd;
    }

    public void setgShozokucd(String gShozokucd) {
        this.gShozokucd = gShozokucd;
    }

    public Integer getgSex() {
        return gSex;
    }

    public void setgSex(Integer gSex) {
        this.gSex = gSex;
    }

    public Integer getShinrokbn() {
        return shinrokbn;
    }

    public void setShinrokbn(Integer shinrokbn) {
        this.shinrokbn = shinrokbn;
    }

    public String getGyoshucd() {
        return gyoshucd;
    }

    public void setGyoshucd(String gyoshucd) {
        this.gyoshucd = gyoshucd;
    }

    public String getShokushucd() {
        return shokushucd;
    }

    public void setShokushucd(String shokushucd) {
        this.shokushucd = shokushucd;
    }

    public String getKinmuchicd() {
        return kinmuchicd;
    }

    public void setKinmuchicd(String kinmuchicd) {
        this.kinmuchicd = kinmuchicd;
    }

    public String getShinrocd() {
        return shinrocd;
    }

    public void setShinrocd(String shinrocd) {
        this.shinrocd = shinrocd;
    }

    public String getShinronm() {
        return shinronm;
    }

    public void setShinronm(String shinronm) {
        this.shinronm = shinronm;
    }

    public Integer getKannaiflg() {
        return kannaiflg;
    }

    public void setKannaiflg(Integer kannaiflg) {
        this.kannaiflg = kannaiflg;
    }

    public java.util.Date getNaiteiymd() {
        return naiteiymd;
    }

    public void setNaiteiymd(java.util.Date naiteiymd) {
        this.naiteiymd = naiteiymd;
    }

    public Integer getKokaiflg() {
        return kokaiflg;
    }

    public void setKokaiflg(Integer kokaiflg) {
        this.kokaiflg = kokaiflg;
    }

    public String getGakkosecchicd() {
        return gakkosecchicd;
    }

    public void setGakkosecchicd(String gakkosecchicd) {
        this.gakkosecchicd = gakkosecchicd;
    }

    public String getKoyokeitaicd() {
        return koyokeitaicd;
    }

    public void setKoyokeitaicd(String koyokeitaicd) {
        this.koyokeitaicd = koyokeitaicd;
    }

    public String getBiko() {
        return biko;
    }

    public void setBiko(String biko) {
        this.biko = biko;
    }

}
