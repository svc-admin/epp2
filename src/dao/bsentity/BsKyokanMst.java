package dao.bsentity;

public abstract class BsKyokanMst {

    /** KYOKANCD: {PK, NotNull, VARCHAR(10)} */
    protected String KYOKANCD;

    /** KYOKANNM: {, VARCHAR(40)} */
    protected String  KYOKANNM;

    /** KYOKANKN: {, VARCHAR(80)} */
    protected String  KYOKANKN;

    /** KYOKANENG: {, VARCHAR(40)} */
    protected String  KYOKANENG;

    /** KYOKANRK: {, VARCHAR(10)} */
    protected String  KYOKANRK;

    /** KYOKANSEX: {, DECIMAL(1,0)} */
    protected int KYOKANSEX;

    /** KYOKANYUBIN: {, VARCHAR(10)} */
    protected String  KYOKANYUBIN;

    /** KYOKANADD1: {, VARCHAR(100)} */
    protected String  KYOKANADD1;

    /** KYOKANADD2: {, VARCHAR(100)} */
    protected String  KYOKANADD2;

    /** KYOKANADD3: {, VARCHAR(100)} */
    protected String  KYOKANADD3;

    /** KYOKANTEL: {, VARCHAR(20)} */
    protected String  KYOKANTEL;

    /** K_SHOZOKUCD: {, VARCHAR(14)} */
    protected String  K_SHOZOKUCD;

    /** YAKUSHOKUCD: {, VARCHAR(4)} */
    protected String  YAKUSHOKUCD;

    /** KYOKANKBNCD: {, CHAR(1)} */
    protected String  KYOKANKBNCD;

    /** ZAISHOKUCD: {, CHAR(1)} */
    protected String  ZAISHOKUCD;

    /** KINMUKBNCD: {, CHAR(1)} */
    protected String  KINMUKBNCD;

    /**
     * @return the kYOKANCD
     */
    public String getKYOKANCD() {
        return KYOKANCD;
    }

    /**
     * @param kYOKANCD the kYOKANCD to set
     */
    public void setKYOKANCD(String kYOKANCD) {
        KYOKANCD = kYOKANCD;
    }

    /**
     * @return the kYOKANNM
     */
    public String getKYOKANNM() {
        return KYOKANNM;
    }

    /**
     * @param kYOKANNM the kYOKANNM to set
     */
    public void setKYOKANNM(String kYOKANNM) {
        KYOKANNM = kYOKANNM;
    }

    /**
     * @return the kYOKANKN
     */
    public String getKYOKANKN() {
        return KYOKANKN;
    }

    /**
     * @param kYOKANKN the kYOKANKN to set
     */
    public void setKYOKANKN(String kYOKANKN) {
        KYOKANKN = kYOKANKN;
    }

    /**
     * @return the kYOKANENG
     */
    public String getKYOKANENG() {
        return KYOKANENG;
    }

    /**
     * @param kYOKANENG the kYOKANENG to set
     */
    public void setKYOKANENG(String kYOKANENG) {
        KYOKANENG = kYOKANENG;
    }

    /**
     * @return the kYOKANRK
     */
    public String getKYOKANRK() {
        return KYOKANRK;
    }

    /**
     * @param kYOKANRK the kYOKANRK to set
     */
    public void setKYOKANRK(String kYOKANRK) {
        KYOKANRK = kYOKANRK;
    }

    /**
     * @return the kYOKANSEX
     */
    public int getKYOKANSEX() {
        return KYOKANSEX;
    }

    /**
     * @param kYOKANSEX the kYOKANSEX to set
     */
    public void setKYOKANSEX(int kYOKANSEX) {
        KYOKANSEX = kYOKANSEX;
    }

    /**
     * @return the kYOKANYUBIN
     */
    public String getKYOKANYUBIN() {
        return KYOKANYUBIN;
    }

    /**
     * @param kYOKANYUBIN the kYOKANYUBIN to set
     */
    public void setKYOKANYUBIN(String kYOKANYUBIN) {
        KYOKANYUBIN = kYOKANYUBIN;
    }

    /**
     * @return the kYOKANADD1
     */
    public String getKYOKANADD1() {
        return KYOKANADD1;
    }

    /**
     * @param kYOKANADD1 the kYOKANADD1 to set
     */
    public void setKYOKANADD1(String kYOKANADD1) {
        KYOKANADD1 = kYOKANADD1;
    }

    /**
     * @return the kYOKANADD2
     */
    public String getKYOKANADD2() {
        return KYOKANADD2;
    }

    /**
     * @param kYOKANADD2 the kYOKANADD2 to set
     */
    public void setKYOKANADD2(String kYOKANADD2) {
        KYOKANADD2 = kYOKANADD2;
    }

    /**
     * @return the kYOKANADD3
     */
    public String getKYOKANADD3() {
        return KYOKANADD3;
    }

    /**
     * @param kYOKANADD3 the kYOKANADD3 to set
     */
    public void setKYOKANADD3(String kYOKANADD3) {
        KYOKANADD3 = kYOKANADD3;
    }

    /**
     * @return the kYOKANTEL
     */
    public String getKYOKANTEL() {
        return KYOKANTEL;
    }

    /**
     * @param kYOKANTEL the kYOKANTEL to set
     */
    public void setKYOKANTEL(String kYOKANTEL) {
        KYOKANTEL = kYOKANTEL;
    }

    /**
     * @return the k_SHOZOKUCD
     */
    public String getK_SHOZOKUCD() {
        return K_SHOZOKUCD;
    }

    /**
     * @param k_SHOZOKUCD the k_SHOZOKUCD to set
     */
    public void setK_SHOZOKUCD(String k_SHOZOKUCD) {
        K_SHOZOKUCD = k_SHOZOKUCD;
    }

    /**
     * @return the yAKUSHOKUCD
     */
    public String getYAKUSHOKUCD() {
        return YAKUSHOKUCD;
    }

    /**
     * @param yAKUSHOKUCD the yAKUSHOKUCD to set
     */
    public void setYAKUSHOKUCD(String yAKUSHOKUCD) {
        YAKUSHOKUCD = yAKUSHOKUCD;
    }

    /**
     * @return the kYOKANKBNCD
     */
    public String getKYOKANKBNCD() {
        return KYOKANKBNCD;
    }

    /**
     * @param kYOKANKBNCD the kYOKANKBNCD to set
     */
    public void setKYOKANKBNCD(String kYOKANKBNCD) {
        KYOKANKBNCD = kYOKANKBNCD;
    }

    /**
     * @return the zAISHOKUCD
     */
    public String getZAISHOKUCD() {
        return ZAISHOKUCD;
    }

    /**
     * @param zAISHOKUCD the zAISHOKUCD to set
     */
    public void setZAISHOKUCD(String zAISHOKUCD) {
        ZAISHOKUCD = zAISHOKUCD;
    }

    /**
     * @return the kINMUKBNCD
     */
    public String getKINMUKBNCD() {
        return KINMUKBNCD;
    }

    /**
     * @param kINMUKBNCD the kINMUKBNCD to set
     */
    public void setKINMUKBNCD(String kINMUKBNCD) {
        KINMUKBNCD = kINMUKBNCD;
    }



}
