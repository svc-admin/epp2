package dao.bsentity;

public abstract class BsStoredProgrametable {

    /** JSON: {MEDIUMTEXT(16777215)} */
    protected String json;

    /** LOCALE: {VARCHAR(10)} */
    protected String locale;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
