package dao.bsentity;

public abstract class BsDirectory {

    /** UID: {VARCHAR(10)} */
    protected String uid;

    /** CID: {VARCHAR(64)} */
    protected String cid;

    /** DEPTH: {DECIMAL(2)} */
    protected Integer depth;

    /** NAME: {VARCHAR(256)} */
    protected String name;

    /** DATE: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]} */
    protected java.sql.Timestamp date;

    /** ISDIR: {DECIMAL(1)} */
    protected Integer isdir;

    /** PARENT: {VARCHAR(256)} */
    protected String parent;

    /** ABSDIR: {VARCHAR(1024)} */
    protected String absdir;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public Integer getDepth() {
        return depth;
    }

    public void setDepth(Integer depth) {
        this.depth = depth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public java.sql.Timestamp getDate() {
        return date;
    }

    public void setDate(java.sql.Timestamp date) {
        this.date = date;
    }

    public Integer getIsdir() {
        return isdir;
    }

    public void setIsdir(Integer isdir) {
        this.isdir = isdir;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getAbsdir() {
        return absdir;
    }

    public void setAbsdir(String absdir) {
        this.absdir = absdir;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
