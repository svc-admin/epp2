package dao.bsentity;

public abstract class BsAuthority {

    /** uid: {VARCHAR(10)} */
    protected String uid;

    /** YOKEN_NENDO: {DECIMAL(4)} */
    protected Integer yokennendo;

    /** DAIGAKUINKBNCD: {VARCHAR(1)} */
    protected String daigakuinkbncd;

    /** SHOZOKUCD: {VARCHAR(14)} */
    protected String shozokucd;

    /** SYSNO: {VARCHAR(10)} */
    protected String sysno;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getYokenNendo() {
        return yokennendo;
    }

    public void setYokenNendo(Integer yokennendo) {
        this.yokennendo = yokennendo;
    }

    public String getDaigakuinKbncd() {
        return daigakuinkbncd;
    }

    public void setDaigakuinKbncd(String daigakuinkbncd) {
        this.daigakuinkbncd = daigakuinkbncd;
    }

    public String getShozokucd() {
        return shozokucd;
    }

    public void setShozokucd(String shozokucd) {
        this.shozokucd = shozokucd;
    }

    public String getSysno() {
        return sysno;
    }

    public void setSysno(String sysno) {
        this.sysno = sysno;
    }

}
