package dao.bsentity;

public abstract class BsToeicAvr {

    /** NYUGAKU_NENDO: {NotNull, DECIMAL(4)} */
    protected Integer nyugakuNendo;

    /** SHOZOKUCD: {NotNull, VARCHAR(14)} */
    protected String shozokucd;

    /** JYUKEN_NENDO: {NotNull, DECIMAL(4)} */
    protected Integer jyukenNendo;

    /** score: {NotNull, DECIMAL(3)} */
    protected Integer score;

    /** member: {NotNull, DECIMAL(8)} */
    protected Integer member;

    /** INSERT_DATE: {NotNull, DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {NotNull, DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {NotNull, VARCHAR(10)} */
    protected String userid;

    public Integer getNyugakuNendo() {
        return nyugakuNendo;
    }

    public void setNyugakuNendo(Integer nyugakuNendo) {
        this.nyugakuNendo = nyugakuNendo;
    }

    public String getShozokucd() {
        return shozokucd;
    }

    public void setShozokucd(String shozokucd) {
        this.shozokucd = shozokucd;
    }

    public Integer getJyukenNendo() {
        return jyukenNendo;
    }

    public void setJyukenNendo(Integer jyukenNendo) {
        this.jyukenNendo = jyukenNendo;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getMember() {
        return member;
    }

    public void setMember(Integer member) {
        this.member = member;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
