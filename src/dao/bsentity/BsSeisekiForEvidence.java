package dao.bsentity;

public abstract class BsSeisekiForEvidence {

    /** G_SYSNO: {PK, NotNull, VARCHAR(10)} */
    protected String gSysno;

    /** KAMOKUCD: {PK, NotNull, VARCHAR(10)} */
    protected String kamokucd;

    /** KAMOKUNM: {NotNull, VARCHAR(150)} */
    protected String kamokunm;

    /** KAMOKUNMENG: {NotNull, VARCHAR(150)} */
    protected String kamokunmeng;

    /** HYOGOCD: {VARCHAR(2)} */
    protected String hyogocd;

    /** HYOGONM: {VARCHAR(10)} */
    protected String hyogonm;

    /** TANISU: {decimal(4,1)} */
    protected java.math.BigDecimal tanisu;

    /** NINTEI_NENDO: {decimal(4,0)} */
    protected Integer ninteiNendo;

    /** NINTEI_GAKKIKBNCD: {VARCHAR(2)} */
    protected String ninteiGakkikbncd;

    /** HITSUSENKBNCD: {VARCHAR(2)} */
    protected String hitsusenkbncd;

    /** JIKANWARI_NENDO: {NotNull, decimal(4,0)} */
    protected Integer jikanwariNendo;

    /** JIKANWARI_SHOZOKUCD: {NotNull, VARCHAR(14)} */
    protected String jikanwariShozokucd;

    /** JIKANWARICD: {NotNull, VARCHAR(8)} */
    protected String jikanwaricd;

    /** ENTERD_FLG: {decimal(1,0)} */
    protected Integer enterdFlg;

    /** CODE1: {decimal(3,0)} */
    protected Integer code1;

    /** CODE2: {decimal(3,0)} */
    protected Integer code2;

    /** CODE3: {decimal(3,0)} */
    protected Integer code3;

    /** CODE4: {decimal(3,0)} */
    protected Integer code4;

    /** CODE5: {decimal(3,0)} */
    protected Integer code5;

    /** CODE6: {decimal(3,0)} */
    protected Integer code6;

    /** CODE7: {decimal(3,0)} */
    protected Integer code7;

    /** COMMENT: {VARCHAR(500)} */
    protected String comment;

    /** DIR: {VARCHAR(256)} */
    protected String dir;

    /** EVIDENCE: {VARCHAR(256)} */
    protected String evidence;

    /** SIZE: {BIGINT(20)} */
    protected long size;

    /** lastUpdate: {TIMESTAMP} */
    protected java.sql.Timestamp lastUpdate;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    public String getgSysno() {
        return gSysno;
    }

    public void setgSysno(String gSysno) {
        this.gSysno = gSysno;
    }

    public String getKamokucd() {
        return kamokucd;
    }

    public void setKamokucd(String kamokucd) {
        this.kamokucd = kamokucd;
    }

    public String getKamokunm() {
        return kamokunm;
    }

    public void setKamokunm(String kamokunm) {
        this.kamokunm = kamokunm;
    }

    public String getKamokunmeng() {
        return kamokunmeng;
    }

    public void setKamokunmeng(String kamokunmeng) {
        this.kamokunmeng = kamokunmeng;
    }

    public String getHyogocd() {
        return hyogocd;
    }

    public void setHyogocd(String hyogocd) {
        this.hyogocd = hyogocd;
    }

    public String getHyogonm() {
        return hyogonm;
    }

    public void setHyogonm(String hyogonm) {
        this.hyogonm = hyogonm;
    }

    public java.math.BigDecimal getTanisu() {
        return tanisu;
    }

    public void setTanisu(java.math.BigDecimal tanisu) {
        this.tanisu = tanisu;
    }

    public Integer getNinteiNendo() {
        return ninteiNendo;
    }

    public void setNinteiNendo(Integer ninteiNendo) {
        this.ninteiNendo = ninteiNendo;
    }

    public String getNinteiGakkikbncd() {
        return ninteiGakkikbncd;
    }

    public void setNinteiGakkikbncd(String ninteiGakkikbncd) {
        this.ninteiGakkikbncd = ninteiGakkikbncd;
    }

    public String getHitsusenkbncd() {
        return hitsusenkbncd;
    }

    public void setHitsusenkbncd(String hitsusenkbncd) {
        this.hitsusenkbncd = hitsusenkbncd;
    }

    public Integer getJikanwariNendo() {
        return jikanwariNendo;
    }

    public void setJikanwariNendo(Integer jikanwariNendo) {
        this.jikanwariNendo = jikanwariNendo;
    }

    public String getJikanwariShozokucd() {
        return jikanwariShozokucd;
    }

    public void setJikanwariShozokucd(String jikanwariShozokucd) {
        this.jikanwariShozokucd = jikanwariShozokucd;
    }

    public String getJikanwaricd() {
        return jikanwaricd;
    }

    public void setJikanwaricd(String jikanwaricd) {
        this.jikanwaricd = jikanwaricd;
    }

    public Integer getEnterdFlg() {
        return enterdFlg;
    }

    public void setEnterdFlg(Integer enterdFlg) {
        this.enterdFlg = enterdFlg;
    }

    public Integer getCode1() {
        return code1;
    }

    public void setCode1(Integer code1) {
        this.code1 = code1;
    }

    public Integer getCode2() {
        return code2;
    }

    public void setCode2(Integer code2) {
        this.code2 = code2;
    }

    public Integer getCode3() {
        return code3;
    }

    public void setCode3(Integer code3) {
        this.code3 = code3;
    }

    public Integer getCode4() {
        return code4;
    }

    public void setCode4(Integer code4) {
        this.code4 = code4;
    }

    public Integer getCode5() {
        return code5;
    }

    public void setCode5(Integer code5) {
        this.code5 = code5;
    }

    public Integer getCode6() {
        return code6;
    }

    public void setCode6(Integer code6) {
        this.code6 = code6;
    }

    public Integer getCode7() {
        return code7;
    }

    public void setCode7(Integer code7) {
        this.code7 = code7;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getEvidence() {
        return evidence;
    }

    public void setEvidence(String evidence) {
        this.evidence = evidence;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public java.sql.Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(java.sql.Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
