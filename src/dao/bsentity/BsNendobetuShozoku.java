package dao.bsentity;

public abstract class BsNendobetuShozoku {

    /** NYUGAKU_NENDO: {IX, DECIMAL(4)} */
    protected Integer nyugakuNendo;

    /** SHOZOKUCD: {IX, VARCHAR(14)} */
    protected String shozokucd;

    /** OYA_SHOZOKUCD: {IX, VARCHAR(14)} */
    protected String oyaShozokucd;

    /** KAISOFLG: {IX, DECIMAL(1)} */
    protected Integer kaisoflg;

    /** DAIGAKUINKBNCD: {IX, VARCHAR(1)} */
    protected String daigakuinkbncd;

    /** SHOZOKUNM: {VARCHAR(120)} */
    protected String shozokunm;

    /** SHOZOKUKN: {VARCHAR(120)} */
    protected String shozokukn;

    /** SHOZOKUNMENG: {VARCHAR(60)} */
    protected String shozokunmeng;

    /** SHOZOKURKNM: {VARCHAR(10)} */
    protected String shozokurknm;

    public Integer getNyugakuNendo() {
        return nyugakuNendo;
    }

    public void setNyugakuNendo(Integer nyugakuNendo) {
        this.nyugakuNendo = nyugakuNendo;
    }

    public String getShozokucd() {
        return shozokucd;
    }

    public void setShozokucd(String shozokucd) {
        this.shozokucd = shozokucd;
    }

    public String getOyaShozokucd() {
        return oyaShozokucd;
    }

    public void setOyaShozokucd(String oyaShozokucd) {
        this.oyaShozokucd = oyaShozokucd;
    }

    public Integer getKaisoflg() {
        return kaisoflg;
    }

    public void setKaisoflg(Integer kaisoflg) {
        this.kaisoflg = kaisoflg;
    }

    public String getDaigakuinkbncd() {
        return daigakuinkbncd;
    }

    public void setDaigakuinkbncd(String daigakuinkbncd) {
        this.daigakuinkbncd = daigakuinkbncd;
    }

    public String getShozokunm() {
        return shozokunm;
    }

    public void setShozokunm(String shozokunm) {
        this.shozokunm = shozokunm;
    }

    public String getShozokukn() {
        return shozokukn;
    }

    public void setShozokukn(String shozokukn) {
        this.shozokukn = shozokukn;
    }

    public String getShozokunmeng() {
        return shozokunmeng;
    }

    public void setShozokunmeng(String shozokunmeng) {
        this.shozokunmeng = shozokunmeng;
    }

    public String getShozokurknm() {
        return shozokurknm;
    }

    public void setShozokurknm(String shozokurknm) {
        this.shozokurknm = shozokurknm;
    }

}
