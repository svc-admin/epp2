package dao.bsentity;

import java.math.BigDecimal;

public abstract class BsHyojunTanisu {

    /** NYUGAKU_NENDO: {DECIMAL(4,0)} */
    protected String nyugakuNendo;

    /** G_BUKYOKUCD: {IX, VARCHAR(14)} */
    protected String gBukyokucd;

    /** BUKYOKUNM */
    protected String gBukyokunm;

    /** HITSUYOTANISU: {DECIMAL(3,0)} */
    protected BigDecimal hitsuyotanisu;

    /** SYUGYONENGEN: {DECIMAL(3,0)} */
    protected Integer syugyonengen;

    /** INSERT_DATE: {NotNull, DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {NotNull, DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {NotNull, VARCHAR(10)} */
    protected String userid;

    public String getNyugakuNendo() {
        return nyugakuNendo;
    }

    public void setNyugakuNendo(String nyugakunendo) {
        this.nyugakuNendo = nyugakunendo;
    }

    public String getgBukyokucd() {
        return gBukyokucd;
    }

    public void setgBukyokucd(String gBukyokucd) {
        this.gBukyokucd = gBukyokucd;
    }

    public String getgBukyokunm() {
        return gBukyokunm;
    }

    public void setgBukyokunm(String gBukyokunm) {
        this.gBukyokunm = gBukyokunm;
    }

    public BigDecimal getHitsuyotanisu() {
        return hitsuyotanisu;
    }

    public void setHitsuyotanisu(BigDecimal hitsuyotanisu) {
        this.hitsuyotanisu = hitsuyotanisu;
    }

    public Integer getSyugyonengen() {
        return syugyonengen;
    }

    public void setSyugyonengen(Integer syugyonengen) {
        this.syugyonengen = syugyonengen;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
