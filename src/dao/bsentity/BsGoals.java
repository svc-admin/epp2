package dao.bsentity;

public abstract class BsGoals {

    /** NENDO: {IX, DECIMAL(4)} */
    protected Integer nendo;

    /** JIKANWARI_SHOZOKUCD: {IX, VARCHAR(14)} */
    protected String jikanwariShozokucd;

    /** JIKANWARICD: {IX, VARCHAR(8)} */
    protected String jikanwaricd;

    /** ENTERD_FLG: {DECIMAL(1)} */
    protected Integer enterdFlg;

    /** CODE1: {DECIMAL(3)} */
    protected Integer code1;

    /** CODE2: {DECIMAL(3)} */
    protected Integer code2;

    /** CODE3: {DECIMAL(3)} */
    protected Integer code3;

    /** CODE4: {DECIMAL(3)} */
    protected Integer code4;

    /** CODE5: {DECIMAL(3)} */
    protected Integer code5;

    /** CODE6: {DECIMAL(3)} */
    protected Integer code6;

    /** CODE7: {DECIMAL(3)} */
    protected Integer code7;

    /** COMMENT: {VARCHAR(500)} */
    protected String comment;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    public Integer getNendo() {
        return nendo;
    }

    public void setNendo(Integer nendo) {
        this.nendo = nendo;
    }

    public String getJikanwariShozokucd() {
        return jikanwariShozokucd;
    }

    public void setJikanwariShozokucd(String jikanwariShozokucd) {
        this.jikanwariShozokucd = jikanwariShozokucd;
    }

    public String getJikanwaricd() {
        return jikanwaricd;
    }

    public void setJikanwaricd(String jikanwaricd) {
        this.jikanwaricd = jikanwaricd;
    }

    public Integer getEnterdFlg() {
        return enterdFlg;
    }

    public void setEnterdFlg(Integer enterdFlg) {
        this.enterdFlg = enterdFlg;
    }

    public Integer getCode1() {
        return code1;
    }

    public void setCode1(Integer code1) {
        this.code1 = code1;
    }

    public Integer getCode2() {
        return code2;
    }

    public void setCode2(Integer code2) {
        this.code2 = code2;
    }

    public Integer getCode3() {
        return code3;
    }

    public void setCode3(Integer code3) {
        this.code3 = code3;
    }

    public Integer getCode4() {
        return code4;
    }

    public void setCode4(Integer code4) {
        this.code4 = code4;
    }

    public Integer getCode5() {
        return code5;
    }

    public void setCode5(Integer code5) {
        this.code5 = code5;
    }

    public Integer getCode6() {
        return code6;
    }

    public void setCode6(Integer code6) {
        this.code6 = code6;
    }

    public Integer getCode7() {
        return code7;
    }

    public void setCode7(Integer code7) {
        this.code7 = code7;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
