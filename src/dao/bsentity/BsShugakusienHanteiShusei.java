package dao.bsentity;

import java.math.BigDecimal;


public abstract class BsShugakusienHanteiShusei extends BsShugakusienHanteiRireki {

	/**
	 * テーブル：shugakusien_hantei_shuseiの項目
	 */
	protected String g_sysno;
	protected BigDecimal t_gpa;
	protected BigDecimal n_gpa;
	protected String bikou;
	// 在学採用判定の項目
	protected String z_gpa_result;
	protected String z_tani_result;
	// 廃止判定の項目
	protected String h_sotsu_result;
	protected String h_tani_result;
	protected String h_shu_result;
	protected String h_zen_result;
	// 警告判定の項目
	protected String k_tani_result;
	protected String k_gpa_result;
	protected String k_shu_result;
	// 遡及取消判定の項目
	protected String s_tani_result;
	protected String s_shu_result;

	public String getG_sysno() {
		return g_sysno;
	}
	public void setG_sysno(String g_sysno) {
		this.g_sysno = g_sysno;
	}
	public BigDecimal getT_gpa() {
		return t_gpa;
	}
	public void setT_gpa(BigDecimal t_gpa) {
		this.t_gpa = t_gpa;
	}
	public BigDecimal getN_gpa() {
		return n_gpa;
	}
	public void setN_gpa(BigDecimal n_gpa) {
		this.n_gpa = n_gpa;
	}
	public String getBikou() {
		return bikou;
	}
	public void setBikou(String bikou) {
		this.bikou = bikou;
	}
	public String getZ_gpa_result() {
		return z_gpa_result;
	}
	public void setZ_gpa_result(String z_gpa_result) {
		this.z_gpa_result = z_gpa_result;
	}
	public String getZ_tani_result() {
		return z_tani_result;
	}
	public void setZ_tani_result(String z_tani_result) {
		this.z_tani_result = z_tani_result;
	}
	public String getH_sotsu_result() {
		return h_sotsu_result;
	}
	public void setH_sotsu_result(String h_sotsu_result) {
		this.h_sotsu_result = h_sotsu_result;
	}
	public String getH_tani_result() {
		return h_tani_result;
	}
	public void setH_tani_result(String h_tani_result) {
		this.h_tani_result = h_tani_result;
	}
	public String getH_shu_result() {
		return h_shu_result;
	}
	public void setH_shu_result(String h_shu_result) {
		this.h_shu_result = h_shu_result;
	}
	public String getH_zen_result() {
		return h_zen_result;
	}
	public void setH_zen_result(String h_zen_result) {
		this.h_zen_result = h_zen_result;
	}
	public String getK_tani_result() {
		return k_tani_result;
	}
	public void setK_tani_result(String k_tani_result) {
		this.k_tani_result = k_tani_result;
	}
	public String getK_gpa_result() {
		return k_gpa_result;
	}
	public void setK_gpa_result(String k_gpa_result) {
		this.k_gpa_result = k_gpa_result;
	}
	public String getK_shu_result() {
		return k_shu_result;
	}
	public void setK_shu_result(String k_shu_result) {
		this.k_shu_result = k_shu_result;
	}
	public String getS_tani_result() {
		return s_tani_result;
	}
	public void setS_tani_result(String s_tani_result) {
		this.s_tani_result = s_tani_result;
	}
	public String getS_shu_result() {
		return s_shu_result;
	}
	public void setS_shu_result(String s_shu_result) {
		this.s_shu_result = s_shu_result;
	}

}
