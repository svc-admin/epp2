package dao.bsentity;

public abstract class BsJugyoKoma {

    /** NENDO: {NotNull, DECIMAL(4)} */
    protected Integer nendo;

    /** JIKANWARI_SHOZOKUCD: {NotNull, VARCHAR(14)} */
    protected String jikanwariShozokucd;

    /** JIKANWARICD: {NotNull, VARCHAR(8)} */
    protected String jikanwaricd;

    /** YOBI: {NotNull, DECIMAL(1)} */
    protected Integer yobi;

    /** JIGEN: {NotNull, DECIMAL(2)} */
    protected Integer jigen;

    /** TEKIYO_ST_YMD: {NotNull, DATE(10)} */
    protected java.util.Date tekiyoStYmd;

    /** GAKKIKBNCD: {VARCHAR(2)} */
    protected String gakkikbncd;

    /** TEKIYO_END_YMD: {DATE(10)} */
    protected java.util.Date tekiyoEndYmd;

    /** ST_TIME: {VARCHAR(4)} */
    protected String stTime;

    /** END_TIME: {VARCHAR(4)} */
    protected String endTime;

    /** SHISETSUCD: {VARCHAR(6)} */
    protected String shisetsucd;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    public Integer getNendo() {
        return nendo;
    }

    public void setNendo(Integer nendo) {
        this.nendo = nendo;
    }

    public String getJikanwariShozokucd() {
        return jikanwariShozokucd;
    }

    public void setJikanwariShozokucd(String jikanwariShozokucd) {
        this.jikanwariShozokucd = jikanwariShozokucd;
    }

    public String getJikanwaricd() {
        return jikanwaricd;
    }

    public void setJikanwaricd(String jikanwaricd) {
        this.jikanwaricd = jikanwaricd;
    }

    public Integer getYobi() {
        return yobi;
    }

    public void setYobi(Integer yobi) {
        this.yobi = yobi;
    }

    public Integer getJigen() {
        return jigen;
    }

    public void setJigen(Integer jigen) {
        this.jigen = jigen;
    }

    public java.util.Date getTekiyoStYmd() {
        return tekiyoStYmd;
    }

    public void setTekiyoStYmd(java.util.Date tekiyoStYmd) {
        this.tekiyoStYmd = tekiyoStYmd;
    }

    public String getGakkikbncd() {
        return gakkikbncd;
    }

    public void setGakkikbncd(String gakkikbncd) {
        this.gakkikbncd = gakkikbncd;
    }

    public java.util.Date getTekiyoEndYmd() {
        return tekiyoEndYmd;
    }

    public void setTekiyoEndYmd(java.util.Date tekiyoEndYmd) {
        this.tekiyoEndYmd = tekiyoEndYmd;
    }

    public String getStTime() {
        return stTime;
    }

    public void setStTime(String stTime) {
        this.stTime = stTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getShisetsucd() {
        return shisetsucd;
    }

    public void setShisetsucd(String shisetsucd) {
        this.shisetsucd = shisetsucd;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
