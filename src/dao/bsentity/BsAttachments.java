package dao.bsentity;


public abstract class BsAttachments {

    /** id: {PK, NotNull, int(11)} */
    protected Integer id;

    /** G_SYSNO: {PK, NotNull, VARCHAR(10)} */
    protected String gSysno;

    /** KAMOKUCD: {PK, NotNull, VARCHAR(10)} */
    protected String kamokucd;

    /** filename: {VARCHAR(255)} */
    protected String filename;

    /** filepath: {VARCHAR(255)} */
    protected String filepath;

    /** SIZE: {BIGINT(20)} */
    protected long filesize;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getgSysno() {
		return gSysno;
	}

	public void setgSysno(String gSysno) {
		this.gSysno = gSysno;
	}

	public String getKamokucd() {
		return kamokucd;
	}

	public void setKamokucd(String kamokucd) {
		this.kamokucd = kamokucd;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public long getFilesize() {
		return filesize;
	}

	public void setFilesize(long filesize) {
		this.filesize = filesize;
	}

	public java.util.Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(java.util.Date insertDate) {
		this.insertDate = insertDate;
	}

	public java.util.Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(java.util.Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}
}
