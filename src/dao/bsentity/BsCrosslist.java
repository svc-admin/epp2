package dao.bsentity;

public abstract class BsCrosslist {

    /** ID: {PK, NotNull, VARCHAR(64)} */
    protected String id;

    /** SHORT_NAME: {VARCHAR(256)} */
    protected String shortName;

    /** LONG_NAME: {VARCHAR(256)} */
    protected String longName;

    /** LEVEL: {DECIMAL(2)} */
    protected Integer level;

    /** GAKKI_ID: {VARCHAR(64)} */
    protected String gakkiId;

    /** CROSSLIST_ID: {VARCHAR(64)} */
    protected String crosslistId;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getGakkiId() {
        return gakkiId;
    }

    public void setGakkiId(String gakkiId) {
        this.gakkiId = gakkiId;
    }

    public String getCrosslistId() {
        return crosslistId;
    }

    public void setCrosslistId(String crosslistId) {
        this.crosslistId = crosslistId;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
