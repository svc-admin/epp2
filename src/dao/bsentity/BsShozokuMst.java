package dao.bsentity;

public abstract class BsShozokuMst {

    /** SHOZOKUCD: {PK, NotNull, VARCHAR(14)} */
    protected String shozokucd;

    /** SHOZOKUNM1: {VARCHAR(120)} */
    protected String shozokunm1;

    /** SHOZOKUKN1: {VARCHAR(120)} */
    protected String shozokukn1;

    /** SHOZOKUNMENG1: {VARCHAR(60)} */
    protected String shozokunmeng1;

    /** SHOZOKURKNM1: {VARCHAR(10)} */
    protected String shozokurknm1;

    /** SHOZOKUNM2: {VARCHAR(120)} */
    protected String shozokunm2;

    /** SHOZOKUKN2: {VARCHAR(120)} */
    protected String shozokukn2;

    /** SHOZOKUNMENG2: {VARCHAR(60)} */
    protected String shozokunmeng2;

    /** SHOZOKURKNM2: {VARCHAR(10)} */
    protected String shozokurknm2;

    /** SHOZOKUNM3: {VARCHAR(120)} */
    protected String shozokunm3;

    /** SHOZOKUKN3: {VARCHAR(120)} */
    protected String shozokukn3;

    /** SHOZOKUNMENG3: {VARCHAR(60)} */
    protected String shozokunmeng3;

    /** SHOZOKURKNM3: {VARCHAR(10)} */
    protected String shozokurknm3;

    /** SHOZOKUNM4: {VARCHAR(120)} */
    protected String shozokunm4;

    /** SHOZOKUKN4: {VARCHAR(120)} */
    protected String shozokukn4;

    /** SHOZOKUNMENG4: {VARCHAR(60)} */
    protected String shozokunmeng4;

    /** SHOZOKURKNM4: {VARCHAR(10)} */
    protected String shozokurknm4;

    /** SHOZOKUNM5: {VARCHAR(120)} */
    protected String shozokunm5;

    /** SHOZOKUKN5: {VARCHAR(120)} */
    protected String shozokukn5;

    /** SHOZOKUNMENG5: {VARCHAR(60)} */
    protected String shozokunmeng5;

    /** SHOZOKURKNM5: {VARCHAR(10)} */
    protected String shozokurknm5;

    /** ZAIGAKUNENGEN: {DECIMAL(2)} */
    protected Integer zaigakunengen;

    /** KAISOFLG: {DECIMAL(1)} */
    protected Integer kaisoflg;

    /** NYUDISPGRPCD: {VARCHAR(1)} */
    protected String nyudispgrpcd;

    /** KYODISPGRPCD: {VARCHAR(1)} */
    protected String kyodispgrpcd;

    /** SHORTRKNM: {VARCHAR(20)} */
    protected String shortrknm;

    /** BUKYOKUKBNCD: {VARCHAR(1)} */
    protected String bukyokukbncd;

    /** GAKUSEIFLG: {DECIMAL(1)} */
    protected Integer gakuseiflg;

    /** KYOKANFLG: {DECIMAL(1)} */
    protected Integer kyokanflg;

    /** SHOKUINFLG: {DECIMAL(1)} */
    protected Integer shokuinflg;

    /** DAIGAKUINKBNCD: {VARCHAR(1)} */
    protected String daigakuinkbncd;

// 2018.06.19 SVC)S.Furuta ADD START >> 部局計算GPAの表示対応
    // gpa_bukyoku_disp の表示On/Offフラグをjoinするために追記
    /** disp_flg: {DECIMAL(1)} */
    protected Integer disp_flg;
// 2018.06.19 SVC)S.Furuta ADD END <<

    public String getShozokucd() {
        return shozokucd;
    }

    public void setShozokucd(String shozokucd) {
        this.shozokucd = shozokucd;
    }

    public String getShozokunm1() {
        return shozokunm1;
    }

    public void setShozokunm1(String shozokunm1) {
        this.shozokunm1 = shozokunm1;
    }

    public String getShozokukn1() {
        return shozokukn1;
    }

    public void setShozokukn1(String shozokukn1) {
        this.shozokukn1 = shozokukn1;
    }

    public String getShozokunmeng1() {
        return shozokunmeng1;
    }

    public void setShozokunmeng1(String shozokunmeng1) {
        this.shozokunmeng1 = shozokunmeng1;
    }

    public String getShozokurknm1() {
        return shozokurknm1;
    }

    public void setShozokurknm1(String shozokurknm1) {
        this.shozokurknm1 = shozokurknm1;
    }

    public String getShozokunm2() {
        return shozokunm2;
    }

    public void setShozokunm2(String shozokunm2) {
        this.shozokunm2 = shozokunm2;
    }

    public String getShozokukn2() {
        return shozokukn2;
    }

    public void setShozokukn2(String shozokukn2) {
        this.shozokukn2 = shozokukn2;
    }

    public String getShozokunmeng2() {
        return shozokunmeng2;
    }

    public void setShozokunmeng2(String shozokunmeng2) {
        this.shozokunmeng2 = shozokunmeng2;
    }

    public String getShozokurknm2() {
        return shozokurknm2;
    }

    public void setShozokurknm2(String shozokurknm2) {
        this.shozokurknm2 = shozokurknm2;
    }

    public String getShozokunm3() {
        return shozokunm3;
    }

    public void setShozokunm3(String shozokunm3) {
        this.shozokunm3 = shozokunm3;
    }

    public String getShozokukn3() {
        return shozokukn3;
    }

    public void setShozokukn3(String shozokukn3) {
        this.shozokukn3 = shozokukn3;
    }

    public String getShozokunmeng3() {
        return shozokunmeng3;
    }

    public void setShozokunmeng3(String shozokunmeng3) {
        this.shozokunmeng3 = shozokunmeng3;
    }

    public String getShozokurknm3() {
        return shozokurknm3;
    }

    public void setShozokurknm3(String shozokurknm3) {
        this.shozokurknm3 = shozokurknm3;
    }

    public String getShozokunm4() {
        return shozokunm4;
    }

    public void setShozokunm4(String shozokunm4) {
        this.shozokunm4 = shozokunm4;
    }

    public String getShozokukn4() {
        return shozokukn4;
    }

    public void setShozokukn4(String shozokukn4) {
        this.shozokukn4 = shozokukn4;
    }

    public String getShozokunmeng4() {
        return shozokunmeng4;
    }

    public void setShozokunmeng4(String shozokunmeng4) {
        this.shozokunmeng4 = shozokunmeng4;
    }

    public String getShozokurknm4() {
        return shozokurknm4;
    }

    public void setShozokurknm4(String shozokurknm4) {
        this.shozokurknm4 = shozokurknm4;
    }

    public String getShozokunm5() {
        return shozokunm5;
    }

    public void setShozokunm5(String shozokunm5) {
        this.shozokunm5 = shozokunm5;
    }

    public String getShozokukn5() {
        return shozokukn5;
    }

    public void setShozokukn5(String shozokukn5) {
        this.shozokukn5 = shozokukn5;
    }

    public String getShozokunmeng5() {
        return shozokunmeng5;
    }

    public void setShozokunmeng5(String shozokunmeng5) {
        this.shozokunmeng5 = shozokunmeng5;
    }

    public String getShozokurknm5() {
        return shozokurknm5;
    }

    public void setShozokurknm5(String shozokurknm5) {
        this.shozokurknm5 = shozokurknm5;
    }

    public Integer getZaigakunengen() {
        return zaigakunengen;
    }

    public void setZaigakunengen(Integer zaigakunengen) {
        this.zaigakunengen = zaigakunengen;
    }

    public Integer getKaisoflg() {
        return kaisoflg;
    }

    public void setKaisoflg(Integer kaisoflg) {
        this.kaisoflg = kaisoflg;
    }

    public String getNyudispgrpcd() {
        return nyudispgrpcd;
    }

    public void setNyudispgrpcd(String nyudispgrpcd) {
        this.nyudispgrpcd = nyudispgrpcd;
    }

    public String getKyodispgrpcd() {
        return kyodispgrpcd;
    }

    public void setKyodispgrpcd(String kyodispgrpcd) {
        this.kyodispgrpcd = kyodispgrpcd;
    }

    public String getShortrknm() {
        return shortrknm;
    }

    public void setShortrknm(String shortrknm) {
        this.shortrknm = shortrknm;
    }

    public String getBukyokukbncd() {
        return bukyokukbncd;
    }

    public void setBukyokukbncd(String bukyokukbncd) {
        this.bukyokukbncd = bukyokukbncd;
    }

    public Integer getGakuseiflg() {
        return gakuseiflg;
    }

    public void setGakuseiflg(Integer gakuseiflg) {
        this.gakuseiflg = gakuseiflg;
    }

    public Integer getKyokanflg() {
        return kyokanflg;
    }

    public void setKyokanflg(Integer kyokanflg) {
        this.kyokanflg = kyokanflg;
    }

    public Integer getShokuinflg() {
        return shokuinflg;
    }

    public void setShokuinflg(Integer shokuinflg) {
        this.shokuinflg = shokuinflg;
    }

    public String getDaigakuinkbncd() {
        return daigakuinkbncd;
    }

    public void setDaigakuinkbncd(String daigakuinkbncd) {
        this.daigakuinkbncd = daigakuinkbncd;
    }

// 2018.06.19 SVC)S.Furuta ADD START >> 部局計算GPAの表示対応
    // gpa_bukyoku_disp の表示On/Offフラグをjoinするために追記
    public Integer getDispflg() {
        return disp_flg;
    }

    public void setDispflg(Integer disp_flg) {
        this.disp_flg = disp_flg;
    }
// 2018.06.19 SVC)S.Furuta ADD END <<

}
