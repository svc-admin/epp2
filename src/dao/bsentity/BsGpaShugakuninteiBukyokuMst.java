package dao.bsentity;

public abstract class BsGpaShugakuninteiBukyokuMst extends BsBase{

    /** G_BUKYOKUCD: {IX, VARCHAR(14)} */
    protected String gBukyokucd;

    /** SHORI_NENDO: {DECIMAL(4,0)} */
    protected Integer shori_start;

    /** SHORI_END: {DECIMAL(4,0)} */
    protected Integer shori_end;

    public String getgBukyokucd() {
        return gBukyokucd;
    }

    public void setgBukyokucd(String gBukyokucd) {
        this.gBukyokucd = gBukyokucd;
    }

	public Integer getShori_start() {
		return shori_start;
	}

	public void setShori_start(Integer shori_start) {
		this.shori_start = shori_start;
	}

	public Integer getShori_end() {
		return shori_end;
	}

	public void setShori_end(Integer shori_end) {
		this.shori_end = shori_end;
	}


}
