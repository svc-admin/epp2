package dao.bsentity;

public abstract class BsKamokuMst {

    /** KAMOKUCD: {PK, NotNull, VARCHAR(10)} */
    protected String kamokucd;

    /** KAMOKUNM: {VARCHAR(150)} */
    protected String kamokunm;

    /** KAMOKUKN: {VARCHAR(300)} */
    protected String kamokukn;

    /** KAMOKUNMENG: {VARCHAR(150)} */
    protected String kamokunmeng;

    /** KAMOKURKNM: {VARCHAR(30)} */
    protected String kamokurknm;

    /** KAMOKU_TANISU: {DECIMAL(4, 1)} */
    protected java.math.BigDecimal kamokuTanisu;

    /** KAMOKU_SHOZOKUCD: {VARCHAR(14)} */
    protected String kamokuShozokucd;

    /** NENDO: {DECIMAL(4)} */
    protected Integer nendo;

    /** TANI_NINTEI_KBNCD: {DECIMAL(1)} */
    protected Integer taniNinteiKbncd;

    public String getKamokucd() {
        return kamokucd;
    }

    public void setKamokucd(String kamokucd) {
        this.kamokucd = kamokucd;
    }

    public String getKamokunm() {
        return kamokunm;
    }

    public void setKamokunm(String kamokunm) {
        this.kamokunm = kamokunm;
    }

    public String getKamokukn() {
        return kamokukn;
    }

    public void setKamokukn(String kamokukn) {
        this.kamokukn = kamokukn;
    }

    public String getKamokunmeng() {
        return kamokunmeng;
    }

    public void setKamokunmeng(String kamokunmeng) {
        this.kamokunmeng = kamokunmeng;
    }

    public String getKamokurknm() {
        return kamokurknm;
    }

    public void setKamokurknm(String kamokurknm) {
        this.kamokurknm = kamokurknm;
    }

    public java.math.BigDecimal getKamokuTanisu() {
        return kamokuTanisu;
    }

    public void setKamokuTanisu(java.math.BigDecimal kamokuTanisu) {
        this.kamokuTanisu = kamokuTanisu;
    }

    public String getKamokuShozokucd() {
        return kamokuShozokucd;
    }

    public void setKamokuShozokucd(String kamokuShozokucd) {
        this.kamokuShozokucd = kamokuShozokucd;
    }

    public Integer getNendo() {
        return nendo;
    }

    public void setNendo(Integer nendo) {
        this.nendo = nendo;
    }

    public Integer getTaniNinteiKbncd() {
        return taniNinteiKbncd;
    }

    public void setTaniNinteiKbncd(Integer taniNinteiKbncd) {
        this.taniNinteiKbncd = taniNinteiKbncd;
    }

}
