package dao.bsentity;

import java.math.BigDecimal;

public abstract class BsCocStepMaster {

    /** COC_KBN: {PK, NotNull, VARCHAR(10)} */
    protected String cocKbn;

    /** NENDO: {PK, NotNull, DECIMAL(4,0)} */
    protected Integer nendo;

    /** G_SYSNO: {PK, NotNull, VARCHAR(10)} */
    protected String gSysNo;

    /** STEP: {PK, NotNull, VARCHAR(2)} */
    protected String step;

    /** TANISU: {DECIMAL(4,1)} */
    protected BigDecimal tanisu;

    /** DISP_FLG: {NotNull, DECIMAL(1,0)} */
    protected Integer dispFlg;

    /** INSERT_DATE: {DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {VARCHAR(10)} */
    protected String userid;

    /**
     * COC区分
     **/
    public String getCocKbn(){
        return this.cocKbn;
    }
    public void setCocKbn(String cocKbn){
        this.cocKbn = cocKbn;
    }

    /**
     * 年度
     **/
    public Integer getNendo(){
        return nendo;
    }
    public void setNendo(Integer nendo){
        this.nendo = nendo;
    }

    /**
     * 学生ID
     **/
    public String getGSysNo(){
        return this.gSysNo;
    }
    public void setGSysNo(String gSysNo){
        this.gSysNo = gSysNo;
    }

    /**
     * ステップ
     **/
    public String getStep(){
        return this.step;
    }
    public void setStep(String step){
        this.step= step;
    }

    /**
     * 単位数
     **/
    public BigDecimal getTanisu(){
        return this.tanisu;
    }
    public void setTanisu(BigDecimal tanisu){
        this.tanisu = tanisu;
    }

    /**
     * 表示フラグ
     **/
    public Integer getDispFlg(){
        return dispFlg;
    }
    public void setDispFlg(Integer dispFlg){
        this.dispFlg = dispFlg;
    }

    /**
     * 登録日
     **/
    public java.util.Date getInsertDate() {
        return insertDate;
    }
    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * 更新日
     **/
    public java.util.Date getUpdateDate() {
        return updateDate;
    }
    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * ユーザID
     **/
    public String getUserid() {
        return userid;
    }
    public void setUserid(String userid) {
        this.userid = userid;
    }

}
