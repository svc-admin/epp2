package dao.bsentity;

public abstract class BsGpa {

    /** uid: {IX, NotNull, VARCHAR(10)} */
    protected String uid;

    /** NINTEI_NENDO: {IX, NotNull, DECIMAL(4)} */
    protected Integer ninteiNendo;

    /** semester: {IX, NotNull, DECIMAL(2)} */
    protected Integer semester;

    /** goal: {IX, NotNull, DECIMAL(2)} */
    protected Integer goal;

    /** gpa: {NotNull, DECIMAL(4, 3)} */
    protected java.math.BigDecimal gpa;

    /** credit: {NotNull, DECIMAL(4, 1)} */
    protected java.math.BigDecimal credit;

    /** gpt: {NotNull, DECIMAL(4, 1)} */
    protected java.math.BigDecimal gpt;

    /** INSERT_DATE: {NotNull, DATE(10)} */
    protected java.util.Date insertDate;

    /** UPDATE_DATE: {NotNull, DATE(10)} */
    protected java.util.Date updateDate;

    /** USERID: {NotNull, VARCHAR(10)} */
    protected String userid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getNinteiNendo() {
        return ninteiNendo;
    }

    public void setNinteiNendo(Integer ninteiNendo) {
        this.ninteiNendo = ninteiNendo;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public Integer getGoal() {
        return goal;
    }

    public void setGoal(Integer goal) {
        this.goal = goal;
    }

    public java.math.BigDecimal getGpa() {
        return gpa;
    }

    public void setGpa(java.math.BigDecimal gpa) {
        this.gpa = gpa;
    }

    public java.math.BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(java.math.BigDecimal credit) {
        this.credit = credit;
    }

    public java.math.BigDecimal getGpt() {
        return gpt;
    }

    public void setGpt(java.math.BigDecimal gpt) {
        this.gpt = gpt;
    }

    public java.util.Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(java.util.Date insertDate) {
        this.insertDate = insertDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
