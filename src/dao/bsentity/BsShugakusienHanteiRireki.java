package dao.bsentity;

public abstract class BsShugakusienHanteiRireki extends BsShoriJokenMst{

	protected String g_bukyokucd;
	protected Integer gakunen;
	protected Integer num;
	protected String shozokunm;

	public String getG_bukyokucd() {
		return g_bukyokucd;
	}
	public void setG_bukyokucd(String g_bukyokucd) {
		this.g_bukyokucd = g_bukyokucd;
	}
	public Integer getGakunen() {
		return gakunen;
	}
	public void setGakunen(Integer gakunen) {
		this.gakunen = gakunen;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public String getShozokunm() {
		return shozokunm;
	}
	public void setShozokunm(String shozokunm) {
		this.shozokunm = shozokunm;
	}

}
