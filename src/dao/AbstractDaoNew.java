package dao;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;

import org.apache.log4j.Logger;

import util.PropertiesUtil;
import util.Tools;
import dto.SysPropertiesDto;

// logの表示を親クラスに変更 20160101 by nakano

public abstract class  AbstractDaoNew<T> {

    private static Logger logger = null;
    private SysPropertiesDto sysPropDto = null;

    private static final int COMMIT_SIZE = 1000;

    /**
     * 定義ファイルからパラメータを読み込んで初期化
     */
    public AbstractDaoNew() {
        try {
//		    logger = Tools.getCallerLogger();
            logger = Logger.getLogger(getClass());

            //SYS情報の取得
            PropertiesUtil propUtil = new PropertiesUtil();
            sysPropDto = propUtil.getSysProperties();

        } catch (MissingResourceException e) {
            e.printStackTrace();
        }
    }

    /**
     * DBからの読み込み
     *
     * @param : ruser : remoteUser (熊大ID), queryStr: queryするsql文, valueList: パラメータ
     * @return : ArrayList<Object> 1番目からデータ (最初の要素はラベルではない)
     */
    protected ArrayList<T> get(String ruser, String queryStr,List<Object> valueList) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<T> list = new ArrayList<T>(); // return ArrayList
        try {
            conn = getConnection();
//			conn.setAutoCommit(true);

            pstmt = conn.prepareStatement(queryStr);

            //パラメータを設定
            setPreparedStatement(pstmt,valueList);

            // queryの実行
            rs = pstmt.executeQuery();

            // logging
            logger.info("ruser=" + ruser
            + ", queryStatement=" + rs.getStatement());

            // ResultSetをArrayList<T>に変換
     list = setEntityList(rs);

        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        // System.out.println("list=" + list.toString());
        // 上に移動 nakano 2016-01-02
/*
        logger.info("ruser=" + ruser
                + ", queryStr=" + queryStr.toString()
                + ", valueList=" + valueList
                + ", list=" + list);
*/
        return list;
    }

    private void setPreparedStatement(PreparedStatement pstmt, List<Object> valueList) throws Exception {

        int i =1;

        for (Object obj : valueList){

            if (obj instanceof String) {
                pstmt.setString(i,(String)obj);
            }
            else if (obj instanceof Integer) {
                pstmt.setInt(i,((Integer)obj).intValue());
            }
            else if (obj instanceof Long) {
              pstmt.setLong(i,((Long)obj).intValue());
          }
            else if (obj instanceof BigDecimal) {
                pstmt.setBigDecimal(i,((BigDecimal)obj));
            }
            else if (obj instanceof Date) {
                pstmt.setDate(i,((Date)obj));
            }
            else if (obj instanceof java.util.Date) {
                pstmt.setDate(i,(new Date(((java.util.Date)obj).getTime())));
          }
            else if (obj instanceof Timestamp) {
                  pstmt.setTimestamp(i,((Timestamp)obj));
            }

            i++;

        }

    }

    abstract ArrayList<T> setEntityList(ResultSet rs);

// 2014.11.21 SVC 修飾子を変更 >>
//    private Connection getConnection(){
    protected Connection getConnection(){
// 2014.11.21 SVC 修飾子を変更 <<
        Connection con = null;

        try {
            Class.forName(sysPropDto.getDbDriver()).newInstance();
            con = DriverManager.getConnection(sysPropDto.getDbUri(), sysPropDto.getDbProps());
            return con;
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
    }

    protected String likeEscape(String str) {
        return str .replaceAll("%","\\\\%").replaceAll("_","\\\\_");
    }

    protected String getSql(String fileName) {

        StringBuilder sb = new StringBuilder();

        try {
            List<String> sqlList = Files.readAllLines(Paths.get(sysPropDto.getSqldir()+fileName), Charset.forName("UTF-8"));

            for (String str :sqlList){
                sb.append(str).append(" ");
            }

        } catch (IOException e) {
            logger.error(e);
        }
        return sb.toString();
    }


    /**
     * データ追加/更新/削除のデフォルトメソッド (update)
     *
     * @param : ruser : remoteUser (熊大ID), editStr : 追加/更新/削除するsql文
     * @return : LinkedHashMap<String,String> ステータス
     */
    public int update(String ruser, String editStr,List<Object> valueList) {

        Connection conn = null;
        PreparedStatement pstmt = null;

        int ret = 0;

        try {
            conn = getConnection();

            pstmt = conn.prepareStatement(editStr);

            //パラメータを設定
            setPreparedStatement(pstmt,valueList);

            // queryの実行
            ret = pstmt.executeUpdate();

        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        logger.info("ruser=" + ruser + ", editStr=" + editStr.toString() + ", ret=" + ret);
        return ret;
    }

    /**
     * 複数のデータ追加/更新/削除のデフォルトメソッド (updateAll)
     * 2014-11-10 conn.setAutoCommit(false)がありませんでした！これでは速くならない！
     * @param : ruser : remoteUser (熊大ID), editStr : 追加/更新/削除するsql文
     * @return : LinkedHashMap<String,String> ステータス
     */
    public int updateAll(String ruser, String editStr,List<List<Object>> paramList) {

        Connection conn = null;
        PreparedStatement pstmt = null;

        int ret = 0;
        int sum = 0;

        try {
            conn = getConnection();

            conn.setAutoCommit(false); // add by nakano@cc.kumamoto-u.ac.jp

            pstmt = conn.prepareStatement(editStr);

            for(List<Object> valueList: paramList){

                //パラメータを設定
                setPreparedStatement(pstmt,valueList);

                // queryの実行
                ret = pstmt.executeUpdate();

                sum += ret;

                if(sum >= COMMIT_SIZE) conn.commit();

            }

        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                conn.commit(); // add by nakano@cc.kumamoto-u.ac.jp
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        logger.info("ruser=" + ruser + ", editStr=" + editStr.toString() + ", ret=" + ret);
        return sum;
    }
}
