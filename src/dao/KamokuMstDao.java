package dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import dao.entity.KamokuMst;

public class KamokuMstDao extends AbstractDao<KamokuMst> {

    public KamokuMstDao() {
        super();
    }

    @Override
    ArrayList<KamokuMst> setEntityList(ResultSet rs) {
        KamokuMst entity = new KamokuMst();
        return entity.setRsToList(rs);
    }

}
