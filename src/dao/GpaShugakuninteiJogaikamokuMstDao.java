package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.GpaShugakuninteiJogaikamokuMst;
import dto.ParameterDto;

public class GpaShugakuninteiJogaikamokuMstDao extends AbstractDao<GpaShugakuninteiJogaikamokuMst> {

    private static Logger logger = null;

    public GpaShugakuninteiJogaikamokuMstDao() {
        super();
    }

    @Override
    ArrayList<GpaShugakuninteiJogaikamokuMst> setEntityList(ResultSet rs) {
        GpaShugakuninteiJogaikamokuMst entity = new GpaShugakuninteiJogaikamokuMst();
        return entity.setRsToList(rs);
    }

    public ArrayList<GpaShugakuninteiJogaikamokuMst> getGpaShugakuninteiJogaikamokuMst() {

    	String strSql = getSql("GpaShugakuninteiJogaikamokuMstDao_getGpaShugakuninteiJogaikamokuMst.sql");
    	List<Object> valueList = new ArrayList<Object>();
    	return get("",strSql,valueList);
    }


    public Boolean deletegGpaShugakuninteiJogaikamokuMstDao01(ParameterDto pramDto) {

        Boolean res = false;

        Connection conn = null;
        PreparedStatement pstmt = null;

        String strSql = "delete from gpa_shugakunintei_jogaikamoku_mst ";
        String strWhere = "";

        if(pramDto.getGpaShugakuninteiJogaikamokuMst().getYokenNendo() != null){
            if(strWhere == ""){
                strWhere+="where ";
            }
            strWhere = strWhere + "YOKEN_NENDO = '" + pramDto.getGpaShugakuninteiJogaikamokuMst().getYokenNendo()+ "' ";
        }
        if(pramDto.getGpaShugakuninteiJogaikamokuMst().getgBukyokucd() != null){
            if (strWhere != ""){
                strWhere = strWhere + " and ";
            }else{
                strWhere += "where ";
            }
            strWhere = strWhere + "G_BUKYOKUCD = '" + pramDto.getGpaShugakuninteiJogaikamokuMst().getgBukyokucd() + "' ";
        }
        if(pramDto.getGpaShugakuninteiJogaikamokuMst().getKamokucd() != null){
            if (strWhere != ""){
                strWhere = strWhere + " and ";
            }else{
                strWhere += "where ";
            }
            strWhere = strWhere + "KAMOKUCD = '" + pramDto.getGpaShugakuninteiJogaikamokuMst().getKamokucd() + "' ";
        }
        // queryの生成
        strSql = strSql + strWhere;

        System.out.println(strSql);

        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            int num = pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return true;
    }

    public Boolean addGpaShugakuninteiJogaikamokuMstDao01(ParameterDto pramDto) {

        Boolean res = false;

        Connection conn = null;
        PreparedStatement pstmt = null;

        String strSql = "insert into gpa_shugakunintei_jogaikamoku_mst (YOKEN_NENDO";

        String strPram = ")values(" ;

        if(pramDto.getGpaShugakuninteiJogaikamokuMst().getYokenNendo() != -1){
            strPram = strPram + pramDto.getGpaShugakuninteiJogaikamokuMst().getYokenNendo();
        }
        if(pramDto.getGpaShugakuninteiJogaikamokuMst().getgBukyokucd().equals("") == false){
            strSql = strSql + ",G_BUKYOKUCD";
            strPram = strPram + ",'" + pramDto.getGpaShugakuninteiJogaikamokuMst().getgBukyokucd() + "'";
        }
        if(pramDto.getGpaShugakuninteiJogaikamokuMst().getKamokucd().equals("") == false){
            strSql = strSql + ",KAMOKUCD";
            strPram = strPram + ",'" + pramDto.getGpaShugakuninteiJogaikamokuMst().getKamokucd() + "'";
        }

        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
        strSql = strSql + ",INSERT_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",UPDATE_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",USERID";
        strPram = strPram + ",'" + pramDto.getRuser() + "'";

        strSql = strSql + strPram + ")";

        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            int num = pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return true;
    }

}
