package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.ShoriJokenMst;
import dto.ParameterDto;

public class ShoriJokenMstDao extends AbstractDao<ShoriJokenMst> {

    private static Logger logger = null;

    public ShoriJokenMstDao() {
        super();
    }

    @Override
    ArrayList<ShoriJokenMst> setEntityList(ResultSet rs) {
    	ShoriJokenMst entity = new ShoriJokenMst();
        return entity.setRsToList(rs);
    }

    public ArrayList<ShoriJokenMst> getShoriJokenMst() {

    	String strSql = getSql("ShoriJokenMstDao_getShoriJokenMst.sql");
    	List<Object> valueList = new ArrayList<Object>();
    	return get("",strSql,valueList);
    }

    public Boolean deleteShoriJokenMst01(ParameterDto pramDto) {

        Boolean res = false;

        Connection conn = null;
        PreparedStatement pstmt = null;

        String strSql = "delete from shori_joken_mst where ";
        String strWhere = "";

        if(pramDto.getShoriJokenMst().getShorinendo() != 0){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "SHORI_NENDO = '" + pramDto.getShoriJokenMst().getShorinendo() + "' ";
        }

        if(pramDto.getShoriJokenMst().getGakkikbncd() != "" ){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "GAKKIKBNCD = '" + pramDto.getShoriJokenMst().getGakkikbncd() + "' ";
        }

        if(pramDto.getShoriJokenMst().getKijundate() != ""){
            if (strWhere != ""){
                 strWhere = strWhere + " and ";
            }
            strWhere = strWhere + "KIJUN_DATE = '" + pramDto.getShoriJokenMst().getKijundate() + "' ";
        }
        // queryの生成
        strSql = strSql + strWhere;

        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            int num = pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return true;
    }

    public Boolean addShoriJokenMst01(ParameterDto pramDto) {

        Boolean res = false;

        Connection conn = null;
        PreparedStatement pstmt = null;

        // 年度別所属テーブルにデータがない場合はエラーとする
        Dao d = new Dao();
        String checkSql = "SELECT * FROM nendobetu_shozoku WHERE NYUGAKU_NENDO = " + "'" + pramDto.getShoriJokenMst().getShorinendo() + "'";
        ArrayList<Object> chk = d.query2( pramDto.getRuser(), checkSql);
        if (chk == null || chk.size() == 0) {
            return res;
        }

        // データを追加する(重複がある場合は追加しない)
        String strSql = "insert ignore into shori_joken_mst (";
        String strPram = ")values(" ;

        strSql = strSql + "SHORI_NENDO";
        strPram = strPram + "'" + pramDto.getShoriJokenMst().getShorinendo() + "'";

        strSql = strSql + ",GAKKIKBNCD";
        strPram = strPram + ",'" + pramDto.getShoriJokenMst().getGakkikbncd() + "'";

        strSql = strSql + ",KIJUN_DATE";
        strPram = strPram + ",'" + pramDto.getShoriJokenMst().getKijundate() + "'";

        java.sql.Date today = new java.sql.Date(System.currentTimeMillis());
        strSql = strSql + ",INSERT_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",UPDATE_DATE";
        strPram = strPram + ",'" + today + "'";

        strSql = strSql + ",USERID";
        strPram = strPram + ",'" + pramDto.getRuser() + "'";

        strSql = strSql + strPram + ")";

        System.out.println(strSql);

        try{
            conn = getConnection();
            pstmt = conn.prepareStatement(strSql);

            // queryの実行
            int num = pstmt.executeUpdate();

        }catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
                if (conn != null) conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        return true;
    }
}