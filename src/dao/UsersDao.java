package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.Users;

public class UsersDao extends AbstractDao<Users> {

    public UsersDao() {
        super();
    }

    @Override
    ArrayList<Users> setEntityList(ResultSet rs) {
        Users entity = new Users();
        return entity.setRsToList(rs);
    }

    public ArrayList<Users> getUsersList01(String ruser) {
        //外だしSQLの取得
        String strSql = getSql("UsersDao_getUsersList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.熊大ID
        valueList.add(ruser);

        return get(ruser,strSql,valueList);
    }

    public ArrayList<Users> getUsersList02(String ruser,String uid) {
        //外だしSQLの取得
        String strSql = getSql("UsersDao_getUsersList02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.UID
        valueList.add(uid);

        return get(ruser,strSql,valueList);
    }

    public ArrayList<Users> getUsersList03(String ruser) {
        //外だしSQLの取得
        String strSql = getSql("UsersDao_getUsersList03.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.熊大ID
        valueList.add(ruser);

        return get(ruser,strSql,valueList);
    }
}
