package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dao.entity.ShugakusienHanteiShusei;
import dto.ParameterDto;

public class ShugakusienHanteiShuseiDao extends AbstractDao<ShugakusienHanteiShusei> {

	private static Logger logger = null;
	Connection conn = null;
	PreparedStatement pstmt = null;

	public ShugakusienHanteiShuseiDao() {
		super();
	}

	@Override
	ArrayList<ShugakusienHanteiShusei> setEntityList(ResultSet rs) {
		ShugakusienHanteiShusei entity = new ShugakusienHanteiShusei();
		return entity.setRsToList(rs);
	}

	/**
	 * 判定結果修正データを取得する
	 * @param paramDto
	 * @return
	 */
	public ArrayList<ShugakusienHanteiShusei> getModifyData(ParameterDto paramDto) {

		// SQL文の作成
		String strSql = "";
		strSql += " SELECT";
		strSql += "     A.G_SYSNO";
		strSql += "    ,A.Z_GPA_RESULT";
		strSql += "    ,A.Z_TANI_RESULT";
		strSql += "    ,A.H_SOTSU_RESULT";
		strSql += "    ,A.H_TANI_RESULT";
		strSql += "    ,A.H_SHU_RESULT";
		strSql += "    ,A.H_ZEN_RESULT";
		strSql += "    ,A.K_TANI_RESULT";
		strSql += "    ,A.K_GPA_RESULT";
		strSql += "    ,A.K_SHU_RESULT";
		strSql += "    ,A.S_TANI_RESULT";
		strSql += "    ,A.S_SHU_RESULT";
		strSql += "    ,A.T_GPA";
		strSql += "    ,A.N_GPA";
		strSql += "    ,A.BIKOU";
		strSql += " FROM";
		strSql += "     shugakusien_hantei_shusei A";
		strSql += " WHERE";
		strSql += "     A.SHORI_NENDO = ?";
		strSql += " AND A.GAKKIKBNCD = ?";
		strSql += " AND A.KIJUN_DATE = ?";
		strSql += " AND A.G_BUKYOKUCD = ?";
		strSql += " AND A.GAKUNEN = ?";

		// パラメータの設定
		List<Object> valueList = new ArrayList<Object>();
		valueList.add(paramDto.getShoriJokenMst().getShorinendo());
		valueList.add(paramDto.getShoriJokenMst().getGakkikbncd());
		valueList.add(paramDto.getShoriJokenMst().getKijundate());
		valueList.add(paramDto.getgBukyokucd());
		valueList.add(paramDto.getGakunen());

		// SQLの実行
		return get(paramDto.getRuser(), strSql, valueList);
	}

	/**
	 * 修正データの削除と登録を行う
	 * @param paramDto
	 * @param paramList
	 * @return
	 */
	public Boolean registModifyData(ParameterDto paramDto, ArrayList<ParameterDto> paramList) {
		Boolean ret = false;

		try {
			conn = getConnection();
			conn.setAutoCommit(false);

			// 修正データの削除を実施
			for (ParameterDto param : paramList) {
				delModifyData(param);
			}

			// 修正データの登録を実施
			for (ParameterDto param : paramList) {
				addModifyData(param);
			}

			conn.commit();
			ret = true;
		} catch (Exception ex) {
			logger.error(ex);
			try {
				conn.rollback();
			} catch (Exception e) {
				logger.error(e);
			}
			ret = false;
		} finally {
			try {
				if (pstmt != null) pstmt.close();
				if (conn != null) conn.close();
			} catch (Exception ex) {
				logger.error(ex);
				ret = false;
			}
		}

		return ret;
	}

	/**
	 * 判定修正データを削除する
	 * @param paramDto
	 * @return
	 */
	public Boolean delModifyData(ParameterDto paramDto) {

		Boolean ret = false;

		String strSql = "";
		strSql += " DELETE FROM shugakusien_hantei_shusei";
		strSql += " WHERE";
		strSql += "     SHORI_NENDO = ?";
		strSql += " AND GAKKIKBNCD = ?";
		strSql += " AND KIJUN_DATE = ?";
		strSql += " AND G_BUKYOKUCD = ?";
		strSql += " AND GAKUNEN = ?";
		strSql += " AND G_SYSNO = ?";

		try {
			pstmt = conn.prepareStatement(strSql);
			pstmt.setInt(1, paramDto.getShoriJokenMst().getShorinendo());
			pstmt.setString(2, paramDto.getShoriJokenMst().getGakkikbncd());
			pstmt.setString(3, paramDto.getShoriJokenMst().getKijundate());
			pstmt.setString(4, paramDto.getgBukyokucd());
			pstmt.setInt(5, paramDto.getGakunen());
			pstmt.setString(6, paramDto.getHanteiShuseiData().getG_sysno());
			pstmt.executeUpdate();

			ret = true;
		} catch (Exception ex) {
			logger.error(ex);
		}
		return ret;
	}

	/**
	 * 判定修正データを登録する
	 * @param paramDto
	 * @return
	 */
	public Boolean addModifyData(ParameterDto paramDto) {

		Boolean ret = false;

		java.sql.Date today = new java.sql.Date(System.currentTimeMillis());

		String strSql = "";
		strSql += " INSERT IGNORE INTO shugakusien_hantei_shusei (";
		strSql += "  SHORI_NENDO";
		strSql += " ,GAKKIKBNCD";
		strSql += " ,KIJUN_DATE";
		strSql += " ,G_BUKYOKUCD";
		strSql += " ,GAKUNEN";
		strSql += " ,G_SYSNO";
		strSql += " ,Z_GPA_RESULT";
		strSql += " ,Z_TANI_RESULT";
		strSql += " ,H_SOTSU_RESULT";
		strSql += " ,H_TANI_RESULT";
		strSql += " ,H_SHU_RESULT";
		strSql += " ,H_ZEN_RESULT";
		strSql += " ,K_TANI_RESULT";
		strSql += " ,K_GPA_RESULT";
		strSql += " ,K_SHU_RESULT";
		strSql += " ,S_TANI_RESULT";
		strSql += " ,S_SHU_RESULT";
		strSql += " ,T_GPA";
		strSql += " ,N_GPA";
		strSql += " ,BIKOU";
		strSql += " ,INSERT_DATE";
		strSql += " ,UPDATE_DATE";
		strSql += " ,USERID";
		strSql += " ) VALUES (";
		strSql += "  ?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " ,?";
		strSql += " )";

		List<Object> valueList = new ArrayList<Object>();
		valueList.add(paramDto.getShoriJokenMst().getShorinendo());
		valueList.add(paramDto.getShoriJokenMst().getGakkikbncd());
		valueList.add(paramDto.getShoriJokenMst().getKijundate());
		valueList.add(paramDto.getgBukyokucd());
		valueList.add(paramDto.getGakunen());
		valueList.add(paramDto.getHanteiShuseiData().getG_sysno());
		valueList.add(paramDto.getHanteiShuseiData().getZ_gpa_result());
		valueList.add(paramDto.getHanteiShuseiData().getZ_tani_result());
		valueList.add(paramDto.getHanteiShuseiData().getH_sotsu_result());
		valueList.add(paramDto.getHanteiShuseiData().getH_tani_result());
		valueList.add(paramDto.getHanteiShuseiData().getH_shu_result());
		valueList.add(paramDto.getHanteiShuseiData().getH_zen_result());
		valueList.add(paramDto.getHanteiShuseiData().getK_tani_result());
		valueList.add(paramDto.getHanteiShuseiData().getK_gpa_result());
		valueList.add(paramDto.getHanteiShuseiData().getK_shu_result());
		valueList.add(paramDto.getHanteiShuseiData().getS_tani_result());
		valueList.add(paramDto.getHanteiShuseiData().getS_shu_result());
		valueList.add(paramDto.getHanteiShuseiData().getT_gpa());
		valueList.add(paramDto.getHanteiShuseiData().getN_gpa());
		valueList.add(paramDto.getHanteiShuseiData().getBikou());
		valueList.add(today);
		valueList.add(today);
		valueList.add(paramDto.getRuser());

		try {
			pstmt = conn.prepareStatement(strSql);
			setPreparedStatement(pstmt, valueList);
			pstmt.executeUpdate();

			ret = true;
		} catch (Exception ex) {
			logger.error(ex);
		}
		return ret;
	}
}