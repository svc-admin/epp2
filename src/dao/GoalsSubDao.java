package dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import dao.entity.GoalsSub;

public class GoalsSubDao extends AbstractDao<GoalsSub> {

    public GoalsSubDao() {
        super();
    }

    @Override
    ArrayList<GoalsSub> setEntityList(ResultSet rs) {
        GoalsSub entity = new GoalsSub();
        return entity.setRsToList(rs);
    }

}
