package dao.cstmentity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsCocStepMaster;

public class CenterOfCommunity extends BsCocStepMaster {

    public ArrayList<CenterOfCommunity> setEntityList(ResultSet rs){
        ResultSetMetaData rsm;
        ArrayList<CenterOfCommunity> list = new ArrayList<CenterOfCommunity>();

        try{

            //メタデータ取得
            rsm = rs.getMetaData();

            //取得件数分繰り返す
            while (rs.next()){
                CenterOfCommunity entity = new CenterOfCommunity();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);

                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){

                    case "STEP":
                        entity.setStep(rs.getString(sColName));
                        break;
                    case "KAMOKUCD":
                        entity.setKamokucd(rs.getString(sColName));
                        break;
                    case "KAMOKUNM":
                        entity.setKamokunm(rs.getString(sColName));
                        break;
                    case "KAMOKUNMENG":
                        entity.setKamokunmeng(rs.getString(sColName));
                        break;
                    case "TANISU":
                        entity.setTanisu(rs.getBigDecimal(sColName));
                        break;
                    case "RISHUU_FLG":
                        entity.setRishuuFlg(rs.getString(sColName));
                        break;
                    case "HYOGONM":
                        entity.setHyogonm(rs.getString(sColName));
                        break;
                    case "HYOGOCD":
                        entity.setHyogocd(rs.getString(sColName));
                        break;
                    case "NINTEI_NENDO":
                        entity.setNinteiNendo(rs.getInt(sColName));
                        break;
                    case "NINTEI_GAKKIKBNCD":
                        entity.setNinteiGakkikbncd(rs.getString(sColName));
                        break;
                    case "JIKANWARI_SHOZOKUCD":
                        entity.setJikanwariShozokucd(rs.getString(sColName));
                        break;
                    case "NENDO":
                        entity.setJikanwariNendo(rs.getInt(sColName));
                        break;
                    case "JIKANWARICD":
                        entity.setJikanwariCd(rs.getString(sColName));
                        break;
                    case "GAKKIKBNCD":
                        entity.setGakkiKbnCd(rs.getString(sColName));
                        break;
                   default:
                        ;
                    }
                }
                list.add(entity);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    /** KAMOKUCD: {VARCHAR(2)} */
    protected String kamokucd;

    /** KAMOKUNM: {VARCHAR()} */
    protected String kamokunm;

    /** KAMOKUNMENG: {VARCHAR()} */
    protected String kamokunmeng;

    /** RISHUU_FLG: {VARCHAR()} */
    protected String rishuu_flg;

    /** HYOGONM: {VARCHAR()} */
    protected String hyogonm;

    /** HYOGOCD: {VARCHAR()} */
    protected String hyogocd;

    /** NINTEI_NENDO: {VARCHAR()} */
    protected Integer nintei_nendo;

    /** NINTEI_GAKKIKBNCD: {VARCHAR()} */
    protected String nintei_gakkikbncd;

    /** JIKANWARI_SHOZOKUCD: {VARCHAR()} */
    protected String jikanwari_shozokucd;

    /** JIKANWARI_NENDO: {VARCHAR()} */
    protected Integer jikanwari_nendo;

    /** JIKANWARICD: {VARCHAR()} */
    protected String jikanwaricd;

    /** GAKKIKBNCD: {VARCHAR()} */
    protected String gakkikbncd;

    // TODO COC関連履修科目のテーブル定義が決まったら修正
    /**
     * 科目CD
     * @return
     */
    public String getKamokucd() {
        return kamokucd;
    }
    public void setKamokucd(String kamokucd) {
        this.kamokucd = kamokucd;
    }

    /**
     * 科目名
     * @return
     */
    public String getKamokunm() {
        return kamokunm;
    }
    public void setKamokunm(String kamokunm) {
        this.kamokunm = kamokunm;
    }

    /**
     * 科目名（英名）
     * @return
     */
    public String getKamokunmeng() {
        return kamokunmeng;
    }
    public void setKamokunmeng(String kamokunmeng) {
        this.kamokunmeng = kamokunmeng;
    }

    /**
     * 履修フラグ
     * @return
     */
    public String getRishuuFlg() {
        return rishuu_flg;
    }
    public void setRishuuFlg(String rishuu_flg) {
        this.rishuu_flg = rishuu_flg;
    }

    /**
     * 評価名
     * @return
     */
    public String getHyogonm() {
        return hyogonm;
    }
    public void setHyogonm(String hyogonm) {
        this.hyogonm = hyogonm;
    }
    /**
     * 評価CD
     * @return
     */
    public String getHyogocd() {
        return hyogocd;
    }
    public void setHyogocd(String hyogocd) {
        this.hyogocd = hyogocd;
    }

    /**
     * 認定年度
     * @return
     */
    public Integer getNinteiNendo() {
        return nintei_nendo;
    }
    public void setNinteiNendo(Integer nintei_nendo) {
        this.nintei_nendo = nintei_nendo;
    }

    /**
     * 認定学期区分CD
     * @return
     */
    public String getNinteiGakkikbncd() {
        return nintei_gakkikbncd;
    }
    public void setNinteiGakkikbncd(String nintei_gakkikbncd) {
        this.nintei_gakkikbncd = nintei_gakkikbncd;
    }

    /**
     * 時間割所属CD
     * @return
     */
    public String getJikanwariShozokucd() {
        return jikanwari_shozokucd;
    }
    public void setJikanwariShozokucd(String jikanwari_shozokucd) {
        this.jikanwari_shozokucd = jikanwari_shozokucd;
    }

    /**
     * 時間割年度
     * @return
     */
    public Integer getJikanwariNendo() {
        return jikanwari_nendo;
    }
    public void setJikanwariNendo(Integer jikanwari_nendo) {
        this.jikanwari_nendo = jikanwari_nendo;
    }

    /**
     * 時間割CD
     * @return
     */
    public String getJikanwariCd() {
        return jikanwaricd;
    }
    public void setJikanwariCd(String jikanwaricd) {
        this.jikanwaricd = jikanwaricd;
    }

    /**
     * 学期区分CD
     * @return
     */
    public String getGakkiKbnCd() {
        return gakkikbncd;
    }
    public void setGakkiKbnCd(String gakkikbncd) {
        this.gakkikbncd = gakkikbncd;
    }

}
