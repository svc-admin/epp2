package dao.cstmentity;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.bsentity.BsKakuteiSeiseki;

public class GoalSeiseki extends BsKakuteiSeiseki {

    public ArrayList<GoalSeiseki> setRsToList(ResultSet rs){

        ResultSetMetaData rsm;
        ArrayList<GoalSeiseki> list = new ArrayList<GoalSeiseki>();

        try {
            //メタデータ取得
            rsm = rs.getMetaData();
//System.out.println("++++++++ GoalSeiseki: rsm.toString() = " + rsm.toString());
//System.out.println("++++++++ GoalSeiseki: rs.getFetchSize() = " + rs.getFetchSize());
//System.out.println("++++++++ GoalSeiseki: rs.getStatement() = " + rs.getStatement());

            //取得件数分繰り返す
            while (rs.next()){

                GoalSeiseki entity = new GoalSeiseki();

                //取得項目分繰り返す
                for (int i = 0 ;i < rsm.getColumnCount() ; i ++){
                    String sColName = rsm.getColumnName(i + 1);
                    if(sColName == null){
                        //System.out.println("Null!");
                    }else switch(sColName){
                        case "G_BUKYOKUCD":
                            entity.setgBukyokucd(rs.getString(sColName));
                            break;
                        case "G_GAKUNO":
                            entity.setgGakuno(rs.getString(sColName));
                            break;
                        case "KAMOKUCD":
                            entity.setKamokucd(rs.getString(sColName));
                            break;
                        case "SEQ_NO":
                            entity.setSeqNo((Integer)rs.getInt(sColName));
                            break;
                        case "G_SYSNO":
                            entity.setgSysno(rs.getString(sColName));
                            break;
                        case "NINTEI_NENDO":
                            entity.setNinteiNendo((Integer)rs.getInt(sColName));
                            break;
                        case "NINTEI_GAKKIKBNCD":
                            entity.setNinteiGakkikbncd(rs.getString(sColName));
                            break;
                        case "NINTEI_KAIKOKBNCD":
                            entity.setNinteiKaikokbncd(rs.getString(sColName));
                            break;
                        case "CUR_NENDO":
                            entity.setCurNendo(rs.getString(sColName));
                            break;
                        case "CUR_SHOZOKUCD":
                            entity.setCurShozokucd(rs.getString(sColName));
                            break;
                        case "KAMOKUD_SHOZOKUCD":
                            entity.setKamokudShozokucd(rs.getString(sColName));
                            break;
                        case "KAMOKUDKBNCD":
                            entity.setKamokudkbncd(rs.getString(sColName));
                            break;
                        case "KAMOKUM_SHOZOKUCD":
                            entity.setKamokumShozokucd(rs.getString(sColName));
                            break;
                        case "KAMOKUMKBNCD":
                            entity.setKamokumkbncd(rs.getString(sColName));
                            break;
                        case "KAMOKUS_SHOZOKUCD":
                            entity.setKamokusShozokucd(rs.getString(sColName));
                            break;
                        case "KAMOKUSKBNCD":
                            entity.setKamokuskbncd(rs.getString(sColName));
                            break;
                        case "KAMOKUSS_SHOZOKUCD":
                            entity.setKamokussShozokucd(rs.getString(sColName));
                            break;
                        case "KAMOKUSSKBNCD":
                            entity.setKamokusskbncd(rs.getString(sColName));
                            break;
                        case "HYOGOCD":
                            entity.setHyogocd(rs.getString(sColName));
                            break;
                        case "HYOTEN":
                            entity.setHyoten(rs.getString(sColName));
                            break;
                        case "HYOGONM":
                            entity.setHyogonm(rs.getString(sColName));
                            break;
                        case "TANISU":
                            entity.setTanisu(rs.getBigDecimal(sColName));
                            break;
                        case "GOHIKBN":
                            entity.setGohikbn((Integer)rs.getInt(sColName));
                            break;
                        case "SAITENYMD":
                            entity.setSaitenymd(rs.getDate(sColName));
                            break;
                        case "SAITENSHA_SHOZOKUCD":
                            entity.setSaitenshaShozokucd(rs.getString(sColName));
                            break;
                        case "SAITENSHACD":
                            entity.setSaitenshacd(rs.getString(sColName));
                            break;
                        case "JIKANWARI_NENDO":
                            entity.setJikanwariNendo((Integer)rs.getInt(sColName));
                            break;
                        case "JIKANWARI_SHOZOKUCD":
                            entity.setJikanwariShozokucd(rs.getString(sColName));
                            break;
                        case "JIKANWARICD":
                            entity.setJikanwaricd(rs.getString(sColName));
                            break;
                        case "INSERT_DATE":
                            entity.setInsertDate(rs.getDate(sColName));
                            break;
                        case "UPDATE_DATE":
                            entity.setUpdateDate(rs.getDate(sColName));
                            break;
                        case "USERID":
                            entity.setUserid(rs.getString(sColName));
                            break;
                        case "HITSUSENKBNCD":
                            entity.setHitsusenkbncd(rs.getString(sColName));
                            break;
                        case "KAMOKUNM":
                            entity.setKamokunm(rs.getString(sColName));
                            break;
                        case "KAMOKUNMENG":
                            entity.setKamokunmeng(rs.getString(sColName));
                            break;
                        case "CODE1":
                            entity.setCode1((Integer)rs.getInt(sColName));
                            break;
                        case "CODE2":
                            entity.setCode2((Integer)rs.getInt(sColName));
                            break;
                        case "CODE3":
                            entity.setCode3((Integer)rs.getInt(sColName));
                            break;
                        case "CODE4":
                            entity.setCode4((Integer)rs.getInt(sColName));
                            break;
                        case "CODE5":
                            entity.setCode5((Integer)rs.getInt(sColName));
                            break;
                        case "CODE6":
                            entity.setCode6((Integer)rs.getInt(sColName));
                            break;
                        case "CODE7":
                            entity.setCode7((Integer)rs.getInt(sColName));
                            break;
                        case "ENTERD_FLG":
                            entity.setCode(rs.getString(sColName));
                            break;
                       default:
                            ;
                    }
                }

                list.add(entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;

    }

    /** HITSUSENKBNCD: {VARCHAR(2)} */
    protected String hitsusenkbncd;

    /** KAMOKUNM: {VARCHAR()} */
    protected String kamokunm;

    /** KAMOKUNMENG: {VARCHAR()} */
    protected String kamokunmeng;


    /** CODE1: {DECIMAL(3)} */
    protected Integer code1;

    /** CODE2: {DECIMAL(3)} */
    protected Integer code2;

    /** CODE3: {DECIMAL(3)} */
    protected Integer code3;

    /** CODE4: {DECIMAL(3)} */
    protected Integer code4;

    /** CODE5: {DECIMAL(3)} */
    protected Integer code5;

    /** CODE6: {DECIMAL(3)} */
    protected Integer code6;

    /** CODE7: {DECIMAL(3)} */
    protected Integer code7;

    /** CODE: {VARCHAR()} */
    protected String code;


    public String getHitsusenkbncd() {
        return hitsusenkbncd;
    }

    public void setHitsusenkbncd(String hitsusenkbncd) {
        this.hitsusenkbncd = hitsusenkbncd;
    }

    public String getKamokunm() {
        return kamokunm;
    }

    public void setKamokunm(String kamokunm) {
        this.kamokunm = kamokunm;
    }

    public Integer getCode1() {
        return code1;
    }

    public void setCode1(Integer code1) {
        this.code1 = code1;
    }

    public Integer getCode2() {
        return code2;
    }

    public void setCode2(Integer code2) {
        this.code2 = code2;
    }

    public Integer getCode3() {
        return code3;
    }

    public void setCode3(Integer code3) {
        this.code3 = code3;
    }

    public Integer getCode4() {
        return code4;
    }

    public void setCode4(Integer code4) {
        this.code4 = code4;
    }

    public Integer getCode5() {
        return code5;
    }

    public void setCode5(Integer code5) {
        this.code5 = code5;
    }

    public Integer getCode6() {
        return code6;
    }

    public void setCode6(Integer code6) {
        this.code6 = code6;
    }

    public Integer getCode7() {
        return code7;
    }

    public void setCode7(Integer code7) {
        this.code7 = code7;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getKamokunmeng() {
        return kamokunmeng;
    }

    public void setKamokunmeng(String kamokunmeng) {
        this.kamokunmeng = kamokunmeng;
    }

}
