package dao.cstmentity;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import org.apache.commons.fileupload.FileItem;

public class Result {

    /** UID */
    private String uid;
    /** 日付 */
    private Timestamp date;
    /** 分類マスタ BUNRUICD */
    private String bunruiMstBUNRUICD;
    /** 分類マスタ SHOZOKUCD */
    private String bunruiMstSHOZOKUCD;
    /** 分類 */
    private String classification;
    /** 分類 (自由入力) */
    private String classificationCustom;
    /** タイトル */
    private String title;
    /** 解説 */
    private String comment;
    /** 著者名 */
    private String authorName;
    /** 著者の区分 */
    private String authorType;
    /** 発表方法の区分 */
    private String presentationFormat;
    /** 媒体名 */
    private String mediumTitle;
    /** 媒体の区分 */
    private String mediumClass;
    /** インパクトファクターの有無 */
    private int hasImpactFactor = -1;
    /** インパクトファクターの値 */
    private String impactFactor;
    /** 開催地 */
    private String location;
    /** 発行者 */
    private String publisher;
    /** ISBN */
    private String isbn;
    /** 巻 */
    private String journalVolume;
    /** 号 */
    private String journalNumber;
    /** 開始ページ */
    private int pageFrom = -1;
    /** 終了ページ */
    private int pageTo = -1;
    /** 公開年 */
    private int publishedYear = -1;
    /** 公開月 */
    private int publishedMonth = -1;
    /** 査読の有無 */
    private int hasReviewed = -1;
    /** 原書名 */
    private String originalMediumName;
    /** 原著者名 */
    private String originalAuthorName;
    /** 原書発行所 */
    private String originalPublisher;
    /** 原書該当開始ページ */
    private int originalPageFrom = -1;
    /** 原書該当終了ページ */
    private int originalPageTo = -1;
    /** 原書出版年 */
    private int originalPublishedYear = -1;
    /** 原書出版月 */
    private int originalPublishedMonth = -1;

    /**
     * コンストラクタ
     * @param params
     */
    public Result(Iterator<?> params) {

        while ( params.hasNext() ) {

            FileItem item = (FileItem)params.next();

            if ( ! item.isFormField() ) continue;

            String name;
            String value;
            try {
                name = item.getFieldName();
                value = item.getString("utf-8");
            } catch (Exception e) {
                System.out.println(e.toString());
                continue;
            }

            switch(name) {
            // UID
            case "uid":
                this.uid = value;
                break;
            // 分類
            case "selGroup":
                this.classification = value;
                break;

            // 分類 (自由入力)
            case "txtGroup":
                this.classificationCustom = value;
                break;

            // タイトル
            case "txtTitle":
                this.title = value;
                break;

            // 日付
            case "txtDate":
                try {
                    this.date = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                } catch (Exception e) {
                    System.out.println(e.toString());
                }
                break;

            // 解説 / 備考
            case "txtComment":
            case "txtNote":
                this.comment = value;
                break;

            // 著者名 / 発表者名
            case "txtAuthorName":
            case "txtPresenterName":
                this.authorName = value;
                break;

            // 著者の区分 / 発表者の区分
            case "selAuthorType":
            case "selPresenterType":
                this.authorType = value;
                break;

            // 発表方法の区分
            case "selPresentationFormat":
                this.presentationFormat = value;
                break;

            // 媒体名
            case "txtJournalTitle":
            case "txtConferenceName":
                this.mediumTitle = value;
                break;

            // 媒体の区分
            case "selJournalClass":
            case "selConferenceClass":
                this.mediumClass = value;
                break;

            // インパクトファクターの有無
            case "selHasImpactFactor":
                switch (value) {
                case "有":
                case "Yes":
                    this.hasImpactFactor = 1;
                    break;
                case "無":
                case "No":
                    this.hasImpactFactor = 0;
                    break;
                }
                break;

            // インパクトファクターの値
            case "txtImpactFactor":
                this.impactFactor = value;
                break;

            // 開催地
            case "txtLocation":
                this.location = value;
                break;

            // 巻
            case "txtJournalVolume":
                this.journalVolume = value;
                break;

            // 号
            case "txtJournalNumber":
                this.journalNumber = value;
                break;

            // 開始ページ
            case "txtPageFrom":
                try {
                    this.pageFrom = Integer.parseInt(value);
                } catch(Exception e) {
                    this.pageFrom = -1;
                }
                break;

            // 終了ページ
            case "txtPageTo":
                try {
                    this.pageTo = Integer.parseInt(value);
                } catch(Exception e) {
                    this.pageTo = -1;
                }
                break;

            // 出版年 / 開催年
            case "txtPublishedYear":
            case "txtHeldYear":
                try {
                    this.publishedYear = Integer.parseInt(value);
                } catch(Exception e) {
                    this.publishedYear = -1;
                }
                break;

            // 出版月 / 開催月
            case "txtPublishedMonth":
            case "txtHeldMonth":
                try {
                    this.publishedMonth = Integer.parseInt(value);
                } catch(Exception e) {
                    this.publishedMonth = -1;
                }
                break;

            // 査読の有無
            case "selHasReviewed":
                switch (value) {
                case "有":
                case "Yes":
                    this.hasReviewed = 1;
                    break;
                case "無":
                case "No":
                    this.hasReviewed = 0;
                }
                break;
            }
        }

        // TODO - 出版年＆出版月（あるいは開催年＆開催月）から日付を生成する

        // TODO - 成果物ファイルのデータ作成

        // TODO - 英語外部試験のデータ作成

        // TODO - 留学経験情報のデータ作成
    }

    /**
     * 閲覧表示用 HTML 帳票作成
     * @return
     */
    public String generateHtmlContent() {
        // TODO
        return "test";
    }

    /**
     * DB へ書き出し
     * @return
     */
    public Boolean save() {
        // TODO
        return true;
    }
}
