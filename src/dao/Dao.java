package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import util.Tools;

/**
 * Database access skeleton, version 2014-01-28 since 2013-01-28 Copyright 2014
 * Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class Dao {
    private static Logger logger = Tools.getCallerLogger();
    private String dbDriver = null;
    private String dbUri = null;
    private Properties dbProps = null;
    private static final int COMMIT_SIZE = 1000;

    /**
     * 定義ファイルからパラメータを読み込んで初期化
     */
    public Dao() {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("sys");
            dbDriver = bundle.getString("driver");
            dbUri = bundle.getString("uri");
            dbProps = new Properties();
            dbProps.put("user", bundle.getString("user"));
            dbProps.put("password", bundle.getString("password"));
            dbProps.put("zeroDateTimeBehavior", bundle.getString("zeroDateTimeBehavior"));
        } catch (MissingResourceException e) {
            logger.error(e);
        }
    }

    /**
     * DBからの読み込み (query)
     *
     * @param : ruser : remoteUser (熊大ID), queryStr: queryするsql文
     * @return : ArrayList<Object> 最初の要素はラベルで2番目からデータ
     */
    public ArrayList<Object> query(String ruser, String queryStr) {
        Connection conn = null;
        Statement state = null;
        ResultSet rs = null;
        ArrayList<Object> list = new ArrayList<Object>(); // return ArrayList
        try {
            Class.forName(dbDriver).newInstance();
            conn = DriverManager.getConnection(dbUri, dbProps);
//			conn.setAutoCommit(true);
            state = conn.createStatement();
            rs = state.executeQuery(queryStr); // queryの実行
            list.clear();
            // ラベル格納
            ArrayList<Object> line = new ArrayList<Object>();
            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                line.add(rs.getMetaData().getColumnName(i));
            }
            list.add(line);
            // データ取得
            while (rs.next()) {
                line = new ArrayList<Object>();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    line.add(rs.getObject(i));
                }
                list.add(line);
            }
            // for exception
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                rs.close();
                state.close();
                conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        // System.out.println("list=" + list.toString());
        logger.info("ruser=" + ruser + ", queryStr=" + queryStr.toString() + ", list=" + list);
        return list;
    }

    /**
     * DBからの読み込み (query)
     *
     * @param : ruser : remoteUser (熊大ID), queryStr: queryするsql文
     * @return : ArrayList<Object> 1番目からデータ (最初の要素はラベルではない)
     */
    public ArrayList<Object> query2(String ruser, String queryStr) {
        Connection conn = null;
        Statement state = null;
        ResultSet rs = null;
        ArrayList<Object> list = new ArrayList<Object>(); // return ArrayList
        try {
            Class.forName(dbDriver).newInstance();
            conn = DriverManager.getConnection(dbUri, dbProps);
//			conn.setAutoCommit(true);
            state = conn.createStatement();
            rs = state.executeQuery(queryStr); // queryの実行
            list.clear();
            // データ取得
            ArrayList<Object> line = new ArrayList<Object>();
            while (rs.next()) {
                line = new ArrayList<Object>();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    line.add(rs.getObject(i));
                }
                list.add(line);
            }
            // for exception
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                rs.close();
                state.close();
                conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        // System.out.println("list=" + list.toString());
        logger.info("ruser=" + ruser + ", queryStr=" + queryStr.toString());
        logger.info("list=" + list);
        return list;
    }

    /**
     * データ追加/更新/削除のデフォルトメソッド (insert)
     *
     * @param : ruser : remoteUser (熊大ID), editStr : 追加/更新/削除するsql文
     * @return : LinkedHashMap<String,String> ステータス
     */
    public LinkedHashMap<String, String> insert(String ruser, String editStr) {

        LinkedHashMap<String, String> ret = new LinkedHashMap<String, String>();
//		System.out.println("params=" + parms.toString());
        Connection conn = null;
        Statement state = null;

        try {
            Class.forName(dbDriver).newInstance();
            conn = DriverManager.getConnection(dbUri, dbProps);
//			conn.setAutoCommit(true);
            state = conn.createStatement();

            int updateCount = state.executeUpdate(editStr);
            ret.put("updated rows", "" + updateCount);
            // for exception
        } catch (Exception ex) {
            logger.error(ex);
            ret.put("exception", "" + ex.getMessage());
        } finally {
            try {
                state.close();
                conn.close();
            } catch (Exception ex) {
                logger.error(ex);
                ret.put("exception", "" + ex.getMessage());
            }
        }
        logger.info("ruser=" + ruser + ", editStr=" + editStr.toString());
        logger.info("ret=" + ret);
        return ret;
    }

    /**
     * 複数のデータ追加/更新/削除のデフォルトメソッド (insertAll)
     *
     * @param : ruser : remoteUser (熊大ID), editStr : 追加/更新/削除するsql文
     * @return : LinkedHashMap<String,String> ステータス
     */
    public int insertAll(String ruser, ArrayList<String> editStr) {

        int ret = 0;
        Connection conn = null;
        Statement state = null;

        try {
            Class.forName(dbDriver).newInstance();
            conn = DriverManager.getConnection(dbUri, dbProps);
            conn.setAutoCommit(false);
            state = conn.createStatement();

            for(int i = 0; i < editStr.size(); i++) {
                try {
                    state.executeUpdate(editStr.get(i));
                    ret++;
                    logger.info(editStr.get(i));
                } catch(Exception e) {
                    logger.error(editStr.get(i)+" has an error of "+e.getMessage());
                }
                if(i % COMMIT_SIZE == 0) conn.commit();
            }
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                conn.commit();
                state.close();
                conn.close();
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
        logger.info("ruser=" + ruser + ", number of command=" + ret);
        return ret;
    }

    /**
     * 1行のデータ追加又のデフォルトメソッド (writeOne)
     *
     * @param String ruser : remoteUser (熊大ID), String table : 対象table,
     *  Map<String, Object> insert : 更新データ(項目名と値)
     * @return : LinkedHashMap<String,String> ステータス
     */
    public LinkedHashMap<String, String> insertOne(String ruser, String table, Map<String, Object> insert) {
        LinkedHashMap<String, String> ret = new LinkedHashMap<String, String>();
        PreparedStatement pStr = null;
        String sStr = "";
        Connection conn = null;
        Statement state = null;
        String log = "";

        try {
            Class.forName(dbDriver).newInstance();
            conn = DriverManager.getConnection(dbUri, dbProps);
//			conn.setAutoCommit(true);
            state = conn.createStatement();

            Object[] keys = insert.keySet().toArray();
            sStr = "insert " + table + " (";
            for (int i = 0; i < keys.length; i++) {
                sStr += keys[i] + (i < (keys.length - 1) ? ", " : ") values (");
            }
            for (int i = 0; i < keys.length; i++) {
                sStr += "?" + (i < (keys.length - 1) ? ", " : ")");
            }
            pStr = conn.prepareStatement(sStr);
            for (int i = 0; i < keys.length; i++) {
                pStr.setObject(i + 1, insert.get(keys[i]));
            }
            log += pStr.toString();
            pStr.execute();
            ret.put("inserted rows", "" + pStr.getUpdateCount());

            // for exception
        } catch (Exception ex) {
            logger.error(ex);
            ret.put("exception", "" + ex.getMessage());
        } finally {
            try {
                state.close();
                conn.close();
            } catch (Exception ex) {
                logger.error(ex);
                ret.put("exception", "" + ex.getMessage());
            }
        }
        logger.info("ruser=" + ruser + ", pStr=" + log + ", ret=" + ret);
        return ret;
    }

    /**
     * 1行のデータ更新のデフォルトメソッド (writeOne)
     *
     * @param ruser : remoteUser (熊大ID), String table : 対象table,
     * Map<String, Object> update : 更新データ(項目名と値),
     * Map<String, Object> condition AND条件(項目名と値)
     * @return : LinkedHashMap<String,String> ステータス
     */
    public LinkedHashMap<String, String> updateOne(String ruser, String table, Map<String, Object> update,
            Map<String, Object> condition) {
        LinkedHashMap<String, String> ret = new LinkedHashMap<String, String>();
        PreparedStatement pStr = null;
        String sStr = "";
        Connection conn = null;
        Statement state = null;
        String log = "";

        try {
            Class.forName(dbDriver).newInstance();
            conn = DriverManager.getConnection(dbUri, dbProps);
//			conn.setAutoCommit(true);
            state = conn.createStatement();

            sStr = "update " + table + " set ";
            Object [] keysU = update.keySet().toArray();
            for(int i = 0; i < keysU.length; i++) {
                sStr += keysU[i].toString() + "=?" + (i < (keysU.length - 1) ? ", " : " where ");
            }
            Object [] keysC = condition.keySet().toArray();
            for(int i = 0; i < keysC.length; i++) {
                sStr += keysC[i].toString() + "=?" + (i < (keysC.length - 1) ? " and " : "");
            }
            pStr = conn.prepareStatement(sStr);
            for(int i = 0; i < keysU.length; i++) {
                pStr.setObject(i+1, update.get(keysU[i]).toString());
            }
            for(int i = 0; i < keysC.length; i++) {
                pStr.setObject(keysU.length + i+1, condition.get(keysC[i]).toString());
            }
            log += pStr.toString();
            pStr.execute();
            int updateCount = pStr.getUpdateCount();
            ret.put("updated rows", "" + updateCount);
            // for exception
        } catch (Exception ex) {
            logger.error(ex);
            ret.put("exception", "" + ex.getMessage());
        } finally {
            try {
                state.close();
                conn.close();
            } catch (Exception ex) {
                logger.error(ex);
                ret.put("exception", "" + ex.getMessage());
            }
        }
        logger.info("ruser=" + ruser + ", pStr=" + log + ", ret=" + ret);
        return ret;
    }
}
