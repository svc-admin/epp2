package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.cstmentity.CenterOfCommunity;
import dto.ParameterDto;

/**
 * @author t.iwamoto
 * 2016/02/29 t.iwamoto
 *
 */
public class CenterOfCommunityDao extends AbstractDao<CenterOfCommunity>{

    @Override
    ArrayList<CenterOfCommunity> setEntityList(ResultSet rs) {
        CenterOfCommunity entity = new CenterOfCommunity();
        return entity.setEntityList(rs);
    }

    /**
     * 対象生徒の修得済科目リストの取得
     * @param pramDto
     * @return
     */
    public ArrayList<CenterOfCommunity> getStudentDataList01(ParameterDto pramDto){
        //外だしSQLの取得
        String strSql = getSql("CenterOfCommunityDao_getCocStepInfo02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.Locale
        valueList.add(pramDto.getLang());
        //2.学生ID(kakutei_seisekiテーブル)
        valueList.add(pramDto.getUid());
       return get(pramDto.getRuser(),strSql,valueList);
    }

    /**
     * COC関連事業の履修対象科目リストの取得
     * @param pramDto
     * @return
     */
    public ArrayList<CenterOfCommunity> getCenterOfCommunityList01(ParameterDto pramDto){
        //外だしSQLの取得
        String strSql = getSql("CenterOfCommunityDao_getCocStepInfo01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.Locale
        valueList.add(pramDto.getLang());
        //2.Locale
        valueList.add(pramDto.getLang());
        return get(pramDto.getRuser(),strSql,valueList);
    }

}
