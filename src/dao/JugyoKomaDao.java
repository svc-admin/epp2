package dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import dao.entity.JugyoKoma;

public class JugyoKomaDao extends AbstractDao<JugyoKoma> {

    public JugyoKomaDao() {
        super();
    }

    @Override
    ArrayList<JugyoKoma> setEntityList(ResultSet rs) {
        JugyoKoma entity = new JugyoKoma();
        return entity.setRsToList(rs);
    }

}
