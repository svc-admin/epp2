package dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.entity.KakuteiSeiseki;
import dto.ParameterDto;

public class KakuteiSeisekiSpDao extends AbstractDao<KakuteiSeiseki> {

    public KakuteiSeisekiSpDao() {
        super();
    }

    @Override
    ArrayList<KakuteiSeiseki> setEntityList(ResultSet rs) {
        KakuteiSeiseki entity = new KakuteiSeiseki();
        return entity.setRsToList(rs);
    }

    public ArrayList<KakuteiSeiseki> getKakuteiSeisekiList01(String ruser, String uid) {
        //外だしSQLの取得
        String strSql = getSql("KakuteiSeisekiSpDao_getKakuteiSeisekiList01.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生ID
        valueList.add(uid);

        return get(ruser,strSql,valueList);
    }

    public ArrayList<KakuteiSeiseki> getKakuteiSeisekiList02(ParameterDto pramDto) {
        //外だしSQLの取得
        String strSql = getSql("KakuteiSeisekiSpDao_getKakuteiSeisekiList02.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生ID
        valueList.add(pramDto.getUid());
        //2.学生ID
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }

    public ArrayList<KakuteiSeiseki> getKakuteiSeisekiList03(ParameterDto pramDto) {
        //外だしSQLの取得
        String strSql = getSql("KakuteiSeisekiSpDao_getKakuteiSeisekiList03.sql");

        //SQLの?に設定する値をListに順に設定する。
        List<Object> valueList = new ArrayList<Object>();
        //1.学生ID
        valueList.add(pramDto.getUid());

        return get(pramDto.getRuser(),strSql,valueList);
    }
}
