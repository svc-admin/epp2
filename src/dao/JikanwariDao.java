package dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import dao.entity.Jikanwari;

public class JikanwariDao extends AbstractDao<Jikanwari> {

    public JikanwariDao() {
        super();
    }

    @Override
    ArrayList<Jikanwari> setEntityList(ResultSet rs) {
        Jikanwari entity = new Jikanwari();
        return entity.setRsToList(rs);
    }

}
