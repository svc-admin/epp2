select distinct
       shozoku_mst.SHOZOKUCD
      ,shozoku_mst.SHOZOKUNM1
      ,shozoku_mst.SHOZOKUKN1
      ,shozoku_mst.SHOZOKUNMENG1
      ,shozoku_mst.SHOZOKUNM2
      ,shozoku_mst.SHOZOKUKN2
      ,shozoku_mst.SHOZOKUNMENG2
      ,shozoku_mst.DAIGAKUINKBNCD
      ,ifnull(gpa_bukyoku_disp.disp_flg,0) as disp_flg
from shozoku_mst
inner join gakuseki on left(shozoku_mst.SHOZOKUCD,2) =left(gakuseki.G_SHOZOKUCD,2)
left join gpa_bukyoku_disp on (shozoku_mst.SHOZOKUCD = gpa_bukyoku_disp.g_bukyokucd)
where convert(substring(shozoku_mst.SHOZOKUCD,1,2),unsigned) < 80
  and convert(substring(shozoku_mst.SHOZOKUCD,1,2),unsigned) > 04
  and length(shozoku_mst.SHOZOKUCD) = 2
  and gakuseki.GENKYOKBNCD in ('1','2','3','4','8')
union
select distinct
       shozoku_mst.SHOZOKUCD
      ,shozoku_mst.SHOZOKUNM1
      ,shozoku_mst.SHOZOKUKN1
      ,shozoku_mst.SHOZOKUNMENG1
      ,shozoku_mst.SHOZOKUNM2
      ,shozoku_mst.SHOZOKUKN2
      ,shozoku_mst.SHOZOKUNMENG2
      ,shozoku_mst.DAIGAKUINKBNCD
      ,ifnull(gpa_bukyoku_disp.disp_flg,0) as disp_flg
from shozoku_mst
inner join gakuseki on left(shozoku_mst.SHOZOKUCD,4) =left(gakuseki.G_SHOZOKUCD,4)
left join gpa_bukyoku_disp on (shozoku_mst.SHOZOKUCD = gpa_bukyoku_disp.g_bukyokucd)
where convert(substring(shozoku_mst.SHOZOKUCD,1,2),unsigned) < 80
  and convert(substring(shozoku_mst.SHOZOKUCD,1,2),unsigned) > 04
  and length(shozoku_mst.SHOZOKUCD) = 4
  and shozoku_mst.DAIGAKUINKBNCD = 0
  and gakuseki.GENKYOKBNCD in ('1','2','3','4','8')
union
select distinct
       shozoku_mst.SHOZOKUCD
      ,shozoku_mst.SHOZOKUNM1
      ,shozoku_mst.SHOZOKUKN1
      ,shozoku_mst.SHOZOKUNMENG1
      ,shozoku_mst.SHOZOKUNM3 as SHOZOKUNM2
      ,shozoku_mst.SHOZOKUKN3 as SHOZOKUKN2
      ,shozoku_mst.SHOZOKUNMENG3 as SHOZOKUNMENG2
      ,shozoku_mst.DAIGAKUINKBNCD
      ,ifnull(gpa_bukyoku_disp.disp_flg,0) as disp_flg
from shozoku_mst
inner join gakuseki on left(shozoku_mst.SHOZOKUCD,6) =left(gakuseki.G_SHOZOKUCD,6)
left join gpa_bukyoku_disp on (shozoku_mst.SHOZOKUCD = gpa_bukyoku_disp.g_bukyokucd)
where convert(substring(shozoku_mst.SHOZOKUCD,1,2),unsigned) < 80
  and convert(substring(shozoku_mst.SHOZOKUCD,1,2),unsigned) > 04
  and length(shozoku_mst.SHOZOKUCD) = 6
  and shozoku_mst.DAIGAKUINKBNCD = 1
  and gakuseki.GENKYOKBNCD in ('1','2','3','4','8')
order by SHOZOKUCD