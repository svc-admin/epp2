SELECT
    JW.NENDO
   ,JW.JIKANWARI_SHOZOKUCD
   ,JW.JIKANWARICD
   ,JW.GAKKIKBNCD
   ,JW.KAMOKUCD
   ,CONCAT(JW.KAIKO_KAMOKUNM,'(',SL.JUGYO_THEME,')') AS KAMOKUNM
   ,CONCAT(JW.KAIKO_KAMOKUNMENG,'(',SL.JUGYO_THEME,')') AS KAMOKUNMENG
   ,JW.TANISU AS KAMOKU_TANISU
   ,'0'     AS TANISU
   ,' '     AS HYOGOCD
   ,'　'    AS HYOGONM
   ,SL.STEP AS STEP
FROM
    jikanwari JW
    INNER JOIN (SELECT
                    NENDO
                   ,JIKANWARI_SHOZOKUCD
                   ,JIKANWARICD
                   ,KEYWORD AS STEP
                   ,JUGYO_THEME
                FROM
                    syl_coc
                WHERE
                    KEYWORD LIKE '%COC%'
                AND LOCALE = ?
                AND KANRYO_FLG = 1
                AND NENDO  = (SELECT MAX(NENDO) FROM syl_coc WHERE KEYWORD LIKE '%COC%' AND LOCALE = ?)
                ) SL
            ON (    JW.NENDO               = SL.NENDO
                AND JW.JIKANWARI_SHOZOKUCD = SL.JIKANWARI_SHOZOKUCD
                AND JW.JIKANWARICD         = SL.JIKANWARICD
               )
ORDER BY
    JW.KAMOKUCD
