SELECT 
    b.PARENT, a.NAME, a.ABSDIR
FROM
    directory as a,
    (select 
        NAME, PARENT
    from
        directory
    where
        UID = ? and DEPTH = 2) as b
where
    a.UID = ? and a.DEPTH = 3
        and a.PARENT = b.NAME
        and a.ISDIR = 0
order by b.PARENT , a.NAME