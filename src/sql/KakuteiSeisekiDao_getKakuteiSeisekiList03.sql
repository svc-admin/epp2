select distinct
    NINTEI_NENDO, NINTEI_GAKKIKBNCD, NINTEI_KAIKOKBNCD
from
    kakutei_seiseki
where
    G_SYSNO =?
union
select distinct
    NINTEI_NENDO, NINTEI_GAKKIKBNCD, NINTEI_GAKKIKBNCD
from
    seiseki_for_evidence
where
    G_SYSNO = ?
order by NINTEI_NENDO asc , NINTEI_GAKKIKBNCD asc