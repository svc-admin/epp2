select
    a.*
from
    (
     select
         uid
        ,NENDO
        ,JIKANWARI_SHOZOKUCD
        ,JIKANWARICD
        ,dir
        ,evidence
        ,size
        ,lastUpdate
        ,INSERT_DATE
        ,UPDATE_DATE
        ,USERID
     from
         evidence
     where
         uid=?
     union
     select
         G_SYSNO          AS uid
        ,JIKANWARI_NENDO  AS NENDO
        ,JIKANWARI_SHOZOKUCD
        ,JIKANWARICD
        ,dir
        ,evidence
        ,size
        ,lastUpdate
        ,INSERT_DATE
        ,UPDATE_DATE
        ,USERID
     from
         seiseki_for_evidence
     where
         G_SYSNO=?
    ) a
order by
    a.NENDO
   ,a.JIKANWARI_SHOZOKUCD
   ,a.JIKANWARICD
   ,a.dir
   ,a.evidence
