UPDATE attachments
SET    filepath = ?
     , update_date = ?
     , userid = ?
WHERE id = ?
