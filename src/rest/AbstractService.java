package rest;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import ku.util.Restriction;

import org.apache.log4j.Logger;

import util.Tools;
import constants.CommonConst;
import constants.ParamsConst;
import dao.GakusekiDao;
import dao.entity.Gakuseki;
import dto.ParameterDto;
//+ for using variable request / request変数を利用するため

/**
 *
 */
public abstract class AbstractService {
    public HttpServletRequest request; // Commons-Logging利用のため (CASも利用)
    private static Logger logger = Tools.getCallerLogger();
    protected ParameterDto pramDto;

    protected int rid;
    protected int mode;

    protected boolean checkRestriction() {

        boolean rtn = false;

        //アクセス権限チェック
        Restriction rst = new Restriction();
//        if(rst.allow(pramDto.getRuser(), pramDto.getRuser(), rid, mode)) { // rid = 0, read only
//            rtn =true;
//        }
        if(rst.allow(pramDto, rid, mode)) { // rid = 0, read only
            rtn =true;
        }

        return rtn;
    }

    protected boolean setParams(Map<String, Object> params) {
        String method_name = "setParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;
        pramDto = new ParameterDto();

        if(checkParams(params)){

            // 学生番号
            if(params.containsKey(ParamsConst.UID)) {
                pramDto.setUid(params.get(ParamsConst.UID).toString().replaceAll("\"", ""));
            }

            // 所属コード
            if(params.containsKey(ParamsConst.SHOZOKUCD)) {
                pramDto.setShozokucd(params.get(ParamsConst.SHOZOKUCD).toString().replaceAll("\"", ""));
            }

            // 入学年度
            if(params.containsKey(ParamsConst.NYUGAKU_NENDO)) {
                pramDto.setNyugakunendo(params.get(ParamsConst.NYUGAKU_NENDO).toString().replaceAll("\"", ""));
                // 2019.11.06 SVC add start 修学支援認定対応
                pramDto.getHyojunTanisu().setNyugakuNendo(params.get(ParamsConst.NYUGAKU_NENDO).toString().replaceAll("\"", ""));
                // 2019.11.06 SVC add end 修学支援認定対応
            }

            // 言語
            if(params.containsKey(ParamsConst.LANG)) {
                pramDto.setLang(params.get(ParamsConst.LANG).toString().replaceAll("\"", ""));
            }
            else {
                pramDto.setLang(CommonConst.JAPANESE);
            }

            // num
            if(params.containsKey(ParamsConst.NUM)) {
                try {
                    Integer num = new Integer(params.get(ParamsConst.NUM).toString());
                    pramDto.setNum(num);
                } catch (Exception e) {
                    logger.error(e);
                }
            }
            else {
                pramDto.setNum(new Integer(-1));
            }

            // exp
            if(params.containsKey(ParamsConst.EXP)) {
                pramDto.setExp((boolean)params.get(ParamsConst.EXP));
            }

            // ユーザIDの取得
            pramDto.setRuser(request.getRemoteUser());

//2014.11.21 SVC パラメータの追加 >>
            // 要件年度
            if(params.containsKey(ParamsConst.YOKEN_NENDO)) {
                pramDto.setYokenNendo(params.get(ParamsConst.YOKEN_NENDO).toString().replaceAll("\"", ""));
// 2019.11.11 SVC add start 修学支援認定対応
                pramDto.getGpaShugakuninteiJogaikamokuMst().setYokenNendo(Integer.parseInt(params.get(ParamsConst.YOKEN_NENDO).toString().replaceAll("\"", "")));
// 2019.11.11 SVC add end 修学支援認定対応
            }

            // 大学院区分コード
            if(params.containsKey(ParamsConst.DAIGAKUINKBNCD)) {
                pramDto.setDaigakuinKbncd(params.get(ParamsConst.DAIGAKUINKBNCD).toString());
            }

            // システム番号
            if(params.containsKey(ParamsConst.SYSNO)) {
                pramDto.setSysNo(params.get(ParamsConst.SYSNO).toString().replaceAll("\"", ""));
            }
//2014.11.21 SVC パラメータの追加 <<
// 2017.01.19 SVC ADD START >> パラメータ追加
            // COC/COC+区分
            if(params.containsKey(ParamsConst.COCORPLUS)) {
                pramDto.setCocOrPlus(params.get(ParamsConst.COCORPLUS).toString().replaceAll("\"", ""));
            }
            // 絞り込み条件文字列
            if(params.containsKey(ParamsConst.SEARCHCON)) {
                pramDto.setSearchCon(params.get(ParamsConst.SEARCHCON).toString().replaceAll("\"", ""));
            }
// 2017.01.19 SVC ADD END   <<
// 2019.01.09 s.furuta add start 1.学生番号検索機能追加
            // 検索区分
            if(params.containsKey(ParamsConst.SEARCHTYPE)) {
                pramDto.setSearchType(params.get(ParamsConst.SEARCHTYPE).toString().replaceAll("\"", ""));
            }
            // 検索学生番号
            if(params.containsKey(ParamsConst.SEARCHID)) {
                pramDto.setSearchId(params.get(ParamsConst.SEARCHID).toString().replaceAll("\"", ""));
            }
// 2019.01.09 s.furuta add end
// 2019.01.16 s.furuta add start 2.学修成果修正
            // 科目コード
            if(params.containsKey(ParamsConst.KAMOKUCD)){
                pramDto.setKamokuCd(params.get(ParamsConst.KAMOKUCD).toString().replaceAll("\"", ""));
                // 2019.11.06 SVC add start 修学支援認定対応
                pramDto.getGpaShugakuninteiJogaikamokuMst().setKamokucd(params.get(ParamsConst.KAMOKUCD).toString().replaceAll("\"", ""));
                // 2019.11.06 SVC add end 修学支援認定対応
            }
// 2019.01.16 s.furuta add end
// 2019.05.16 s.furuta add start バグ対応（restrictionなし教員での学生抽出時エラーが起きる）
            // 学修成果取得対象学生番号
            if(params.containsKey(ParamsConst.TARGETUIDS)){
                pramDto.setTargetUids(params.get(ParamsConst.TARGETUIDS).toString().replaceAll("\"", ""));
            }
// 2019.05.16 s.furuta add start バグ対応（restrictionなし教員での学生抽出時エラーが起きる）
// 2019.11.06 SVC add start 修学支援認定対応
            // 認定年度
             if(params.containsKey(ParamsConst.NINTEINENDO)){
               Integer nintei_nendo = 0;
               if(params.get(ParamsConst.NINTEINENDO) != null) {
                   nintei_nendo = Integer.parseInt(params.get(ParamsConst.NINTEINENDO).toString());
               }
// 2019.11.11 SVC del start 修学支援認定対応
//               pramDto.getGpaShugakuninteiBukyokuMst().setNinteiNendo(nintei_nendo);
//               pramDto.getGpaShugakuninteiGpMst().setNinteiNendo(nintei_nendo);
//               pramDto.getGpaShugakuninteiJogaikamokuMst().setNinteiNendo(nintei_nendo);
// 2019.11.11 SVC del end 修学支援認定対応
            }
            // 部局コード
            if(params.containsKey(ParamsConst.GBUKYOKUCD)){
                pramDto.getGpaShugakuninteiBukyokuMst().setgBukyokucd(params.get(ParamsConst.GBUKYOKUCD).toString().replaceAll("\"", ""));
                pramDto.getGpaShugakuninteiGpMst().setgBukyokucd(params.get(ParamsConst.GBUKYOKUCD).toString().replaceAll("\"", ""));
                pramDto.getGpaShugakuninteiJogaikamokuMst().setgBukyokucd(params.get(ParamsConst.GBUKYOKUCD).toString().replaceAll("\"", ""));
                pramDto.getHyojunTanisu().setgBukyokucd(params.get(ParamsConst.GBUKYOKUCD).toString().replaceAll("\"", ""));
                pramDto.setgBukyokucd(params.get(ParamsConst.GBUKYOKUCD).toString());
            }
            // 評語
            if(params.containsKey(ParamsConst.HYOGONM)){
                pramDto.getGpaShugakuninteiGpMst().setHyogonm(params.get(ParamsConst.HYOGONM).toString().replaceAll("\"", ""));
            }
            // GP
            if(params.containsKey(ParamsConst.GP)){
            	BigDecimal gp = BigDecimal.valueOf(0.0);
                if (params.get(ParamsConst.GP) != null){
                    BigDecimal bigdecimalgp = new BigDecimal((String)params.get(ParamsConst.GP));
                    gp = bigdecimalgp;
                }
                pramDto.getGpaShugakuninteiGpMst().setGp(gp);
            }
            // GPA計算の対象可否
            if(params.containsKey(ParamsConst.GPATAISHO)){
                Integer gpa_taisyo = 0;
                if(params.get(ParamsConst.GPATAISHO) != null) {
                    gpa_taisyo = Integer.parseInt(params.get(ParamsConst.GPATAISHO).toString());
                }
                pramDto.getGpaShugakuninteiGpMst().setGpaTaisho(gpa_taisyo);
            }
            // 必要単位数
            if(params.containsKey(ParamsConst.HITSUYOTANISU)){
            	BigDecimal hitsuyo_tanisu = BigDecimal.valueOf(0.0);
                if(params.get(ParamsConst.HITSUYOTANISU) != null) {
                    hitsuyo_tanisu = new BigDecimal((String)params.get(ParamsConst.HITSUYOTANISU).toString());
                }
                pramDto.getHyojunTanisu().setHitsuyotanisu(hitsuyo_tanisu);
            }
            // 修業年限
            if(params.containsKey(ParamsConst.SYUGYONENGEN)){
                Integer syugyo_nengen = 0;
                if(params.get(ParamsConst.SYUGYONENGEN) != null) {
                    syugyo_nengen = Integer.parseInt(params.get(ParamsConst.SYUGYONENGEN).toString());
                }
                    pramDto.getHyojunTanisu().setSyugyonengen(syugyo_nengen);
            }



// 2019.11.06 SVC add end 修学支援認定対応
// 2019.11.06 SVC add start 修学支援認定対応
            // 利用開始年度
            if(params.containsKey(ParamsConst.SHORI_START)){
                Integer shori_start = 0;
                if(params.get(ParamsConst.SHORI_START) != null) {
                    shori_start = Integer.parseInt(params.get(ParamsConst.SHORI_START).toString());
                }
                    pramDto.getGpaShugakuninteiBukyokuMst().setShori_start(shori_start);
                    pramDto.getGpaShugakuninteiGpMst().setShori_start(shori_start);

            }
            //利用終了年度
            if(params.containsKey(ParamsConst.SHORI_END)){
                Integer shori_end = 0;
                if(params.get(ParamsConst.SHORI_START) != null) {
                    shori_end = Integer.parseInt(params.get(ParamsConst.SHORI_END).toString());
                }
                    pramDto.getGpaShugakuninteiBukyokuMst().setShori_end(shori_end);
                    pramDto.getGpaShugakuninteiGpMst().setShori_end(shori_end);
            }

// 2019.11.06 SVC add end 修学支援認定対応
// 2019.11.11 SVC add start 修学支援認定対応
            //処理年度
            if(params.containsKey(ParamsConst.SHORI_NENDO)){
                Integer shori_nendo = 0;
                if(params.get(ParamsConst.SHORI_NENDO) != null) {
                    shori_nendo = Integer.parseInt(params.get(ParamsConst.SHORI_NENDO).toString());
                }
//                    pramDto.getGpaShugakuninteiGpMst().setShoriNendo(shori_nendo);
                pramDto.getShoriJokenMst().setShorinendo(shori_nendo);

                // 当該年度の最初の日付と最終の日付を保持
                try {
                    if (shori_nendo > 0) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        pramDto.setNendoStart(sdf.parse(shori_nendo.toString() + "-04-01"));
                        shori_nendo = shori_nendo + 1;
                        pramDto.setNendoEnd(sdf.parse(shori_nendo.toString() + "-03-31"));
                    }
                } catch (Exception e) {
                }
            }
// 2019.11.11 SVC add end 修学支援認定対応
// 2019.11.21 SVC add start 修学支援認定対応
            if(params.containsKey(ParamsConst.GAKKIKBNCD)){
                pramDto.getShoriJokenMst().setGakkikbncd(params.get(ParamsConst.GAKKIKBNCD).toString());
            }
            if(params.containsKey(ParamsConst.KIJUN_DATE)){
                pramDto.getShoriJokenMst().setKijundate(params.get(ParamsConst.KIJUN_DATE).toString());
            }
            if(params.containsKey(ParamsConst.GAKUNEN)){
                pramDto.setGakunen(Integer.parseInt(params.get(ParamsConst.GAKUNEN).toString()));
            }
            if(params.containsKey(ParamsConst.ETCPARAM)){
                pramDto.setEtcParam(params.get(ParamsConst.ETCPARAM).toString());
            }

            if(params.containsKey(ParamsConst.KAMOKUDKBNCD)){
                pramDto.getShutokutaniTaishoMst().setKamokudkbncd(params.get(ParamsConst.KAMOKUDKBNCD).toString().replaceAll("\"", ""));
            }
            if(params.containsKey(ParamsConst.KAMOKUM_SHOZOKUCD)){
                pramDto.getShutokutaniTaishoMst().setKamokum_shozokucd(params.get(ParamsConst.KAMOKUM_SHOZOKUCD).toString().replaceAll("\"", ""));
            }
            if(params.containsKey(ParamsConst.KAMOKUMKBNCD)){
                pramDto.getShutokutaniTaishoMst().setKamokumkbncd(params.get(ParamsConst.KAMOKUMKBNCD).toString().replaceAll("\"", ""));
            }
            if(params.containsKey(ParamsConst.KAMOKUS_SHOZOKUCD)){
                pramDto.getShutokutaniTaishoMst().setKamokus_shozokucd(params.get(ParamsConst.KAMOKUS_SHOZOKUCD).toString().replaceAll("\"", ""));
            }
            if(params.containsKey(ParamsConst.KAMOKUSKBNCD)){
                pramDto.getShutokutaniTaishoMst().setKamokuskbncd(params.get(ParamsConst.KAMOKUSKBNCD).toString().replaceAll("\"", ""));
            }
            if(params.containsKey(ParamsConst.KAMOKUSS_SHOZOKUCD)){
                pramDto.getShutokutaniTaishoMst().setKamokuss_shozokucd(params.get(ParamsConst.KAMOKUSS_SHOZOKUCD).toString().replaceAll("\"", ""));
            }
            if(params.containsKey(ParamsConst.KAMOKUSSKBNCD)){
                pramDto.getShutokutaniTaishoMst().setKamokusskbncd(params.get(ParamsConst.KAMOKUSSKBNCD).toString().replaceAll("\"", ""));
            }
// 2019.11.21 SVC add end 修学支援認定対応
            rtn = true;
        }

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;

    }

    protected abstract boolean checkParams(Map<String, Object> params);


    public String getUidLog(String log){
        return log + " at uid='"+pramDto.getUid()+"'&shozokucd='"+pramDto.getShozokucd()+"', ruser="+pramDto.getRuser();
    }

    protected String getLangObj(String value1, String value2) {

        if (CommonConst.JAPANESE.equals(pramDto.getLang())){
            return value1;
        }
        else {
            return value2;
        }
    }

    protected void getUserInfo() {
        String method_name = "getUserInfo";
        logger.info(CommonConst.START_LOG + method_name);

        GakusekiDao gakusekiDao = new GakusekiDao();
        ArrayList<Gakuseki> res = gakusekiDao.getGakusekiList04(pramDto);
        if(res.size() > 0){
            pramDto.setGakuseki(res.get(0));
        }
        logger.info(CommonConst.END_LOG + method_name);
    }
}
