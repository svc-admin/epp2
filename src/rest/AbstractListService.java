package rest;
import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;

import util.Tools;
import constants.CommonConst;
//+ for using variable request / request変数を利用するため

/**
 *
 */
public abstract class AbstractListService extends AbstractService{

    private static Logger logger = Tools.getCallerLogger();

    /**
     *
     * @param params : 連想配列(項目名と値)
     * @return : Object (hash array here) / Object (この場合は連想配列)
     */
    public ArrayList<Object> find(Map<String,Object> params) {

        String method_name = "find";
        logger.info(CommonConst.START_LOG + method_name);

        ArrayList<Object> ret;

        //パラメータの取得
        if(!setParams(params)){
            logger.info(CommonConst.END_LOG + method_name);
            return new ArrayList<Object>();
        };

        //アクセス権限チェック
        if(!checkRestriction()) { // rid = 0, read only
            logger.info(CommonConst.END_LOG + method_name);
            return new ArrayList<Object>();
        }

        //データ取得
        ret = getJsonData();

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    protected abstract ArrayList<Object> getJsonData();

    protected abstract boolean checkParams(Map<String, Object> params);

}
