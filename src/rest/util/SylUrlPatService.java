package rest.util;
import java.util.LinkedHashMap;
import java.util.Map;

import rest.AbstractMapService;
import util.PropertiesUtil;
import dto.SysPropertiesDto;
//+ for using variable request / request変数を利用するため

/**
 *  シラバスURIパターンの取得 REST service using JSONIC, version 2014-02-02 (since 2014-02-02)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class SylUrlPatService extends AbstractMapService {

    public SylUrlPatService() {
        //権限ID
        rid = 0;
        mode = 0;
    }

    /**
    (rest.)util/sylUrlPat.json<br />
    <pre>
    {"pat":"http://aaa.kumamoto-u.ac.jp?y=#y&d=#d&c=#c"]
    </pre>
     * @return : Object (hash array here) / Object (この場合は連想配列)
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {

        LinkedHashMap<String,Object> ret = new LinkedHashMap<String,Object>();

        PropertiesUtil propUtil = new PropertiesUtil();
        SysPropertiesDto sysPropDto = propUtil.getSysProperties();

        ret.put("pat", sysPropDto.getSyllabusuri());

        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {

        return true;
    }

//2014.11.21 SVC 追加、削除処理追加に伴うもの >>
    @Override
    protected Boolean deleteJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }

    @Override
    protected Boolean addJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }
//2014.11.21 SVC 追加、削除処理追加に伴うもの <<
}
