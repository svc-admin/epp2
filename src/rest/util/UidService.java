package rest.util;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import ku.util.UserID;

import org.apache.log4j.Logger;

import rest.AbstractListService;
import util.Tools;
import constants.CommonConst;
import dao.GakusekiDao;
import dao.NendobetuShozokuDao;
import dao.entity.Gakuseki;
import dao.entity.NendobetuShozoku;

/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class UidService extends AbstractListService {
    private static Logger logger = Tools.getCallerLogger();

    public UidService() {
        //権限ID
        rid = 0;
        mode = 0;
    }

    /**
     * (rest.)util/uid.json?<br />
     * SSOでRemoteUserとして格納されている熊大IDに対する学生IDのリストを返す。
     * @return : JSON String
     */
    @Override
    public ArrayList<Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        ArrayList<Object> ret = new ArrayList<Object>();

        UserID userID = new UserID();

        // 2017-01-25 nakano 学生番号のみで、かつ、入学年度が新しい順に変更
//		String[] aruid = userID.getUID(pramDto.getRuser());
        String[] aruid = userID.getGUID(pramDto.getRuser());

        GakusekiDao gakusekiDao = new GakusekiDao();
        // 2015.02.06 SVC 追加、大学院区分を取得 >>
        NendobetuShozokuDao shozokuDao = new NendobetuShozokuDao();
        // 2015.02.06 SVC 追加、大学院区分を取得 <<

        if (aruid != null) {
            for (String uid : aruid) {
                LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();

                ArrayList<Gakuseki> res = gakusekiDao.getGakusekiList06(pramDto, uid);
                if (res.size() > 0) {
                    Gakuseki gakuseki = res.get(0);
                    map.put("uid", uid);
                    map.put("shozokucd", gakuseki.getgBukyokucd());
                    // 2015.02.06 SVC 追加、大学院区分を取得 >>
                    ArrayList<NendobetuShozoku> res2 = shozokuDao.getDaigakuinKbn01(uid, gakuseki.getYokenNendo(), gakuseki.getgShozokucd());
                    // TODO 要件年度 + 所属コードで年度別所属データが取れない場合、強制的に大学院区分コードは「0：学部」にする
                    if (res2.size() > 0){
                        map.put("daigakuinkbncd", res2.get(0).getDaigakuinkbncd());
                    }
                    else{
                        map.put("daigakuinkbncd", 0);
                    }
                    // 2015.02.06 SVC 追加、大学院区分を取得 <<
                    ret.add(map);

                }
            }
        }
        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }
}
