package rest.pf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.ShutokutaniTaishoMstDao;
import dao.entity.ShutokutaniTaishoMst;
/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class ShutokutaniTaishoMstService extends AbstractMapService {
	private static Logger logger = Tools.getCallerLogger();

	public ShutokutaniTaishoMstService() {
		//権限ID
		rid = 2;
		mode = 0;
	}

	/**
	 * (rest.)pf/programs.json?lang="ja"<br />
	 * @return : JSON String
	 */
	@Override
	protected LinkedHashMap<String, Object> getJsonData() {
		String method_name = "getJsonData";
		logger.info(CommonConst.START_LOG + method_name);
		LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

		ArrayList<Object> aaData = new ArrayList<Object>();
		ShutokutaniTaishoMstDao shutokutaniTaishoMstDao = new ShutokutaniTaishoMstDao();
		ArrayList<ShutokutaniTaishoMst> aut = shutokutaniTaishoMstDao.getShutokutaniTaishoMst();

		for(ShutokutaniTaishoMst shutokutaniTaishoMst : aut){
			ArrayList<Object> o = new ArrayList<Object>();
			o.add(null);
			o.add(shutokutaniTaishoMst.getKamokudkbncd());
			o.add(shutokutaniTaishoMst.getKamokum_shozokucd());
			o.add(shutokutaniTaishoMst.getKamokumkbncd());
			o.add(shutokutaniTaishoMst.getKamokus_shozokucd());
			o.add(shutokutaniTaishoMst.getKamokuskbncd());
			o.add(shutokutaniTaishoMst.getKamokuss_shozokucd());
			o.add(shutokutaniTaishoMst.getKamokusskbncd());
			aaData.add(o);
		}
		ret.put("aaData", aaData);
		ret.put("aoColumns", aoColumns.get(pramDto.getLang()));


		return ret;
	}

	final Map<String, ArrayList<Map<String,String>>> aoColumns = // DBの構造に入っていないのでしかたなく
		new HashMap<String, ArrayList<Map<String,String>>>() {{
			put(CommonConst.JAPANESE, new ArrayList<Map<String,String>>() {{
				add(new HashMap<String,String>() {{
					put("sTitle", "削除");
				}});
				add(new HashMap<String,String>() {{
					put("sTitle", "大区分コード");
				}});
				add(new HashMap<String,String>() {{
					put("sTitle", "中区分所属コード");
				}});
				add(new HashMap<String,String>() {{
					put("sTitle", "中区分コード");
				}});
				add(new HashMap<String,String>() {{
					put("sTitle", "小区分所属コード");
				}});
				add(new HashMap<String,String>() {{
					put("sTitle", "小区分コード");
				}});
				add(new HashMap<String,String>() {{
					put("sTitle", "詳細区分所属コード");
				}});
				add(new HashMap<String,String>() {{
					put("sTitle", "詳細区分コード");
				}});
			}});
			put(CommonConst.ENGLISH, new ArrayList<Map<String,String>>() {{
				add(new HashMap<String,String>() {{
					put("sTitle", "delete");
				}});
				add(new HashMap<String,String>() {{
					put("sTitle", "DKBNCD");
				}});
				add(new HashMap<String,String>() {{
					put("sTitle", "M_SHOZOKUCD");
				}});
				add(new HashMap<String,String>() {{
					put("sTitle", "MKBNCD");
				}});
				add(new HashMap<String,String>() {{
					put("sTitle", "S_SHOZOKUCD");
				}});
				add(new HashMap<String,String>() {{
					put("sTitle", "SKBNCD");
				}});
				add(new HashMap<String,String>() {{
					put("sTitle", "SS_SHOZOKUCD");
				}});
				add(new HashMap<String,String>() {{
					put("sTitle", "SSKBNCD");
				}});
			}});
		}};

	@Override
	protected boolean checkParams(Map<String, Object> params) {
		String method_name = "checkParams";
		logger.info(CommonConst.START_LOG + method_name);

		boolean rtn = false;

		rtn = true;

		logger.info(CommonConst.END_LOG + method_name);
		return rtn;
	}


	@Override
	protected Boolean deleteJsonData() {
		String method_name = "deleteJsonData";
		logger.info(CommonConst.END_LOG + method_name);

		ShutokutaniTaishoMstDao shutokutaniTaishoMstDao = new ShutokutaniTaishoMstDao();
		Boolean res = shutokutaniTaishoMstDao.deleteShutokutaniTaishoMstDao01(pramDto);

		return res;
	}

	@Override
	protected Boolean addJsonData() {
		String method_name = "addJsonData";
		logger.info(CommonConst.END_LOG + method_name);

		ShutokutaniTaishoMstDao shutokutaniTaishoMstDao = new ShutokutaniTaishoMstDao();
		Boolean res = shutokutaniTaishoMstDao.addShutokoutaniTaishoMstDao01(pramDto);

		return res;
	}
}
