package rest.pf;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import constants.ParamsConst;
//2018.06.06 SVC)S.Furuta ADD END <<
import dao.GakusekiDao;
import dao.GpaBukyokuDao;
//2018.06.06 SVC)S.Furuta ADD START >> 部局計算GPAの表示対応
import dao.GpaBukyokuDispDao;
import dao.GpaDao;
import dao.KyokihonDao;
import dao.NendobetuShozokuDao;
import dao.ShozokuMstDao;
//2018.06.06 SVC)S.Furuta ADD END <<
//+ for using variable request / request変数を利用するため
import dao.entity.Gakuseki;
import dao.entity.Gpa;
import dao.entity.GpaBukyoku;
//2018.06.06 SVC)S.Furuta ADD START >> 部局計算GPAの表示対応
import dao.entity.GpaBukyokuDisp;
import dao.entity.Kyokihon;
import dao.entity.NendobetuShozoku;
import dao.entity.ShozokuMst;

/**
 *  学生情報取得 REST service using JSONIC, version 2014-02-02 (since 2014-02-02)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class UserInfoService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public UserInfoService() {
        //権限ID
        rid = 0;
        mode = 0;
    }

    /**
    (rest.)pf/Student.json?lang="ja"&uid=XX&shozokucd="0513"<br />
      学生番号、所属コードを指定して学生情報を返す。(※)<br />
    <pre>
    {"uid":"071-A6201",
     "name":"久保田　花子 (Kubota Hanako)",
     "affil":"文学部 コミュニケーション情報学科 コミュニケーション情報学コース",
     "email":"aaa@aaa.jp",
     "zaiseki":"2007-2010",
     "dataupdatedate":"2013-02-02"
    ]
    </pre>
     * @return : Object (hash array here) / Object (この場合は連想配列)
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        LinkedHashMap<String,Object> ret = new LinkedHashMap<String,Object>();

        GakusekiDao gakusekiDao = new GakusekiDao();
        ArrayList<Gakuseki> res = gakusekiDao.getGakusekiList04(pramDto);
        if (res.size()==0) return ret;
        Gakuseki gakuseki = res.get(0);

        KyokihonDao kyokihonDao = new KyokihonDao();
        ArrayList<Kyokihon> kyokihonList = kyokihonDao.getKyokihonList01(pramDto,gakuseki.getgSysno());
        if(kyokihonList.size() == 0) return ret;
        Kyokihon kyokihon = kyokihonList.get(0);

        ShozokuMstDao shozokuMstDao = new ShozokuMstDao();
        ArrayList<ShozokuMst> shozokuMstList = shozokuMstDao.getShozokuMstList01(pramDto,gakuseki.getgShozokucd());
        if(shozokuMstList.size() == 0) return ret;
        ShozokuMst shozokuMst = shozokuMstList.get(0);

        GpaDao gpaDao = new GpaDao();
        ArrayList<Gpa> gpaList = gpaDao.getGpaList07(pramDto);
        String updatedate = "no entried GPA data";
        if(gpaList.size() != 0) {
//        	logger.info("gpaList.size() = "+gpaList.size());
//        	logger.info("gpaList.get(0).getUpdateDate() = "+gpaList.get(0).getUpdateDate());
//        	2017-01-15 サイズが0にならないためnullで判断 by nakano@cc.kumamoto-u.ac.jp
            if(gpaList.get(0).getUpdateDate() != null) updatedate=gpaList.get(0).getUpdateDate().toString();
        }

        String name = getLangObj(kyokihon.getgName()+" ("+kyokihon.getgNameeng()+")",kyokihon.getgNameeng()+" ("+kyokihon.getgName()+")");
        String affil = getLangObj(shozokuMst.getShozokunm1() +" "+ shozokuMst.getShozokunm2() +" "+ shozokuMst.getShozokunm3(),
                            shozokuMst.getShozokunmeng1() +" "+ shozokuMst.getShozokunmeng2() +" "+ shozokuMst.getShozokunmeng3());
        String grdyear="";
        if (gakuseki.getSotsugyoymd() != null ){
            Date date = gakuseki.getSotsugyoymd();

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int year = calendar.get(Calendar.YEAR);
            grdyear = String.valueOf(year);
        }

        // 2015.02.06 SVC 追加、大学院区分を取得 >>
        ArrayList<NendobetuShozoku> res2 = null;
        if (pramDto.getShozokucd() != null){
            NendobetuShozokuDao shozokuDao = new NendobetuShozokuDao();
            res2 = shozokuDao.getDaigakuinKbn01(gakuseki.getgSysno(), gakuseki.getYokenNendo(), pramDto.getShozokucd());
            NendobetuShozoku shozoku = res2.get(0);
            ret.put("daigakuinkbncd", shozoku.getDaigakuinkbncd());
        }
        // 2015.02.06 SVC 追加、大学院区分を取得 <<

// 2018.06.04 SVC)S.Furuta ADD START >> 累積GPA・GPT表示対応
        ArrayList<Gpa> gpaList2 = gpaDao.getGpaList09(pramDto);
        String gpa = "no entried GPA data";
        String gpt = "no entried GPT data";
        if(gpaList2.size() != 0) {
            if(gpaList2.get(0).getGpa() != null) gpa=gpaList2.get(0).getGpa().toString();
            if(gpaList2.get(0).getGpt() != null) gpt=gpaList2.get(0).getGpt().toString();
        }
// 2018.06.04 SVC)S.Furuta ADD END <<

// 2018.06.06 SVC)S.Furuta ADD START >> 部局計算GPAの表示対応
        String gpabukyoku = "none";
        boolean bukyokuflg = false;
        GpaBukyokuDispDao gpabukyokudispDao = new GpaBukyokuDispDao();
        ArrayList<GpaBukyokuDisp> gpabukyokudispList = gpabukyokudispDao.getGpaBukyokuDispList01(pramDto, gakuseki.getgBukyokucd());
        if(gpabukyokudispList.size() != 0 && gpabukyokudispList.get(0).getBukyokucd() != null) bukyokuflg = true;
        // bukyoku_disp を元に表示On/Off判定
        if(bukyokuflg) {
            // 表示On
            GpaBukyokuDao gpabukyokuDao = new GpaBukyokuDao();
            ArrayList<GpaBukyoku> gpabukyokuList = gpabukyokuDao.getGpaBukyokuList01(pramDto, gakuseki.getgSysno());
            if(gpabukyokuList.size() != 0) {
                if(gpabukyokuList.get(0).getGpa() != null) gpabukyoku=gpabukyokuList.get(0).getGpa().toString();
            }
        } else {
            // 表示Off
            gpabukyoku = "none";
        }
// 2018.06.06 SVC)S.Furuta ADD END <<

        ret.put("lang", pramDto.getLang());
        ret.put("uid", gakuseki.getgSysno());
        ret.put("shozokucd", gakuseki.getgBukyokucd());
        ret.put("name", name);
        ret.put("dep", affil);
        ret.put("email", kyokihon.getgEmail());
        ret.put("entyear", gakuseki.getNyugakuNendo().toString());
        ret.put("grdyear", grdyear);
        ret.put("update", updatedate);
// 2018.06.04 SVC)S.Furuta ADD START >> 累積GPA・GPT表示対応
        ret.put("gpa", gpa);
        ret.put("gpt", gpt);
// 2018.06.04 SVC)S.Furuta ADD END <<
// 2018.06.06 SVC)S.Furuta ADD START >> 部局計算GPAの表示対応
        ret.put("gpabukyoku", gpabukyoku);
// 2018.06.06 SVC)S.Furuta ADD END <<

        logger.info("ruser="+pramDto.getRuser()+", denied : lang="+pramDto.getLang());

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        //学生番号、所属コードが設定されていない場合は処理しない。
        if(params.containsKey(ParamsConst.UID)) {
            rtn = true;
        }
        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

//2014.11.21 SVC 追加、削除処理追加に伴うもの >>
    @Override
    protected Boolean deleteJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }

    @Override
    protected Boolean addJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }
//2014.11.21 SVC 追加、削除処理追加に伴うもの <<
}
