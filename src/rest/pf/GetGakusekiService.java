package rest.pf;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.CocAdministerDao;
import dao.entity.CocAdminister;

public class GetGakusekiService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();
    public GetGakusekiService() {
        //権限ID
        rid = 2;
        mode = 0;
    }

    /**
     * (rest.)pf/programs.json?lang="ja"<br />
     *
     * @return : JSON String
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

        CocAdministerDao cocAdministerDao = new CocAdministerDao();

        ArrayList<CocAdminister> res = cocAdministerDao.verificationCoc01(pramDto);
        ArrayList<Object> aaData = new ArrayList<Object>();

        if(res != null){
            for(CocAdminister cocAdminister :res){
                ArrayList<Object> o = new ArrayList<Object>();
                o.add(cocAdminister.getgSysno());
                aaData.add(o);
            }
        }
        else{
            ArrayList<Object> o = new ArrayList<Object>();
            o.add(null);
            aaData.add(o);
        }

        ret.put("aaData" ,aaData);
        logger.info("authoritylist for dataTables.js allowed where nyugaku_nendo="+pramDto.getNyugakunendo()
                +", shozokucd="+pramDto.getShozokucd()+", lang="+pramDto.getLang()+", ruser="+pramDto.getRuser());

        logger.info("ruser="+pramDto.getRuser()+", denied : lang="+pramDto.getLang());

        logger.info(CommonConst.END_LOG + method_name);

        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    @Override
    protected Boolean deleteJsonData() {
        return true;
    }

    @Override
    protected Boolean addJsonData() {
        return true;
    }
}
