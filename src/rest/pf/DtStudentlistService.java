package rest.pf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import ku.util.UserID;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import constants.ParamsConst;
import dao.AuthorityDao;
//2018.06.05 SVC)S.Furuta ADD END <<
import dao.GakusekiDao;
//2018.06.05 SVC)S.Furuta ADD START >> 部局計算GPAの表示対応
import dao.GpaBukyokuDao;
import dao.GpaDao;
import dao.KussShinroDao;
import dao.KyokihonDao;
import dao.NendobetuShozokuDao;
//2018.06.04 SVC)S.Furuta ADD END <<
//2018.08.07 SVC)Y.Yoshijima ADD START >> アクセス制限設定機構の改善対応
import dao.RestrictionGroupDao;
//2018.08.07 SVC)Y.Yoshijima ADD END <<
import dao.ShozokuMstDao;
import dao.ToeicDao;
import dao.entity.Authority;
//2018.06.05 SVC)S.Furuta ADD END <<
//2014.11.21 SVC 追加 <<
import dao.entity.Gakuseki;
import dao.entity.Gpa;
//2018.06.05 SVC)S.Furuta ADD START >> 部局計算GPAの表示対応
import dao.entity.GpaBukyoku;
import dao.entity.KussShinro;
import dao.entity.Kyokihon;
import dao.entity.NendobetuShozoku;
import dao.entity.RestrictionGroup;
//2018.06.04 SVC)S.Furuta ADD END <<
//+ for using variable request / request変数を利用するため
import dao.entity.ShozokuMst;
import dao.entity.Toeic;
//2014.11.21 SVC 追加 >>
//2014.11.21 SVC 追加 <<
//2014.11.21 SVC 追加 >>
//2018.06.04 SVC)S.Furuta ADD START >> アクセス制限設定機構の改善対応
//2018.06.04 SVC)S.Furuta ADD START >> アクセス制限設定機構の改善対応

/**
 *  学生リスト取得 (dataTables.js用) REST service using JSONIC, version 2014-02-02 (since 2014-02-02)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class DtStudentlistService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public DtStudentlistService() {
        //権限ID
        rid = 0;
        mode = 0;
    }

    /**
    (rest.)pf/dtStudentlist.json?lang="ja"&nyugaku_nendo="2007"&shozokucd="0513"<br />
      入学年度、所属を指定して学生のdataTables.js用のリストを返す。(※)<br />
    <pre>
    {
    "aaData": [
        [ "071-A6201", "久保田　花子 (Kubota Hanako)", "文学部 コミュニケーション情報学科 コミュニケーション情報学コース", 2.75,132,"2011-03-25","熊もん銀行"],
        [ "074-A6202", "齋藤　よし子 (Saitoh Yoshiko)", "文学部 コミュニケーション情報学科 コミュニケーション情報学コース", 2.54,136,"2012-03-25","野村商会"],
        .....
    ],
    "aoColumns": [
        { "sTitle": "学生番号" },
        { "sTitle": "氏名" },
        { "sTitle": "所属" },
        { "sTitle": "GPA" },
        { "sTitle": "取得単位" },
        { "sTitle": "卒業年月日" }
        { "sTitle": "進路" }
        ]
    }
    </pre>
     * @return : Object (hash array here) / Object (この場合は連想配列)
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        ArrayList<Long> dbMillis = new ArrayList<Long>();
        dbMillis.add(System.currentTimeMillis());

        LinkedHashMap<String,Object> ret = new LinkedHashMap<String,Object>();

        GakusekiDao gakusekiDao = new GakusekiDao();
// 2018.06.14 SVC)S.Furuta CHG START >>
//// 2018.06.04 SVC)S.Furuta CHG START >> アクセス制限設定機構の改善対応
//////2014.11.21 SVC 学生の絞り込み機能を追加 >>
//////        ArrayList<Gakuseki> res = gakusekiDao.getGakusekiList02(pramDto);
////        ArrayList<Gakuseki> res01 = gakusekiDao.getGakusekiList02(pramDto);
//        // ユーザが管理者であるか判定
//        RestrictionGroupDao restrictiongroupDao = new RestrictionGroupDao();
//        ArrayList<RestrictionGroup> res04 = restrictiongroupDao.getRestrictionGroupList02(pramDto.getRuser());
//
//        ArrayList<Gakuseki> res01 = gakusekiDao.getGakusekiList02(pramDto);
//        // 管理者ユーザの時は現況区分コードに関わらず全員取得
//        if(res04.size() != 0 && res04.get(0).getKid() != null){
//            res01 = gakusekiDao.getGakusekiList08(pramDto);
//        }
//// 2018.06.04 SVC)S.Furuta CHG END <<
//
//        ArrayList<Gakuseki> res02 = new ArrayList<Gakuseki>();
//
//        UserID userID = new UserID();
//        String[] aruid = userID.getUID(pramDto.getRuser());
//
//        AuthorityDao authorityDao = new AuthorityDao();

        ArrayList<Gakuseki> res01 = new ArrayList<Gakuseki>();
        ArrayList<Gakuseki> res02 = new ArrayList<Gakuseki>();

        UserID userID = new UserID();
        String[] aruid = userID.getUID(pramDto.getRuser());

      // ユーザが管理者であるかkidで判定
      RestrictionGroupDao restrictiongroupDao = new RestrictionGroupDao();
      ArrayList<RestrictionGroup> res04 = restrictiongroupDao.getRestrictionGroupList02(pramDto.getRuser());

      AuthorityDao authorityDao = new AuthorityDao();

      // 管理者ユーザの時は現況区分コードに関わらず全員取得
      if(res04.size() != 0 && res04.get(0).getKid() != null){
// 2019.01.09 s.furuta chg start 1.学生番号検索機能追加
          //res01 = gakusekiDao.getGakusekiList08(pramDto);
          if(pramDto.getSearchType().equals("studentid")){
              res01 = gakusekiDao.getGakusekiList09(pramDto);
          } else {
              res01 = gakusekiDao.getGakusekiList08(pramDto);
          }
// 2019.01.09 s.furuta chg end
      }else{
// 2019.01.09 s.furuta chg start 1.学生番号検索機能追加
//        // Authorityにデータがあるかuidで判定
//        for(int i = 0; i < aruid.length; i++){
//            ArrayList<Authority> aut2 = authorityDao.getAuthority02(pramDto.getRuser(), aruid[i]);
//            if(aut2.size() != 0 && aut2.get(0).getUid() != null){
//                res01 = gakusekiDao.getGakusekiList08(pramDto);
//                break;
//            }
//        }
//        if(res01.isEmpty()){
//        	res01 = gakusekiDao.getGakusekiList02(pramDto);
//        }
        if(pramDto.getSearchType().equals("studentid")){
          // Authorityにデータがあるかuidで判定
          for(int i = 0; i < aruid.length; i++){
            ArrayList<Authority> aut2 = authorityDao.getAuthority02(pramDto.getRuser(), aruid[i]);
            if(aut2.size() != 0 && aut2.get(0).getUid() != null){
              res01 = gakusekiDao.getGakusekiList09(pramDto);
              break;
            }
          }
            if(res01.isEmpty()){
              res01 = gakusekiDao.getGakusekiList10(pramDto);
            }
        } else {
          // Authorityにデータがあるかuidで判定
          for(int i = 0; i < aruid.length; i++){
            ArrayList<Authority> aut2 = authorityDao.getAuthority02(pramDto.getRuser(), aruid[i]);
            if(aut2.size() != 0 && aut2.get(0).getUid() != null){
              res01 = gakusekiDao.getGakusekiList08(pramDto);
              break;
            }
          }
          if(res01.isEmpty()){
            res01 = gakusekiDao.getGakusekiList02(pramDto);
          }
        }
// 2019.01.09 s.furuta chg end
      }
// 2018.06.14 SVC)S.Furuta CHG END <<

        for(Gakuseki gakuseki :res01){
            Integer addFlg = 0;

            NendobetuShozokuDao nendobetushozokuDao = new NendobetuShozokuDao();
            ArrayList<NendobetuShozoku> res03 = nendobetushozokuDao.getDaigakuinKbn01(pramDto.getUid(),gakuseki.getYokenNendo(),gakuseki.getgShozokucd());

loop2:
            for(int j = 0; j < aruid.length; j++){
                ArrayList<Authority> aut = authorityDao.getAuthority01(aruid[j]);

                for(Authority authority :aut) {
                    if(authority.getYokenNendo().equals(0) == false && gakuseki.getYokenNendo().equals(authority.getYokenNendo()) == false ){
                        continue;
                    }
                    if(res03.isEmpty() == false){
                        if(authority.getDaigakuinKbncd().equals("") == false && res03.get(0).getDaigakuinkbncd().equals(authority.getDaigakuinKbncd()) == false){
                        continue;
                        }
                    }
                    if(authority.getShozokucd().equals("") == false && gakuseki.getgShozokucd().startsWith(authority.getShozokucd()) == false){
                        continue;
                    }
                    if(authority.getSysno().equals("") == false && gakuseki.getgSysno().equals(authority.getSysno()) == false){
                        continue;
                    }
                    addFlg = 1;
                    break loop2;
                }
            }

            if(addFlg == 1){
                res02.add(gakuseki);
            }

        }
        dbMillis.add(System.currentTimeMillis());
//2014.11.21 SVC 学生の絞り込み機能を追加 <<

        ArrayList<Object> aaData = new ArrayList<Object>();

        KyokihonDao kyokihonDao = new KyokihonDao();
        ShozokuMstDao shozokuMstDao = new ShozokuMstDao();
        GpaDao gpaDao = new GpaDao();
        ToeicDao toeicDao = new ToeicDao();
        KussShinroDao kussShinroDao = new KussShinroDao();
// 2018.06.05 SVC)S.Furuta ADD START >> 部局計算GPAの表示対応
        GpaBukyokuDao gpaBukyokuDao = new GpaBukyokuDao();
// 2018.06.05 SVC)S.Furuta ADD END <<

        // 2015.02.06 SVC 追加、大学院区分を取得 >>
        NendobetuShozokuDao shozokuDao = new NendobetuShozokuDao();
        // 2015.02.06 SVC 追加、大学院区分を取得 <<

//2014.11.21 SVC 学生の絞り込み後の配列を回す >>
//        for(Gakuseki gakuseki :res) {
        for(Gakuseki gakuseki :res02) {
//2014.11.21 SVC 学生の絞り込み後の配列を回す <<
            ArrayList<Kyokihon> kyokihonList = kyokihonDao.getKyokihonList01(pramDto,gakuseki.getgSysno());
            if(kyokihonList.size() == 0) continue;
            Kyokihon kyokihon = kyokihonList.get(0);

            ArrayList<ShozokuMst> shozokuMstList = shozokuMstDao.getShozokuMstList01(pramDto,gakuseki.getgShozokucd());
            if(shozokuMstList.size() == 0) continue;
            ShozokuMst shozokuMst = shozokuMstList.get(0);


            String name = getLangObj(kyokihon.getgName()+" ("+kyokihon.getgNameeng()+")",kyokihon.getgNameeng()+" ("+kyokihon.getgName()+")");
            String affil = getLangObj(shozokuMst.getShozokunm1() +" "+ shozokuMst.getShozokunm2() +" "+ shozokuMst.getShozokunm3(),
                                shozokuMst.getShozokunmeng1() +" "+ shozokuMst.getShozokunmeng2() +" "+ shozokuMst.getShozokunmeng3());
            String year="";
            if (gakuseki.getSotsugyoymd() != null ){
                year = gakuseki.getSotsugyoymd().toString();
            }

            ArrayList<Gpa> gpaList = gpaDao.getGpaList03(pramDto,gakuseki.getgSysno());

            double gpavalue = 0, creditvalue = 0;
            for (Gpa gpa :gpaList){
                gpavalue = gpa.getGpa().doubleValue();
                creditvalue = gpa.getCredit().doubleValue();
                break;
            }
            if(gpaList.size() > 1) logger.equals(gakuseki.getgSysno()+" has more than two entries at gpa!");

            ArrayList<Toeic> toeicList = toeicDao.getToeicList3(pramDto,gakuseki.getgSysno());

            double toeicvalue = 0;
            if(toeicList.size() > 0) toeicvalue = toeicList.get(0).getScore().doubleValue();

            ArrayList<KussShinro> kussShinroList = kussShinroDao.getKussShinroList01(pramDto,gakuseki.getgSysno());

            String path = "";
            for (KussShinro kussShinro :kussShinroList){
                path = kussShinro.getShinronm();
                break;
            }

// 2018.06.05 SVC)S.Furuta ADD START >> 部局計算GPAの表示対応
            ArrayList<GpaBukyoku> gpabukyokuList =gpaBukyokuDao.getGpaBukyokuList01(pramDto,gakuseki.getgSysno());

            double gpabukyokuvalue = 0;
            for (GpaBukyoku gpabukyoku :gpabukyokuList){
                gpabukyokuvalue = gpabukyoku.getGpa().doubleValue();
                break;
            }
            if(gpabukyokuList.size() > 1) logger.equals(gakuseki.getgSysno()+"(bukyokucd="+gakuseki.getgBukyokucd()+") has more than two entries at Departmental gpa!");
// 2018.06.05 SVC)S.Furuta ADD END <<

            if(kussShinroList.size() > 1) logger.equals(gakuseki.getgSysno()+" has more than two entries at kuss_shinro!");

            ArrayList<Object> o = new ArrayList<Object>();
            o.add(gakuseki.getgSysno());
            o.add(name);
            o.add(affil);
            o.add(gpaList.size() > 0 ? gpavalue : null);
// 2018.06.05 SVC)S.Furuta ADD START >> 部局計算GPAの表示対応
            o.add(gpabukyokuList.size() > 0 ? gpabukyokuvalue : null);
// 2018.06.05 SVC)S.Furuta ADD END <<
            o.add(gpaList.size() > 0 ? creditvalue : null);
            o.add(toeicList.size() > 0 ? toeicvalue : null);
            o.add(year);
            o.add(path);

// 2019.01.10 s.furuta add start 1.学生番号検索機能追加
            if(pramDto.getSearchType().equals("studentid")){
              pramDto.setShozokucd(gakuseki.getgShozokucd());
              pramDto.setNyugakunendo(gakuseki.getNyugakuNendo().toString());
            }
// 2019.01.10 s.furuta add end
            ArrayList<NendobetuShozoku> res2 = shozokuDao.getDaigakuinKbn01(gakuseki.getgSysno(), gakuseki.getYokenNendo(), pramDto.getShozokucd());

            NendobetuShozoku shozoku = res2.get(0);
            o.add(shozoku.getDaigakuinkbncd());

// 2019.01.23 s.furuta add start 1.学生番号検索機能追加
            o.add(gakuseki.getgShozokucd());
// 2019.01.23 s.furuta add end

            aaData.add(o);
        }
        dbMillis.add(System.currentTimeMillis());

        ret.put("aaData", aaData);
        ret.put("aoColumns", aoColumns.get(pramDto.getLang()));
        logger.info("studentlist for dataTables.js allowed where nyugaku_nendo="+pramDto.getNyugakunendo()
                +", shozokucd="+pramDto.getShozokucd()+", lang="+pramDto.getLang()+", ruser="+pramDto.getRuser());

        logger.info("ruser="+pramDto.getRuser()+", denied : lang="+pramDto.getLang());

        logger.info(CommonConst.END_LOG + method_name);
        String s = "Time in sec @ " + method_name +" :";
        for(int i = 1; i < dbMillis.size(); i++) s +=  (double)(dbMillis.get(i) - dbMillis.get(i-1)) / 1000D + ",";
        logger.info(s);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        //入学年度、所属コードが設定されていない場合は処理しない。
        if(params.containsKey(ParamsConst.NYUGAKU_NENDO) && (params.containsKey(ParamsConst.SHOZOKUCD))) {
            rtn = true;
        }
        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    final Map<String, ArrayList<Map<String,String>>> aoColumns = // DBの構造に入っていないのでしかたなく
            new HashMap<String, ArrayList<Map<String,String>>>() {{
                put(CommonConst.JAPANESE, new ArrayList<Map<String,String>>() {{
                    add(new HashMap<String,String>() {{
                        put("sTitle", "学生番号");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "氏名");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "所属");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "GPA");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "取得単位");
                    }});
                    add(new HashMap<String,String>() {{
                      put("sTitle", "TOEIC");
                  }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "卒業");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "進路");
                    }});
// 2018.06.05 SVC)S.Furuta ADD START >> 部局計算GPAの表示対応
                    add(new HashMap<String,String>() {{
                        put("sTitle", "部局GPA");
                    }});
// 2018.06.05 SVC)S.Furuta ADD END <<
                }});
                put(CommonConst.ENGLISH, new ArrayList<Map<String,String>>() {{
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Student ID");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Name");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Affiliation");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "GPA");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Credit");
                    }});
                    add(new HashMap<String,String>() {{
                      put("sTitle", "TOEIC");
                  }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Grad. date");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Path");
                    }});
// 2018.06.05 SVC)S.Furuta ADD START >> 部局計算GPAの表示対応
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Departmental GPA");
                    }});
// 2018.06.05 SVC)S.Furuta ADD END <<
                }});
            }};

//2014.11.21 SVC 追加、削除処理追加に伴うもの >>
    @Override
    protected Boolean deleteJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }

    @Override
    protected Boolean addJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }
//2014.11.21 SVC 追加、削除処理追加に伴うもの <<

}
