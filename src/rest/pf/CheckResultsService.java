package rest.pf;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.ResultsDao;
import dao.entity.Results;

/**
 *  REST service using JSONIC, version 2015-02-05 (since 2015-02-05)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class CheckResultsService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();
    boolean enFlag = false;

    // コンストラクタ
    public CheckResultsService() {
        rid = 0;
        mode = 0;
    }

    /**
     * (rest.)pf/inputResults.json?<br />
     * @see rest.AbstractMapService#getJsonData()
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

        ResultsDao resDao = new ResultsDao();
        ArrayList<Object> resultsList = new ArrayList<Object>();
        ArrayList<Results> res = null;

        // 登録済み且つ未削除のresultsを取得(分類、タイトルともに一致するresults有り 且つ seiseki_for_evidenceが残っている)
        res = resDao.getResults02(pramDto);
        for(Results result : res){
            LinkedHashMap<String,Object> lineRes = new LinkedHashMap<String,Object>();

            lineRes.put("classification", result.getClassification());
            lineRes.put("title", result.getTitle());

            resultsList.add(lineRes);
        }

        ret.put("resultsList", resultsList);

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    /* (非 Javadoc)
     * @see rest.AbstractMapService#deleteJsonData()
     */
    @Override
    protected Boolean deleteJsonData() {
        String method_name = "deleteJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        logger.info(CommonConst.END_LOG + method_name);
        return true;
    }

    /**
     * (rest.)pf/inputResults.json3<br />
     * 画面の情報を保存する
     * @see rest.AbstractMapService#addJsonData()
     * @return : true/false
     */
    @Override
    protected Boolean addJsonData() {
        String method_name = "addJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        logger.info(CommonConst.END_LOG + method_name);
        return true;
    }

    /* (非 Javadoc)
     * @see rest.AbstractMapService#checkParams(java.util.Map)
     */
    @Override
    protected boolean checkParams(Map<String, Object> params) {
        return true;
    }
}
