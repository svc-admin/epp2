package rest.pf;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractListService;
import util.Tools;
import constants.CommonConst;
import constants.ParamsConst;
import dao.KakuteiSeisekiDao;
import dao.entity.KakuteiSeiseki;
//+ for using variable request / request変数を利用するため

/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class NinteiNendoService extends AbstractListService{
    private static Logger logger = Tools.getCallerLogger();

    public NinteiNendoService() {
        //権限ID
        rid = 0;
        mode = 0;
    }

    /**
<pre class="brush: html; gutter: false; highlight: 1; toolbar: false;">
(rest.)pf/Toeic.json?uid=XX
</pre>
                TOICのリストを返す。<br />
<pre class="brush: js; toolbar: false;">
[{"year":2007, "score":600, "avr":580, "member":28},
{"year":2008, "score":630, "avr":605, "member":30},... ]
</pre>
</pre>
     * @return : Array
     */
    @Override
    public ArrayList<Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        ArrayList<Object> ret = new ArrayList<Object>();

        KakuteiSeisekiDao kakuteiSeisekiDao = new KakuteiSeisekiDao();
        ArrayList<KakuteiSeiseki> nendoList = kakuteiSeisekiDao.getKakuteiSeisekiList02(pramDto);

        for(KakuteiSeiseki nendo:nendoList){
            LinkedHashMap<String,Object> map = new LinkedHashMap<String,Object>();

            map.put("nendo", nendo.getNinteiNendo());

            ret.add(map);

        }

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

      //学生番号が設定されていない場合は処理しない。
        if(params.containsKey(ParamsConst.UID)) {
            rtn = true;
        }

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

}
