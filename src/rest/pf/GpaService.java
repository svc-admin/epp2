package rest.pf;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractListService;
import util.Tools;
import constants.CommonConst;
import constants.ParamsConst;
import dao.GpaAvrDao;
import dao.GpaDao;
import dao.entity.Gpa;
import dao.entity.GpaAvr;
//+ for using variable request / request変数を利用するため

/**
 *  GPA取得 REST service using JSONIC, version 2014-02-02 (since 2014-02-02)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class GpaService extends AbstractListService {
    private static Logger logger = Tools.getCallerLogger();

    public GpaService() {
        //権限ID
        rid = 0;
        mode = 0;
    }

    /**
    (rest.)pf/gpa.json?lang="ja"&uid=XX&shozokucd="0513"<br />
      学生番号を指定して学生情報を返す。(※)<br />
    <pre>
    {"uid":"071-A6201",
     "name":"久保田　花子 (Kubota Hanako)",
     "affil":"文学部 コミュニケーション情報学科 コミュニケーション情報学コース",
     "email":"aaa@aaa.jp",
     "zaiseki":"2007-2010",
     "dataupdatedate":"2013-02-02"
    ]
    </pre>
     * @return : Object (hash array here) / Object (この場合は連想配列)
     */
    @Override
    protected ArrayList<Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        ArrayList<Object> ret = new ArrayList<Object>();

        //ユーザ情報の取得
        getUserInfo();

        GpaDao gpaDao = new GpaDao();
        ArrayList<Gpa> gpaList = gpaDao.getGpaList08(pramDto);

        // 追加 2014-11-06 by nakano@cc.kumamoto-u.ac.jp
        logger.info(method_name+": 4桁以上の所属コードを禁止 "+pramDto.getShozokucd()+","+pramDto.getGakuseki().getgShozokucd()+","+pramDto.getGakuseki().getgBukyokucd());
        if(pramDto.getGakuseki().getgShozokucd().length() > 4) {
//        	pramDto.setShozokucd(pramDto.getGakuseki().getgShozokucd().substring(0, 3));
//        	pramDto.getGakuseki().setgShozokucd(pramDto.getGakuseki().getgShozokucd().substring(0, 4));
            pramDto.getGakuseki().setgBukyokucd(pramDto.getGakuseki().getgShozokucd().substring(0, 4));
        }

        GpaAvrDao gpaAvrDao = new GpaAvrDao();

        for(Gpa gpa : gpaList){

            LinkedHashMap<String,Object> dataMap = new LinkedHashMap<String,Object>();

            ArrayList<GpaAvr> gpaAvrList = gpaAvrDao.getGpaAvrList05(pramDto,gpa.getGoal());
            GpaAvr gpaAvr = gpaAvrList.get(0);

            dataMap.put("goal",gpa.getGoal());
            dataMap.put("gpa",gpa.getGpa().doubleValue());
            dataMap.put("gpt",gpa.getGpt().doubleValue());
            dataMap.put("credit",gpa.getCredit().doubleValue());
            dataMap.put("gpaavr",gpaAvr.getGpa().doubleValue());
            dataMap.put("gptavr",gpaAvr.getGpt().doubleValue());
            dataMap.put("creditavr",gpaAvr.getCredit().doubleValue());
            dataMap.put("member",gpaAvr.getMember().intValue());

            ret.add(dataMap);
        }

        logger.info("ruser="+pramDto.getRuser()+", denied : lang="+pramDto.getLang());
        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        //学生番号、所属コードが設定されていない場合は処理しない。
        if(params.containsKey(ParamsConst.UID) ) {
            rtn = true;
        }
        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }
}
