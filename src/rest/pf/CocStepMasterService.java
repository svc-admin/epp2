package rest.pf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.CocStepMasterDao;
import dao.entity.CocStepMaster;

public class CocStepMasterService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();
    int tanisuList[] = new int[20];
    public CocStepMasterService() {
        //権限ID
        rid = 2;
        mode = 0;
    }

    /**
     * (rest.)pf/programs.json?lang="ja"<br />
     *
     * @return : JSON String
     */

    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

        CocStepMasterDao cocStepMasterDao = new CocStepMasterDao();
        ArrayList<CocStepMaster> res = cocStepMasterDao.getStepInfoList03(pramDto);
        ArrayList<CocStepMaster> res2 = cocStepMasterDao.getStepInfoList04(pramDto);

        ArrayList<Object> aaData = new ArrayList<Object>();
        ArrayList<Object> aaData2 = new ArrayList<Object>();

        for(CocStepMaster cocStepMaster :res){
            ArrayList<Object> o = new ArrayList<Object>();
            o.add(cocStepMaster.getStep());
            aaData.add(o);
        }
        for(CocStepMaster cocStepMaster :res2){
            ArrayList<Object> o = new ArrayList<Object>();
            o.add(cocStepMaster.getTanisu());
            aaData2.add(o);
        }

        ret.put("aaData" ,aaData);
        ret.put("aoColumns",aaData2);
        logger.info("authoritylist for dataTables.js allowed where nyugaku_nendo="+pramDto.getNyugakunendo()
                +", shozokucd="+pramDto.getShozokucd()+", lang="+pramDto.getLang()+", ruser="+pramDto.getRuser());

        logger.info("ruser="+pramDto.getRuser()+", denied : lang="+pramDto.getLang());

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    final Map<String, ArrayList<Map<String,String>>> aoColumns = // DBの構造に入っていないのでしかたなく
            new HashMap<String, ArrayList<Map<String,String>>>() {{
            }};

    @Override
    protected Boolean deleteJsonData() {
       return true;
    }

    @Override
    protected Boolean addJsonData() {
       return true;
    }
}
