package rest.pf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.CocAdministerDao;
import dao.CocStepMasterDao;
import dao.entity.CocAdminister;
import dao.entity.CocStepMaster;

public class CocStatusService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();
    int tanisuList[] = new int[20];
    public CocStatusService() {
        //権限ID
        rid = 2;
        mode = 0;
    }

    /**
     * (rest.)pf/programs.json?lang="ja"<br />
     *
     * @return : JSON String
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

        CocAdministerDao cocAdministerDao = new CocAdministerDao();
        CocStepMasterDao cocStepMasterDao = new CocStepMasterDao();

        ArrayList<CocStepMaster> step = cocStepMasterDao.getStepInfoList03(pramDto);
        ArrayList<String> stepList = new ArrayList<String>();
        ArrayList<CocStepMaster> tanisu = cocStepMasterDao.getStepInfoList04(pramDto);
        ArrayList<String> tanisuList = new ArrayList<String>();

        for(CocStepMaster cocStepMaster :step){
            String s = "";
            s = cocStepMaster.getStep();
            stepList.add(s);
        }

        for(CocStepMaster cocStepMaster :tanisu){
            String t = "";
            t = String.valueOf(cocStepMaster.getTanisu());
            tanisuList.add(t);
        }

        ArrayList<CocAdminister> res = cocAdministerDao.cocStatusList01(pramDto, stepList);
        ArrayList<Object> aaData = new ArrayList<Object>();

        for(CocAdminister cocAdminister :res){
        ArrayList<Object> o = new ArrayList<Object>();
            o.add(cocAdminister.getgGakuno());
            o.add(cocAdminister.getgSysno());
            o.add(getLangObj(cocAdminister.getgName(),cocAdminister.getgNameeng()));
            o.add(getLangObj(cocAdminister.getShozokunm(),cocAdminister.getShozokunmeng()));
            o.add(pramDto.getCocOrPlus());
            o.add(cocAdminister.getStep1());
            o.add(cocAdminister.getStep2());
            o.add(cocAdminister.getStep3());
            o.add(cocAdminister.getStep4());
            o.add(cocAdminister.getStep5());
            o.add(cocAdminister.getStep6());
            o.add(cocAdminister.getStep7());
            o.add(cocAdminister.getStep8());
            o.add(cocAdminister.getStep9());
            o.add(cocAdminister.getStep10());
            aaData.add(o);
        }

        ret.put("aaData" ,aaData);
        ret.put("aoColumns", aoColumns.get(pramDto.getLang()));
        logger.info("authoritylist for dataTables.js allowed where nyugaku_nendo="+pramDto.getNyugakunendo()
                +", shozokucd="+pramDto.getShozokucd()+", lang="+pramDto.getLang()+", ruser="+pramDto.getRuser());

        logger.info("ruser="+pramDto.getRuser()+", denied : lang="+pramDto.getLang());

        logger.info(CommonConst.END_LOG + method_name);

        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    final Map<String, ArrayList<Map<String,String>>> aoColumns = // DBの構造に入っていないのでしかたなく
            new HashMap<String, ArrayList<Map<String,String>>>() {{
                put(CommonConst.JAPANESE, new ArrayList<Map<String,String>>() {{
                    add(new HashMap<String,String>() {{
                        put("sTitle", "対象");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "学生番号");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "氏名");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "所属");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "区分");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP1");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP2");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP3");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP4");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP5");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP6");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP7");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP8");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP9");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP10");
                    }});
                }});
                put(CommonConst.ENGLISH, new ArrayList<Map<String,String>>() {{
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Operation");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Student ID");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Name");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Affiliation");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Classification");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP1");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP2");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP3");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP4");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP5");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP6");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP7");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP8");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP9");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "STEP10");
                    }});
                }});
            }};

    @Override
    protected Boolean deleteJsonData() {
        return true;
    }

    @Override
    protected Boolean addJsonData() {
        return true;
    }
}
