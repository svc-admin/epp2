package rest.pf;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.ExternalEnglishTestsDao;
import dao.ResultsDao;
import dao.StudyingAbroadExperiencesDao;
import dao.entity.ExternalEnglishTests;
import dao.entity.Results;
import dao.entity.StudyingAbroadExperieces;

/**
 *  REST service using JSONIC, version 2015-02-05 (since 2015-02-05)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class GetPrintInputResultsService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();
    boolean enFlag = false;

    // コンストラクタ
    public GetPrintInputResultsService() {
        rid = 0;
        mode = 0;
    }

    /**
     * (rest.)pf/inputResults.json?<br />
     * @see rest.AbstractMapService#getJsonData()
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

        ResultsDao resDao = new ResultsDao();
        ExternalEnglishTestsDao engDao = new ExternalEnglishTestsDao();
        StudyingAbroadExperiencesDao abrDao = new StudyingAbroadExperiencesDao();
        ArrayList<Object> resultsList = new ArrayList<Object>();
        ArrayList<Object> englishList = new ArrayList<Object>();
        ArrayList<Object> abroadList = new ArrayList<Object>();
        ArrayList<Results> res = null;
        ArrayList<ExternalEnglishTests> eng = null;
        ArrayList<StudyingAbroadExperieces> abr = null;

        res = resDao.getResults03(pramDto);
        for(Results result : res){
            LinkedHashMap<String,Object> lineRes = new LinkedHashMap<String,Object>();

            lineRes.put("G_SYSNO", result.getgSysno());
            lineRes.put("KAMOKUCD", result.getKamokucd());
            lineRes.put("date", result.getDate());
            lineRes.put("classification", result.getClassification());
            lineRes.put("classification_custom", result.getClassificationCustom());
            lineRes.put("title", result.getTitle());
            lineRes.put("comment", result.getComment());
            lineRes.put("author_name", result.getAuthorName());
            lineRes.put("author_type", result.getAuthorType());
            lineRes.put("presentation_format", result.getPresentationFormat());
            lineRes.put("medium_title", result.getMediumTitle());
            lineRes.put("medium_class", result.getMediumClass());
            lineRes.put("has_impact_factor", result.getHasImpactFactor());
            lineRes.put("impact_factor", result.getImpactFactor());
            lineRes.put("location", result.getLocation());
            lineRes.put("publisher", result.getPublisher());
            lineRes.put("isbn", result.getIsbn());
            lineRes.put("journal_volume", result.getJournalVolume());
            lineRes.put("journal_number", result.getJournalNumber());
            lineRes.put("page_from", result.getPageFrom());
            lineRes.put("page_to", result.getPageTo());
            lineRes.put("published_year", result.getPublishedYear());
            lineRes.put("published_month", result.getPublishedMonth());
            lineRes.put("has_reviewed", result.getHasReviewed());
            lineRes.put("original_medium_name", result.getOriginalMediumName());
            lineRes.put("original_author_name", result.getOriginalAuthorName());
            lineRes.put("original_publisher", result.getOriginalPublisher());
            lineRes.put("original_page_from", result.getOriginalPageFrom());
            lineRes.put("original_page_to", result.getOriginalPageTo());
            lineRes.put("original_published_year", result.getOriginalPublishedYear());
            lineRes.put("original_published_month", result.getOriginalPublishedMonth());
            lineRes.put("insert_data", result.getInsertDate());
            lineRes.put("update_date", result.getUpdateDate());
            lineRes.put("userid", result.getUserid());

            resultsList.add(lineRes);
        }

        eng = engDao.getExternalEnglishTests02(pramDto);
        for(ExternalEnglishTests english : eng){
            LinkedHashMap<String,Object> lineEng = new LinkedHashMap<String,Object>();

            lineEng.put("G_SYSNO", english.getgSysno());
            lineEng.put("KAMOKUCD", english.getKamokucd());
            lineEng.put("test_name", english.getTestName());
            lineEng.put("score", english.getScore());
            lineEng.put("date", english.getDate());
            lineEng.put("insert_date", english.getInsertDate());
            lineEng.put("update_date", english.getUpdateDate());
            lineEng.put("userid", english.getUserid());

            englishList.add(lineEng);
        }

        abr = abrDao.getStudyingAbroadExperiences02(pramDto);
        for(StudyingAbroadExperieces abroad : abr){
            LinkedHashMap<String,Object> lineAbr = new LinkedHashMap<String,Object>();

            lineAbr.put("G_SYSNO", abroad.getgSysno());
            lineAbr.put("KAMOKUCD", abroad.getKamokucd());
            lineAbr.put("purpose_type", abroad.getPurposeType());
            lineAbr.put("nation", abroad.getNation());
            lineAbr.put("date_from", abroad.getDateFrom());
            lineAbr.put("date_to", abroad.getDateTo());
            lineAbr.put("destination", abroad.getDestination());
            lineAbr.put("insert_date", abroad.getInsertDate());
            lineAbr.put("update_date", abroad.getUpdateDate());
            lineAbr.put("userid", abroad.getUserid());

            abroadList.add(lineAbr);
        }

        ret.put("resultsList", resultsList);
        ret.put("englishList", englishList);
        ret.put("abroadList", abroadList);

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    /* (非 Javadoc)
     * @see rest.AbstractMapService#deleteJsonData()
     */
    @Override
    protected Boolean deleteJsonData() {
        String method_name = "deleteJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        logger.info(CommonConst.END_LOG + method_name);
        return true;
    }

    /**
     * (rest.)pf/inputResults.json3<br />
     * @see rest.AbstractMapService#addJsonData()
     * @return : true/false
     */
    @Override
    protected Boolean addJsonData() {
        String method_name = "addJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        logger.info(CommonConst.END_LOG + method_name);
        return true;
    }

    /* (非 Javadoc)
     * @see rest.AbstractMapService#checkParams(java.util.Map)
     */
    @Override
    protected boolean checkParams(Map<String, Object> params) {
        return true;
    }
}
