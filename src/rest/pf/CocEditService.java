package rest.pf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.CocAdministerDao;

public class CocEditService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public CocEditService() {
        //権限ID
        rid = 2;
        mode = 0;
    }

    /**
     * (rest.)pf/programs.json?lang="ja"<br />
     *
     * @return : JSON String
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    final Map<String, ArrayList<Map<String,String>>> aoColumns = // DBの構造に入っていないのでしかたなく
            new HashMap<String, ArrayList<Map<String,String>>>() {{
            }};

    @Override
    protected Boolean deleteJsonData() {
        return true;
    }

    @Override
    protected Boolean addJsonData() {
        String method_name = "addJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        CocAdministerDao cocAdministerDao = new CocAdministerDao();
        cocAdministerDao.editCoc01(pramDto);

        logger.info(CommonConst.END_LOG + method_name);

        return true;
    }
}
