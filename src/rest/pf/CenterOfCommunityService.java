package rest.pf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractListService;
import util.PropertiesUtil;
import util.Tools;
import constants.CommonConst;
import constants.ParamsConst;
import dao.CenterOfCommunityDao;
import dao.CocStepMasterDao;
import dao.cstmentity.CenterOfCommunity;
import dao.entity.CocStepMaster;
import dto.SysPropertiesDto;

/**
 *
 * COC事業関連における単位取得状況確認機能サービス
 *
 * @author t.iwamoto
 *
 */
public class CenterOfCommunityService extends AbstractListService {
    private static Logger logger = Tools.getCallerLogger();
    /**
     * COC分類区分：COC
     */
    private static final String BNR_KBN_COC = "Coc";
    /**
     * COC分類区分：COC＋
     */
    private static final String BNR_KBN_COC_PLUS = "CocPlus";
    /**
     * COC関連科目検索キーワード
     */
    private static final String SEARCH_COC = "COC";
    private static final String SEARCH_COC_PLUS = "COCP";
    private static final String LAST_PRESEN_KEY = "COC Presentation";
    /**
     * COCステップ数
     */
    private static final int COC_STEP_NUM = 10;

    String syllabusUrl = new String();

    public CenterOfCommunityService(){
        //権限ID
        rid = 0;
        mode = 0;
    }
    /**
    (rest.)pf/centerOfCommunity.json?lang="ja"&uid="14X-XXXXX"<br />
      学生IDを指定してdataTables.js用のリストを返す。(※)<br />
    <pre>
    [{
        "stepCoc":[{
            "step1":"0",
            "step2":"0",
            "step3":"0",
            "step4":"0",
            "step5":"-1",
            "step6":"-1",
            "step7":"-1",
            "step8":"-1",
            "step9":"-1",
            "step10":"1"
        }],
        "stepCocPlus":[{
            "step1":"0",
            "step2":"0",
            "step3":"0",
            "step4":"0",
            "step5":"-1",
            "step6":"-1",
            "step7":"-1",
            "step8":"-1"
            "step9":"-1",
            "step10","1""
        }],
        "unitCocPlus":[{
            "unit1":[
                {"lecture":[{"lecturename":"人体構造学I","period":"2014年前期","syllabus":"http://syllabusurl.net","success":0}]},
                {"lecture":[{"lecturename":"人体構造学II","period":"2014年後期","syllabus":"http://syllabusurl.net","success":2}]},
                {"lecture":[{"lecturename":"生体機能学I","period":"2014年前期","syllabus":"http://syllabusurl.net","success":0}]},
                {"lecture":[{"lecturename":"生体機能学II","period":"2014年後期","syllabus":"http://syllabusurl.net","success":3}]}
            ],
            ・・・
            "unit9":[
                {"lecture":[{"lecturename":"地方創生プレゼンテーション","period":"2014年前期","syllabus":"http://syllabusurl.net","success":0}]}
            ]
        }],
        "unitCoc":[{
            "unit1":[
                {"lecture":[{"lecturename":"肥後熊本学","period":"2014年後期","syllabus":"http://syllabusurl.net","success":0}]}
                {"lecture":[{"lecturename":"生活の中の物理学","period":"2014年前期","syllabus":"http://syllabusurl.net","success":0}]},
                {"lecture":[{"lecturename":"化学入門D","period":"2014年前期","syllabus":"http://syllabusurl.net","success":0}]},
                {"lecture":[{"lecturename":"化学と社会A","period":"2014年前期","syllabus":"http://syllabusurl.net","success":0}]},
                {"lecture":[{"lecturename":"都市・建築入門B","period":"2014年後期","syllabus":"http://syllabusurl.net","success"1.5}]}
            ],
            ・・・
            "unit9":[
                {"lecture":[{"lecturename":"地方創生プレゼンテーション","period":"2014年前期","syllabus":"http://syllabusurl.net","success":0}]}
            ]
        }]
    }]
    </pre>
     * @return : Object (hash array here) / Object (この場合は連想配列)
     */
    @Override
    protected ArrayList<Object> getJsonData() {

        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        ArrayList<Object> ret = new ArrayList<Object>();

        CenterOfCommunityDao cocDao = new CenterOfCommunityDao();
        LinkedHashMap<String,Object> dataMap = new LinkedHashMap<String,Object>();

        // coc_step_masterから各ステップの単位数を取得
        CocStepMasterDao dao = new CocStepMasterDao();
        ArrayList<CocStepMaster> stepList = new ArrayList<CocStepMaster>();
        LinkedHashMap<String, Object> stepMap = new LinkedHashMap<String,Object>();
        ArrayList<LinkedHashMap<String,Object>> mapList = new ArrayList<LinkedHashMap<String,Object>>();

        // COC関連事業のSTEP情報を取得
        stepList = dao.getStepInfoList01(pramDto);
        for(int i=0; i<stepList.size(); i++){
            CocStepMaster stepInfo = stepList.get(i);
            int stepCnt = Integer.parseInt(stepInfo.getStep());
            // 表示フラグが「1：有効」の場合
            if(stepInfo.getDispFlg() != null && stepInfo.getDispFlg() == 1){
                stepMap.put("step" + Integer.toString(stepCnt), stepInfo.getTanisu().toString() );
            }else{
                stepMap.put("step" + Integer.toString(stepCnt), "-1");
            }
        }
        mapList.add(stepMap);
        // COC関連事業のSTEP情報を戻り値に設定
        dataMap.put("stepCoc", mapList);

        stepList = new ArrayList<CocStepMaster>();
        stepMap = new LinkedHashMap<String,Object>();
        mapList = new ArrayList<LinkedHashMap<String,Object>>();

        // COC+関連事業のSTEP情報を取得
        stepList = dao.getStepInfoList02(pramDto);
        for(int i=0; i<stepList.size(); i++){
            CocStepMaster stepInfo = stepList.get(i);
            int stepCnt = Integer.parseInt(stepInfo.getStep());
            // 表示フラグが「1：有効」の場合
            if(stepInfo.getDispFlg() != null && stepInfo.getDispFlg() == 1){
                stepMap.put("step" + Integer.toString(stepCnt), stepInfo.getTanisu().toString() );
            }else{
                stepMap.put("step" + Integer.toString(stepCnt), "-1");
            }
        }
        mapList.add(stepMap);
        // COC+関連事業のSTEP情報を戻り値に設定
        dataMap.put("stepCocPlus", mapList);

        // シラバスURLのパターンを取得
        getSyllabusUrl();

        // 対象学生のCOC関連事業の履修状況を取得
        ArrayList<CenterOfCommunity> studentList = cocDao.getStudentDataList01(pramDto);
        LinkedHashMap<String,CenterOfCommunity> studentMap = new LinkedHashMap<String,CenterOfCommunity>();
        for(CenterOfCommunity entity : studentList){
            // Key項目：科目CD、Value項目：エンティティ
            studentMap.put(entity.getKamokucd(), entity);
        }

        // COC関連事業の講義情報を取得
        ArrayList<CenterOfCommunity> cocKamokuList = cocDao.getCenterOfCommunityList01(pramDto);
        editUnit(BNR_KBN_COC, cocKamokuList, studentMap, dataMap);

// 2017.04.10 SVC ADD START >> COCとCOC+で修得している場合、COC+に表示されない不具合対応
        // 修得状況を再取得
        studentList = cocDao.getStudentDataList01(pramDto);
        studentMap = new LinkedHashMap<String,CenterOfCommunity>();
        for(CenterOfCommunity entity : studentList){
            // Key項目：科目CD、Value項目：エンティティ
            studentMap.put(entity.getKamokucd(), entity);
        }
// 2017.04.10 SVC ADD END   <<

        // COC+関連事業の講義情報を取得
        ArrayList<CenterOfCommunity> cocPlusKamokuList = cocDao.getCenterOfCommunityList01(pramDto);
        editUnit(BNR_KBN_COC_PLUS, cocPlusKamokuList, studentMap, dataMap);

        ret.add(dataMap);

        logger.info("ruser="+pramDto.getRuser()+", denied : lang="+pramDto.getLang());
        logger.info(CommonConst.END_LOG + method_name);

        return ret;
    }

    /**
     * 取得したCOC、COC＋関連科目情報を編集
     * （STEP数は最大値10を最終審査として使用していないSTEPについては「-1」を設定）
     *
     * @param kbn：Coc or CocPlus
     * @param list 講義情報リスト
     * @param risyuKamoku 生徒の履修済講義のリスト
     * @param map 戻り値用のMAP情報
     */
    private void editUnit(String kbn, ArrayList<CenterOfCommunity> list, LinkedHashMap<String,CenterOfCommunity> risyuKamoku, LinkedHashMap<String,Object> map){

        ArrayList<LinkedHashMap<String,Object>> dataList = null;
        LinkedHashMap<String, Object> dataMap = null;	// 科目情報

        // 科目情報の一覧を保持
        ArrayList<LinkedHashMap<String,Object>> lectureList = null;
        LinkedHashMap<String, Object> lectureMap = null;
        // STEPで対象となる科目の情報を保持
        LinkedHashMap<String, Object> unitMap =  new LinkedHashMap<String, Object>();

        // 科目情報を各ステップに振り分ける
        for(int i = 1; i <= COC_STEP_NUM; i++){

            lectureList = new ArrayList<LinkedHashMap<String,Object>>();
            for(CenterOfCommunity entity : list){

                try{
                    // シラバスのキーワードからSTEPを取得
                    String searchVal = "";
                    String afterKeyword = "";
                    if(kbn.equals(BNR_KBN_COC)){
                        // COCPのキーワードは除去
                        afterKeyword = entity.getStep().replace(SEARCH_COC_PLUS, "").replace(LAST_PRESEN_KEY, "");
                        searchVal = SEARCH_COC;
                    }
                    else{
                        afterKeyword = entity.getStep();
                        searchVal = SEARCH_COC_PLUS;
                        // 最終ステップは「最終プレゼンテーション科目」のみ探す
                        if(i == COC_STEP_NUM){
                            searchVal = LAST_PRESEN_KEY;
                        }
                    }

                    // STEP数を取得（COC1やCOC2のようにキーワードに入っているので、その数字を取得する）
                    int stepPosition = afterKeyword.indexOf(searchVal);
                    int stepVal = 0;
                    if(stepPosition > -1){
                        if(i < COC_STEP_NUM){
                            int idx = stepPosition + searchVal.length();
                            stepVal = Integer.parseInt(afterKeyword.substring(idx, idx + 1));
                        }
                        else{
                            // 最終プレゼンテーション科目
                            stepVal = 10;
                        }
                    }

                    // 取得済みの科目かどうかをチェックし、送信用データの形式に整形
                    // ※取得済みのチェックは科目CDでチェックする
                    dataMap = new LinkedHashMap<String,Object>();
                    dataMap.put("lecturename", pramDto.getLang().equals("ja") ? entity.getKamokunm() : entity.getKamokunmeng());
                    // 取得済情報が存在する場合は取得状況に単位数を、存在しない場合は0を設定
                    CenterOfCommunity kamokuData = risyuKamoku.get(entity.getKamokucd());
                    stepPosition = -1;
                    if(kamokuData != null){
                        if(kbn.equals(BNR_KBN_COC)){
                            // COCPのキーワードは除去
                            afterKeyword = kamokuData.getStep().replace(SEARCH_COC_PLUS, "").replace(LAST_PRESEN_KEY, "");
                            searchVal = SEARCH_COC;
                        }
                        else{
                            afterKeyword = kamokuData.getStep();
                            searchVal = SEARCH_COC_PLUS;
                            // 最終ステップは「最終プレゼンテーション科目」のみ探す
                            if(i == COC_STEP_NUM){
                                searchVal = LAST_PRESEN_KEY;
                            }
                        }
                        stepPosition = afterKeyword.indexOf(searchVal);
                    }
                    if(stepPosition > -1){
                        setData(dataMap, kamokuData, kamokuData.getTanisu());
                    }
                    else{
                        setData(dataMap, entity, new BigDecimal(0));
                    }

                    dataList = new ArrayList<LinkedHashMap<String,Object>>();
                    lectureMap = new LinkedHashMap<String,Object>();
                    dataList.add(dataMap);
                    lectureMap.put("lecture", dataList);
                    // STEP数が同じ場合のみ科目リストに追加
                    if(i == stepVal){
                        lectureList.add(lectureMap);
                        if(kamokuData != null && stepPosition > -1){
                            risyuKamoku.remove(entity.getKamokucd());
                        }
                    }
                }
                catch(Exception e){
                    logger.info("ruser="+pramDto.getRuser()+", exception : "+e.getMessage()+", keyword : "+entity.getStep());
                }
            }

            // 既得科目で最新シラバスにない科目を追加する
            for(Map.Entry<String,CenterOfCommunity> kamoku : risyuKamoku.entrySet()){
                CenterOfCommunity entity = kamoku.getValue();
                try{
                    // シラバスのキーワードからSTEPを取得
                    String searchVal = "";
                    String afterKeyword = "";
                    if(kbn.equals(BNR_KBN_COC)){
                        // COCPのキーワードは除去
                        afterKeyword = entity.getStep().replace(SEARCH_COC_PLUS, "").replace(LAST_PRESEN_KEY, "");
                        searchVal = SEARCH_COC;
                    }
                    else{
                        afterKeyword = entity.getStep();
                        searchVal = SEARCH_COC_PLUS;
                        // 最終ステップは「最終プレゼンテーション科目」のみ探す
                        if(i == COC_STEP_NUM){
                            searchVal = LAST_PRESEN_KEY;
                        }
                    }

                    // STEP数を取得（COC1やCOC2のようにキーワードに入っているので、その数字を取得する）
                    int stepPosition = afterKeyword.indexOf(searchVal);
                    int stepVal = 0;
                    if(stepPosition > -1){
                        if(i < COC_STEP_NUM){
                            int idx = stepPosition + searchVal.length();
                            stepVal = Integer.parseInt(afterKeyword.substring(idx, idx + 1));
                        }
                        else{
                            // 最終プレゼンテーション科目
                            stepVal = 10;
                        }
                    }

                    dataMap = new LinkedHashMap<String,Object>();
                    dataMap.put("lecturename", pramDto.getLang().equals("ja") ? entity.getKamokunm() : entity.getKamokunmeng());
                    setData(dataMap, entity, entity.getTanisu());

                    dataList = new ArrayList<LinkedHashMap<String,Object>>();
                    lectureMap = new LinkedHashMap<String,Object>();
                    dataList.add(dataMap);
                    lectureMap.put("lecture", dataList);
                    // STEP数が同じ場合のみ科目リストに追加
                    if(i == stepVal){
                        lectureList.add(lectureMap);
                    }
                }
                catch(Exception e){
                    logger.info("ruser="+pramDto.getRuser()+", exception : "+e.getMessage()+", keyword : "+entity.getStep());
                }
            }

            // COCかつ最終ステップの場合、ダミー科目を1件登録
            // ※最終プレゼン科目はCOC+のみに存在するため。
            if(kbn.equals(BNR_KBN_COC) && i == COC_STEP_NUM){
                dataMap = new LinkedHashMap<String,Object>();
                dataMap.put("lecturename", "coc dummy");
                dataMap.put("period", "");
                dataMap.put("success", new BigDecimal(0));
                dataMap.put("syllabus", "");
                dataList = new ArrayList<LinkedHashMap<String,Object>>();
                dataList.add(dataMap);
                lectureMap = new LinkedHashMap<String,Object>();
                lectureMap.put("lecture", dataList);
                lectureList = new ArrayList<LinkedHashMap<String,Object>>();
                lectureList.add(lectureMap);
            }
            unitMap.put("unit"+ String.valueOf(i), lectureList);

        }

        // COC,COC+の情報を引数によって分ける。
        ArrayList<LinkedHashMap<String,Object>> mapList = new ArrayList<LinkedHashMap<String,Object>>();
        mapList.add(unitMap);
        if(kbn.equals(BNR_KBN_COC_PLUS)){
            map.put("unitCocPlus", mapList);
        }
        else if(kbn.equals(BNR_KBN_COC)){
            map.put("unitCoc", mapList);
        }

    }

    /**
     * 講義情報を設定
     * @param dataMap: 戻り値へ設定する講義情報MAP
     * @param entity: 講義情報の検索結果
     * @param tanisu: 取得状況へ設定する単位数
     */
    private void setData(LinkedHashMap<String, Object> dataMap, CenterOfCommunity entity, BigDecimal tanisu){
        String prdVal = null;
        // 講義が開かれた時期を設定
        prdVal = entity.getJikanwariNendo() + (pramDto.getLang().equals("ja") ? "年" : " ");
        // 学期区分：1(前期)、2(後期)
        if(entity.getGakkiKbnCd().equals("1")){
            prdVal += pramDto.getLang().equals("ja") ? "前期" : "previous period";
        }else if(entity.getGakkiKbnCd().equals("2")){
            prdVal += pramDto.getLang().equals("ja") ? "後期" : "latter period";
        }

        dataMap.put("period", prdVal);    // 開講時期
        dataMap.put("success", tanisu);    // 取得状況:｛単位数｝=履修済
        // シラバスURL{ 0:locale, 1:nendo, 2:shozoku_cd, 3:jikanwaricd }
        dataMap.put("syllabus", convertSyllabusUrl( Integer.toString( entity.getJikanwariNendo() ),
                                                    entity.getJikanwariShozokucd(),
                                                    entity.getJikanwariCd()));
    }

    /**
     * シラバスURLパターンを取得
     */
    private void getSyllabusUrl(){
        PropertiesUtil propUtil = new PropertiesUtil();
        SysPropertiesDto sysPropDto = propUtil.getSysProperties();
        syllabusUrl = sysPropDto.getSyllabusuri();
    }

    /**
     * 引数を元にシラバスのリンクURLに変換
     * @param nendo
     * @param shozokuCd
     * @param jikanwariCd
     * @return
     */
    private String convertSyllabusUrl(String nendo, String shozokuCd, String jikanwariCd){
        String rtn = syllabusUrl;
        String[] param = new String[]{pramDto.getLang(), nendo, shozokuCd, jikanwariCd};

        for(int i=0; i<param.length; i++){
            rtn = rtn.replace("{"+i+"}", param[i]);
        }
        return rtn;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        if(params.containsKey(ParamsConst.UID)) {
            rtn = true;
        }

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

}
