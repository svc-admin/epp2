package rest.pf;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.HanteiShubetsuDataDao;
import dao.entity.HanteiShubetsuData;

/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2019 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class JudgeTypeService extends AbstractMapService {
	private static Logger logger = Tools.getCallerLogger();

	public JudgeTypeService() {
		// 権限ID
		rid = 2;
		mode = 0;
	}

	/**
	 * 判定種別情報を取得する
	 * @return : JSON String
	 */
	@Override
	protected LinkedHashMap<String, Object> getJsonData() {
		String method_name = "getJsonData";
		logger.info(CommonConst.START_LOG + method_name);
		LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

		try {
			// パラメータの分解と設定
			if (pramDto.getEtcParam() != null) {
				String[] params = pramDto.getEtcParam().split("-");
				pramDto.getShoriJokenMst().setShorinendo(Integer.parseInt(params[0]));
				pramDto.getShoriJokenMst().setGakkikbncd(params[1]);
			}

			// 判定種別データの取得
			ArrayList<HanteiShubetsuData> typeList = new ArrayList<HanteiShubetsuData>();
			HanteiShubetsuDataDao typeDao = new HanteiShubetsuDataDao();
			typeList = typeDao.getTypeData(pramDto);

			ret.put("result", true);
			ret.put("dataList", typeList);
		} catch(Exception e) {
			logger.info(e.getMessage());
			ret.put("result", false);
		}
		return ret;
	}

	@Override
	protected Boolean deleteJsonData() {
		return null;
	}

	/**
	 * 判定種別情報を登録する
	 */
	@Override
	protected Boolean addJsonData() {
		String method_name = "addJsonData";
		logger.info(CommonConst.START_LOG + method_name);
		Boolean ret = false;

		try {
			// パラメータの分解と設定
			String[] params = null;
			if (pramDto.getEtcParam() != null) {
				params = pramDto.getEtcParam().split("#");
				String[] pBase = params[1].split("-");
				pramDto.getShoriJokenMst().setShorinendo(Integer.parseInt(pBase[0]));
				pramDto.getShoriJokenMst().setGakkikbncd(pBase[1]);
			}

			HanteiShubetsuDataDao typeDao = new HanteiShubetsuDataDao();
			// 判定種別データの削除
			if (typeDao.delTypeData(pramDto)) {
				// 判定種別データの登録
				if (params[0] != null) {
					String[] addParam = params[0].split(";");
					for (int i = 0; i < addParam.length; i++) {
						String[] addValue = addParam[i].split("\\+");
						pramDto.getHanteiShubetsuData().setHantei_shubetsu(addValue[0]);
						pramDto.getHanteiShubetsuData().setG_sysno(addValue[1].toUpperCase());

						typeDao.addTypeData(pramDto);
					}
				}
			}

			ret = true;
		} catch(Exception e) {
			logger.info(e.getMessage());
		}
		return ret;
	}

	protected LinkedHashMap<String, Object> makeRetJson(Boolean ret) {
		LinkedHashMap<String, Object> retMap = new LinkedHashMap<String, Object>();

		if (ret) {
			retMap.put("status", true);
		} else {
			retMap.put("status", false);
		}
		return retMap;
	}

	@Override
	protected boolean checkParams(Map<String, Object> params) {
		return true;
	}
}
