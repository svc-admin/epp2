package rest.pf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.GpaShugakuninteiBukyokuMstDao;
import dao.ShugakusienHanteiDataDao;
import dao.ShugakusienHanteiShuseiDao;
import dao.entity.ShugakusienHanteiData;
import dao.entity.ShugakusienHanteiShusei;

/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2019 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class JudgeService extends AbstractMapService {
	private static Logger logger = Tools.getCallerLogger();
	private static final String CLEAR = "C";
	private static final String NOTCLEAR = "N";
	private static final String WARNING = "警告";
	private static final int G_SYSNO = 0;
	private static final int JUDGE_TYPE = 1;
	private static final int Z_JUDGE = 2;
	private static final int T_JUDGE = 3;
	private static final int BIKOU = 4;
	private static final int T_GPT = 5;
	private static final int T_GP_TANISU = 6;
	private static final int T_GPA_JUNI = 7;
	private static final int N_GPT = 8;
	private static final int N_GP_TANISU = 9;
	private static final int N_GPA_JUNI = 10;
	private static final int GPA_PARAM = 11;
	private static final int ZAISEKI_MONTH = 12;
	private static final int KYUGAKU_MONTH = 13;
	private static final int ZAIGAKU_NENSU = 14;
	private static final int HITSUYO_TANISU = 15;
	private static final int SHUGYO_NENGEN = 16;
	private static final int HYOJUN_TANISU = 17;
	private static final int SHUTOKU_TANISU = 18;
	private static final int SHUTOKU_RITSU = 19;
	private static final int RISHU_TANISU = 20;
	private static final int KESSEKI_TANISU = 21;
	private static final int SHUSSEKI_RITSU = 22;
	private static final int RYUNENDATA = 23;
	private static final int KEIKAKUSHO = 24;
	private static final int Z_GPA = 25;
	private static final int Z_GPA_B = 26;
	private static final int Z_TANI = 27;
	private static final int Z_TANI_B = 28;
	private static final int H_SOTSU = 29;
	private static final int H_SOTSU_B = 30;
	private static final int H_TANI = 31;
	private static final int H_TANI_B = 32;
	private static final int H_SHU = 33;
	private static final int H_SHU_B = 34;
	private static final int H_ZEN = 35;
	private static final int H_ZEN_B = 36;
	private static final int K_TANI = 37;
	private static final int K_TANI_B = 38;
	private static final int K_GPA = 39;
	private static final int K_GPA_B = 40;
	private static final int K_SHU = 41;
	private static final int K_SHU_B = 42;
	private static final int S_TANI = 43;
	private static final int S_TANI_B = 44;
	private static final int S_SHU = 45;
	private static final int S_SHU_B = 46;
	private static final int T_GPA = 47;
	private static final int T_GPA_B = 48;
	private static final int N_GPA = 49;
	private static final int N_GPA_B = 50;
	private static final int G_NAME = 51;

	public JudgeService() {
		// 権限ID
		rid = 2;
		mode = 0;
	}

	/**
	 * 修学支援の判定に必要な情報を収集し、判定。結果を返す
	 * @return : JSON String
	 */
	@Override
	protected LinkedHashMap<String, Object> getJsonData() {
		String method_name = "getJsonData";
		logger.info(CommonConst.START_LOG + method_name);
		LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

		try {
			// GPA集計用部局管理テーブルにデータが存在するかチェック
			GpaShugakuninteiBukyokuMstDao gpaBukyokuMstDao = new GpaShugakuninteiBukyokuMstDao();
			boolean hasGpaDivData = gpaBukyokuMstDao.getGpaBukyokuJoken(pramDto);

			// 判定用データの取得
			ArrayList<ShugakusienHanteiData> judgeBaseDataList = new ArrayList<ShugakusienHanteiData>();
			ShugakusienHanteiDataDao hanteiDao = new ShugakusienHanteiDataDao();
			judgeBaseDataList = hanteiDao.getJudgeBaseData(hasGpaDivData, pramDto);

			// 修正データの取得
			ArrayList<ShugakusienHanteiShusei> modifyList = new ArrayList<ShugakusienHanteiShusei>();
			ShugakusienHanteiShuseiDao shuseiDao = new ShugakusienHanteiShuseiDao();
			modifyList = shuseiDao.getModifyData(pramDto);

			// 修正データを判定元データに格納しておく
			for (ShugakusienHanteiShusei modify : modifyList) {
				if (modify.getT_gpa() != null) {
					for (int i = 0; i < judgeBaseDataList.size(); i++) {
						if (modify.getG_sysno().equals(judgeBaseDataList.get(i).getG_gakuno())) {
							judgeBaseDataList.get(i).setT_gpa_b(modify.getT_gpa());
							// 修正データがあったことをフラグ"1"で保持しておく
							judgeBaseDataList.get(i).setT_gpa_ch("1");
						}
					}
				}
				if (modify.getN_gpa() != null) {
					for (int i = 0; i < judgeBaseDataList.size(); i++) {
						if (modify.getG_sysno().equals(judgeBaseDataList.get(i).getG_gakuno())) {
							judgeBaseDataList.get(i).setN_gpa_b(modify.getN_gpa());
							judgeBaseDataList.get(i).setN_gpa_ch("1");
						}
					}
				}
			}

			// 通算GPAの順位づけ
			LinkedHashMap<BigDecimal, Integer> gpaRankMapT = new LinkedHashMap<BigDecimal, Integer>();
			BigDecimal points[] = new BigDecimal[judgeBaseDataList.size()];
			for (int i = 0; i < judgeBaseDataList.size(); i++) {
				points[i] = judgeBaseDataList.get(i).getT_gpa_b() != null ? judgeBaseDataList.get(i).getT_gpa_b() : judgeBaseDataList.get(i).getT_gpa();
			}
			gpaRankMapT = getRankingMap(points);

			// 年度GPAの順位づけ
			LinkedHashMap<BigDecimal, Integer> gpaRankMapN = new LinkedHashMap<BigDecimal, Integer>();
			BigDecimal pointsN[] = new BigDecimal[judgeBaseDataList.size()];
			for (int i = 0; i < judgeBaseDataList.size(); i++) {
				pointsN[i] = judgeBaseDataList.get(i).getN_gpa_b() != null ? judgeBaseDataList.get(i).getN_gpa_b() : judgeBaseDataList.get(i).getN_gpa();
			}
			gpaRankMapN = getRankingMap(pointsN);

			// GPA順位の反映と修得単位率、出席率の計算＆反映
			for (ShugakusienHanteiData judgeBase : judgeBaseDataList) {
				// GPA順位の反映
				judgeBase.setGpa_param(judgeBaseDataList.size());
				judgeBase.setT_gpa_juni(gpaRankMapT.get(judgeBase.getT_gpa_b() != null ? judgeBase.getT_gpa_b() : judgeBase.getT_gpa()));
				judgeBase.setN_gpa_juni(gpaRankMapN.get(judgeBase.getN_gpa_b() != null ? judgeBase.getN_gpa_b() : judgeBase.getN_gpa()));
				// 在学年数の計算(小数点第4位で切り捨て)
				BigDecimal zaisekiGetsusu = BigDecimal.valueOf(judgeBase.getZaiseki_month());
				BigDecimal divideBase = BigDecimal.valueOf(12);
				BigDecimal kyugakuGetsusu = BigDecimal.valueOf(judgeBase.getKyugaku_month());
				BigDecimal zaigakuNensu = zaisekiGetsusu.subtract(kyugakuGetsusu).divide(divideBase, 3, BigDecimal.ROUND_DOWN);
				judgeBase.setZaigaku_nensu(zaigakuNensu);
				// 標準単位数の計算
				BigDecimal hyojunTani = judgeBase.getHitsuyo_tanisu().multiply(zaigakuNensu).divide(BigDecimal.valueOf(judgeBase.getShugyo_nengen()), 0, BigDecimal.ROUND_UP);
				judgeBase.setHyojun_tanisu(hyojunTani);
				// 修得単位率の計算(小数点第4位で四捨五入)
				BigDecimal shutokuTani = judgeBase.getShutoku_tanisu();
				BigDecimal shutokuTaniRitsu = shutokuTani.divide(hyojunTani, 3, BigDecimal.ROUND_HALF_UP);
				judgeBase.setShutoku_ritsu(shutokuTaniRitsu.multiply(BigDecimal.valueOf(100)));
				// 出席率の計算(小数点第4位で四捨五入)
				BigDecimal rishuTani = judgeBase.getRishu_tanisu();
				BigDecimal kessekiTani = judgeBase.getKesseki_tanisu();
				judgeBase.setShusseki_ritsu(BigDecimal.valueOf(0));
				if (rishuTani.signum() > 0) {
					judgeBase.setShusseki_ritsu(BigDecimal.valueOf(100).subtract(kessekiTani.divide(rishuTani, 3, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(100))));
				}

				// 各種判定を行う
				checkRecruit(judgeBase);
				checkAbolished(judgeBase);
				checkWarning(judgeBase);
				checkCancel(judgeBase);
			}

			ret.put("result", true);
			ret.put("dataList", judgeBaseDataList);
		} catch(Exception e) {
			logger.info(e.getMessage());
			ret.put("result", false);
		}
		return ret;
	}

	/**
	 * 渡されたリストの値を元に順位付けしたMapを返す
	 * @param points
	 * @return
	 */
	private LinkedHashMap<BigDecimal, Integer> getRankingMap(BigDecimal points[]) {
		// 昇順にソート
		int len = points.length;
		BigDecimal temp[] = new BigDecimal[len];
		for (int i = 0; i < len; i++) {
			temp[i] = points[i];
		}
		Arrays.sort(temp);

		// 降順にする
		BigDecimal pointsDesc[] = new BigDecimal[len];
		for (int i = 0; i < len; i++) {
			pointsDesc[i] = temp[len - 1 - i];
		}

		// ランクをつける
		LinkedHashMap<BigDecimal, Integer> retMap = new LinkedHashMap<BigDecimal, Integer>();
		int rank = 1;
		retMap.put(pointsDesc[0], rank);

		for (int i = 1; i < len; i++) {
			if (pointsDesc[i].compareTo(pointsDesc[i-1]) != 0) {
				// 点数が異なる場合はrankに通し番号を設定
				rank = i + 1;
			}
			// すでに存在するキーでなければ登録する
			if (retMap.containsKey(pointsDesc[i]) == false) {
				retMap.put(pointsDesc[i], rank);
			}
		}

		return retMap;
	}

	/**
	 * 元データを使って在学採用の基準を満たすかチェックする
	 * @param judgeBase
	 * @return
	 */
	private boolean checkRecruit(ShugakusienHanteiData judgeBase) {
		boolean hantei = false;

		// 初期化
		judgeBase.setZ_gpa_b(NOTCLEAR);
		judgeBase.setZ_tani_b(NOTCLEAR);
		judgeBase.setZ_sotsu_b(NOTCLEAR);

		// GPAが上位2分の1である
		Integer gpaJuniBase = judgeBase.getGpa_param() / 2;
		if (judgeBase.getT_gpa_juni() <= gpaJuniBase) {
			judgeBase.setZ_gpa_b(CLEAR);
			// GPAの修正が行われた結果でCLEARの場合は、手修正部分も合わせる
			if ("1".equals(judgeBase.getT_gpa_ch())) {
				judgeBase.setZ_gpa(CLEAR);
			}
			hantei = true;
		}
		// 修得単位数が標準単位数以上
		if (judgeBase.getShutoku_tanisu().compareTo(judgeBase.getHyojun_tanisu()) >= 0) {
			judgeBase.setZ_tani_b(CLEAR);
			hantei = true;
		}
		// 留年データがなければCLEAR
		if (judgeBase.getRyunendata() == null || "".equals(judgeBase.getRyunendata())) {
			judgeBase.setZ_sotsu_b(CLEAR);
			hantei = true;
		}
		return hantei;
	}

	/**
	 * 元データを使って廃止の基準に該当しないかチェックする
	 * @param judgeBase
	 * @return 廃止基準に該当する場合はfalse
	 */
	private boolean checkAbolished(ShugakusienHanteiData judgeBase) {
		boolean hantei = true;

		// 初期化
		judgeBase.setH_sotsu_b(CLEAR);
		judgeBase.setH_tani_b(CLEAR);
		judgeBase.setH_shu_b(CLEAR);
		judgeBase.setH_zen_b(CLEAR);

		// 修得単位率が5割以下
		if (judgeBase.getShutoku_ritsu().compareTo(new BigDecimal("50.0")) < 1) {
			judgeBase.setH_tani_b(NOTCLEAR);
			hantei = false;
		}
		// 出席率が5割以下
		if (judgeBase.getShusseki_ritsu().compareTo(new BigDecimal("50.0")) < 1) {
			judgeBase.setH_shu_b(NOTCLEAR);
			hantei = false;
		}
		// 前年度が警告
		if (WARNING.equals(judgeBase.getShien_jokyo())) {
			judgeBase.setH_zen_b(NOTCLEAR);
			hantei = false;
		}
		return hantei;
	}

	/**
	 * 元データを使って警告の基準に該当しないかチェックする
	 * @param judgeBase
	 * @return 警告基準に該当する場合はfalse
	 */
	private boolean checkWarning(ShugakusienHanteiData judgeBase) {
		boolean hantei = true;

		// 初期化
		judgeBase.setK_tani_b(CLEAR);
		judgeBase.setK_gpa_b(CLEAR);
		judgeBase.setK_shu_b(CLEAR);
		judgeBase.setT_sotsu_b(CLEAR);

		// 修得単位率が6割以下
		if (judgeBase.getShutoku_ritsu().compareTo(new BigDecimal("60.0")) < 1) {
			judgeBase.setK_tani_b(NOTCLEAR);
			hantei = false;
		}
		// GPAが下位4分の1である
		BigDecimal gpaJuniBase = BigDecimal.valueOf(judgeBase.getGpa_param()).divide(BigDecimal.valueOf(4), 3, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(3));
		if (gpaJuniBase.compareTo(BigDecimal.valueOf(judgeBase.getN_gpa_juni())) < 0) {
			judgeBase.setK_gpa_b(NOTCLEAR);
			// GPAの修正が行われた結果でNOTCLEARの場合は、手修正部分も合わせる
			if ("1".equals(judgeBase.getN_gpa_ch())) {
				judgeBase.setK_gpa(NOTCLEAR);
			}
			hantei = false;
		}
		// 出席率が8割以下
		if (judgeBase.getShusseki_ritsu().compareTo(new BigDecimal("80.0")) < 1) {
			judgeBase.setK_shu_b(NOTCLEAR);
			hantei = false;
		}
		// 適格認定時は当該年度のデータが存在する場合に留年となる
		if (judgeBase.getRyunendata() != null && judgeBase.getRyunendata().indexOf(pramDto.getShoriJokenMst().getShorinendo().toString()) != -1) {
			judgeBase.setT_sotsu_b(NOTCLEAR);
			hantei = false;
		}
		return hantei;
	}

	/**
	 * 元データを使って遡及取消の基準に該当しないかチェックする
	 * @param judgeBase
	 * @return 遡及取消基準に該当する場合はfalse
	 */
	private boolean checkCancel(ShugakusienHanteiData judgeBase) {
		boolean hantei = true;

		// 初期化
		judgeBase.setS_tani_b(CLEAR);
		judgeBase.setS_shu_b(CLEAR);

		// 修得単位率が1割以下
		if (judgeBase.getShutoku_ritsu().compareTo(new BigDecimal("10.0")) < 1) {
			judgeBase.setS_tani_b(NOTCLEAR);
			hantei = false;
		}
		// 出席率が1割以下
		if (judgeBase.getShusseki_ritsu().compareTo(new BigDecimal("10.0")) < 1) {
			judgeBase.setS_shu_b(NOTCLEAR);
			hantei = false;
		}
		return hantei;
	}

	@Override
	protected Boolean deleteJsonData() {
		return null;
	}

	@Override
	protected Boolean addJsonData() {
		String method_name = "addJsonData";
		logger.info(CommonConst.START_LOG + method_name);
		Boolean ret = false;

		try {
			// パラメータの分解と設定
			String[] paramList = null;
			if (pramDto.getEtcParam() != null) {
				paramList = pramDto.getEtcParam().split("#");
			}

			if (paramList != null) {
				// まず採番
				ShugakusienHanteiDataDao dataDao = new ShugakusienHanteiDataDao();
				ArrayList<ShugakusienHanteiData> maxNumList = new ArrayList<ShugakusienHanteiData>();
				maxNumList = dataDao.getMaxNum(pramDto);
				int maxNum = 1;
				if (maxNumList != null && maxNumList.size() > 0) {
					maxNum = maxNumList.get(0).getNum();
				}
				pramDto.setNum(maxNum);

				// 登録用のオブジェクト設定
				ArrayList<ShugakusienHanteiData> dataList = new ArrayList<ShugakusienHanteiData>();
				for (int i = 0; i < paramList.length; i++) {
					String[] param = paramList[i].split("_");
					ShugakusienHanteiData data = new ShugakusienHanteiData();
					data.setG_gakuno(param[G_SYSNO]);
					data.setJudge_type(param[JUDGE_TYPE]);
					data.setZ_judge(param[Z_JUDGE]);
					data.setT_judge(param[T_JUDGE]);
					data.setBikou(param[BIKOU]);
					data.setT_gpt(param[T_GPT] != null ? new BigDecimal(param[T_GPT]) : new BigDecimal('0'));
					data.setT_gp_tanisu(param[T_GP_TANISU] != null ? new BigDecimal(param[T_GP_TANISU]) : new BigDecimal('0'));
					data.setT_gpa_juni(param[T_GPA_JUNI] != null ? Integer.parseInt(param[T_GPA_JUNI]) : 0);
					data.setN_gpt(param[N_GPT] != null ? new BigDecimal(param[N_GPT]) : new BigDecimal('0'));
					data.setN_gp_tanisu(param[N_GP_TANISU] != null ? new BigDecimal(param[N_GP_TANISU]) : new BigDecimal('0'));
					data.setN_gpa_juni(param[N_GPA_JUNI] != null ? Integer.parseInt(param[N_GPA_JUNI]) : 0);
					data.setGpa_param(param[GPA_PARAM] != null ? Integer.parseInt(param[GPA_PARAM]) : 0);
					data.setZaiseki_month(param[ZAISEKI_MONTH] != null ? Integer.parseInt(param[ZAISEKI_MONTH]) : 0);
					data.setKyugaku_month(param[KYUGAKU_MONTH] != null ? Integer.parseInt(param[KYUGAKU_MONTH]) : 0);
					data.setZaigaku_nensu(param[ZAIGAKU_NENSU] != null ? new BigDecimal(param[ZAIGAKU_NENSU]) : new BigDecimal('0'));
					data.setHitsuyo_tanisu(param[HITSUYO_TANISU] != null ? new BigDecimal(param[HITSUYO_TANISU]) : new BigDecimal('0'));
					data.setShugyo_nengen(param[SHUGYO_NENGEN] != null ? Integer.parseInt(param[SHUGYO_NENGEN]) : 0);
					data.setHyojun_tanisu(param[HYOJUN_TANISU] != null ? new BigDecimal(param[HYOJUN_TANISU]) : new BigDecimal('0'));
					data.setShutoku_tanisu(param[SHUTOKU_TANISU] != null ? new BigDecimal(param[SHUTOKU_TANISU]) : new BigDecimal('0'));
					data.setShutoku_ritsu(param[SHUTOKU_RITSU] != null ? new BigDecimal(param[SHUTOKU_RITSU]) : new BigDecimal('0'));
					data.setRishu_tanisu(param[RISHU_TANISU] != null ? new BigDecimal(param[RISHU_TANISU]) :  new BigDecimal('0'));
					data.setKesseki_tanisu(param[KESSEKI_TANISU] != null ? new BigDecimal(param[KESSEKI_TANISU]) : new BigDecimal('0'));
					data.setShusseki_ritsu(param[SHUSSEKI_RITSU] != null ? new BigDecimal(param[SHUSSEKI_RITSU]) : new BigDecimal('0'));
					data.setRyunendata(param[RYUNENDATA]);
					data.setKeikakusho(param[KEIKAKUSHO]);
					data.setZ_gpa(param[Z_GPA]);
					data.setZ_gpa_b(param[Z_GPA_B]);
					data.setZ_tani(param[Z_TANI]);
					data.setZ_tani_b(param[Z_TANI_B]);
					data.setH_sotsu(param[H_SOTSU]);
					data.setH_sotsu_b(param[H_SOTSU_B]);
					data.setH_tani(param[H_TANI]);
					data.setH_tani_b(param[H_TANI_B]);
					data.setH_shu(param[H_SHU]);
					data.setH_shu_b(param[H_SHU_B]);
					data.setH_zen(param[H_ZEN]);
					data.setH_zen_b(param[H_ZEN_B]);
					data.setK_tani(param[K_TANI]);
					data.setK_tani_b(param[K_TANI_B]);
					data.setK_gpa(param[K_GPA]);
					data.setK_gpa_b(param[K_GPA_B]);
					data.setK_shu(param[K_SHU]);
					data.setK_shu_b(param[K_SHU_B]);
					data.setS_tani(param[S_TANI]);
					data.setS_tani_b(param[S_TANI_B]);
					data.setS_shu(param[S_SHU]);
					data.setS_shu_b(param[S_SHU_B]);
					data.setT_gpa(param[T_GPA] != null ? new BigDecimal(param[T_GPA]) : new BigDecimal('0'));
					data.setT_gpa_b(param[T_GPA_B] != null ? new BigDecimal(param[T_GPA_B]) : new BigDecimal('0'));
					data.setN_gpa(param[N_GPA] != null ? new BigDecimal(param[N_GPA]) : new BigDecimal('0'));
					data.setN_gpa_b(param[N_GPA_B] != null ? new BigDecimal(param[N_GPA_B]) : new BigDecimal('0'));
					data.setG_name(param[G_NAME]);
					dataList.add(data);
				}
				// データ追加
				dataDao.addJudgeData(pramDto, dataList);
			}

			ret = true;
		} catch(Exception e) {
			logger.info(e.getMessage());
		}
		return ret;
	}

	protected LinkedHashMap<String, Object> makeRetJson(Boolean ret){
		LinkedHashMap<String, Object> retMap = new LinkedHashMap<String, Object>();

		if (ret) {
			retMap.put("status", true);
		} else {
			retMap.put("status", false);
		}
		return retMap;
	}

	@Override
	protected boolean checkParams(Map<String, Object> params) {
		return true;
	}
}
