package rest.pf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.GpaShugakuninteiBukyokuMstDao;
import dao.GpaShugakuninteiGpMstDao;
import dao.KakuteiSeisekiDao;
import dao.NendobetuShozokuDao;
import dao.entity.GpaShugakuninteiBukyokuMst;
import dao.entity.KakuteiSeiseki;
import dao.entity.NendobetuShozoku;

/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class GpaShugakuninteiBukyokuMstService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public GpaShugakuninteiBukyokuMstService() {
        //権限ID
        rid = 2;
        mode = 0;
    }

    /**
     * (rest.)pf/programs.json?lang="ja"<br />
     * ユーザIDが持つ参照権限のリストを返す。(AuthorityTable()参照)
     * @return : JSON String
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();
        ArrayList<Object> aaData = new ArrayList<Object>();
        ArrayList<Object> hyogonmResult = new ArrayList<Object>();
        GpaShugakuninteiBukyokuMstDao GpaShugakuninteiBukyokuMstDao = new GpaShugakuninteiBukyokuMstDao();
        ArrayList<GpaShugakuninteiBukyokuMst> aut = GpaShugakuninteiBukyokuMstDao.getGpaShugakuninteiBukyokuMst();
        KakuteiSeisekiDao KakuteiSeisekiDao = new KakuteiSeisekiDao();
        ArrayList<KakuteiSeiseki> hyogonm = KakuteiSeisekiDao.getKakuteiSeisekiList04();
        NendobetuShozokuDao NendobetuShozokuDao = new NendobetuShozokuDao();

        for(GpaShugakuninteiBukyokuMst gpaShugakuninteiBukyokuMst : aut){
            ArrayList<NendobetuShozoku> shozoku = NendobetuShozokuDao.getShozokunm01(gpaShugakuninteiBukyokuMst.getgBukyokucd());
            ArrayList<Object> o = new ArrayList<Object>();
            o.add(null);
            o.add(gpaShugakuninteiBukyokuMst.getgBukyokucd());
            if (shozoku != null && shozoku.size() > 0) {
                o.add(shozoku.get(0).getShozokunm());
            } else {
                if ("-1".equals(gpaShugakuninteiBukyokuMst.getgBukyokucd().toString())) {
                    o.add("全学共通");
                } else {
                    o.add("");
                }
            }
            o.add(gpaShugakuninteiBukyokuMst.getShori_start());
            o.add(gpaShugakuninteiBukyokuMst.getShori_end());
            aaData.add(o);

        }

        for(KakuteiSeiseki kakutei : hyogonm){
        	ArrayList<Object> o = new ArrayList<Object>();
        	o.add(kakutei.getHyogonm());
        	hyogonmResult.add(o);
        }
        ret.put("aaData", aaData);
        ret.put("hyogonm",hyogonmResult);
        ret.put("aoColumns", aoColumns.get(pramDto.getLang()));


        return ret;
    }

    final Map<String, ArrayList<Map<String,String>>> aoColumns = // DBの構造に入っていないのでしかたなく
        new HashMap<String, ArrayList<Map<String,String>>>() {{
            put(CommonConst.JAPANESE, new ArrayList<Map<String,String>>() {{
                add(new HashMap<String,String>() {{
                    put("sTitle", "削除");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "部局コード");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "部局名");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "利用開始年度");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "利用終了年度");
                }});
            }});
            put(CommonConst.ENGLISH, new ArrayList<Map<String,String>>() {{
                add(new HashMap<String,String>() {{
                    put("sTitle", "delete");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "bukyokucd");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "bukyokumei");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "kaishibi");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "syuryobi");
                }});
            }});
        }};

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    @Override
    protected Boolean deleteJsonData() {
       String method_name = "deleteJsonData";
       logger.info(CommonConst.END_LOG + method_name);

       GpaShugakuninteiBukyokuMstDao gpaShugakuninteiBukyokuMstDao = new GpaShugakuninteiBukyokuMstDao();
       Boolean res = gpaShugakuninteiBukyokuMstDao.deleteGpaShugakuninteiBukyokuMst01(pramDto);
       // 部局を消したら紐づくデータも消す
       GpaShugakuninteiGpMstDao gpaShugakuninteiGpMstDao = new GpaShugakuninteiGpMstDao();
       res = gpaShugakuninteiGpMstDao.deleteGpaShugakuninteiGpMst01(pramDto);

       return res;
    }

    @Override
    protected Boolean addJsonData() {
       String method_name = "addJsonData";
       logger.info(CommonConst.END_LOG + method_name);
       System.out.println(pramDto.getGpaShugakuninteiBukyokuMst().getgBukyokucd());
       System.out.println(pramDto.getGpaShugakuninteiBukyokuMst().getShori_start());
       System.out.println(pramDto.getGpaShugakuninteiBukyokuMst().getShori_end());
       GpaShugakuninteiBukyokuMstDao gpaShugakuninteiBukyokuMstDao = new GpaShugakuninteiBukyokuMstDao();
       Boolean res = gpaShugakuninteiBukyokuMstDao.addGpaShugakuninteiBukyokuMst01(pramDto);

       return res;
    }
}
