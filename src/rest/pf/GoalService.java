package rest.pf;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractListService;
import util.Tools;
import constants.CommonConst;
import dao.GoalsMstDao;
import dao.entity.GoalsMst;
//+ for using variable request / request変数を利用するため

/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class GoalService extends AbstractListService{
    private static Logger logger = Tools.getCallerLogger();

    public GoalService() {
        //権限ID
        rid = 0;
        mode = 0;
    }

    /**
<pre class="brush: html; gutter: false; highlight: 1; toolbar: false;">
(rest.)pf/goal.json?lang="ja"&num=X&exp
</pre>
                学習成果(学科毎の枝番は除く)のリストを返す。numがあれば番号を指定、
                指定がないかnumのみなら7つの学習成果のみ、expがあれば説明も返す(※)<br />
<pre class="brush: js; toolbar: false;">
[
 {"num":1, "title":"豊かな教養", "exp":"教養ある社会人に必要な文化・社会や..."},
 {...},
 {...},
}
</pre>
</pre>
     * @return : Array
     */
    @Override
    public ArrayList<Object> getJsonData() {
        String method_name  = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        ArrayList<Object> ret = new ArrayList<Object>();

        GoalsMstDao goalsMstDao = new GoalsMstDao();
        ArrayList<GoalsMst> res = null;

        if ((pramDto.getNum() != -1) && (pramDto.getNum() != 0) && (pramDto.getNum() != 99)){
            res = goalsMstDao.getGoalsMstList01(pramDto);
        }
        else if((pramDto.getNum() == 0)) {
            LinkedHashMap<String,Object> line = new LinkedHashMap<String,Object>();

            line.put("num", 0);
            line.put("title", getLangObj("その他","Other"));
            line.put("exp", "");

            ret.add(line);

            return ret;

        }
        else {
            res = goalsMstDao.getGoalsMstList02(pramDto);
        }

        for (GoalsMst goalsMst : res){
            LinkedHashMap<String,Object> line = new LinkedHashMap<String,Object>();

            line.put("num", goalsMst.getCode());
            line.put("title", getLangObj(goalsMst.getName(),goalsMst.getNameEng()));
            if(pramDto.getExp()){
                line.put("exp", getLangObj(goalsMst.getExpl(),goalsMst.getExplEng()));
            }

            ret.add(line);

        }

        // 2015.02.12 SVC 追加 >>
        // 99の場合「その他」を追加する
        if (pramDto.getNum() == 99){
            LinkedHashMap<String,Object> line = new LinkedHashMap<String,Object>();
            line.put("num", 0);
            line.put("title", getLangObj("その他","Other"));
            line.put("exp", "");
            ret.add(line);
        }
        // 2015.02.12 SVC 追加 <<

        logger.info("Allowed ruser="+pramDto.getRuser()+" at lang="+pramDto.getLang()+", ret="+ret);

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
       logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

}
