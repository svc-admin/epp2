package rest.pf;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ku.util.SystemUtil;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.PropertiesUtil;
import util.Tools;
import constants.CommonConst;
import constants.HtmlTemplates;
import dao.AttachmentsDao;
import dao.BunruiMstDao;
import dao.ExternalEnglishTestsDao;
import dao.ResultsDao;
import dao.SeisekiForEvidenceDao;
import dao.StudyingAbroadExperiencesDao;
import dao.entity.Attachments;
import dao.entity.BunruiMst;
import dao.entity.ExternalEnglishTests;
import dao.entity.Results;
import dao.entity.SeisekiForEvidence;
import dao.entity.StudyingAbroadExperieces;
import dto.ParameterDto;
import dto.SysPropertiesDto;

/**
 *  REST service using JSONIC, version 2015-02-05 (since 2015-02-05)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class InputResultService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();
    private SysPropertiesDto sysPropDto = null;
    boolean enFlag = false;

// 2019.01.21 s.furuta add start 2.学修成果修正
    private String editKamokuCd = "";
    private String editInsertDate = "";
// 2019.01.21 s.furuta add end
// 2019.03.26 s.furuta add start 顧客検証No2
    private String commitKamokuCd = "";
// 2019.03.26 s.furuta add end

    // コンストラクタ
    public InputResultService() {
        rid = 0;
        mode = 0;
        PropertiesUtil propUtil = new PropertiesUtil();
        sysPropDto = propUtil.getSysProperties();
    }

    /**
     * (rest.)pf/inputResults.json?<br />
     * @see rest.AbstractMapService#getJsonData()
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();
        BunruiMstDao bunDao = new BunruiMstDao();
        ArrayList<Object> bunruiList = new ArrayList<Object>();
        ArrayList<BunruiMst> res = null;

        // 分類名を取得
        res = bunDao.getBunruiMst01(pramDto);
        for(BunruiMst bun : res){
            LinkedHashMap<String,Object> line = new LinkedHashMap<String,Object>();

            line.put("code", bun.getBunruicd());
            line.put("value", getLangObj(bun.getBunruinm(), bun.getBunruinmeng()));

            bunruiList.add(line);
        }
        ret.put("bunruiList", bunruiList);

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    /* (非 Javadoc)
     * @see rest.AbstractMapService#deleteJsonData()
     */
    @Override
    protected Boolean deleteJsonData() {
        String method_name = "deleteJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        SeisekiForEvidenceDao dao = new SeisekiForEvidenceDao();
        dao.deleteSeisekiForEvidence01(pramDto);

        logger.info(CommonConst.END_LOG + method_name);
        return true;
    }

    /**
     * (rest.)pf/inputResults.json3<br />
     * 画面の情報を保存する
     * @see rest.AbstractMapService#addJsonData()
     * @return : true/false
     */
    @Override
    protected Boolean addJsonData() {
        String method_name = "addJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        pramDto.setSeisekiForEvidence(new SeisekiForEvidence());
        pramDto.setAttachments(new Attachments());
        pramDto.setExternalEnglishTests(new ExternalEnglishTests());
        pramDto.setResults(new Results());
        pramDto.setStudyingAbroadExperiences(new StudyingAbroadExperieces());

        // 実体の保存
        this.saveFile(pramDto);

        // データの保存（seiseki_for_evidence）
        this.saveSeisekiForEvidence(pramDto);

        logger.info(CommonConst.END_LOG + method_name);
        return true;
    }

    /* (非 Javadoc)
     * @see rest.AbstractMapService#checkParams(java.util.Map)
     */
    @Override
    protected boolean checkParams(Map<String, Object> params) {
        return true;
    }
    /*
     * 成果物の実体（file）を保存
     */
    private boolean saveFile(ParameterDto paramDto) {
        String method_name = "saveFile";
        logger.info(CommonConst.START_LOG + method_name);

        try{
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            List<FileItem> uploadItems = upload.parseRequest(request);

// 2019.01.21 s.furuta add start 2.学修成果修正
            // 科目コードを取得（この有無によって、新規追加か編集かを判別）
            editKamokuCd = fetchKamokuCd(uploadItems.iterator());
            // 追加日を取得（修正時、insert_dateは初期登録時のもの引き継ぐ）
            editInsertDate = fetchInsertDate(uploadItems.iterator());
            if("".equals(editInsertDate) || editInsertDate.isEmpty() || editInsertDate.equals(null)){
                // 【追加】
                java.sql.Date today = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
                paramDto.getSeisekiForEvidence().setInsertDate(today);
                paramDto.getResults().setInsertDate(today);
                paramDto.getExternalEnglishTests().setInsertDate(today);
                paramDto.getStudyingAbroadExperiences().setInsertDate(today);
            } else {
                // 【修正】
                Timestamp ins = new Timestamp(new SimpleDateFormat("yyyy/mm/dd").parse(editInsertDate).getTime());
                paramDto.getSeisekiForEvidence().setInsertDate(ins);
                paramDto.getResults().setInsertDate(ins);
                paramDto.getExternalEnglishTests().setInsertDate(ins);
                paramDto.getStudyingAbroadExperiences().setInsertDate(ins);
            }
// 2019.01.21 s.furuta add end

            // 分類を取得
            String groupName = fetchGroupName(uploadItems.iterator());
            String txtGroupName = fetchTxtGroupName(uploadItems.iterator());
            CommonConst.ResultGroup resultGroup = CommonConst.ResultGroup.common;
            if (txtGroupName.isEmpty()) {
                resultGroup = getResultGroup(groupName);
            }

            // 分類に応じて HTML の雛型を分岐
            // TODO: 分類マスタ変更時には都度書き換え
            enFlag = getEnFlag(groupName);
            String outHtml = "";
            String tempFlg = "";
            switch(resultGroup){
            case paper:
                // 論文
                outHtml = enFlag ? HtmlTemplates.JOURNAL_PAPER_EN : HtmlTemplates.JOURNAL_PAPER_JA;
                tempFlg = "4";
                break;
            case presentation:
                // 学会発表
                outHtml = enFlag ? HtmlTemplates.PRESENTATION_EN : HtmlTemplates.PRESENTATION_JA;
                tempFlg = "4";
                break;
            case EnglishTest:
                // 英語外部試験
                outHtml = enFlag ? HtmlTemplates.ENGLISH_TEST_EN : HtmlTemplates.ENGLISH_TEST_JA;
                tempFlg = enFlag ? "2" : "3";
                break;
            case studyingAbroad:
                // 海外での学修経験
                outHtml = enFlag ? HtmlTemplates.STUDYING_ABROAD_EN : HtmlTemplates.STUDYING_ABROAD_JA;
                tempFlg = enFlag ? "2" : "3";
                break;
            default:
                // 共通
                outHtml = HtmlTemplates.COMMON;
                tempFlg = "1";
            }

            String scoreStr ="";
            String abroadStr = "";
            String selGroup = "";
            Integer intGoalValue = 0;
            paramDto.getSeisekiForEvidence().setCode1(0);
            paramDto.getSeisekiForEvidence().setCode2(0);
            paramDto.getSeisekiForEvidence().setCode3(0);
            paramDto.getSeisekiForEvidence().setCode4(0);
            paramDto.getSeisekiForEvidence().setCode5(0);
            paramDto.getSeisekiForEvidence().setCode6(0);
            paramDto.getSeisekiForEvidence().setCode7(0);

            // データ保存用テーブルの主キーを生成（seiseki_for_evidenceと同値）
            SeisekiForEvidenceDao seiEviDao = new SeisekiForEvidenceDao();
            ResultsDao resultDao = new ResultsDao();
            AttachmentsDao attachDao = new AttachmentsDao();
            ExternalEnglishTestsDao extEngDao = new ExternalEnglishTestsDao();
            StudyingAbroadExperiencesDao stuAbExpDao = new StudyingAbroadExperiencesDao();
            String kamokucdStr = "";
            paramDto.getSeisekiForEvidence().setgSysno(paramDto.getUid());
// 2019.01.21 s.furuta chg start 2.学修成果修正
//            kamokucdStr = seiEviDao.checkGSysNoMakeCode(paramDto);
            if(editKamokuCd.equals(null) || editKamokuCd.equals("")) {
                kamokucdStr = seiEviDao.checkGSysNoMakeCode(paramDto);
            } else {
                kamokucdStr = editKamokuCd;
                // 既存データ削除
                paramDto.setKamokuCd(kamokucdStr);
                resultDao.deleteResults01(paramDto);
                extEngDao.deleteExternalEnglishTests01(paramDto);
                stuAbExpDao.deleteStudyingAbroadExperiences01(paramDto);
            }
// 2019.01.21 s.furuta chg end
// 2019.03.26 s.furuta add start 顧客検証No2
            commitKamokuCd = kamokucdStr;
// 2019.03.26 s.furuta add end
            paramDto.getResults().setgSysno(paramDto.getUid());
            paramDto.getResults().setKamokucd(kamokucdStr);
            paramDto.getAttachments().setgSysno(paramDto.getUid());
            paramDto.getAttachments().setKamokucd(kamokucdStr);
            // 英語外部試験スコアは最大数分インスタンス化
            ExternalEnglishTests extEngTest1 = new ExternalEnglishTests();
            ExternalEnglishTests extEngTest2 = new ExternalEnglishTests();
            ExternalEnglishTests extEngTest3 = new ExternalEnglishTests();
            ExternalEnglishTests extEngTest4 = new ExternalEnglishTests();
            ExternalEnglishTests extEngTest5 = new ExternalEnglishTests();
            ExternalEnglishTests extEngTest6 = new ExternalEnglishTests();
            ExternalEnglishTests extEngTest7 = new ExternalEnglishTests();
            ExternalEnglishTests extEngTest8 = new ExternalEnglishTests();
// 2019.02.01 s.furuta add start 2.学修成果修正
            extEngTest1.setInsertDate(paramDto.getExternalEnglishTests().getInsertDate());
            extEngTest2.setInsertDate(paramDto.getExternalEnglishTests().getInsertDate());
            extEngTest3.setInsertDate(paramDto.getExternalEnglishTests().getInsertDate());
            extEngTest4.setInsertDate(paramDto.getExternalEnglishTests().getInsertDate());
            extEngTest5.setInsertDate(paramDto.getExternalEnglishTests().getInsertDate());
            extEngTest6.setInsertDate(paramDto.getExternalEnglishTests().getInsertDate());
            extEngTest7.setInsertDate(paramDto.getExternalEnglishTests().getInsertDate());
            extEngTest8.setInsertDate(paramDto.getExternalEnglishTests().getInsertDate());
// 2019.02.01 s.furuta add end
            // 海外での学修成果は最大数分インスタンス化
            StudyingAbroadExperieces stuAbExp1 = new StudyingAbroadExperieces();
            StudyingAbroadExperieces stuAbExp2 = new StudyingAbroadExperieces();
            StudyingAbroadExperieces stuAbExp3 = new StudyingAbroadExperieces();
            StudyingAbroadExperieces stuAbExp4 = new StudyingAbroadExperieces();
            StudyingAbroadExperieces stuAbExp5 = new StudyingAbroadExperieces();
// 2019.02.01 s.furuta add start 2.学修成果修正
            stuAbExp1.setInsertDate(paramDto.getStudyingAbroadExperiences().getInsertDate());
            stuAbExp2.setInsertDate(paramDto.getStudyingAbroadExperiences().getInsertDate());
            stuAbExp3.setInsertDate(paramDto.getStudyingAbroadExperiences().getInsertDate());
            stuAbExp4.setInsertDate(paramDto.getStudyingAbroadExperiences().getInsertDate());
            stuAbExp5.setInsertDate(paramDto.getStudyingAbroadExperiences().getInsertDate());
// 2019.02.01 s.furuta add end

            paramDto.getStudyingAbroadExperiences().setgSysno(paramDto.getUid());
            paramDto.getStudyingAbroadExperiences().setKamokucd(kamokucdStr);

            Iterator<?> iter = uploadItems.iterator();
            while(iter.hasNext()){
                FileItem item = (FileItem)iter.next();
                if(!item.isFormField()) continue;

                // ファイル以外のフォームデータの処理
                String name = item.getFieldName();
                String value = item.getString("utf-8");
                switch(name){
                case "txtTitle":
                    outHtml = outHtml.replaceAll("TEMP_TITLE", value);
                    paramDto.getSeisekiForEvidence().setEvidence(value);
                    paramDto.getResults().setTitle(value);
                    break;
                case "selGroup":
                    selGroup = value;
                    paramDto.getResults().setClassification(groupName);
                    break;
                case "txtGroup":
                    paramDto.getResults().setClassification(groupName);
                    break;
                case "selGoal":
                    intGoalValue++; // あとで割合の計算につかうために数をカウント
                    switch(value){
                    case "1":
                        paramDto.getSeisekiForEvidence().setCode1(1);
                        break;
                    case "2":
                        paramDto.getSeisekiForEvidence().setCode2(1);
                        break;
                    case "3":
                        paramDto.getSeisekiForEvidence().setCode3(1);
                        break;
                    case "4":
                        paramDto.getSeisekiForEvidence().setCode4(1);
                        break;
                    case "5":
                        paramDto.getSeisekiForEvidence().setCode5(1);
                        break;
                    case "6":
                        paramDto.getSeisekiForEvidence().setCode6(1);
                        break;
                    case "7":
                        paramDto.getSeisekiForEvidence().setCode7(1);
                        break;
                    }
                    break;
                case "txtDate":
                    Timestamp ts = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    paramDto.getSeisekiForEvidence().setLastUpdate(ts);
                    outHtml = outHtml.replaceAll("TEMP_TIMESTAMP", ts.toString());
                    SystemUtil sysUtil = new SystemUtil();
                    paramDto.getSeisekiForEvidence().setNinteiNendo(sysUtil.getFiscalYear(new Date(ts.getTime())));
                    paramDto.getSeisekiForEvidence().setNinteiGakkikbncd(sysUtil.getGakkiKbnCD(new Date(ts.getTime())));
                    paramDto.getResults().setDate(ts);
                    break;
                case "txtComment":
                    paramDto.getSeisekiForEvidence().setComment(value);
                    paramDto.getResults().setComment(value);
                    if(tempFlg.equals("1") || tempFlg.equals("4")){
                        outHtml = outHtml.replaceAll("TEMP_COMMENT", value);
                    }
                    break;
                case "txtNote":
                    if(tempFlg.equals("2") || tempFlg.equals("3")){
                        paramDto.getSeisekiForEvidence().setComment(value);
                        paramDto.getResults().setComment(value);
                        outHtml = outHtml.replaceAll("TEMP_COMMENT", value);
                    }
                    break;
                // 英語外部試験スコア
                case "textToeicScore":
                    if(value.isEmpty()) break;
                    scoreStr += tempFlg == "2" ? "<tr><td width='40%'>TOEIC</td><td width='20%' align='right'>"+value+"points</td>" : "<tr><td width='40%'>TOEIC</td><td width='20%' align='right'>"+value+"点</td>";
                    extEngTest1.setTestName("TOEIC");
                    extEngTest1.setScore(value);
                    break;
                case "textToeflpScore":
                    if(value.isEmpty()) break;
                    scoreStr += tempFlg == "2" ? "<tr><td width='40%'>TOEFL PBT</td><td width='20%' align='right'>"+value+"points</td>" : "<tr><td width='40%'>TOEFL PBT</td><td width='20%' align='right'>"+value+"点</td>";
                    extEngTest2.setTestName("TOEFL PBT");
                    extEngTest2.setScore(value);
                    break;
                case "textToefliScore":
                    if(value.isEmpty()) break;
                    scoreStr += tempFlg == "2" ? "<tr><td width='40%'>TOEFL IBT</td><td width='20%' align='right'>"+value+"points</td>" : "<tr><td width='40%'>TOEFL IBT</td><td width='20%' align='right'>"+value+"点</td>";
                    extEngTest3.setTestName("TOEFL IBT");
                    extEngTest3.setScore(value);
                    break;
                case "textIeltsScore":
                    if(value.isEmpty()) break;
                    scoreStr += tempFlg == "2" ? "<tr><td width='40%'>IELTS</td><td width='20%' align='right'>"+value+"points</td>" : "<tr><td width='40%'>IELTS</td><td width='20%' align='right'>"+value+"点</td>";
                    extEngTest4.setTestName("IELTS");
                    extEngTest4.setScore(value);
                    break;
                case "selectEiken":
                    if(value.isEmpty() || value.equals("none")) break;
                    scoreStr += tempFlg == "2" ? "<tr><td width='40%'>EIKEN</td><td width='20%' align='right'>"+value+"</td>" : "<tr><td width='40%'>実用英語技能検定（英検）</td><td width='20%' align='right'>"+value+"級</td>";
                    extEngTest5.setTestName(tempFlg == "2" ? "EIKEN" : "実用英語技能検定（英検）");
                    extEngTest5.setScore(value);
                    break;
                case "textSonota1Score":
                    if(value.isEmpty()) break;
                    scoreStr += tempFlg == "2" ? "<td width='20%' align='right'>"+value+"points（grade）</td>" : "<td width='20%' align='right'>"+value+"点（級）</td>";
                    extEngTest6.setScore(value);
                    break;
                case "textSonota2Score":
                    if(value.isEmpty()) break;
                    scoreStr += tempFlg == "2" ? "<td width='20%' align='right'>"+value+"points（grade）</td>" : "<td width='20%' align='right'>"+value+"点（級）</td>";
                    extEngTest7.setScore(value);
                    break;
                case "textSonota3Score":
                    if(value.isEmpty()) break;
                    scoreStr += tempFlg == "2" ? "<td width='20%' align='right'>"+value+"points（grade）</td>" : "<td width='20%' align='right'>"+value+"点（級）</td>";
                    extEngTest8.setScore(value);
                    break;
                case "textToeicDate":
                    if(value.isEmpty()) break;
                    scoreStr += "<td width='40%' align='center'>"+this.dateEditor(value)+"</td></tr>";
                    Timestamp ts1 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    extEngTest1.setDate(ts1);
                    break;
                case "textToeflpDate":
                    if(value.isEmpty()) break;
                    scoreStr += "<td width='40%' align='center'>"+this.dateEditor(value)+"</td></tr>";
                    Timestamp ts2 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    extEngTest2.setDate(ts2);
                    break;
                case "textToefliDate":
                    if(value.isEmpty()) break;
                    scoreStr += "<td width='40%' align='center'>"+this.dateEditor(value)+"</td></tr>";
                    Timestamp ts3 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    extEngTest3.setDate(ts3);
                    break;
                case "textIeltsDate":
                    if(value.isEmpty()) break;
                    scoreStr += "<td width='40%' align='center'>"+this.dateEditor(value)+"</td></tr>";
                    Timestamp ts4 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    extEngTest4.setDate(ts4);
                    break;
                case "textEikenDate":
                    if(value.isEmpty()) break;
                    scoreStr += "<td width='40%' align='center'>"+this.dateEditor(value)+"</td></tr>";
                    Timestamp ts5 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    extEngTest5.setDate(ts5);
                    break;
                case "textSonota1Date":
                    if(value.isEmpty()) break;
                    scoreStr += "<td width='40%' align='center'>"+this.dateEditor(value)+"</td></tr>";
                    Timestamp ts6 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    extEngTest6.setDate(ts6);
                    break;
                case "textSonota2Date":
                    if(value.isEmpty()) break;
                    scoreStr += "<td width='40%' align='center'>"+this.dateEditor(value)+"</td></tr>";
                    Timestamp ts7 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    extEngTest7.setDate(ts7);
                    break;
                case "textSonota3Date":
                    if(value.isEmpty()) break;
                    scoreStr += "<td width='40%' align='center'>"+this.dateEditor(value)+"</td></tr>";
                    Timestamp ts8 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    extEngTest8.setDate(ts8);
                    break;
                case "textSonota1":
                    if(value.isEmpty()) break;
                    scoreStr += "<tr><td width='40%'>"+value+"</td>";
                    //extEngTest6.setTestType("その他");
                    extEngTest6.setTestName(value);
                    break;
                case "textSonota2":
                    if(value.isEmpty()) break;
                    scoreStr += "<tr><td width='40%'>"+value+"</td>";
                    //extEngTest7.setTestType("その他");
                    extEngTest7.setTestName(value);
                    break;
                case "textSonota3":
                    if(value.isEmpty()) break;
                    scoreStr += "<tr><td width='40%'>"+value+"</td>";
                    //extEngTest8.setTestType("その他");
                    extEngTest8.setTestName(value);
                    break;
                // 海外での学修経験
                case "selectPurpose1":
                    if(value.isEmpty() || value.equals("none")) break;
                    abroadStr += "<tr><td width='45%'>"+value+"</td>";
                    stuAbExp1.setPurposeType(value);
                    break;
                case "selectPurpose2":
                    if(value.isEmpty() || value.equals("none")) break;
                    abroadStr += "<tr><td width='45%'>"+value+"</td>";
                    stuAbExp2.setPurposeType(value);
                    break;
                case "selectPurpose3":
                    if(value.isEmpty() || value.equals("none")) break;
                    abroadStr += "<tr><td width='45%'>"+value+"</td>";
                    stuAbExp3.setPurposeType(value);
                    break;
                case "selectPurpose4":
                    if(value.isEmpty() || value.equals("none")) break;
                    abroadStr += "<tr><td width='45%'>"+value+"</td>";
                    stuAbExp4.setPurposeType(value);
                    break;
                case "selectPurpose5":
                    if(value.isEmpty() || value.equals("none")) break;
                    abroadStr += "<tr><td width='45%'>"+value+"</td>";
                    stuAbExp5.setPurposeType(value);
                    break;
                case "textCountry1":
                    if(value.isEmpty()) break;
                    abroadStr += "<td width='15%'>"+value+"</td>";
                    stuAbExp1.setNation(value);
                    break;
                case "textCountry2":
                    if(value.isEmpty()) break;
                    abroadStr += "<td width='15%'>"+value+"</td>";
                    stuAbExp2.setNation(value);
                    break;
                case "textCountry3":
                    if(value.isEmpty()) break;
                    abroadStr += "<td width='15%'>"+value+"</td>";
                    stuAbExp3.setNation(value);
                    break;
                case "textCountry4":
                    if(value.isEmpty()) break;
                    abroadStr += "<td width='15%'>"+value+"</td>";
                    stuAbExp4.setNation(value);
                    break;
                case "textCountry5":
                    if(value.isEmpty()) break;
                    abroadStr += "<td width='15%'>"+value+"</td>";
                    stuAbExp5.setNation(value);
                    break;
                case "textPeriodFrom1":
                    if(value.isEmpty()) break;
                    abroadStr += "<td width='20%' align='center'>"+this.dateEditor(value)+" ～ ";
                    Timestamp ts01 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    stuAbExp1.setDateFrom(ts01);
                    break;
                case "textPeriodFrom2":
                    if(value.isEmpty()) break;
                    abroadStr += "<td width='20%' align='center'>"+this.dateEditor(value)+" ～ ";
                    Timestamp ts02 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    stuAbExp2.setDateFrom(ts02);
                    break;
                case "textPeriodFrom3":
                    if(value.isEmpty()) break;
                    abroadStr += "<td width='20%' align='center'>"+this.dateEditor(value)+" ～ ";
                    Timestamp ts03 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    stuAbExp3.setDateFrom(ts03);
                    break;
                case "textPeriodFrom4":
                    if(value.isEmpty()) break;
                    abroadStr += "<td width='20%' align='center'>"+this.dateEditor(value)+" ～ ";
                    Timestamp ts04 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    stuAbExp4.setDateFrom(ts04);
                    break;
                case "textPeriodFrom5":
                    if(value.isEmpty()) break;
                    abroadStr += "<td width='20%' align='center'>"+this.dateEditor(value)+" ～ ";
                    Timestamp ts05 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    stuAbExp5.setDateFrom(ts05);
                    break;
                case "textPeriodTo1":
                    if(value.isEmpty()) break;
                    abroadStr += this.dateEditor(value)+"</td>";
                    Timestamp ts06 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    stuAbExp1.setDateTo(ts06);
                    break;
                case "textPeriodTo2":
                    if(value.isEmpty()) break;
                    abroadStr += this.dateEditor(value)+"</td>";
                    Timestamp ts07 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    stuAbExp2.setDateTo(ts07);
                    break;
                case "textPeriodTo3":
                    if(value.isEmpty()) break;
                    abroadStr += this.dateEditor(value)+"</td>";
                    Timestamp ts08 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    stuAbExp3.setDateTo(ts08);
                    break;
                case "textPeriodTo4":
                    if(value.isEmpty()) break;
                    abroadStr += this.dateEditor(value)+"</td>";
                    Timestamp ts09 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    stuAbExp4.setDateTo(ts09);
                    break;
                case "textPeriodTo5":
                    if(value.isEmpty()) break;
                    abroadStr += this.dateEditor(value)+"</td>";
                    Timestamp ts10 = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(value).getTime());
                    stuAbExp5.setDateTo(ts10);
                    break;
                case "textDestination1":
                    if(value.isEmpty()) break;
                    abroadStr += "<td width='20%'>"+value+"</td></tr>";
                    stuAbExp1.setDestination(value);
                    break;
                case "textDestination2":
                    if(value.isEmpty()) break;
                    abroadStr += "<td width='20%'>"+value+"</td></tr>";
                    stuAbExp2.setDestination(value);
                    break;
                case "textDestination3":
                    if(value.isEmpty()) break;
                    abroadStr += "<td width='20%'>"+value+"</td></tr>";
                    stuAbExp3.setDestination(value);
                    break;
                case "textDestination4":
                    if(value.isEmpty()) break;
                    abroadStr += "<td width='20%'>"+value+"</td></tr>";
                    stuAbExp4.setDestination(value);
                    break;
                case "textDestination5":
                    if(value.isEmpty()) break;
                    abroadStr += "<td width='20%'>"+value+"</td></tr>";
                    stuAbExp5.setDestination(value);
                    break;
                // 論文
                case "txtAuthorName":
                    paramDto.getResults().setAuthorName(value);
                    outHtml = outHtml.replaceAll("TEMP_AUTHOR_NAME", value);
                    break;
                case "txtJournalTitle":
                    paramDto.getResults().setMediumTitle(value);
                    outHtml = outHtml.replaceAll("TEMP_JOURNAL_TITLE", value);
                    break;
                case "txtJournalVolume":
                    paramDto.getResults().setJournalVolume(value);
                    outHtml = outHtml.replaceAll("TEMP_JOURNAL_VOLUME", value);
                    break;
                case "txtJournalNumber":
                    paramDto.getResults().setJournalNumber(value);
                    outHtml = outHtml.replaceAll("TEMP_JOURNAL_NUMBER", value);
                    break;
                case "txtPageFrom":
                    if(value.isEmpty()) break;
                    paramDto.getResults().setPageFrom(Integer.parseInt(value));
                    outHtml = outHtml.replaceAll("TEMP_PAGE_FROM", value);
                    break;
                case "txtPageTo":
                    if(value.isEmpty()) break;
                    paramDto.getResults().setPageTo(Integer.parseInt(value));
                    outHtml = outHtml.replaceAll("TEMP_PAGE_TO", value);
                    break;
                case "txtPublishedYear":
                    if(value.isEmpty()) break;
                    paramDto.getResults().setPublishedYear(Integer.parseInt(value));
                    outHtml = outHtml.replaceAll("TEMP_PUBLISHED_YEAR", value);
                    break;
                case "txtPublishedMonth":
                    if(value.isEmpty()) break;
                    paramDto.getResults().setPublishedMonth(Integer.parseInt(value));
                    outHtml = outHtml.replaceAll("TEMP_PUBLISHED_MONTH", value);
                    break;
                case "selAuthorType":
                    paramDto.getResults().setAuthorType(value);
                    outHtml = outHtml.replaceAll("TEMP_AUTHOR_TYPE", value);
                    break;
                case "selJournalClass":
                    paramDto.getResults().setMediumClass(value);
                    outHtml = outHtml.replaceAll("TEMP_JOURNAL_TYPE", value);
                    break;
                case "selHasReviewed":
                    paramDto.getResults().setHasReviewed(value);
                    switch(value){
                      case "Yes":
                        value = "Peer-reviewed";
                        break;
                      case "No":
                        value = "Non peer-reviewed";
                        break;
                    }
                    outHtml = outHtml.replaceAll("TEMP_REVIEWED", value);
                    break;
                case "txtImpactFactor":
                    paramDto.getResults().setImpactFactor(value);
                    outHtml = outHtml.replaceAll("TEMP_IMPACT_FACTOR", value);
                    break;
                case "selHasImpactFactor":
                    paramDto.getResults().setHasImpactFactor(value);
                    if(value.equals("有")) break;
                    if(value.equals("Yes")) break;
                    if(value.equals("No")){
                      value = "None";
                    }
                    outHtml = outHtml.replaceAll("TEMP_IMPACT_FACTOR", value);
                    break;
                // 学会発表
                case "txtPresenterName":
                    paramDto.getResults().setAuthorName(value);
                    outHtml = outHtml.replaceAll("TEMP_AUTHOR_NAME", value);
                    break;
                case "txtConferenceName":
                    paramDto.getResults().setMediumTitle(value);
                    outHtml = outHtml.replaceAll("TEMP_PRESENTATION_TITLE", value);
                    break;
                case "txtLocation":
                    paramDto.getResults().setLocation(value);
                    outHtml = outHtml.replaceAll("TEMP_PLACE", value);
                    break;
                case "txtHeldYear":
                    if(value.isEmpty()) break;
                    paramDto.getResults().setPublishedYear(Integer.parseInt(value));
                    outHtml = outHtml.replaceAll("TEMP_HELD_YEAR", value);
                    break;
                case "txtHeldMonth":
                    if(value.isEmpty()) break;
                    paramDto.getResults().setPublishedMonth(Integer.parseInt(value));
                    outHtml = outHtml.replaceAll("TEMP_HELD_MONTH", value);
                    break;
                case "selPresenterType":
                    paramDto.getResults().setAuthorType(value);
                    outHtml = outHtml.replaceAll("TEMP_PRESENTER_TYPE", value);
                    break;
                case "selPresentationFormat":
                    paramDto.getResults().setPresentationFormat(value);
                    outHtml = outHtml.replaceAll("TEMP_PRESENTATION_TYPE", value);
                    break;
                case "selConferenceClass":
                    paramDto.getResults().setMediumClass(value);
                    outHtml = outHtml.replaceAll("TEMP_CONFERENCE_TYPE", value);
                    break;
                case "uid":
                    paramDto.setUid(value);
                    break;
                default:
// 2019.01.22 s.furuta add start 2.学修成果修正
                    if(name.substring(0,7).equals("delFile")){
                        // 削除チェックの入った成果物はデータ削除（アップロードファイルやフォルダはそのままでOK?）
                        paramDto.getAttachments().setId(Integer.parseInt(name.substring(7)));
                        attachDao.deleteAttachments01(paramDto);
                    }
// 2019.01.22 s.furuta add end
                    break;
                }
            }

            if(tempFlg == "1"){
                // 学習成果の割合登録用の情報作成
                if(intGoalValue != 0){
                    Integer per = 100 / intGoalValue;
                    if(paramDto.getSeisekiForEvidence().getCode1() == 1){ paramDto.getSeisekiForEvidence().setCode1(per); }
                    if(paramDto.getSeisekiForEvidence().getCode2() == 1){ paramDto.getSeisekiForEvidence().setCode2(per); }
                    if(paramDto.getSeisekiForEvidence().getCode3() == 1){ paramDto.getSeisekiForEvidence().setCode3(per); }
                    if(paramDto.getSeisekiForEvidence().getCode4() == 1){ paramDto.getSeisekiForEvidence().setCode4(per); }
                    if(paramDto.getSeisekiForEvidence().getCode5() == 1){ paramDto.getSeisekiForEvidence().setCode5(per); }
                    if(paramDto.getSeisekiForEvidence().getCode6() == 1){ paramDto.getSeisekiForEvidence().setCode6(per); }
                    if(paramDto.getSeisekiForEvidence().getCode7() == 1){ paramDto.getSeisekiForEvidence().setCode7(per); }
                }
                // 分類名をディレクトリ名にセット (テキストボックスの入力があればそちらを優先)
                paramDto.getSeisekiForEvidence().setDir(groupName);
            }
            else{
                paramDto.getSeisekiForEvidence().setDir(selGroup);
                outHtml = outHtml.replaceAll("TEMP_SCORE", scoreStr);
                outHtml = outHtml.replaceAll("TEMP_ABROAD", abroadStr);
            }
            outHtml = outHtml.replaceAll("TEMP_HTMLTITLE", paramDto.getSeisekiForEvidence().getDir() + " - " + paramDto.getSeisekiForEvidence().getEvidence());
            outHtml = outHtml.replaceAll("TEMP_KAMOKUNM", paramDto.getSeisekiForEvidence().getDir());

            // フォルダが存在しない場合は作成
            String path = sysPropDto.getEvdir()  + paramDto.getUid().replace("-", "").toLowerCase() + "/" + paramDto.getSeisekiForEvidence().getDir() + "/" + paramDto.getSeisekiForEvidence().getEvidence();
            File f = new File(path);
            if(!f.exists()) f.mkdirs();

            String linkHtml = "";
            long sizeInBytes = 0;
// 2019.01.22 s.furuta chg start 2.学修成果修正
//            iter = uploadItems.iterator();
//            while(iter.hasNext()){
//                FileItem item = (FileItem)iter.next();
//                if(item.isFormField()) continue;
//                // ファイルのアップロード処理
//                String fileName = item.getName();
//                sizeInBytes += item.getSize();
//                if(fileName != null && !"".equals(fileName)){
//                    fileName = (new File(fileName)).getName();
//                    linkHtml += "<li><a href=\"" + fileName + "\">" + fileName + "</a></li>\n";
//                    item.write(new File(path + "/" + fileName));
//                }
//                // ファイルの情報を保存
//                paramDto.getAttachments().setFilename(fileName);
//                paramDto.getAttachments().setFilepath(path);
//                paramDto.getAttachments().setFilesize(item.getSize());
//                attachDao.addAttachments01(paramDto);
//            }
            // 成果物リスト(アップロード済み + 新規アップロード)
            List<String> linkList = new ArrayList<String>();

            // ========== 既にアップロード済みの成果物を取得 ==========
            // ★★ 修正前フォルダ・ファイルを削除しない場合は▼▲で挟んでいるコードをコメントアウトすること ★★
            // ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
            // 削除パスリスト
            List<String> delList = new ArrayList<String>();
            boolean listFlg;
            // ▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
            // ファイルパスセパレータ('/')
            String spa = FileSystems.getDefault().getSeparator();
            if(!editKamokuCd.equals(null) && !editKamokuCd.equals("")){
                ArrayList<Attachments> existAttachments = null;
                // 登録済みの成果物を取得
                existAttachments = attachDao.getAttachments01(paramDto);
                if(!existAttachments.isEmpty()){
                    for(Attachments atch : existAttachments){
                        sizeInBytes += atch.getFilesize();
                        if(atch.getFilename() != null && !"".equals(atch.getFilename())){
                            // ファイルが存在すれば成果物リストに追加
                            linkList.add(atch.getFilename());
                            // 分類・タイトルが修正された場合はファイルの格納先が変わるのでコピー
                            // (コピー元は削除しない?  ==> 既存の削除処理では物理削除は行っていない(seiseki_for_evidenceからデータ削除のみ)ため、これに準拠?)
                            try {
                                // コピー元(修正前)パス
                                Path sourcePath = Paths.get(atch.getFilepath() + spa + atch.getFilename());
                                // コピー先(修正後)パス
                                Path targetPath = Paths.get(path + spa + atch.getFilename());
                                // コピー元パスからコピー先パスへファイルをコピー
                                Files.copy(sourcePath, targetPath);
                            } catch (Exception e) {
                                System.out.println(e.toString());
                            }
                            // アップロード済み成果物データを更新(ファイルパス、更新日、ユーザ)
                            paramDto.getAttachments().setId(atch.getId());
                            paramDto.getAttachments().setFilepath(path);
                            attachDao.updateAttachments01(paramDto);
                        }
                        // ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
                        listFlg = true;
                        // 削除リスト登録済みか検証
                        for(int i=0; i<delList.size(); i++){
                            if(delList.get(i).equals(atch.getFilepath())){
                                listFlg = false;
                            }
                        }
                        // 削除パスリスト未登録なら追加
                        if(listFlg){
                            delList.add(atch.getFilepath());
                        }
                        // ▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
                    }
                    // ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
                    for(int i=0; i<delList.size(); i++){
                        // 修正後パスと異なる場合、既存のパス(修正前)はディレクトリ削除
                        if(!delList.get(i).equals(path)){
                            try {
                                File dir = new File(delList.get(i));
                                delDirectory(dir);
                            } catch (Exception e) {
                                System.out.println(e.toString());
                            }
                        }
                    }
                    // ▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲

                }
            }
            // ========== 今回追加する成果物をアップロード ==========
            iter = uploadItems.iterator();
            while(iter.hasNext()){
                FileItem item = (FileItem)iter.next();
                if(item.isFormField()) continue;
                // ファイルのアップロード処理
                String fileName = item.getName();
                sizeInBytes += item.getSize();
                if(fileName != null && !"".equals(fileName)){
                    fileName = (new File(fileName)).getName();
                    for(int i=0; i<linkList.size(); i++){
                        // ファイル名重複(ファイル名が一致 且つ 拡張子(大文字小文字区別無し)が一致)チェック
                        if(getPrefixFileName(fileName).equals((getPrefixFileName(linkList.get(i)))) && getSuffixFileName(fileName).toLowerCase().equals(getSuffixFileName(linkList.get(i)).toLowerCase())){
                            String newFileName = new String();
                            boolean dupFlg = true;
                            for(int j=1; dupFlg; j++){
                                // アップロードファイル名が重複したら末尾にナンバリング
                                newFileName = getPrefixFileName(fileName)+"("+j+")."+getSuffixFileName(fileName);
                                dupFlg = false;
                                for(int k=0; k<linkList.size(); k++){
                                    // ナンバリング後のファイル名の重複チェック(重複していた場合はナンバリングの数字を増やす)
                                    if(getPrefixFileName(newFileName).equals((getPrefixFileName(linkList.get(k)))) && getSuffixFileName(newFileName).toLowerCase().equals(getSuffixFileName(linkList.get(k)).toLowerCase())) {
                                        dupFlg = true;
                                    }
                                }
                            }
                            fileName = newFileName;
                        }
                    }
                    // 重複を回避後、成果物リストに追加
                    linkList.add(fileName);
                    item.write(new File(path + "/" + fileName));
                }
                // ファイルの情報を保存
                paramDto.getAttachments().setFilename(fileName);
                paramDto.getAttachments().setFilepath(path);
                paramDto.getAttachments().setFilesize(item.getSize());
                attachDao.addAttachments01(paramDto);
            }
            // 昇順で並べ替え
            Collections.sort(linkList);
            for(int i=0; i<linkList.size(); i++){
                linkHtml += "<li><a href=\"" + linkList.get(i) + "\">" + linkList.get(i) + "</a></li>\n";
            }
// 2019.01.22 s.furuta chg end
            paramDto.getSeisekiForEvidence().setSize(sizeInBytes);
            outHtml = outHtml.replaceAll("TEMP_FILELIST", linkHtml);
            // htmlファイル作成
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(path + "/" + paramDto.getSeisekiForEvidence().getEvidence() + ".html")));
            bw.write(outHtml.toString());
            bw.close();

            // 画面で入力された情報を保存
            resultDao.insertResults01(paramDto);
            if(extEngTest1.getScore() != null && !extEngTest1.getScore().isEmpty()) extEngDao.addExternalEnglishTests01(extEngTest1, paramDto.getUid(), kamokucdStr, paramDto.getRuser());
            if(extEngTest2.getScore() != null && !extEngTest2.getScore().isEmpty()) extEngDao.addExternalEnglishTests01(extEngTest2, paramDto.getUid(), kamokucdStr, paramDto.getRuser());
            if(extEngTest3.getScore() != null && !extEngTest3.getScore().isEmpty()) extEngDao.addExternalEnglishTests01(extEngTest3, paramDto.getUid(), kamokucdStr, paramDto.getRuser());
            if(extEngTest4.getScore() != null && !extEngTest4.getScore().isEmpty()) extEngDao.addExternalEnglishTests01(extEngTest4, paramDto.getUid(), kamokucdStr, paramDto.getRuser());
            if(extEngTest5.getScore() != null && !extEngTest5.getScore().isEmpty()) extEngDao.addExternalEnglishTests01(extEngTest5, paramDto.getUid(), kamokucdStr, paramDto.getRuser());
            if(extEngTest6.getScore() != null && !extEngTest6.getScore().isEmpty()) extEngDao.addExternalEnglishTests01(extEngTest6, paramDto.getUid(), kamokucdStr, paramDto.getRuser());
            if(extEngTest7.getScore() != null && !extEngTest7.getScore().isEmpty()) extEngDao.addExternalEnglishTests01(extEngTest7, paramDto.getUid(), kamokucdStr, paramDto.getRuser());
            if(extEngTest8.getScore() != null && !extEngTest8.getScore().isEmpty()) extEngDao.addExternalEnglishTests01(extEngTest8, paramDto.getUid(), kamokucdStr, paramDto.getRuser());
            if(stuAbExp1.getPurposeType() != null && !stuAbExp1.getPurposeType().isEmpty()) stuAbExpDao.addStudyingAbroadExperieces01(stuAbExp1, paramDto.getUid(), kamokucdStr, paramDto.getRuser());
            if(stuAbExp2.getPurposeType() != null && !stuAbExp2.getPurposeType().isEmpty()) stuAbExpDao.addStudyingAbroadExperieces01(stuAbExp2, paramDto.getUid(), kamokucdStr, paramDto.getRuser());
            if(stuAbExp3.getPurposeType() != null && !stuAbExp3.getPurposeType().isEmpty()) stuAbExpDao.addStudyingAbroadExperieces01(stuAbExp3, paramDto.getUid(), kamokucdStr, paramDto.getRuser());
            if(stuAbExp4.getPurposeType() != null && !stuAbExp4.getPurposeType().isEmpty()) stuAbExpDao.addStudyingAbroadExperieces01(stuAbExp4, paramDto.getUid(), kamokucdStr, paramDto.getRuser());
            if(stuAbExp5.getPurposeType() != null && !stuAbExp5.getPurposeType().isEmpty()) stuAbExpDao.addStudyingAbroadExperieces01(stuAbExp5, paramDto.getUid(), kamokucdStr, paramDto.getRuser());

        }catch(Exception e){
            System.out.println(e.toString());
            return false;
        }
        logger.info(CommonConst.END_LOG + method_name);

        return true;
    }

    /*
     * 成果物の情報をSeisekiForEvidenceへ保存
     */
    private boolean saveSeisekiForEvidence(ParameterDto paramDto) {
        String method_name = "saveSeisekiForEvidence";
        logger.info(CommonConst.START_LOG + method_name);

        // 保存用データの作成
        SeisekiForEvidenceDao seiEviDao = new SeisekiForEvidenceDao();

        paramDto.getSeisekiForEvidence().setgSysno(paramDto.getUid());
// 2019.01.21 s.furuta chg start 2.学修成果修正
//        paramDto.getSeisekiForEvidence().setKamokucd(seiEviDao.checkGSysNoMakeCode(paramDto));
        if(editKamokuCd.equals(null) || editKamokuCd.equals("")) {
// 2019.03.26 s.furuta chg start 顧客検証No2
//            paramDto.getSeisekiForEvidence().setKamokucd(seiEviDao.checkGSysNoMakeCode(paramDto));
            paramDto.getSeisekiForEvidence().setKamokucd(commitKamokuCd);
// 2019.03.26 s.furuta chg end
        } else {
            paramDto.getSeisekiForEvidence().setKamokucd(editKamokuCd);
            // 既存データ削除
            seiEviDao.deleteSeisekiForEvidence02(paramDto);
        }
// 2019.01.21 s.furuta chg end
        paramDto.getSeisekiForEvidence().setJikanwaricd(paramDto.getSeisekiForEvidence().getKamokucd());
        paramDto.getSeisekiForEvidence().setJikanwariShozokucd("00");
        paramDto.getSeisekiForEvidence().setKamokunm(paramDto.getSeisekiForEvidence().getDir());
        paramDto.getSeisekiForEvidence().setKamokunmeng(paramDto.getSeisekiForEvidence().getDir());
        // 分類マスタからSHOZOKUCD等を取得
        BunruiMstDao bunDao = new BunruiMstDao();
        ArrayList<BunruiMst> res = null;
        res = bunDao.getBunruiMst02(paramDto.getRuser(), paramDto.getSeisekiForEvidence().getDir());
        for(BunruiMst bun : res){
            paramDto.getSeisekiForEvidence().setJikanwariShozokucd(bun.getShozokucd());
            paramDto.getSeisekiForEvidence().setKamokunm(bun.getBunruinm());
            paramDto.getSeisekiForEvidence().setKamokunmeng(bun.getBunruinmeng());
        }
        paramDto.getSeisekiForEvidence().setEnterdFlg(enFlag ? 0 : 1);
        paramDto.getSeisekiForEvidence().setHyogocd("99");
        paramDto.getSeisekiForEvidence().setHyogonm("99");
        paramDto.getSeisekiForEvidence().setTanisu(new BigDecimal(0));
        paramDto.getSeisekiForEvidence().setHitsusenkbncd("0");
        paramDto.getSeisekiForEvidence().setJikanwariNendo(paramDto.getSeisekiForEvidence().getNinteiNendo());

        // データベースへの保存（seiseki_for_evidence）
        seiEviDao.insertSeisekiForEvidence01(paramDto);

        logger.info(CommonConst.END_LOG + method_name);
        return true;
    }

    /*
     * 分類名を取得 (テキストボックスの入力があればそちらを優先)
     */
    private String fetchGroupName(Iterator<?> items) {
        try{
            String selGroupName = "";
            String txtGroupName = "";
            while(items.hasNext()){
                FileItem item = (FileItem)items.next();
                if(!item.isFormField()) continue;
                if(item.getFieldName().equals("selGroup")) selGroupName = item.getString("utf-8");
                if(item.getFieldName().equals("txtGroup")) txtGroupName = item.getString("utf-8");
            }
            return txtGroupName.isEmpty() ? selGroupName : txtGroupName;
        }catch(Exception e){
            System.out.println(e.toString());
            return "";
        }
    }

    /*
     * その他の分類名を取得 (テキストボックスの入力があればそちらを優先)
     */
    private String fetchTxtGroupName(Iterator<?> items) {
        try{
            String txtGroupName = "";
            while(items.hasNext()){
                FileItem item = (FileItem)items.next();
                if(!item.isFormField()) continue;
                if(item.getFieldName().equals("txtGroup")) txtGroupName = item.getString("utf-8");
            }
            return txtGroupName;
        }catch(Exception e){
            System.out.println(e.toString());
            return "";
        }
    }

// 2019.01.21 s.furuta add start 2.学修成果修正
    /*
     * 科目コードを取得 (編集モードのみ)
     */
    private String fetchKamokuCd(Iterator<?> items) {
        try{
            String txtKamokuCd = "";
            while(items.hasNext()){
                FileItem item = (FileItem)items.next();
                if(!item.isFormField()) continue;
                if(item.getFieldName().equals("kamokucd")) txtKamokuCd = item.getString("utf-8");
            }
            return txtKamokuCd;
        } catch(Exception e){
            System.out.println(e.toString());
            return "";
        }
    }
    /*
     * 追加日を取得 (編集モードのみ)
     */
    private String fetchInsertDate(Iterator<?> items) {
        try{
            String txtInsertDate = "";
            while(items.hasNext()){
                FileItem item = (FileItem)items.next();
                if(!item.isFormField()) continue;
                if(item.getFieldName().equals("insert_date")) txtInsertDate = item.getString("utf-8");
            }
            return txtInsertDate;
        } catch(Exception e){
            System.out.println(e.toString());
            return "";
        }
    }
    /*
     * ファイル名から拡張子を取り除いた名前を返す
     */
    private String getPrefixFileName(String fileName){
        if(fileName == null){
            return null;
        }
        int point = fileName.lastIndexOf(".");
        if(point != -1){
            return fileName.substring(0, point);
        }
        return fileName;
    }
    /*
     * ファイル名から拡張子のみを返す
     */
    private String getSuffixFileName(String fileName){
        if(fileName == null){
            return null;
        }
        int point = fileName.lastIndexOf(".");
        if(point != -1){
            return fileName.substring(point + 1);
        }
        return fileName;
    }
    /*
     * 渡されたディレクトリとディレクトリ内のファイルを物理削除
     */
    public static void delDirectory(File delFile) {
        // 対象ファイルの存在チェック
        if(delFile.exists()){
            // ファイルorディレクトリ判定
            if(delFile.isFile()){
                // ファイルまたは空のディレクトリの場合は削除
                delFile.delete();
            } else if(delFile.isDirectory()) {
                // ディレクトリの場合、内包するファイル・フォルダの一覧を取得
                File[] files = delFile.listFiles();
                // 存在するファイル数分ループして再帰的に削除
                for(int i=0; i<files.length; i++){
                    delDirectory(files[i]);
                }
                // 中身のファイルを全て削除し終えたら、対象ディレクトリを削除
                delFile.delete();
            }
        }
    }
// 2019.01.21 s.furuta add end

    /**
     * 分類を取得
     */
    private CommonConst.ResultGroup getResultGroup(String groupName) {
        if(groupName.startsWith("論文") || groupName.toLowerCase().startsWith("academic paper".toLowerCase()))
            return CommonConst.ResultGroup.paper;
        if(groupName.startsWith("学会発表") || groupName.toLowerCase().startsWith("academic conference presentation".toLowerCase()))
            return CommonConst.ResultGroup.presentation;
        if(groupName.startsWith("英語外部試験スコア") || groupName.toLowerCase().startsWith("external english test scores".toLowerCase()))
            return CommonConst.ResultGroup.EnglishTest;
        if(groupName.startsWith("海外での学修経験") || groupName.toLowerCase().startsWith("experience in overseas study".toLowerCase()))
            return CommonConst.ResultGroup.studyingAbroad;
        return CommonConst.ResultGroup.common;
    }

    /**
     * 英語かどうか判断
     */
    private Boolean getEnFlag(String groupName) {
        String name = groupName.toLowerCase();
        return name.startsWith("academic")
            || name.startsWith("publication")
            || name.startsWith("research")
            || name.startsWith("translation")
            || name.startsWith("extra")
            || name.startsWith("volunteer")
            || name.startsWith("award")
            || name.startsWith("certification")
            || name.startsWith("comissioner")
            || name.startsWith("external")
            || name.startsWith("experience");
    }

    /*
     * 日付の見た目を綺麗にする(2000/01/01)
     */
    private String dateEditor(String dateStr) {
        switch(dateStr.length()){
        case 8:
            return dateStr.substring(0,5)+"0"+dateStr.substring(5,7)+"0"+dateStr.substring(7);
        case 9:
            if(dateStr.substring(7,8).equals("/"))
                return dateStr.substring(0,8)+"0"+dateStr.substring(8);
            else
                return dateStr.substring(0,5)+"0"+dateStr.substring(5);
        default:
            return dateStr;
        }
    }
}
