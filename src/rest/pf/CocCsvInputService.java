package rest.pf;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.CocAdministerDao;
import dao.entity.CocAdminister;

public class CocCsvInputService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public CocCsvInputService() {
        //権限ID
        rid = 2;
        mode = 0;
    }

    /**
     * (rest.)pf/programs.json?lang="ja"<br />
     *
     * @return : JSON String
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        CocAdministerDao cocAdministerDao = new CocAdministerDao();
        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

        ArrayList<CocAdminister> res = cocAdministerDao.getMes01(pramDto);
        ArrayList<Object> aaData = new ArrayList<Object>();

        for(CocAdminister cocAdminister :res){
            ArrayList<String> o = new ArrayList<String>();
            o.add(cocAdminister.getBikou());
            aaData.add(o);
        }
        ret.put("Message", aaData);
        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    final Map<String, ArrayList<Map<String,String>>> aoColumns = // DBの構造に入っていないのでしかたなく
            new HashMap<String, ArrayList<Map<String,String>>>() {{
            }};

    @Override
    protected Boolean deleteJsonData() {
        String method_name = "deleteJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        CocAdministerDao cocAdministerDao = new CocAdministerDao();
        cocAdministerDao.deleteMes01(pramDto);

        logger.info(CommonConst.END_LOG + method_name);
        return true;
    }

    @Override
    protected Boolean addJsonData() {
        String lan = pramDto.getLang();
        String method_name = "addJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        // 行数（追加人数）のカウンタ
        int cnt = 0;
        // 学生番号格納用配列
        ArrayList<String> gakunoArray = new ArrayList<String>();
        // エラーチェック用フラグ
        boolean errFlg = false;

        // 処理後の表示メッセージ格納用配列
        ArrayList<String> o = new ArrayList<String>();
        CocAdministerDao cocAdministerDao = new CocAdministerDao();

        try{
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            FileItemIterator iter = upload.getItemIterator(request);
            while(iter.hasNext()){
                FileItemStream item = iter.next();
                if(!item.isFormField()){
                    InputStream is = item.openStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(is,"UTF-8"));
                    String line = "";

                    // ファイルを行単位で読み込み
                    while((line = br.readLine()) != null){
                        // カンマで分割したString配列を取得
                        String array[] = line.split(",");
                        String gakuno = array[0];
                        gakuno = gakuno.replace("\"","");
                        // 学生番号を逐次配列に格納
                        gakunoArray.add(gakuno);
                        cnt += 1;
                    }
                    br.close();
                    is.close();
                }
            }
            for(int i=1; i<cnt; i++){
                pramDto.setUid(gakunoArray.get(i));
                ArrayList<CocAdminister> res = cocAdministerDao.verificationCoc01(pramDto);
                if(res == null){
                    // gakuseki検証
                    String veriGaku = (lan.equals("en")) ? ("Row " +(i+1)+ " ：  There are no student whose student number : " +gakunoArray.get(i)) : ((i+1)+ "行目：学生番号 " +gakunoArray.get(i)+ " の学生は学籍情報が存在しません");
                    o.add(veriGaku);
                    // retにエラーメッセージ追加していく（行数とgakusekiに存在しない旨）
                    errFlg = true;
                }
                else{
                    // coc_target検証
                    ArrayList<CocAdminister> res2 = cocAdministerDao.verificationCoc02(pramDto);
                    if(res2 != null){
                        String veriCoc = (lan.equals("en")) ? ("Row " +(i+1)+ " :  Student number : " +gakunoArray.get(i)+ " has already been registered") : ((i+1)+ "行目：学生番号 " +gakunoArray.get(i)+ "の学生は既に対象者として登録されています");
                        o.add(veriCoc);
                        errFlg = true;
                    }
                }
            }
            if(errFlg == false){
                String sucTitle = (lan.equals("en")) ? "□□Registration success□□" : "□□追加に成功しました□□";
                o.add(sucTitle);
                o.add("");
                String addStr = (lan.equals("en")) ? "The following students were additionally registered" : "対象者として以下の学生を追加しました";
                o.add(addStr);
                String stuNo = (lan.equals("en")) ? "Student number : " : "学生番号 ： " ;
                o.add(stuNo);
                for(int j=1; j<cnt; j++){
                    pramDto.setUid(gakunoArray.get(j));
                    cocAdministerDao.addCoc01(pramDto);
                    o.add(" " + gakunoArray.get(j));
                }
            }
            else{
                o.add(0, "");
                String errTitle = (lan.equals("en")) ? "■■Registration failure■■" : "■■追加に失敗しました■■";
                o.add(0, errTitle);
            }
        }catch(IOException e){
            System.out.println("Error");
        }catch(NumberFormatException e){
            System.out.println("Error");
        } catch (FileUploadException e) {
            e.printStackTrace();
        }

        for(String value : o){
            pramDto.setSearchCon(value);
            cocAdministerDao.addMes01(pramDto);
        }
        logger.info(CommonConst.END_LOG + method_name);
        return true;
    }
}
