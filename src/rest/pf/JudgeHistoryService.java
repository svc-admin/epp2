package rest.pf;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.ShugakusienHanteiRirekiDao;
import dao.entity.ShugakusienHanteiRireki;

/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2019 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class JudgeHistoryService extends AbstractMapService {
	private static Logger logger = Tools.getCallerLogger();

	public JudgeHistoryService() {
		// 権限ID
		rid = 2;
		mode = 0;
	}

	/**
	 * 判定結果の履歴情報を取得する
	 * @return : JSON String
	 */
	@Override
	protected LinkedHashMap<String, Object> getJsonData() {
		String method_name = "getJsonData";
		logger.info(CommonConst.START_LOG + method_name);
		LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

		try {
			// 判定結果履歴データの取得
			ArrayList<ShugakusienHanteiRireki> historyList = new ArrayList<ShugakusienHanteiRireki>();
			ShugakusienHanteiRirekiDao rirekiDao = new ShugakusienHanteiRirekiDao();
			historyList = rirekiDao.getRirekiData(pramDto);

			ret.put("result", true);
			ret.put("dataList", historyList);
		} catch(Exception e) {
			logger.info(e.getMessage());
			ret.put("result", false);
		}
		return ret;
	}

	protected LinkedHashMap<String, Object> makeRetJson(Boolean ret) {
		LinkedHashMap<String, Object> retMap = new LinkedHashMap<String, Object>();

		if (ret) {
			retMap.put("status", true);
		} else {
			retMap.put("status", false);
		}
		return retMap;
	}

	@Override
	protected Boolean deleteJsonData() {
		return null;
	}

	@Override
	protected Boolean addJsonData() {
		return null;
	}

	@Override
	protected boolean checkParams(Map<String, Object> params) {
		return true;
	}
}
