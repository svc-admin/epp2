package rest.pf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import constants.ParamsConst;
import dao.GakusekiDao;
import dao.KyokihonDao;
//2014.11.21 SVC 追加 <<
import dao.entity.Gakuseki;
import dao.entity.Kyokihon;
//2014.11.21 SVC 追加 >>
//2014.11.21 SVC 追加 <<
//2014.11.21 SVC 追加 >>
//+ for using variable request / request変数を利用するため

/**
 *  参照権限追加用学生リスト取得 (dataTables.js用) REST service using JSONIC, version 2014-11-21 (since 2014-11-21)
 */
public class AuStudentlistService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public AuStudentlistService() {
        //権限ID
        rid = 0;
        mode = 0;
    }

    /**
    (rest.)pf/dtStudentlist.json?lang="ja"&nyugaku_nendo="2007"&shozokucd="0513"<br />
      入学年度、所属を指定して学生のdataTables.js用のリストを返す。(※)<br />
     * @return : Object (hash array here) / Object (この場合は連想配列)
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        LinkedHashMap<String,Object> ret = new LinkedHashMap<String,Object>();

        GakusekiDao gakusekiDao = new GakusekiDao();
        ArrayList<Gakuseki> res = gakusekiDao.getGakusekiList02(pramDto);

        ArrayList<Object> aaData = new ArrayList<Object>();

        KyokihonDao kyokihonDao = new KyokihonDao();

        for(Gakuseki gakuseki :res) {
            ArrayList<Kyokihon> kyokihonList = kyokihonDao.getKyokihonList01(pramDto,gakuseki.getgSysno());
            if(kyokihonList.size() == 0) continue;
            Kyokihon kyokihon = kyokihonList.get(0);

            String name = getLangObj(kyokihon.getgName()+" ("+kyokihon.getgNameeng()+")",kyokihon.getgNameeng()+" ("+kyokihon.getgName()+")");

            ArrayList<Object> o = new ArrayList<Object>();
            o.add(null);
            o.add(gakuseki.getgSysno());
            o.add(name);
            aaData.add(o);
        }
        ret.put("aaData", aaData);
        ret.put("aoColumns", aoColumns.get(pramDto.getLang()));
        logger.info("austudentlist for dataTables.js allowed where nyugaku_nendo="+pramDto.getNyugakunendo()
                +", shozokucd="+pramDto.getShozokucd()+", lang="+pramDto.getLang()+", ruser="+pramDto.getRuser());

        logger.info("ruser="+pramDto.getRuser()+", denied : lang="+pramDto.getLang());

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        //入学年度、所属コードが設定されていない場合は処理しない。
        if(params.containsKey(ParamsConst.NYUGAKU_NENDO) && (params.containsKey(ParamsConst.SHOZOKUCD))) {
            rtn = true;
        }
        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    final Map<String, ArrayList<Map<String,String>>> aoColumns = // DBの構造に入っていないのでしかたなく
            new HashMap<String, ArrayList<Map<String,String>>>() {{
                put(CommonConst.JAPANESE, new ArrayList<Map<String,String>>() {{
                    add(new HashMap<String,String>() {{
                        put("sTitle", "追加");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "学生番号");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "氏名");
                    }});
                }});
                put(CommonConst.ENGLISH, new ArrayList<Map<String,String>>() {{
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Add");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Student ID");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Name");
                    }});
                }});
            }};

//2014.11.21 SVC 追加、削除処理追加に伴うもの >>
    @Override
    protected Boolean deleteJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }

    @Override
    protected Boolean addJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }
//2014.11.21 SVC 追加、削除処理追加に伴うもの <<

}
