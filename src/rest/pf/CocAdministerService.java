package rest.pf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.CocAdministerDao;
import dao.entity.CocAdminister;

public class CocAdministerService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public CocAdministerService() {
        //権限ID
        rid = 2;
        mode = 0;
    }

    /**
     * (rest.)pf/programs.json?lang="ja"<br />
     *
     * @return : JSON String
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

        CocAdministerDao cocAdministerDao = new CocAdministerDao();
        ArrayList<CocAdminister> res = cocAdministerDao.cocTargetList01(pramDto);

        ArrayList<Object> aaData = new ArrayList<Object>();

        for(CocAdminister cocAdminister :res){
            ArrayList<Object> o = new ArrayList<Object>();
            o.add(cocAdminister.getgGakuno());
            o.add(getLangObj(cocAdminister.getgName(),cocAdminister.getgNameeng()));
            o.add(getLangObj(cocAdminister.getShozokunm(),cocAdminister.getShozokunmeng()));
            o.add(cocAdminister.getKubun());
            o.add(cocAdminister.getBikou());
            aaData.add(o);
        }

        ret.put("aaData" ,aaData);
        ret.put("aoColumns", aoColumns.get(pramDto.getLang()));
        logger.info("authoritylist for dataTables.js allowed where nyugaku_nendo="+pramDto.getNyugakunendo()
                +", shozokucd="+pramDto.getShozokucd()+", lang="+pramDto.getLang()+", ruser="+pramDto.getRuser());

        logger.info("ruser="+pramDto.getRuser()+", denied : lang="+pramDto.getLang());

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    final Map<String, ArrayList<Map<String,String>>> aoColumns = // DBの構造に入っていないのでしかたなく
            new HashMap<String, ArrayList<Map<String,String>>>() {{
                put(CommonConst.JAPANESE, new ArrayList<Map<String,String>>() {{
                    add(new HashMap<String,String>() {{
                        put("sTitle", "操作");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "学生番号");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "氏名");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "所属");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "区分 ");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "備考 ");
                    }});
                }});
                put(CommonConst.ENGLISH, new ArrayList<Map<String,String>>() {{
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Operation");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "StudentNO");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Name");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Department Name");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "COC/COC+  ");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Remarks  ");
                    }});
                }});
            }};

    @Override
    protected Boolean deleteJsonData() {
        String method_name = "deleteJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        CocAdministerDao cocAdministerDao = new CocAdministerDao();
        cocAdministerDao.deleteCoc01(pramDto);

        logger.info(CommonConst.END_LOG + method_name);

        return true;
    }

    @Override
    protected Boolean addJsonData() {
        String method_name = "addJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        CocAdministerDao cocAdministerDao = new CocAdministerDao();
        cocAdministerDao.addCoc01(pramDto);

        logger.info(CommonConst.END_LOG + method_name);

        return true;
    }
}
