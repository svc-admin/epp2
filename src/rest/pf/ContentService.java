package rest.pf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractListService;
import util.Tools;
import constants.CommonConst;
import constants.ParamsConst;
import dao.EvidenceDao;
import dao.GoalSeisekiDao;
import dao.GoalsMstDao;
import dao.KakuteiSeisekiDao;
import dao.cstmentity.GoalSeiseki;
import dao.entity.Evidence;
import dao.entity.GoalsMst;
import dao.entity.KakuteiSeiseki;
//+ for using variable request / request変数を利用するため

/**
 *  学生情報取得 REST service using JSONIC, version 2014-02-02 (since 2014-02-02)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class ContentService extends AbstractListService {
    private static Logger logger = Tools.getCallerLogger();

    public ContentService() {
        //権限ID
        rid = 0;
        mode = 0;
    }

    /**
    (rest.)pf/Student.json?lang="ja"&uid=XX&shozokucd="0513"<br />
      学生番号、所属コードを指定して学生情報を返す。(※)<br />
    <pre>
    {"uid":"071-A6201",
     "name":"久保田　花子 (Kubota Hanako)",
     "affil":"文学部 コミュニケーション情報学科 コミュニケーション情報学コース",
     "email":"aaa@aaa.jp",
     "zaiseki":"2007-2010",
     "dataupdatedate":"2013-02-02"
    ]
    </pre>
     * @return : Object (hash array here) / Object (この場合は連想配列)
     */
    @Override
    protected ArrayList<Object> getJsonData() {

        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        ArrayList<Object> ret = new ArrayList<Object>();

        List<Integer> goalList = new ArrayList<Integer>();
        // 2015.02.12 SVC 追加 >>
        // 学習成果のコードを持っておく
        GoalsMstDao goalsMstDao = new GoalsMstDao();
        ArrayList<GoalsMst> goalMstList = goalsMstDao.getGoalsMstList02(pramDto);
        // 積み替える
        for(GoalsMst goal: goalMstList){
            goalList.add(goal.getCode());
        }
        // 最後に「0：その他」を追加
        goalList.add(0);
        // 2015.02.12 SVC 追加 <<

        GoalSeisekiDao goalSeisekiDao = new GoalSeisekiDao();

        KakuteiSeisekiDao kakuteiSeisekiDao = new KakuteiSeisekiDao();
        ArrayList<KakuteiSeiseki> nendoList = kakuteiSeisekiDao.getKakuteiSeisekiList03(pramDto);
// 2017.03.15 SVC)Furuta ADD START >> ポートフォリオ改善
        // 同タームに対してnendoListの要素が複数作られてしまうのでここで整形
        ArrayList<KakuteiSeiseki> nenList = new ArrayList<KakuteiSeiseki>();
        for (KakuteiSeiseki nen : nendoList){
            boolean adFlg = true;
            if(nen.getNinteiGakkikbncd().equals("1")){
                if(nen.getNinteiKaikokbncd().equals("7")){
                    nenList.add(nen);
                } else {
                    nen.setNinteiKaikokbncd("6");
                    for(KakuteiSeiseki n : nenList){
                        if(n.getNinteiNendo().equals(nen.getNinteiNendo()) && n.getNinteiGakkikbncd().equals(nen.getNinteiGakkikbncd()) && n.getNinteiKaikokbncd().equals(nen.getNinteiKaikokbncd())){
                            adFlg = false;
                        }
                    }
                    if(adFlg == true){
                        nenList.add(nen);
                    }
                }
            } else {
                if(nen.getNinteiKaikokbncd().equals("9")){
                    nenList.add(nen);
                } else {
                    nen.setNinteiKaikokbncd("8");
                    for(KakuteiSeiseki n : nenList){
                        if(n.getNinteiNendo().equals(nen.getNinteiNendo()) && n.getNinteiGakkikbncd().equals(nen.getNinteiGakkikbncd()) && n.getNinteiKaikokbncd().equals(nen.getNinteiKaikokbncd())){
                            adFlg = false;
                        }
                    }
                    if(adFlg == true){
                        nenList.add(nen);
                    }
                }
            }
        }
        nendoList = nenList;
// 2017.03.15 SVC)Furuta ADD END   << ポートフォリオ改善
        // 2015.02.24 SVC DirectoryからEvidenceを利用するように修正 >>
        EvidenceDao evidenceDao = new EvidenceDao();
        ArrayList<Evidence> evidenceList = evidenceDao.getEvidencList01(pramDto);
        Map<String,ArrayList<Object>> eviListMap= new HashMap<String,ArrayList<Object>>();
        ArrayList<Object> eviList = null;
        LinkedHashMap<String,Object> eviMap = null;
        String kamokuKey = "";
        for (Evidence evi : evidenceList){
            eviMap = new LinkedHashMap<String,Object>();
            // ディレクトリは学籍番号からハイフンを除去し、英語を小文字にしたもの
            eviMap.put("code", "/" + evi.getUid().replace("-", "").toLowerCase() +
                        "/" + evi.getDir() +
                        "/" + evi.getEvidence() +
                        "/" + evi.getEvidence()  + ".html");
            eviMap.put("name", evi.getEvidence());

            // キーを生成
            kamokuKey = evi.getNendo().toString() + evi.getJikanwariShozokucd() + evi.getJikanwaricd();

            // キーが存在する場合
            if (eviListMap.containsKey(kamokuKey)){
                // Listを取り出す
                eviList = eviListMap.get(kamokuKey);
            }
            // キーが存在しない場合
            else {
                // Listを作成
                eviList = new ArrayList<Object>();
                eviListMap.put(kamokuKey, eviList);
            }
            // 最後にListにMapを追加
            eviList.add(eviMap);
        }
        // 2015.02.24 SVC DirectoryからEvidenceを利用するように修正 <<

        ArrayList<GoalSeiseki> goalSeisekiList = goalSeisekiDao.getGoalSeisekiList01(pramDto);
//System.out.println("********** goalSeisekiList.size() = "+goalSeisekiList.size());

        for(int goal: goalList){
            LinkedHashMap<String,Object> dataMap = new LinkedHashMap<String,Object>();

            dataMap.put("goal", goal);
            ArrayList<Object> seisekiList = new ArrayList<Object>();

             for(KakuteiSeiseki nendo :nendoList){

                LinkedHashMap<String,Object> seisekiMap = new LinkedHashMap<String,Object>();

                seisekiMap.put("year", nendo.getNinteiNendo());
// 2017.02.22 SVC)Furuta CHANGE START >> 前後期制→ターム制移行
                //seisekiMap.put("sem", (CommonConst.GAKKIKBN_ZENKI.equals(nendo.getNinteiGakkikbncd())? CommonConst.SEMESTER_ZENKI : CommonConst.SEMESTER_KOUKI ));
                seisekiMap.put("sem", nendo.getNinteiKaikokbncd());
// 2017.02.22 SVC)Furuta CHANGE END   <<

                ArrayList<Object> kamokuList = new ArrayList<Object>();
                for(GoalSeiseki goalSeiseki:goalSeisekiList){
// 2017.03.15 SVC)Furuta ADD START >> ポートフォリオ改善
                    if(!(goalSeiseki.getNinteiKaikokbncd().equals("6") || goalSeiseki.getNinteiKaikokbncd().equals("7") || goalSeiseki.getNinteiKaikokbncd().equals("8") || goalSeiseki.getNinteiKaikokbncd().equals("9"))){
                        String tempKaiko = goalSeiseki.getNinteiGakkikbncd().equals("1") ? "6" : "8";
                        goalSeiseki.setNinteiKaikokbncd(tempKaiko);
                    }
// 2017.03.15 SVC)Furuta ADD END   <<
                    if(!(nendo.getNinteiNendo().equals(goalSeiseki.getNinteiNendo())
                            && nendo.getNinteiKaikokbncd().equals(goalSeiseki.getNinteiKaikokbncd()))){
                        continue;
                    }

                    int kamokuPercentage = 0;

                    // 2015.02.12 SVC 修正 >>
                    if(goal==1 && goalSeiseki.getCode1() != 0) kamokuPercentage = goalSeiseki.getCode1();
                    if(goal==2 && goalSeiseki.getCode2() != 0) kamokuPercentage = goalSeiseki.getCode2();
                    if(goal==3 && goalSeiseki.getCode3() != 0) kamokuPercentage = goalSeiseki.getCode3();
                    if(goal==4 && goalSeiseki.getCode4() != 0) kamokuPercentage = goalSeiseki.getCode4();
                    if(goal==5 && goalSeiseki.getCode5() != 0) kamokuPercentage = goalSeiseki.getCode5();
                    if(goal==6 && goalSeiseki.getCode6() != 0) kamokuPercentage = goalSeiseki.getCode6();
                    if(goal==7 && goalSeiseki.getCode7() != 0) kamokuPercentage = goalSeiseki.getCode7();
// 2017.03.13 SVC)Furuta CHANGE START >> ポートフォリオ改善
                    if(goal==0 && (goalSeiseki.getCode() == null || goalSeiseki.getCode().equals("0"))) kamokuPercentage = 100;
                    //if(goal==0 && goalSeiseki.getCode() == null) kamokuPercentage = 100;
// 2017.03.13 SVC)Furuta CHANGE END   <<

                    if (kamokuPercentage == 0){
                        continue;
                    }
                    // 2015.02.12 SVC 修正 <<

                    LinkedHashMap<String,Object> kamokuMap = new LinkedHashMap<String,Object>();
                    kamokuMap.put("dep",goalSeiseki.getJikanwariShozokucd());
                    kamokuMap.put("kind",getKind(goalSeiseki.getJikanwariShozokucd(),goalSeiseki.getHitsusenkbncd()));
                    kamokuMap.put("code",goalSeiseki.getJikanwaricd());
                    kamokuMap.put("name",getLangObj(goalSeiseki.getKamokunm(),goalSeiseki.getKamokunmeng()));
                    kamokuMap.put("credit",goalSeiseki.getTanisu().doubleValue());
                    kamokuMap.put("mark",goalSeiseki.getHyogonm());
                    kamokuMap.put("markcd",goalSeiseki.getHyogocd());

                    // 2015.02.24 SVC DirectoryからEvidenceを利用するように修正 >>
//                    if(dirListMap.containsKey(goalSeiseki.getKamokunm())){
//                        kamokuMap.put("evidence",dirListMap.get(goalSeiseki.getKamokunm()));
//                    }
//                    else {
//                        kamokuMap.put("evidence",new ArrayList<Object>());
//                    }
                    // キーを作成
                    kamokuKey = goalSeiseki.getNinteiNendo() + goalSeiseki.getJikanwariShozokucd() + goalSeiseki.getJikanwaricd();
                    if (eviListMap.containsKey(kamokuKey)){
                        kamokuMap.put("evidence",eviListMap.get(kamokuKey));
                    }
                    else {
                        kamokuMap.put("evidence", new ArrayList<Object>());
                    }
                    // 2015.02.24 SVC DirectoryからEvidenceを利用するように修正 <<

                    // 2015.02.12 SVC 追加 >>
                    kamokuMap.put("percentage", kamokuPercentage);
                    // 2015.02.12 SVC 追加 <<

                    kamokuList.add(kamokuMap);
                }
                seisekiMap.put("course", kamokuList);

                seisekiList.add(seisekiMap);
            }

            dataMap.put("content", seisekiList);

            ret.add(dataMap);

        }

        logger.info("ruser="+pramDto.getRuser()+", denied : lang="+pramDto.getLang());
        logger.info(CommonConst.END_LOG + method_name);

        return ret;
    }

    private String getKind(String jikanwariShozokucd, String hitsusenkbncd) {

        String rtn = "";

        //専門
        if(!CommonConst.JIKANWARISHOZOKUCD_KYOYO.equals(jikanwariShozokucd)){
            //必修
            if(CommonConst.HITSUSENKBN_HITSU.equals(hitsusenkbncd)
                    || CommonConst.HITSUSENKBN_HITSUSEN.equals(hitsusenkbncd)){
                rtn = CommonConst.SENMON_HITSU;
            }
            //選択
            else {
                rtn = CommonConst.SENMON_SEN;
            }
        }
        //教養
        else {
            //必修
            if(CommonConst.HITSUSENKBN_HITSU.equals(hitsusenkbncd)
                    || CommonConst.HITSUSENKBN_HITSUSEN.equals(hitsusenkbncd)){
                rtn = CommonConst.KYOYO_HITSU;
            }
            //選択
            else {
                rtn = CommonConst.KYOYO_SEN;
            }
        }

        return rtn;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        //学生番号、所属コードが設定されていない場合は処理しない。
        if(params.containsKey(ParamsConst.UID)) {
            rtn = true;
        }
        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }
}
