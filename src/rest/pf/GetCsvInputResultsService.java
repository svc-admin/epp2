package rest.pf;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.PropertiesUtil;
import util.Tools;
import constants.CommonConst;
import dao.ResultsDao;
import dao.entity.Results;
import dto.SysPropertiesDto;

/**
 *  REST service using JSONIC, version 2015-02-05 (since 2015-02-05)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class GetCsvInputResultsService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();
    boolean enFlag = false;

    // コンストラクタ
    public GetCsvInputResultsService() {
        rid = 0;
        mode = 0;
    }

    /**
     * (rest.)pf/GetCsvInputResults.json?<br />
     * @see rest.AbstractMapService#getJsonData()
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

        ResultsDao resDao = new ResultsDao();
        ArrayList<Object> resultsList = new ArrayList<Object>();
        ArrayList<Results> res = null;

        PropertiesUtil propUtil = new PropertiesUtil();
        SysPropertiesDto sysPropDto = propUtil.getSysProperties();

        // パラメータのカンマ区切り学生番号を配列に格納しなおす
        // 2019.05.16 s.furuta chg start バグ対応（restrictionなし教員での学生抽出時エラーが起きる）
        //String[] uidList = pramDto.getUid().split(",");
        String[] uidList = pramDto.getTargetUids().split(",");
        // 2019.05.16 s.furuta chg end
        for(String uid : uidList) {
            // 引数の学生番号数だけループ
            res = resDao.getResults04(pramDto, uid);

            for(Results result : res){
                LinkedHashMap<String,Object> lineRes = new LinkedHashMap<String,Object>();
                lineRes.put("G_SYSNO", result.getgSysno());
                lineRes.put("KAMOKUCD", result.getKamokucd());
                lineRes.put("date", result.getDate());
                lineRes.put("classification", result.getClassification());
                lineRes.put("classification_custom", result.getClassificationCustom());
                lineRes.put("title", result.getTitle());
                lineRes.put("comment", result.getComment());
                lineRes.put("author_name", result.getAuthorName());
                lineRes.put("author_type", result.getAuthorType());
                lineRes.put("presentation_format", result.getPresentationFormat());
                lineRes.put("medium_title", result.getMediumTitle());
                lineRes.put("medium_class", result.getMediumClass());
                lineRes.put("has_impact_factor", result.getHasImpactFactor());
                lineRes.put("impact_factor", result.getImpactFactor());
                lineRes.put("location", result.getLocation());
                lineRes.put("publisher", result.getPublisher());
                lineRes.put("isbn", result.getIsbn());
                lineRes.put("journal_volume", result.getJournalVolume());
                lineRes.put("journal_number", result.getJournalNumber());
                lineRes.put("page_from", result.getPageFrom());
                lineRes.put("page_to", result.getPageTo());
                lineRes.put("published_year", result.getPublishedYear());
                lineRes.put("published_month", result.getPublishedMonth());
                lineRes.put("has_reviewed", result.getHasReviewed());
                lineRes.put("original_medium_name", result.getOriginalMediumName());   // seiseki_for_evidence.dirを仮格納
                lineRes.put("original_author_name", result.getOriginalAuthorName());   // seiseki_for_evidence.evidenceを仮格納
                lineRes.put("original_publisher", sysPropDto.getEvuri());              // evUriを仮格納
                lineRes.put("original_page_from", result.getOriginalPageFrom());
                lineRes.put("original_page_to", result.getOriginalPageTo());
                lineRes.put("original_published_year", result.getOriginalPublishedYear());
                lineRes.put("original_published_month", result.getOriginalPublishedMonth());
                lineRes.put("insert_data", result.getInsertDate());
                lineRes.put("update_date", result.getUpdateDate());
                lineRes.put("userid", result.getUserid());                             // 学修成果フラグを仮格納

                resultsList.add(lineRes);
            }
        }
        ret.put("resultsList", resultsList);

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    /* (非 Javadoc)
     * @see rest.AbstractMapService#deleteJsonData()
     */
    @Override
    protected Boolean deleteJsonData() {
        String method_name = "deleteJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        logger.info(CommonConst.END_LOG + method_name);
        return true;
    }

    /**
     * (rest.)pf/GetCsvInputResults.json3<br />
     * @see rest.AbstractMapService#addJsonData()
     * @return : true/false
     */
    @Override
    protected Boolean addJsonData() {
        String method_name = "addJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        logger.info(CommonConst.END_LOG + method_name);
        return true;
    }

    /* (非 Javadoc)
     * @see rest.AbstractMapService#checkParams(java.util.Map)
     */
    @Override
    protected boolean checkParams(Map<String, Object> params) {
        return true;
    }
}
