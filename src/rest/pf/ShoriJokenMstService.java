package rest.pf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.GpaShugakuninteiBukyokuMstDao;
import dao.ShoriJokenMstDao;
import dao.entity.ShoriJokenMst;

/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class ShoriJokenMstService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public ShoriJokenMstService() {
        //権限ID
        rid = 2;
        mode = 0;
    }

    /**
     * (rest.)pf/programs.json?lang="ja"<br />
     * ユーザIDが持つ参照権限のリストを返す。(AuthorityTable()参照)
     * @return : JSON String
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();
        ArrayList<Object> aaData = new ArrayList<Object>();
        ShoriJokenMstDao ShoriJokenMstDao = new ShoriJokenMstDao();
        ArrayList<ShoriJokenMst> aut = ShoriJokenMstDao.getShoriJokenMst();


        for(ShoriJokenMst ShoriJokenMst : aut){
            ArrayList<Object> o = new ArrayList<Object>();
            o.add(ShoriJokenMst.getShorinendo());
            if (ShoriJokenMst.getGakkikbncd() != null) {
                o.add(("3".equals(ShoriJokenMst.getGakkikbncd()) ? "年度末" : ShoriJokenMst.getGakkikbncd() + "月"));
            } else {
                o.add((""));
            }
            o.add(ShoriJokenMst.getKijundate());
            o.add(ShoriJokenMst.getGakkikbncd());
            aaData.add(o);

        }

        ret.put("aaData", aaData);
        ret.put("aoColumns", aoColumns.get(pramDto.getLang()));

        return ret;
    }

    final Map<String, ArrayList<Map<String,String>>> aoColumns = // DBの構造に入っていないのでしかたなく
        new HashMap<String, ArrayList<Map<String,String>>>() {{
            put(CommonConst.JAPANESE, new ArrayList<Map<String,String>>() {{
                add(new HashMap<String,String>() {{
                    put("sTitle", "処理年度");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "判定時期");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "基準日");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "学期区分コード");
                }});
            }});
            put(CommonConst.ENGLISH, new ArrayList<Map<String,String>>() {{
                add(new HashMap<String,String>() {{
                    put("sTitle", "shori_nendo");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "hanteijiki");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "kijunbi");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "gakkikbncd");
                }});
            }});
        }};

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    @Override
    protected Boolean deleteJsonData() {
       String method_name = "deleteJsonData";
       logger.info(CommonConst.END_LOG + method_name);

       GpaShugakuninteiBukyokuMstDao gpaShugakuninteiBukyokuMstDao = new GpaShugakuninteiBukyokuMstDao();
       Boolean res = gpaShugakuninteiBukyokuMstDao.deleteGpaShugakuninteiBukyokuMst01(pramDto);

       return res;
    }

    @Override
    protected Boolean addJsonData() {
       String method_name = "addJsonData";
       logger.info(CommonConst.END_LOG + method_name);
       ShoriJokenMstDao  ShoriJokenMstDao = new ShoriJokenMstDao();
       Boolean res = ShoriJokenMstDao.addShoriJokenMst01(pramDto);

       return res;
    }

    protected LinkedHashMap<String, Object> makeRetJson(Boolean ret){
        LinkedHashMap<String, Object> retMap = new LinkedHashMap<String, Object>();

        if (ret) {
            retMap.put("status", "success");
        } else {
            retMap.put("status", "error");
        }
        return retMap;
    }
}
