package rest.pf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.ShugakusienHanteiShuseiDao;
import dao.entity.ShugakusienHanteiShusei;
import dto.ParameterDto;

/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2019 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class ReflectModifyService extends AbstractMapService {
	private static Logger logger = Tools.getCallerLogger();
	private static final int G_SYSNO = 0;
	private static final int BIKOU = 1;
	private static final int Z_GPA = 2;
	private static final int Z_TANI = 3;
	private static final int H_SOTSU = 4;
	private static final int H_TANI = 5;
	private static final int H_SHU = 6;
	private static final int H_ZEN = 7;
	private static final int K_TANI = 8;
	private static final int K_GPA = 9;
	private static final int K_SHU = 10;
	private static final int S_TANI = 11;
	private static final int S_SHU = 12;
	private static final int T_GPA = 13;
	private static final int N_GPA = 14;

	public ReflectModifyService() {
		// 権限ID
		rid = 2;
		mode = 0;
	}

	/**
	 * 判定結果の修正情報を取得する
	 * @return : JSON String
	 */
	@Override
	protected LinkedHashMap<String, Object> getJsonData() {
		String method_name = "getJsonData";
		logger.info(CommonConst.START_LOG + method_name);
		LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

		try {
			// 判定結果修正データの取得
			ArrayList<ShugakusienHanteiShusei> modifyList = new ArrayList<ShugakusienHanteiShusei>();
			ShugakusienHanteiShuseiDao shuseiDao = new ShugakusienHanteiShuseiDao();
			modifyList = shuseiDao.getModifyData(pramDto);

			ret.put("result", true);
			ret.put("dataList", modifyList);
		} catch(Exception e) {
			logger.info(e.getMessage());
			ret.put("result", false);
		}
		return ret;
	}

	@Override
	protected Boolean deleteJsonData() {
		return null;
	}

	@Override
	protected Boolean addJsonData() {
		String method_name = "addJsonData";
		logger.info(CommonConst.START_LOG + method_name);
		Boolean ret = false;

		try {
			// パラメータの分解と設定
			String[] paramList = null;
			if (pramDto.getEtcParam() != null) {
				paramList = pramDto.getEtcParam().split("#");
			}

			if (paramList != null) {
				// 登録用のオブジェクト設定
				ArrayList<ParameterDto> paramDtoList = new ArrayList<ParameterDto>();
				for (int i = 0; i < paramList.length; i++) {
					String[] param = paramList[i].split("_");
					ShugakusienHanteiShusei shusei = new ShugakusienHanteiShusei();
					ParameterDto pDto = new ParameterDto();
					pDto.setRuser(pramDto.getRuser());
					pDto.setgBukyokucd(pramDto.getgBukyokucd());
					pDto.setGakunen(pramDto.getGakunen());
					pDto.setShoriJokenMst(pramDto.getShoriJokenMst());
					shusei.setG_sysno(param[G_SYSNO]);
					shusei.setZ_gpa_result(param[Z_GPA]);
					shusei.setZ_tani_result(param[Z_TANI]);
					shusei.setH_sotsu_result(param[H_SOTSU]);
					shusei.setH_tani_result(param[H_TANI]);
					shusei.setH_shu_result(param[H_SHU]);
					shusei.setH_zen_result(param[H_ZEN]);
					shusei.setK_tani_result(param[K_TANI]);
					shusei.setK_gpa_result(param[K_GPA]);
					shusei.setK_shu_result(param[K_SHU]);
					shusei.setS_tani_result(param[S_TANI]);
					shusei.setS_shu_result(param[S_SHU]);
					shusei.setT_gpa((param[T_GPA] != null ? new BigDecimal(param[T_GPA]) : null));
					shusei.setN_gpa((param[N_GPA] != null ? new BigDecimal(param[N_GPA]) : null));
					shusei.setBikou(param[BIKOU]);
					pDto.setHanteiShuseiData(shusei);
					paramDtoList.add(pDto);
				}
				// 削除して追加
				ShugakusienHanteiShuseiDao shuseiDao = new ShugakusienHanteiShuseiDao();
				ret = shuseiDao.registModifyData(pramDto, paramDtoList);
			}
		} catch(Exception e) {
			logger.info(e.getMessage());
		}
		return ret;
	}

	protected LinkedHashMap<String, Object> makeRetJson(Boolean ret){
		LinkedHashMap<String, Object> retMap = new LinkedHashMap<String, Object>();

		if (ret) {
			retMap.put("status", true);
		} else {
			retMap.put("status", false);
		}
		return retMap;
	}

	@Override
	protected boolean checkParams(Map<String, Object> params) {
		return true;
	}
}
