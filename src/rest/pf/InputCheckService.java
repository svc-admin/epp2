package rest.pf;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.GpaShugakuninteiBukyokuMstDao;
import dao.entity.GpaShugakuninteiBukyokuMst;

/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class InputCheckService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public InputCheckService() {
        //権限ID
        rid = 2;
        mode = 0;
    }

    /**
     * (rest.)pf/programs.json?lang="ja"<br />
     * ユーザIDが持つ参照権限のリストを返す。(AuthorityTable()参照)
     * @return : JSON String
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();
        ArrayList<Object> aaData = new ArrayList<Object>();
        GpaShugakuninteiBukyokuMstDao GpaShugakuninteiBukyokuMstDao = new GpaShugakuninteiBukyokuMstDao();
        /*部局コードが6桁の場合、親の親を探すのに時間がかかる為*/
        if(pramDto.getGpaShugakuninteiBukyokuMst().getgBukyokucd().length() > 4){
          pramDto.getGpaShugakuninteiBukyokuMst().setgBukyokucd(pramDto.getGpaShugakuninteiBukyokuMst().getgBukyokucd().substring(0,2));
        }
        ArrayList<GpaShugakuninteiBukyokuMst> aut = GpaShugakuninteiBukyokuMstDao.InputCheck(pramDto);

        for(GpaShugakuninteiBukyokuMst gpaShugakuninteiBukyokuMst : aut){
            ArrayList<Object> o = new ArrayList<Object>();
            o.add(gpaShugakuninteiBukyokuMst.getgBukyokucd());
            o.add(gpaShugakuninteiBukyokuMst.getShori_end());
            aaData.add(o);
        }

        ret.put("aaData", aaData);
        return ret;
    }


    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    @Override
    protected Boolean deleteJsonData() {
      Boolean res = true;
      return res;
    }

    @Override
    protected Boolean addJsonData() {
       Boolean res = true;
       return res;
    }
}
