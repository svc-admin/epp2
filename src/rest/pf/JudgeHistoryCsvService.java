package rest.pf;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.ShugakusienHanteiDataDao;
import dao.entity.ShugakusienHanteiData;

/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2019 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class JudgeHistoryCsvService extends AbstractMapService {
	private static Logger logger = Tools.getCallerLogger();

	public JudgeHistoryCsvService() {
		// 権限ID
		rid = 2;
		mode = 0;
	}

	/**
	 * 判定結果の履歴情報を取得する
	 * @return : JSON String
	 */
	@Override
	protected LinkedHashMap<String, Object> getJsonData() {
		String method_name = "getJsonData";
		logger.info(CommonConst.START_LOG + method_name);
		LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

		try {
			// パラメータの分解と設定
			if (pramDto.getEtcParam() != null) {
				String[] params = pramDto.getEtcParam().split("-");
				pramDto.getShoriJokenMst().setShorinendo(Integer.parseInt(params[0]));
				pramDto.getShoriJokenMst().setGakkikbncd(params[1]);
				pramDto.getShoriJokenMst().setKijundate(params[2]);
				pramDto.setgBukyokucd(params[3]);
				pramDto.setGakunen(Integer.parseInt(params[4]));
				pramDto.setNum(Integer.parseInt(params[5]));
			}
			// 判定結果履歴データの取得
			ArrayList<ShugakusienHanteiData> csvList = new ArrayList<ShugakusienHanteiData>();
			ShugakusienHanteiDataDao hanteiDao = new ShugakusienHanteiDataDao();
			csvList = hanteiDao.getHanteiDataForCsv(pramDto);

			ret.put("result", true);
			ret.put("dataList", csvList);
		} catch(Exception e) {
			logger.info(e.getMessage());
			ret.put("result", false);
		}
		return ret;
	}

	@Override
	protected Boolean deleteJsonData() {
		return null;
	}

	@Override
	protected Boolean addJsonData() {
		return null;
	}

	@Override
	protected boolean checkParams(Map<String, Object> params) {
		return true;
	}
}
