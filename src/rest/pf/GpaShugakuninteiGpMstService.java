package rest.pf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.GpaShugakuninteiGpMstDao;
import dao.entity.GpaShugakuninteiGpMst;

/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class GpaShugakuninteiGpMstService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public GpaShugakuninteiGpMstService() {
        //権限ID
        rid = 2;
        mode = 0;
    }

    /**
     * (rest.)pf/programs.json?lang="ja"<br />
     * ユーザIDが持つ参照権限のリストを返す。(AuthorityTable()参照)
     * @return : JSON String
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

        ArrayList<Object> aaData = new ArrayList<Object>();
        GpaShugakuninteiGpMstDao GpaShugakuninteiGPMstDao = new GpaShugakuninteiGpMstDao();
        System.out.println(pramDto.getGpaShugakuninteiGpMst().getgBukyokucd());
        System.out.println(pramDto.getGpaShugakuninteiGpMst().getShori_start());
        System.out.println(pramDto.getGpaShugakuninteiGpMst().getShori_end());
        ArrayList<GpaShugakuninteiGpMst> aut = GpaShugakuninteiGPMstDao.getGpaShugakuninteiGpMst(pramDto);

        for(GpaShugakuninteiGpMst gpaShugakuninteiGpMst : aut){
            ArrayList<Object> o = new ArrayList<Object>();
            o.add(null);
            o.add(gpaShugakuninteiGpMst.getgBukyokucd());
            o.add(gpaShugakuninteiGpMst.getShori_start());
            o.add(gpaShugakuninteiGpMst.getShori_end());
            o.add(gpaShugakuninteiGpMst.getHyogonm());
            o.add(gpaShugakuninteiGpMst.getGp());
            o.add("1".equals(gpaShugakuninteiGpMst.getGpaTaisho().toString()) ? "対象にする" : "対象にしない");
            aaData.add(o);
        }
        ret.put("aaData", aaData);
        ret.put("aoColumns", aoColumns.get(pramDto.getLang()));


        return ret;
    }

    final Map<String, ArrayList<Map<String,String>>> aoColumns = // DBの構造に入っていないのでしかたなく
        new HashMap<String, ArrayList<Map<String,String>>>() {{
            put(CommonConst.JAPANESE, new ArrayList<Map<String,String>>() {{
                add(new HashMap<String,String>() {{
                    put("sTitle", "削除");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "部局コード");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "開始年度");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "終了年度");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "標語");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "GP");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "GPA計算対象");
                }});
            }});
            put(CommonConst.ENGLISH, new ArrayList<Map<String,String>>() {{
                add(new HashMap<String,String>() {{
                    put("sTitle", "delete");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "bukyokucd");
                }});
                add(new HashMap<String,String>() {{
                	put("sTitle", "kaishinendo");
                }});
                add(new HashMap<String,String>() {{
                	put("sTitle", "shuryounendo");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "hyogo");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "gp");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "gptaisho");
                }});
            }});
        }};


    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    @Override
    protected Boolean deleteJsonData() {
       String method_name = "deleteJsonData";
       logger.info(CommonConst.END_LOG + method_name);

       GpaShugakuninteiGpMstDao gpaShugakuninteiGpMstDao = new GpaShugakuninteiGpMstDao();
       Boolean res = gpaShugakuninteiGpMstDao.deleteGpaShugakuninteiGpMst01(pramDto);

       return res;
    }

    @Override
    protected Boolean addJsonData() {
       String method_name = "addJsonData";
       logger.info(CommonConst.END_LOG + method_name);
       System.out.println(pramDto.getGpaShugakuninteiGpMst().getgBukyokucd());
       System.out.println(pramDto.getGpaShugakuninteiGpMst().getShori_start());
       System.out.println(pramDto.getGpaShugakuninteiGpMst().getShori_end());
       System.out.println(pramDto.getGpaShugakuninteiGpMst().getHyogonm());
       System.out.println(pramDto.getGpaShugakuninteiGpMst().getGp());
       System.out.println(pramDto.getGpaShugakuninteiGpMst().getGpaTaisho());
       GpaShugakuninteiGpMstDao gpaShugakuninteiGpMstDao = new GpaShugakuninteiGpMstDao();
       Boolean res = gpaShugakuninteiGpMstDao.addGpaShugakuninteiGpMst01(pramDto);

       return res;
    }
}
