package rest.pf;
import java.util.ArrayList;
import java.util.Map;

import ku.util.ProgramTable;

import org.apache.log4j.Logger;

import rest.AbstractListService;
import util.Tools;
import constants.CommonConst;
/**
 *  REST service using JSONIC,
 *  version 2017-01-20 ログ縮小 by nakano
 *  version 2014-02-09 (since 2014-01-30)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class ProgramsService extends AbstractListService {
    static private ProgramTable pgTbl = new ProgramTable();
    private static Logger logger = Tools.getCallerLogger();

    public ProgramsService() {
        //権限ID
        rid = 0;
        mode = 0;
    }

    /**
     * (rest.)pf/programs.json?lang="ja"<br />
     * (学生が実際に存在する)年度別の学部、学科、コースをコードとともに返す。(ProgramTable()参照)
     * @return : JSON String
     */
    @Override
    public ArrayList<Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        ArrayList<Object> ret = new ArrayList<Object>();

        ret.add(pgTbl.getJSON(pramDto));
//		ログを縮小 2017-01-20 by nakano
//        logger.info("Allowed ruser="+pramDto.getRuser()+" at lang="+pramDto.getLang()+", ret="+ret);
        logger.info("Allowed ruser="+pramDto.getRuser()+" at lang="+pramDto.getLang()+", ret="+ret.toString().substring(0, 100)+" ...");

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }
}
