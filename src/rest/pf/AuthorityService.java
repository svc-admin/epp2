package rest.pf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.AuthorityDao;
import dao.ShozokuMstDao;
import dao.entity.Authority;
import dao.entity.ShozokuMst;
/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class AuthorityService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public AuthorityService() {
        //権限ID
        // 2015.02.23 SVC 修正 >>
//        rid = 0;
        rid = 2;
        // 2015.02.23 SVC 修正 <<
        mode = 0;
    }

    /**
     * (rest.)pf/programs.json?lang="ja"<br />
     * ユーザIDが持つ参照権限のリストを返す。(AuthorityTable()参照)
     * @return : JSON String
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();


        ArrayList<Object> aaData = new ArrayList<Object>();
        AuthorityDao authorityDao = new AuthorityDao();
        ShozokuMstDao shozokuMstDao = new ShozokuMstDao();

//        ArrayList<Authority> aut = authorityDao.getAuthority01(pramDto.getUid()); // 2018-11-28 commented out by KU)Nakano 権限リストにデフォルトの権限を表示しないため
        ArrayList<Authority> aut = authorityDao.getAuthority03(pramDto.getUid()); // 2018-11-28 add by KU)Nakano 権限リストにデフォルトの権限を表示しないため

        for(Authority authority : aut){
            ArrayList<Object> o = new ArrayList<Object>();
            o.add(authority.getUid());
            if(authority.getYokenNendo().equals(0)){
                o.add("");
            }else{
                o.add(authority.getYokenNendo());
            }
            o.add(authority.getDaigakuinKbncd());
            o.add(authority.getShozokucd());
            if(authority.getShozokucd().equals("")){
            o.add("");
            } else {
                ArrayList<ShozokuMst> shozokuMstList = shozokuMstDao.getShozokuMstList01(pramDto,authority.getShozokucd());
                if(shozokuMstList.size() == 0) continue;
                ShozokuMst shozokuMst = shozokuMstList.get(0);
                String affil = getLangObj(shozokuMst.getShozokunm1() +" "+ shozokuMst.getShozokunm2() +" "+ shozokuMst.getShozokunm3(),
                        shozokuMst.getShozokunmeng1() +" "+ shozokuMst.getShozokunmeng2() +" "+ shozokuMst.getShozokunmeng3());
                o.add(affil);
            }
            o.add(authority.getSysno());
            aaData.add(o);
        }
        ret.put("aaData",aaData);
        ret.put("aoColumns", aoColumns.get(pramDto.getLang()));
        logger.info("authoritylist for dataTables.js allowed where nyugaku_nendo="+pramDto.getNyugakunendo()
                +", shozokucd="+pramDto.getShozokucd()+", lang="+pramDto.getLang()+", ruser="+pramDto.getRuser());

        logger.info("ruser="+pramDto.getRuser()+", denied : lang="+pramDto.getLang());

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    final Map<String, ArrayList<Map<String,String>>> aoColumns = // DBの構造に入っていないのでしかたなく
            new HashMap<String, ArrayList<Map<String,String>>>() {{
                put(CommonConst.JAPANESE, new ArrayList<Map<String,String>>() {{
                    add(new HashMap<String,String>() {{
                        put("sTitle", "ユーザID");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "年度");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "大学院区分コード");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "所属コード");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "所属名称");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "学生番号");
                    }});
                }});
                put(CommonConst.ENGLISH, new ArrayList<Map<String,String>>() {{
                    add(new HashMap<String,String>() {{
                        put("sTitle", "UserID");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Year");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "DgaigakuinKbnCD");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Department Code");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "Department Name");
                    }});
                    add(new HashMap<String,String>() {{
                        put("sTitle", "StudentNO");
                    }});
                }});
            }};

    @Override
    protected Boolean deleteJsonData() {
       String method_name = "deleteJsonData";
       logger.info(CommonConst.END_LOG + method_name);

       AuthorityDao authorityDao = new AuthorityDao();
       Boolean res = authorityDao.deleteAuthority01(pramDto);

       return res;
    }

    @Override
    protected Boolean addJsonData() {
       String method_name = "addJsonData";
       logger.info(CommonConst.END_LOG + method_name);

       AuthorityDao authorityDao = new AuthorityDao();
       Boolean res = authorityDao.addAuthority01(pramDto);

       return res;
    }
}
