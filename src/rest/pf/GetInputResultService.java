package rest.pf;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.AttachmentsDao;
import dao.ExternalEnglishTestsDao;
import dao.ResultsDao;
import dao.SeisekiForEvidenceDao;
import dao.StudyingAbroadExperiencesDao;
import dao.entity.Attachments;
import dao.entity.ExternalEnglishTests;
import dao.entity.Results;
import dao.entity.SeisekiForEvidence;
import dao.entity.StudyingAbroadExperieces;

/**
 *  REST service using JSONIC, version 2015-02-05 (since 2015-02-05)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class GetInputResultService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();
    boolean enFlag = false;

    // コンストラクタ
    public GetInputResultService() {
        rid = 0;
        mode = 0;
    }

    /**
     * (rest.)pf/inputResults.json?<br />
     * @see rest.AbstractMapService#getJsonData()
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

        // attachments
        AttachmentsDao attDao = new AttachmentsDao();
        ArrayList<Object> attachmentsList = new ArrayList<Object>();
        ArrayList<Attachments> resAtt = null;
        // results
        ResultsDao resDao = new ResultsDao();
        ArrayList<Object> resultsList = new ArrayList<Object>();
        ArrayList<Results> resRes = null;
        // external_english_tests
        ExternalEnglishTestsDao engDao = new ExternalEnglishTestsDao();
        ArrayList<Object> englishList = new ArrayList<Object>();
        ArrayList<ExternalEnglishTests> resEng = null;
        // studying_abroad_experiences
        StudyingAbroadExperiencesDao abrDao = new StudyingAbroadExperiencesDao();
        ArrayList<Object> abroadList = new ArrayList<Object>();
        ArrayList<StudyingAbroadExperieces> resAbr = null;
        // seiseki_for_evidence
        SeisekiForEvidenceDao eviDao = new SeisekiForEvidenceDao();
        ArrayList<Object> evidenceList = new ArrayList<Object>();
        ArrayList<SeisekiForEvidence> resEvi = null;

        // 成果物（アップロードファイル）の情報を取得:attachments
        resAtt = attDao.getAttachments01(pramDto);
        for(Attachments attatchment : resAtt){
            LinkedHashMap<String,Object> lineAtt = new LinkedHashMap<String,Object>();

            lineAtt.put("id", attatchment.getId());
            lineAtt.put("g_sysno", attatchment.getgSysno());
            lineAtt.put("kamokucd", attatchment.getKamokucd());
            lineAtt.put("filename", attatchment.getFilename());
            lineAtt.put("filepath", attatchment.getFilepath());
            lineAtt.put("filesize", attatchment.getFilesize());
            lineAtt.put("insert_date", attatchment.getInsertDate());
            lineAtt.put("update_date", attatchment.getUpdateDate());
            lineAtt.put("userid", attatchment.getUserid());

            attachmentsList.add(lineAtt);
        }
        // ベースの入力情報を取得:results
        resRes = resDao.getResults01(pramDto);
        for(Results result : resRes){
            LinkedHashMap<String,Object> lineRes = new LinkedHashMap<String,Object>();

            lineRes.put("g_sysno", result.getgSysno());
            lineRes.put("kamokucd", result.getKamokucd());
            lineRes.put("date", result.getDate());
            lineRes.put("classification", result.getClassification());
            lineRes.put("title", result.getTitle());
            lineRes.put("comment", result.getComment());
            lineRes.put("author_name", result.getAuthorName());
            lineRes.put("author_type", result.getAuthorType());
            lineRes.put("presentation_format", result.getPresentationFormat());
            lineRes.put("medium_title", result.getMediumTitle());
            lineRes.put("medium_class", result.getMediumClass());
            lineRes.put("has_impact_factor", result.getHasImpactFactor());
            lineRes.put("impact_factor", result.getImpactFactor());
            lineRes.put("location", result.getLocation());
            lineRes.put("publisher", result.getPublisher());
            lineRes.put("isbn", result.getIsbn());
            lineRes.put("journal_volume", result.getJournalVolume());
            lineRes.put("journal_number", result.getJournalNumber());
            lineRes.put("page_from", result.getPageFrom());
            lineRes.put("page_to", result.getPageTo());
            lineRes.put("published_year", result.getPublishedYear());
            lineRes.put("published_month", result.getPublishedMonth());
            lineRes.put("has_reviewed", result.getHasReviewed());
            lineRes.put("original_medium_name", result.getOriginalMediumName());
            lineRes.put("original_author_name", result.getOriginalAuthorName());
            lineRes.put("original_publisher", result.getOriginalPublisher());
            lineRes.put("original_page_from", result.getOriginalPageFrom());
            lineRes.put("original_page_to", result.getOriginalPageTo());
            lineRes.put("original_published_year", result.getOriginalPublishedYear());
            lineRes.put("original_published_month", result.getOriginalPublishedMonth());
            lineRes.put("insert_date", result.getInsertDate());
            lineRes.put("update_date", result.getUpdateDate());
            lineRes.put("userid", result.getUserid());

            resultsList.add(lineRes);
        }
        // 「英語外部試験のスコア」入力情報を取得:external_english_tests
        resEng = engDao.getExternalEnglishTests01(pramDto);
        for(ExternalEnglishTests english : resEng){
            LinkedHashMap<String,Object> lineEng = new LinkedHashMap<String,Object>();

            lineEng.put("id", english.getId());
            lineEng.put("g_sysno", english.getgSysno());
            lineEng.put("kamokucd", english.getKamokucd());
            lineEng.put("test_name", english.getTestName());
            lineEng.put("score", english.getScore());
            lineEng.put("date", english.getDate());
            lineEng.put("insert_date", english.getInsertDate());
            lineEng.put("update_date", english.getUpdateDate());
            lineEng.put("userid", english.getUserid());

            englishList.add(lineEng);
        }
        // 「海外での学修経験」入力情報を取得:studying_abroad_experiences
        resAbr = abrDao.getStudyingAbroadExperiences01(pramDto);
        for(StudyingAbroadExperieces abroad : resAbr){
            LinkedHashMap<String,Object> lineAbr = new LinkedHashMap<String,Object>();

            lineAbr.put("id", abroad.getId());
            lineAbr.put("g_sysno", abroad.getgSysno());
            lineAbr.put("kamokucd", abroad.getKamokucd());
            lineAbr.put("purpose_type", abroad.getPurposeType());
            lineAbr.put("nation", abroad.getNation());
            lineAbr.put("date_from", abroad.getDateFrom());
            lineAbr.put("date_to", abroad.getDateTo());
            lineAbr.put("destination", abroad.getDestination());
            lineAbr.put("insert_date", abroad.getInsertDate());
            lineAbr.put("update_date", abroad.getUpdateDate());
            lineAbr.put("userid", abroad.getUserid());

            abroadList.add(lineAbr);
        }
        // 成績情報ならびに学修成果分類等を取得:seiseki_for_evidence
        resEvi = eviDao.getSeisekiForEvidence01(pramDto);
        for(SeisekiForEvidence evidence : resEvi){
            LinkedHashMap<String,Object> lineEvi = new LinkedHashMap<String,Object>();

            lineEvi.put("g_sysno", evidence.getgSysno());
            lineEvi.put("kamokucd", evidence.getKamokucd());
            lineEvi.put("kamokunm", evidence.getKamokunm());
            lineEvi.put("kamokunmeng", evidence.getKamokunmeng());
            lineEvi.put("hyogocd", evidence.getHyogocd());
            lineEvi.put("hyogonm", evidence.getHyogonm());
            lineEvi.put("tanisu", evidence.getTanisu());
            lineEvi.put("nintei_nendo", evidence.getNinteiNendo());
            lineEvi.put("nintei_gakkikbncd", evidence.getNinteiGakkikbncd());
            lineEvi.put("hitsusenkbncd", evidence.getHitsusenkbncd());
            lineEvi.put("jikanwari_nendo", evidence.getJikanwariNendo());
            lineEvi.put("jikanwari_shozokucd", evidence.getJikanwariShozokucd());
            lineEvi.put("jikanwaricd", evidence.getJikanwaricd());
            lineEvi.put("enterd_flg", evidence.getEnterdFlg());
            lineEvi.put("code1", evidence.getCode1());
            lineEvi.put("code2", evidence.getCode2());
            lineEvi.put("code3", evidence.getCode3());
            lineEvi.put("code4", evidence.getCode4());
            lineEvi.put("code5", evidence.getCode5());
            lineEvi.put("code6", evidence.getCode6());
            lineEvi.put("code7", evidence.getCode7());
            lineEvi.put("comment", evidence.getComment());
            lineEvi.put("dir", evidence.getDir());
            lineEvi.put("evidence", evidence.getEvidence());
            lineEvi.put("size", evidence.getSize());
            lineEvi.put("lastupdate", evidence.getLastUpdate());
            lineEvi.put("insert_date", evidence.getInsertDate());
            lineEvi.put("update_date", evidence.getUpdateDate());
            lineEvi.put("userid", evidence.getUserid());

            evidenceList.add(lineEvi);
        }

        ret.put("attachmentsList", attachmentsList);
        ret.put("resultsList", resultsList);
        ret.put("englishList", englishList);
        ret.put("abroadList", abroadList);
        ret.put("evidenceList", evidenceList);

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    /* (非 Javadoc)
     * @see rest.AbstractMapService#deleteJsonData()
     */
    @Override
    protected Boolean deleteJsonData() {
        String method_name = "deleteJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        logger.info(CommonConst.END_LOG + method_name);
        return true;
    }

    /**
     * (rest.)pf/inputResults.json3<br />
     * 画面の情報を保存する
     * @see rest.AbstractMapService#addJsonData()
     * @return : true/false
     */
    @Override
    protected Boolean addJsonData() {
        String method_name = "addJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        logger.info(CommonConst.END_LOG + method_name);
        return true;
    }

    /* (非 Javadoc)
     * @see rest.AbstractMapService#checkParams(java.util.Map)
     */
    @Override
    protected boolean checkParams(Map<String, Object> params) {
        return true;
    }
}
