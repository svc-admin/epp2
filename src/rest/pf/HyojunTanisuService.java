package rest.pf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.HyojunTanisuDao;
import dao.entity.HyojunTanisu;

/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class HyojunTanisuService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public HyojunTanisuService() {
        //権限ID
        rid = 2;
        mode = 0;
    }

    /**
     * (rest.)pf/programs.json?lang="ja"<br />
     * ユーザIDが持つ参照権限のリストを返す。(AuthorityTable()参照)
     * @return : JSON String
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

        ArrayList<Object> aaData = new ArrayList<Object>();
        HyojunTanisuDao hyojunTanisuDao = new HyojunTanisuDao();
        ArrayList<HyojunTanisu> aut = hyojunTanisuDao.getHyojunTanisu();

        for(HyojunTanisu hyojuntanisu : aut){
            ArrayList<Object> o = new ArrayList<Object>();
            o.add(null);
            o.add(hyojuntanisu.getNyugakuNendo());
            if(hyojuntanisu.getgBukyokucd().equals("")){
                o.add("");
            }else{
                o.add(hyojuntanisu.getgBukyokucd());
            }
            o.add(hyojuntanisu.getgBukyokunm());
            o.add(hyojuntanisu.getHitsuyotanisu());
            o.add(hyojuntanisu.getSyugyonengen());
            aaData.add(o);
        }
        ret.put("aaData", aaData);
        ret.put("aoColumns", aoColumns.get(pramDto.getLang()));


        return ret;
    }

    final Map<String, ArrayList<Map<String,String>>> aoColumns = // DBの構造に入っていないのでしかたなく
        new HashMap<String, ArrayList<Map<String,String>>>() {{
            put(CommonConst.JAPANESE, new ArrayList<Map<String,String>>() {{
                add(new HashMap<String,String>() {{
                    put("sTitle", "削除");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "要件年度");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "部局コード");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "部局名");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "必要単位数");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "修業年限");
                }});
            }});
            put(CommonConst.ENGLISH, new ArrayList<Map<String,String>>() {{
                add(new HashMap<String,String>() {{
                    put("sTitle", "delete");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "youkennedo");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "bukyokucd");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "bukyokunm");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "hitsuyoutanisu");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "syugyonengen");
                }});
            }});
        }};
//
    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    @Override
    protected Boolean deleteJsonData() {
       String method_name = "deleteJsonData";
       logger.info(CommonConst.END_LOG + method_name);

       HyojunTanisuDao hyojunTanisuDao = new HyojunTanisuDao();
       Boolean res = hyojunTanisuDao.deleteHyojunTanisu01(pramDto);
       return res;
    }

    @Override
    protected Boolean addJsonData() {
       String method_name = "addJsonData";
       logger.info(CommonConst.END_LOG + method_name);
       HyojunTanisuDao hyojunTanisuDao = new HyojunTanisuDao();
       Boolean res = hyojunTanisuDao.addHyojunTanisu01(pramDto);

       return res;
    }
}
