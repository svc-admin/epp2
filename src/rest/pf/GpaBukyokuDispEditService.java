package rest.pf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.GpaBukyokuDispDao;

public class GpaBukyokuDispEditService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public GpaBukyokuDispEditService() {
        //権限ID
        rid = 2;
        mode = 0;
    }

    /**
     * (rest.)pf/programs.json?lang="ja"<br />
     *
     * @return : JSON String
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    final Map<String, ArrayList<Map<String,String>>> aoColumns = // DBの構造に入っていないのでしかたなく
            new HashMap<String, ArrayList<Map<String,String>>>() {{
            }};

    @Override
    protected Boolean deleteJsonData() {
        return true;
    }

    @Override
    protected Boolean addJsonData() {
        String method_name = "addJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        // post送信されたjsonデータ（文字列）の取得
        String jStr = this.request.getParameterMap().get("json")[0];

        if(jStr.length() == 0) return true; // 更新件数0なら処理終了(0件なら更新ボタン非表示にしてあるが一応)

        // 更新データ数
        int colNum = 1;
        for(int j=0; j<jStr.length(); j++){
            // serializeしたjsonデータの区切り文字である "&" の数を元に更新するデータ数を算出
            if(jStr.substring(j, j+1).equals("&")){
                // checkbox（チェックONの場合だけserializeに含まれる）が検出されたらカウントしない
                if(!jStr.substring(j+1, j+4).equals("flg")){
                    // shozokucd に続く区切り文字の時のみカウントアップ
                    colNum += 1;
                }
            }
        }

        String shozokucdList[] = new String[colNum];  // 所属課コード格納用配列
        Integer dispflgList[] = new Integer[colNum];  // 表示フラグ格納用配列

        int ind = 0;    // 検索開始位置
        int n = 0;      // "&" が発見された位置
        int colInd = 0; // 配列の要素数

        for(int i=0; i>=0; i++){ // "&"が検出されなくなるまでループ

            n = jStr.indexOf("&", ind);

            if(jStr.substring(ind, ind+3).equals("flg")){
                // 表示フラグ（checkbox）の時
                if(colInd > 0)colInd -= 1; // 格納位置を下げる
                dispflgList[colInd] = 1;   // 表示フラグONに更新
            }else{
                // 所属課コードの時
                if(n < 0){
                    // 最終行がOFF（flg=0）の時、n=-1で回ってくるので分岐
                    if(jStr.substring(jStr.length()-7, jStr.length()-6).equals("=")){
                        shozokucdList[colInd] = jStr.substring(jStr.length()-6, jStr.length());
                    }else{
                        shozokucdList[colInd] = jStr.substring(jStr.length()-4, jStr.length());
                    }
                }else{
                    if(jStr.substring(n-7, n-6).equals("=")){
                        shozokucdList[colInd] = jStr.substring(n-6, n);  // 所属専攻コード（"&" の前6文字）格納
                    }else{
                        shozokucdList[colInd] = jStr.substring(n-4, n);  // 所属課コード（"&" の前4文字）格納
                    }
                }
                dispflgList[colInd] = 0;                         // ひとまず表示フラグOFFとしてセット
            }
            ind = n+1;  // 検索開始位置を "&" の次の文字へ更新
            colInd += 1; //格納位置カウントアップ
            if(n < 0){
                // "&" が検出されなかった場合はループ終了（検索結果なしの場合 -1 が返される）
                break;
            }
        }

        GpaBukyokuDispDao gpaBukyokuDispDao = new GpaBukyokuDispDao();
        // 対象所属課数を順次変数にセットしてDaoへ
        for(int key=0; key<colNum; key++){
            gpaBukyokuDispDao.editBukyokuDisp01(pramDto, shozokucdList[key], dispflgList[key]);
        }
        logger.info(CommonConst.END_LOG + method_name);

        return true;
    }

}
