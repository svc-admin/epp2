package rest.pf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import dao.GpaShugakuninteiJogaikamokuMstDao;
import dao.NendobetuShozokuDao;
import dao.entity.GpaShugakuninteiJogaikamokuMst;
import dao.entity.NendobetuShozoku;
/**
 *  REST service using JSONIC, version 2014-02-09 (since 2014-01-30)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class GpaShugakuninteiJogaikamokuMstService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public GpaShugakuninteiJogaikamokuMstService() {
        //権限ID
        rid = 2;
        mode = 0;
    }

    /**
     * (rest.)pf/programs.json?lang="ja"<br />
     * ユーザIDが持つ参照権限のリストを返す。(AuthorityTable()参照)
     * @return : JSON String
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

        ArrayList<Object> aaData = new ArrayList<Object>();
        GpaShugakuninteiJogaikamokuMstDao GpaShugakuninteiJogaikamokuMstDao = new GpaShugakuninteiJogaikamokuMstDao();
        ArrayList<GpaShugakuninteiJogaikamokuMst> aut = GpaShugakuninteiJogaikamokuMstDao.getGpaShugakuninteiJogaikamokuMst();
        NendobetuShozokuDao NendobetuShozokuDao = new NendobetuShozokuDao();

        for(GpaShugakuninteiJogaikamokuMst gpaShugakuninteiJogaikamokuMst : aut){
            ArrayList<NendobetuShozoku> shozoku = NendobetuShozokuDao.getShozokunm01(gpaShugakuninteiJogaikamokuMst.getgBukyokucd());
            ArrayList<Object> o = new ArrayList<Object>();
            o.add(null);
            o.add(gpaShugakuninteiJogaikamokuMst.getYokenNendo());
            o.add(gpaShugakuninteiJogaikamokuMst.getgBukyokucd());
            o.add(shozoku.get(0).getShozokunm());
            o.add(gpaShugakuninteiJogaikamokuMst.getKamokucd());
            o.add(gpaShugakuninteiJogaikamokuMst.getKamokunm());
            aaData.add(o);
        }
        ret.put("aaData", aaData);
        ret.put("aoColumns", aoColumns.get(pramDto.getLang()));


        return ret;
    }

    final Map<String, ArrayList<Map<String,String>>> aoColumns = // DBの構造に入っていないのでしかたなく
        new HashMap<String, ArrayList<Map<String,String>>>() {{
            put(CommonConst.JAPANESE, new ArrayList<Map<String,String>>() {{
                add(new HashMap<String,String>() {{
                    put("sTitle", "削除");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "要件年度");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "部局コード");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "部局名");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "科目コード");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "科目名");
                }});
            }});
            put(CommonConst.ENGLISH, new ArrayList<Map<String,String>>() {{
                add(new HashMap<String,String>() {{
                    put("sTitle", "delete");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "youkennendo");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "bukyokucd");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "bukyokumei");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "kamokucd");
                }});
                add(new HashMap<String,String>() {{
                    put("sTitle", "kamokunm");
                }});
            }});
        }};

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        rtn = true;

        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }


    @Override
    protected Boolean deleteJsonData() {
       String method_name = "deleteJsonData";
       logger.info(CommonConst.END_LOG + method_name);
       System.out.println(pramDto.getGpaShugakuninteiJogaikamokuMst().getgBukyokucd());
       System.out.println(pramDto.getGpaShugakuninteiJogaikamokuMst().getKamokucd());
       System.out.println(pramDto.getGpaShugakuninteiJogaikamokuMst().getYokenNendo());

       GpaShugakuninteiJogaikamokuMstDao gpaShugakuninteiJogaikamokuMstDao = new GpaShugakuninteiJogaikamokuMstDao();
       Boolean res = gpaShugakuninteiJogaikamokuMstDao.deletegGpaShugakuninteiJogaikamokuMstDao01(pramDto);

       return res;
    }

    @Override
    protected Boolean addJsonData() {
       String method_name = "addJsonData";
       logger.info(CommonConst.END_LOG + method_name);

       GpaShugakuninteiJogaikamokuMstDao gpaShugakuninteiJogaikamokuMstDao = new GpaShugakuninteiJogaikamokuMstDao();
       Boolean res = gpaShugakuninteiJogaikamokuMstDao.addGpaShugakuninteiJogaikamokuMstDao01(pramDto);

       return res;
    }
}
