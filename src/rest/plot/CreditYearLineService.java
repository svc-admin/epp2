package rest.plot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.PropertiesUtil;
import util.Tools;
import constants.CommonConst;
import constants.ParamsConst;
import dao.GakusekiDao;
import dao.GpaAvrDao;
import dao.GpaDao;
import dao.entity.Gakuseki;
import dao.entity.Gpa;
import dao.entity.GpaAvr;
//+ for using variable request / request変数を利用するため
import dto.SysPropertiesDto;

/**
 * 学生IDを指定し、年度毎の累積取得単位数及びその学科平均を返す。(Chart.jsの2番目のグラフ) REST service using JSONIC,
 * version 2017-01-22 sys.properties の openSems 変数に従って、公開範囲を設定
 * version 2014-02-02 (since 2014-02-02) Copyright 2014 Hiroshi Nakano
 * nakano@cc.kumamoto-u.ac.jp
 */
public class CreditYearLineService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();
    private SysPropertiesDto sysPropDto = null;
    private Calendar[] openSemesters;

    private static final int COMMIT_SIZE = 1000;

    public CreditYearLineService() {
        // 権限ID
        rid = 0;
        mode = 0;

        try {
            logger = Tools.getCallerLogger();
            // logger = Logger.getLogger(getClass());

            // SYS情報の取得
            PropertiesUtil propUtil = new PropertiesUtil();
            sysPropDto = propUtil.getSysProperties();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * <li>(rest.)plot/creditBar.json?uid=XXshozokucd="0513"<br />
     * 学生IDを指定し、年度毎の累積取得単位数及びその学科平均を返す。(Chart.jsの2番目のグラフ)<br />
     * {"label" : ["2007S","F","2008S","F","2009S","F","2010S","F"], "credit" :
     * [32,52,71,96,102,112,124,132], "avr" : [32,52,71,96,102,112,124,132],
     * "memberMax" : 45}</li>
     *
     * @return : Object (hash array here) / Object (この場合は連想配列)
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        setOpenSems();
        Calendar today = Calendar.getInstance();

        LinkedHashMap<String, Object> ret = new LinkedHashMap<String, Object>();

        GpaDao gpaDao = new GpaDao();
        ArrayList<Gpa> res1 = gpaDao.getGpaList02(pramDto);

        GpaAvrDao gpaAvrDao = new GpaAvrDao();
        ArrayList<GpaAvr> res2 = gpaAvrDao.getGpaAvrList02(pramDto);

        GakusekiDao gakusekiDao = new GakusekiDao();
        ArrayList<Gakuseki> gradYMD = gakusekiDao.getGakusekiList01(pramDto);

        int yStart = 0, yEnd = 0;
        if (res2.size() > 0) {
            int yMin = 100000, yMax = 0;

            for (GpaAvr entity : res2) {
                int y = entity.getNinteiNendo().intValue();
                if (y > yMax)
                    yMax = y;
                if (y < yMin)
                    yMin = y;
            }
            yStart = yMin;
            yEnd = yMax;
        }
        if (gradYMD.size() > 1) {
            logger.error(getUidLog("Duplicated SOTSUGYOYMD"));
        }
        if (gradYMD.size() > 0) {

            Date date = gradYMD.get(0).getSotsugyoymd();

            if (date != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int y = calendar.get(Calendar.YEAR);
                int m = calendar.get(Calendar.MONTH);
                if (y > 0) {
                    yEnd = y;
                    if (m < 3)
                        yEnd--; // 4月 = 3 (年度計算)
                }
            }
        }
        // logger.error("yStart="+yStart+", yEnd="+yEnd);
        if (yStart > 0 && yEnd >= yStart) {
            if (res2.size() > 0) {
                int aSize = 2 * (yEnd - yStart + 1);
                if (today.before(openSemesters[0]) && yEnd == today.get(Calendar.YEAR))
                    aSize -= 2; // 前期成績公開前
                if (today.before(openSemesters[1]) && yEnd == (today.get(Calendar.YEAR) - 1))
                    aSize -= 1; // 後期成績公開前
                String[] label = new String[aSize];
                double[] credit = new double[aSize];
                double[] avr = new double[aSize];
                for (int i = 0; i < credit.length; i++) {
                    credit[i] = 0.0;
                    avr[i] = 0.0;
                }
                int memberMax = 0;
                for (int y = yStart; y <= yEnd; y++) {
                    int as = 2 * (y - yStart);
                    if(as < label.length) label[as] = y + "S";
                    if(as+1 < label.length) label[as + 1] = "F";
                }
                for (Gpa entity : res1) {
                    int y = entity.getNinteiNendo().intValue();
                    int s = entity.getSemester().intValue();
                    if (s > 0 && y >= yStart && y <= yEnd) {
                        int as = 2 * (y - yStart) + s - 1;
                        if(as < credit.length) credit[as] = entity.getCredit().doubleValue();
                    }
                }
                for (GpaAvr entity : res2) {
                    int y = entity.getNinteiNendo().intValue();
                    int s = entity.getSemester().intValue();
                    if (s > 0 && y >= yStart && y <= yEnd) {
                        int as = 2 * (y - yStart) + s - 1;
                        if(as < avr.length) avr[as] = entity.getCredit().doubleValue();
                        int mem = entity.getMember().intValue();
                        if (mem > memberMax)
                            memberMax = mem;
                    }
                }
                for (int i = 1; i < label.length; i++) {
                    credit[i] += credit[i - 1];
                    avr[i] += avr[i - 1];
                }

                ret.put("label", label);
                ret.put("credit", credit);
                ret.put("avr", avr);
                ret.put("memberMax", memberMax);
                logger.info(getUidLog("creditBar"));

            }
        }
        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        // 学生番号、所属コードが設定されていない場合は処理しない。
        if (params.containsKey(ParamsConst.UID) && (params.containsKey(ParamsConst.SHOZOKUCD))) {
            rtn = true;
        }
        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

    // 2014.11.21 SVC 追加、削除処理追加に伴うもの >>
    @Override
    protected Boolean deleteJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }

    @Override
    protected Boolean addJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }
    // 2014.11.21 SVC 追加、削除処理追加に伴うもの <<


// 2017-01-22 nakano: 今日の年から、前期成績公開日及び後期成績公開日を設定 >>
    private void setOpenSems() {
        // 権限ID
        rid = 0;
        mode = 0;

        try {
            Calendar today = Calendar.getInstance();
            String[] openSems = sysPropDto.getOpenSems();
            openSemesters = new Calendar[openSems.length];
            for (int i = 0; i < openSems.length; i++) {
                // logger.info("==0== openSems["+i+"] = " + openSems[i]);
                Matcher m = Pattern.compile("(\\d+)-(\\d+)").matcher(openSems[i]);
                // logger.info("==1== m.find() = " + m.find());
                if (m.find()) {
                    // logger.info("==2== m.group(1) = " + m.group(1) + ",
                    // m.group(2) = " + m.group(2));
                    openSemesters[i] = Calendar.getInstance();
                    openSemesters[i].set(today.get(Calendar.YEAR), Integer.parseInt(m.group(1)),
                            Integer.parseInt(m.group(2)));
                    // logger.info("==3== openSemesters["+i+"].getTime() =
                    // "+openSemesters[i].getTime());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // 2017-01-22 nakano: 今日の年から、前期成績公開日及び後期成績公開日を設定 <<

}
