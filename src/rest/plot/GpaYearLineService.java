package rest.plot;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.PropertiesUtil;
import util.Tools;
import constants.CommonConst;
import constants.ParamsConst;
import dao.GakusekiDao;
import dao.GpaAvrDao;
import dao.GpaDao;
import dao.entity.Gakuseki;
import dao.entity.Gpa;
import dao.entity.GpaAvr;
//+ for using variable request / request変数を利用するため
import dto.SysPropertiesDto;

/**
 *  学生IDを指定し、年度毎のGPA及びその学科平均を返す。 REST service using JSONIC, version 2014-02-08 (since 2014-02-08)
 * version 2017-01-22 sys.properties の openSems 変数に従って、公開範囲を設定
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class GpaYearLineService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();
    private SysPropertiesDto sysPropDto = null;
    private Calendar[] openSemesters;

    public GpaYearLineService() {
        //権限ID
        rid = 0;
        mode = 0;

        try {
            logger = Tools.getCallerLogger();
            // logger = Logger.getLogger(getClass());

            // SYS情報の取得
            PropertiesUtil propUtil = new PropertiesUtil();
            sysPropDto = propUtil.getSysProperties();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
<pre class="brush: html; gutter: false; highlight: 1; toolbar: false;">
(rest.)plot/gpaYearLine.json?uid=XXshozokucd="0513"
</pre>
                  学生ID及び所属コード範囲を指定し、年度毎のGPA及びその学科平均を返す。(※)
<pre class="brush: js; toolbar: false;">
{"label" : ["2007S","F","2008S","F","2009S","F","2010S","F"],
 "gpa" : [2.45,2.12,2.35,2.53,2.01,2.65,3.12,2.85],
 "avr" : [2.40,2.04,2.15,2.50,2.25,2.51,3.05,2.90], "memberMax" : 45}
 </pre>
     * @return : Object (hash array here) / Object (この場合は連想配列)
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        setOpenSems();
         Calendar today = Calendar.getInstance();

        LinkedHashMap<String,Object> ret = new LinkedHashMap<String,Object>();


        GpaDao gpaDao = new GpaDao();
        ArrayList<Gpa> res1 = gpaDao.getGpaList05(pramDto);

        GpaAvrDao gpaAvrDao = new GpaAvrDao();
        ArrayList<GpaAvr> res2 = gpaAvrDao.getGpaAvrList04(pramDto);

        GakusekiDao gakusekiDao = new GakusekiDao();
        ArrayList<Gakuseki> gradYMD = gakusekiDao.getGakusekiList03(pramDto);

        int yStart = 0, yEnd = 0;
        if(res2.size() > 0) {
            int yMin = 100000, yMax=0;
            for(GpaAvr gpaAvrEntity :res2) {
                int y = gpaAvrEntity.getNinteiNendo().intValue();
                if(y > yMax) yMax = y;
                if(y < yMin) yMin = y;
            }
            yStart = yMin;
            yEnd = yMax;
        }
        if(gradYMD.size() > 1) {
            logger.error("Duplicated SOTSUGYOYMD at gakuseki, uid='"+pramDto.getUid()+"'&shozokucd='"+pramDto.getShozokucd()+"', ruser="+pramDto.getRuser());
        }
        if(gradYMD.size() > 0) {

            Date date = gradYMD.get(0).getSotsugyoymd();

            if(date != null){
                Calendar c = Calendar.getInstance();
                c.setTime(date);
                int y = c.get(Calendar.YEAR);
                int m = c.get(Calendar.MONTH);
                if(y > 0) {
                    yEnd = y;
                    if(m < 3) yEnd--; // 4月 = 3 (年度計算)
                }
            }
        }
//      logger.error("yStart="+yStart+", yEnd="+yEnd);
        if(yStart > 0 && yEnd >= yStart) {
            if(res2.size() > 0) {
                int aSize = 2 * (yEnd - yStart + 1);
                if (today.before(openSemesters[0]) && yEnd == today.get(Calendar.YEAR))
                    aSize -= 2; // 前期成績公開前
                if (today.before(openSemesters[1]) && yEnd == (today.get(Calendar.YEAR) - 1))
                    aSize -= 1; // 後期成績公開前
                String[] label = new String[aSize];
                double[] gpa = new double[aSize];
                double[] avr = new double[aSize];
              for(int i = 0; i < gpa.length; i++) {
                    gpa[i] = 0.0;
                    avr[i] = 0.0;
                }
                int memberMax = 0;
                for(int y = yStart; y <= yEnd; y++) {
                    int as = 2 * (y - yStart);
                    if(as < label.length) label[as] = y + "S";
                    if(as+1 < label.length) label[as + 1] = "F";
                }
                for(Gpa gpaEntity :res1) {
                    int y = gpaEntity.getNinteiNendo().intValue();
                    int s = gpaEntity.getSemester().intValue();
                    if (s > 0 && y >= yStart && y <= yEnd) {
                        int as = 2 * (y - yStart) + s - 1;
                        if(as < gpa.length) gpa[as] = gpaEntity.getGpa().doubleValue();
                    }
                }
                for(GpaAvr gpaAvrEntity :res2) {
                    int y = gpaAvrEntity.getNinteiNendo().intValue();
                    int s = gpaAvrEntity.getSemester().intValue();
                    if(s > 0 && y >= yStart && y <= yEnd) {
                        int as = 2 * (y - yStart) + s - 1;
                        if(as < avr.length) avr[as] = gpaAvrEntity.getGpa().doubleValue();
                     int mem = gpaAvrEntity.getMember().intValue();
                     if(mem > memberMax) memberMax = mem;
                    }
                }
                ret.put("label", label);
                ret.put("gpa", gpa);
                ret.put("avr", avr);
                ret.put("memberMax", memberMax);
                logger.info("gpaBar at uid='"+pramDto.getUid()+"'&shozokucd='"+pramDto.getShozokucd()+"', ruser="+pramDto.getRuser());
            }
        }
        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        //学生番号、所属コードが設定されていない場合は処理しない。
        if(params.containsKey(ParamsConst.UID) && (params.containsKey(ParamsConst.SHOZOKUCD))) {
            rtn = true;
        }
        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

//2014.11.21 SVC 追加、削除処理追加に伴うもの >>
    @Override
    protected Boolean deleteJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }

    @Override
    protected Boolean addJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }
//2014.11.21 SVC 追加、削除処理追加に伴うもの <<

 // 2017-01-22 nakano: 今日の年から、前期成績公開日及び後期成績公開日を設定 >>
     private void setOpenSems() {
         // 権限ID
         rid = 0;
         mode = 0;

         try {
             Calendar today = Calendar.getInstance();
             String[] openSems = sysPropDto.getOpenSems();
             openSemesters = new Calendar[openSems.length];
             for (int i = 0; i < openSems.length; i++) {
                 // logger.info("==0== openSems["+i+"] = " + openSems[i]);
                 Matcher m = Pattern.compile("(\\d+)-(\\d+)").matcher(openSems[i]);
                 // logger.info("==1== m.find() = " + m.find());
                 if (m.find()) {
                     // logger.info("==2== m.group(1) = " + m.group(1) + ",
                     // m.group(2) = " + m.group(2));
                     openSemesters[i] = Calendar.getInstance();
                     openSemesters[i].set(today.get(Calendar.YEAR), Integer.parseInt(m.group(1)),
                             Integer.parseInt(m.group(2)));
                     // logger.info("==3== openSemesters["+i+"].getTime() =
                     // "+openSemesters[i].getTime());
                 }
             }
         } catch (Exception e) {
             e.printStackTrace();
         }
     }
     // 2017-01-22 nakano: 今日の年から、前期成績公開日及び後期成績公開日を設定 <<

}
