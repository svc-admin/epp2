package rest.plot;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import constants.ParamsConst;
import dao.GoalsMstDao;
import dao.GpaAvrDao;
import dao.GpaDao;
import dao.entity.GoalsMst;
import dao.entity.Gpa;
import dao.entity.GpaAvr;
//+ for using variable request / request変数を利用するため

/**
 *  学生リスト取得 (dataTables.js用) REST service using JSONIC, version 2014-02-02 (since 2014-02-02)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 *  2015-12-20 gpa,avr配列が溢れるのを抑止
 */
public class GpaGoalRadarService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public GpaGoalRadarService() {
        //権限ID
        rid = 0;
        mode = 0;
    }

    /**
              (rest.)plot/gpaRadar.json?uid=XX&shozokucd="0513"<br />
                                学生ID及び所属コード範囲を指定し、学習成果に対するGPA及びその学科平均を返す<br />
                  {"gpa":[3.1,2.3,3.5,3.7,2.5,3.1,2.5],"avr":[2.1,2.4,3,4,2,3,1.5],
                    "other":2.4, "other_avr":2.1, "memberMax":45}
     * @return : Object (hash array here) / Object (この場合は連想配列)
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        LinkedHashMap<String,Object> ret = new LinkedHashMap<String,Object>();

        GpaDao gpaDao = new GpaDao();
        ArrayList<Gpa> res1 = gpaDao.getGpaList04(pramDto);

        GpaAvrDao gpaAvrDao = new GpaAvrDao();
        ArrayList<GpaAvr> res2 = gpaAvrDao.getGpaAvrList03(pramDto);

        // 2015.02.06 SVC 追加、大学院区分を考慮する >>
        // TODO ここに大学院区分も加える！！
        GoalsMstDao goalMstDao = new GoalsMstDao();
        ArrayList<GoalsMst> res3 = goalMstDao.getGoalsMstList02(pramDto);

        //double[] gpa = {0,0,0,0,0,0,0};
        //double[] avr = {0,0,0,0,0,0,0};
        double[] gpa = new double[res3.size()];
        double[] avr = new double[res3.size()];
        // 2015.02.06 SVC 追加、大学院区分を考慮する <<

        double other = 0, other_avr = 0;
        int memberMax = 0;
        for(Gpa gpaEntity:res1) {
            int goal = gpaEntity.getGoal().intValue();
            if(goal == 0) {
                other = gpaEntity.getGpa().doubleValue();
            } else if(goal <= gpa.length) { // by nakano 2015-12-20
                gpa[goal-1] = gpaEntity.getGpa().doubleValue();
            } else {
                    logger.info(CommonConst.END_LOG + method_name + " Goal array for gpa size exceeds default size (may be 7 or 4).");
                }
        }

        for(GpaAvr gpaAvrEntity:res2) {
            int goal = gpaAvrEntity.getGoal().intValue();
            if(goal == 0) {
                other_avr = gpaAvrEntity.getGpa().doubleValue();
            } else if(goal <= gpa.length) { // by nakano 2015-12-20
              avr[goal-1] = gpaAvrEntity.getGpa().doubleValue();
            } else {
                      logger.info(CommonConst.END_LOG + method_name + " Goal array for gpa avr size exceeds default size (may be 7 or 4).");
                }
            int mem = gpaAvrEntity.getMember().intValue();
            if(mem > memberMax) memberMax = mem;
        }

        ret.put("gpa", gpa);
        ret.put("avr", avr);
        ret.put("other", other);
        ret.put("other_avr", other_avr);
        ret.put("memberMax", memberMax);
        logger.info("gpaRadar at uid='"+pramDto.getUid()+"'&shozokucd='"+pramDto.getShozokucd()+"', ruser="+pramDto.getRuser());

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        //学生番号、所属コードが設定されていない場合は処理しない。
        if(params.containsKey(ParamsConst.UID) && (params.containsKey(ParamsConst.SHOZOKUCD))) {
            rtn = true;
        }
        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

//2014.11.21 SVC 追加、削除処理追加に伴うもの >>
    @Override
    protected Boolean deleteJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }

    @Override
    protected Boolean addJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }
//2014.11.21 SVC 追加、削除処理追加に伴うもの <<

}
