package rest.plot;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import constants.ParamsConst;
import dao.ToeicAvrDao;
import dao.ToeicDao;
//+ for using variable request / request変数を利用するため
import dao.entity.Toeic;
import dao.entity.ToeicAvr;

/**
 *  学生IDを指定し、年度毎のTOEIC及びその学科平均を返す。 REST service using JSONIC, version 2014-02-08 (since 2014-02-08)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 */
public class ToeicYearLineService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public ToeicYearLineService() {
        //権限ID
        rid = 0;
        mode = 0;
    }

    /**
<pre class="brush: html; gutter: false; highlight: 1; toolbar: false;">
(rest.)plot/toeicYearLine.json?uid=XXshozokucd="0513"
</pre>
                  学生ID及び所属コード範囲を指定し、年度毎のTOEIC及びその学科平均を返す。(※)
<pre class="brush: js; toolbar: false;">
{"label" : ["2007S","F","2008S","F","2009S","F","2010S","F"],
 "toeic" : [600,600,600,600,600,600,600,600],
 "avr" : [600,600,600,600,600,600,600,600], "memberMax" : 45}
 </pre>
     * @return : Object (hash array here) / Object (この場合は連想配列)
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);
        LinkedHashMap<String,Object> ret = new LinkedHashMap<String,Object>();

        //ユーザ情報の取得
        getUserInfo();

        ToeicDao toeicDao = new ToeicDao();
        ArrayList<Toeic> res1 = toeicDao.getToeicList02(pramDto);

        ToeicAvrDao toeicAvrDao = new ToeicAvrDao();
        ArrayList<ToeicAvr> res2 = toeicAvrDao.getToeicAvrList02(pramDto);
        Map<Integer,ToeicAvr> avrMap = new HashMap<Integer,ToeicAvr>();
        for(ToeicAvr toeicAvr :res2){
            avrMap.put(toeicAvr.getJyukenNendo(), toeicAvr);
        }

        if(res1.size() == 0){
            return ret;
        }

        String[] label = new String[res1.size()];
        int[] toeic = new int[res1.size()];
        int[] avr = new int[res1.size()];
        int[] member = new int[res1.size()];

        int i = 0;
        for(Toeic entity :res1){

            label[i] = entity.getYear().toString();
            toeic[i] = entity.getScore().intValue();
            if(avrMap.containsKey(entity.getYear())){
                avr[i] = avrMap.get(entity.getYear()).getScore().intValue();
                member[i] = avrMap.get(entity.getYear()).getMember().intValue();
            }
            i++;
        }

        int memberMax = 0;
        if(member.length > 0){
            Arrays.sort(member);
            memberMax = member[member.length-1];
        }
        ret.put("label", label);
        ret.put("toeic", toeic);
        ret.put("avr", avr);
        ret.put("memberMax", memberMax);
        logger.info("toeic at uid='"+pramDto.getUid()+"'&shozokucd='"+pramDto.getShozokucd()+"', ruser="+pramDto.getRuser());

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);

        boolean rtn = false;

        //学生番号、所属コードが設定されていない場合は処理しない。
        if(params.containsKey(ParamsConst.UID) && (params.containsKey(ParamsConst.SHOZOKUCD))) {
            rtn = true;
        }
        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

//2014.11.21 SVC 追加、削除処理追加に伴うもの >>
    @Override
    protected Boolean deleteJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }

    @Override
    protected Boolean addJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }
//2014.11.21 SVC 追加、削除処理追加に伴うもの <<

}
