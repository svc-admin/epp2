package rest.plot;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rest.AbstractMapService;
import util.Tools;
import constants.CommonConst;
import constants.ParamsConst;
import dao.GoalsMstDao;
import dao.GpaAvrDao;
import dao.GpaDao;
import dao.entity.GoalsMst;
import dao.entity.Gpa;
import dao.entity.GpaAvr;
//+ for using variable request / request変数を利用するため

/**
 *  学習成果に対する取得単位数及びその学科平均 REST service using JSONIC, version 2014-02-04 (since 2014-02-04)
 *  Copyright 2014 Hiroshi Nakano nakano@cc.kumamoto-u.ac.jp
 *  2015-12-20 credit,avr配列が溢れるのを抑止
 */
public class CreditGoalRadarService extends AbstractMapService {
    private static Logger logger = Tools.getCallerLogger();

    public CreditGoalRadarService() {
        //権限ID
        rid = 0;
        mode = 0;
    }

    /**
              <li>(rest.)plot/gpaGoalCredit.json?uid=XX&shozokucd="0513"<br />
                  学生ID及び所属コード範囲を指定し、学習成果に対する取得単位数及びその学科平均を返す。(Chart.jsの2番めのグラフ)(※)<br />
                  {"gpa":[12,23.5,11,20,10,29,39],"avr":[11.1,25.4,11.3,20.1,12.4,31.5,40.1],
                    "other":31, "other_avr":33.1, "memberMax":45}</li>
     * @return : Object (hash array here) / Object (この場合は連想配列)
     */
    @Override
    protected LinkedHashMap<String, Object> getJsonData() {
        String method_name = "getJsonData";
        logger.info(CommonConst.START_LOG + method_name);

        LinkedHashMap<String,Object> ret = new LinkedHashMap<String,Object>();

        GpaDao gpaDao = new GpaDao();
        ArrayList<Gpa> res1 = gpaDao.getGpaList01(pramDto);

        GpaAvrDao gpaAvrDao = new GpaAvrDao();
        ArrayList<GpaAvr> res2 = gpaAvrDao.getGpaAvrList01(pramDto);

        // 2015.02.06 SVC 追加、大学院区分を考慮する >>
        // TODO ここに大学院区分も加える！！
        GoalsMstDao goalMstDao = new GoalsMstDao();
        ArrayList<GoalsMst> res3 = goalMstDao.getGoalsMstList02(pramDto);

        //double[] credit = {0,0,0,0,0,0,0};
        //double[] avr = {0,0,0,0,0,0,0};
        double[] credit = new double[res3.size()];
        double[] avr = new double[res3.size()];
        // 2015.02.06 SVC 追加、大学院区分を考慮する <<

        double other = 0, other_avr = 0;
        int memberMax = 0;
        for(Gpa entity :res1) {
            if(entity.getGoal() == 0) {
                other = entity.getCredit().doubleValue();
            } else if(entity.getGoal() <= credit.length) { // by nakano 2015-12-20
              credit[entity.getGoal()-1] = entity.getCredit().doubleValue();
            } else {
                      logger.info(CommonConst.END_LOG + method_name + " Goal array size for credits exceeds default size (may be 7 or 4).");
                }
        }

        for(GpaAvr entity :res2) {
            if(entity.getGoal() == 0) {
                other_avr = entity.getCredit().doubleValue();
            } else if(entity.getGoal() <= credit.length) { // by nakano 2015-12-20
              avr[entity.getGoal()-1] = entity.getCredit().doubleValue();
            } else {
                      logger.info(CommonConst.END_LOG + method_name + " Goal array size for credit avr exceeds default size (may be 7 or 4).");
                }
            int mem = entity.getMember().intValue();
            if(mem > memberMax) memberMax = mem;
        }

        ret.put("credit", credit);
        ret.put("avr", avr);
        ret.put("other", other);
        ret.put("other_avr", other_avr);
        ret.put("memberMax", memberMax);

        logger.info(getUidLog("gpaRadar"));
        logger.info(CommonConst.END_LOG + method_name);

        return ret;

    }

    @Override
    protected boolean checkParams(Map<String, Object> params) {
        String method_name = "checkParams";
        logger.info(CommonConst.START_LOG + method_name);
        boolean rtn = false;

        //学生番号、所属コードが設定されていない場合は処理しない。
        if(params.containsKey(ParamsConst.UID) && (params.containsKey(ParamsConst.SHOZOKUCD))) {
            rtn = true;
        }
        logger.info(CommonConst.END_LOG + method_name);
        return rtn;
    }

//2014.11.21 SVC 追加、削除処理追加に伴うもの >>
    @Override
    protected Boolean deleteJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }

    @Override
    protected Boolean addJsonData() {
        // TODO 自動生成されたメソッド・スタブ
        return null;
    }
//2014.11.21 SVC 追加、削除処理追加に伴うもの <<
}
