package rest;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import util.Tools;
import constants.CommonConst;
//+ for using variable request / request変数を利用するため

/**
 *
 */
public abstract class AbstractMapService extends AbstractService{
    private static Logger logger = Tools.getCallerLogger();

    /**
     *
     * @param params : 連想配列(項目名と値)
     * @return : Object (hash array here) / Object (この場合は連想配列)
     */
    public LinkedHashMap<String,Object> find(Map<String,Object> params) {
        String method_name = "find";
        logger.info(CommonConst.START_LOG + method_name);

        LinkedHashMap<String,Object> ret;

        //パラメータの取得
        if(!setParams(params)){
            logger.info(CommonConst.END_LOG + method_name);
            return new LinkedHashMap<String,Object>();
        };

        //アクセス権限チェック
        if(!checkRestriction()) { // rid = 0, read only
            logger.info(CommonConst.END_LOG + method_name);
            return new LinkedHashMap<String,Object>();
        }

        //データ取得
        ret = getJsonData();

        logger.info(CommonConst.END_LOG + method_name);
        return ret;
    }

// 2014.11.21 SVC 追加、削除処理を追加 >>
    /**
     *
     * @param params : 配列
     * @return : Boolean(True,False)
     */
   public Boolean delete(Map<String,Object> params) {

       String method_name = "delete";
       logger.info(CommonConst.START_LOG + method_name);

       Boolean ret;

       //パラメータの取得
       if(!setParams(params)){
           logger.info(CommonConst.END_LOG + method_name);
           return false;
       };

       //データ削除
       ret = deleteJsonData();

       logger.info(CommonConst.END_LOG + method_name);
       return ret;
    }

   /**
     *
     * @param params : 配列
     * @return : Boolean(True,False)
     */
  public LinkedHashMap<String,Object> create(Map<String,Object> params) {

      String method_name = "create";
      logger.info(CommonConst.START_LOG + method_name);

      LinkedHashMap<String,Object> retMap = new LinkedHashMap<String, Object>();

      //パラメータの取得
      if(!setParams(params)){
          logger.info(CommonConst.END_LOG + method_name);
          retMap.put("status", "error");
          return retMap;
      };

      //データ削除
      Boolean ret = addJsonData();

      // JSON返却値作成
      retMap = makeRetJson(ret);

      logger.info(CommonConst.END_LOG + method_name);
      return retMap;
    }
// 2014.11.21 SVC 追加、削除処理を追加 <<

    protected abstract LinkedHashMap<String, Object> getJsonData();
// 2014.11.21 SVC 追加、削除処理を追加 >>
    protected abstract Boolean deleteJsonData();
    protected abstract Boolean addJsonData();
 // 2014.11.21 SVC 追加、削除処理を追加 <<

// 2019.12.03 SVC ADD START >> JSONの返却値を継承先で操作できるように関数追加
    protected LinkedHashMap<String, Object> makeRetJson(Boolean ret){
        return new LinkedHashMap<String, Object>();
    }
// 2019.12.03 SVC ADD END   <<
    protected abstract boolean checkParams(Map<String, Object> params);

}
